<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Region;
use aimgroup\DashboardBundle\Form\RegionType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Region controller.
 *
 * @Route("admin/region")
 */
class RegionController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all Region entities.
     *
     * @Route("/", name="admin/region")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Region')->findAll();

        return array(
            'title' => "View Regions",
            'title_descr' => "View Regions",
            'entities' => $entities,
        );
    }

    /**
     * Creates a new Region entity.
     *
     * @Route("/region_create", name="region_create")
     * @Method("POST")
     * @Template("DashboardBundle:Region:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new Region();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('region_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * get a list of all the Regions
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list_regions",name="list_regions")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function listRegions(Request $request) {
        $resp = new JTableResponse();
        try {
            $query = $this->getDoctrine()->getEntityManager()
                    ->createQuery(
                    'SELECT r.id as Value,r.name as DisplayText, r.code as CodeName
                     FROM DashboardBundle:Region r'
            );
            $regions = $query->getResult();
            $resp->setOptions($regions);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * get a list of all the territoires
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list_territories",name="list_territories")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function listTerritories(Request $request) {
        $resp = new JTableResponse();
        try {
            $regions = array();
            $attribs = $request->query->all();
            $regionId = isset($attribs['regionId']) ? $attribs['regionId'] : 0;

            if ($regionId > 0) {
                $query = $this->getDoctrine()->getEntityManager()
                                ->createQuery(
                                        'SELECT t.id as Value,t.name as DisplayText
                     FROM DashboardBundle:Territory t where t.region =:region'
                                )->setParameter('region', $attribs['regionId']);
            } else {
                $query = $this->getDoctrine()->getEntityManager()
                        ->createQuery('SELECT t.id as Value,t.name as DisplayText FROM DashboardBundle:Territory t');
            }

            $regions = $query->getResult();
            $resp->setOptions($regions);

            $resp->setOptions($regions);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Creates a form to create a Region entity.
     *
     * @param Region $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Region $entity) {
        $form = $this->createForm(new RegionType(), $entity, array(
            'action' => $this->generateUrl('region_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Region entity.
     *
     * @Route("/new", name="region_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new Region();
        $form = $this->createCreateForm($entity);

        return array(
            'title' => "Create New Regions",
            'title_descr' => "Create New Regions",
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a Region entity.
     *
     * @Route("/region_show/{id}", name="region_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Region entity.
     *
     * @Route("/{id}/edit", name="region_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a Region entity.
     *
     * @param Region $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(Region $entity) {
        $form = $this->createForm(new RegionType(), $entity, array(
            'action' => $this->generateUrl('region_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing Region entity.
     *
     * @Route("/{id}", name="region_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Region:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Region')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Region entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('region_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Region entity.
     *
     * @Route("/{id}", name="region_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:Region')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Region entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('region'));
    }

    /**
     * Creates a form to delete a Region entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('region_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm();
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/display_map", name="display_map")
     * @Method("GET")
     * @Template("DashboardBundle:Region:map.html.twig")
     */
    public function displaRegionAction(Request $request) {

        if (!in_array(5, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery("SELECT t from DashboardBundle:Territory t ")->getArrayResult(); //getSingleResult();

        foreach ($result as $key => $value) {
            $regionsName[$value['id']] = $value['name'];
        }



       // echo "|".$em->getRepository("DashboardBundle:Territory")->getTerritoryCodeFromName("Geita"); exit;


        $regionskey = array(
            "mwanza" => "MW",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR"
        );

        $where = " WHERE 1=1 AND p.territory != '' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where.= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.territory "
                        . " from RestApiBundle:Registration p " . $where
                        . " GROUP BY p.territory ")
                ->getArrayResult(); //getSingleResult(); //getSql

        foreach ($regionskey as $kkkey => $vvval) {
            $graph_data[strtolower($vvval)] = 0;
        }
        foreach ($result as $kkey => $vval) {
            $graph_data[strtolower(@$regionskey[@$regionsName[$vval['territory']]])] = $vval['ccount'];
        }

//        echo "<pre>";
//        print_r($graph_data);
//        echo "</pre>";
//        echo date('Y-m-d') . " | " . date_format(date_sub(date_create(date('Y-m-d')),date_interval_create_from_date_string('10 days')),'Y-m-d'); exit;

        $agent = array();
        $registrations = array();
        $gender = array();
        $data = array(
            'title' => "Location Data",
            'title_descr' => "List, create, delete, activate System Admins",
            'agent' => $agent,
            'graph_data' => $graph_data,
            'end_date' => date('Y-m-d'),
            'start_date' => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('533 days')), 'Y-m-d'),
            'registations' => $registrations,
            'gender' => $gender
        );

        return $this->prepareResponse($data);
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/display_map_agents", name="display_map_agents")
     * @Method("GET")
     * @Template("DashboardBundle:Region:map_agents.html.twig")
     */
    public function displaRegionAgentsAction(Request $request) {
        $agent = array();
        $registrations = array();
        $gender = array();
        $data = array(
            'title' => "Location Data",
            'title_descr' => "List, create, delete, activate System Admins",
            'agent' => $agent,
            'end_date' => date('Y-m-d'),
            'start_date' => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('533 days')), 'Y-m-d'),
            'registations' => $registrations,
            'gender' => $gender
        );

        return $this->prepareResponse($data);
    }

}
