<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Dao\EkycStatus;
use aimgroup\RestApiBundle\Dao\ImagesOperatorStatus;
use aimgroup\RestApiBundle\Dao\PlainTextOperatorStatus;
use aimgroup\RestApiBundle\Entity\Role;
use aimgroup\RestApiBundle\Services\AbstractFilter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Admin;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Entity\ReportsRequests;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStates;
use aimgroup\DashboardBundle\Form\UserType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use aimgroup\DashboardBundle\Entity\Faq;
use aimgroup\DashboardBundle\Form\FaqType;
use aimgroup\RestApiBundle\Form\RoleType;
use aimgroup\DashboardBundle\DQL\DateFormatFunction;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Session\Session;
use aimgroup\DashboardBundle\Dao\SearchDao;
use aimgroup\DashboardBundle\Entity\SearchResult;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use aimgroup\RestApiBundle\Event\PlainTextProcessEvents;
use aimgroup\DashboardBundle\Entity\UserLog;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use aimgroup\DashboardBundle\Util\UsersHelper;

//use DashboardBundle\Form\FaqType;

/**
 * AdminController controller.
 *
 * @Route("/admin")
 */
class AdminController extends AbstractController
{

    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all Faq entities.
     *
     * @Route("/", name="admin")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction(), true));
        $em = $this->getDoctrine()->getManager();

        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_VERIFYER", $roles)) {
            return $this->redirect($this->generateUrl('indexverify'));
        }
        if (in_array("ROLE_VIEWREGISTRATIONS", $roles)) {
            return $this->redirect($this->generateUrl('admin/registration'));
        }
        if (in_array("ROLE_VERIFYER_SUPERVISOR", $roles)) {
            return $this->redirect($this->generateUrl('verifyer'));
        }
        if (in_array("ROLE_EDITOR_VERIFYER", $roles)) {
            return $this->redirect($this->generateUrl('editnewverification'));
        }


        $data = array(
            'title' => "Index",
            'title_descr' => "View Frequently Asked Questions",
            'ddate' => date('Y-m-d'),
            'akuku' => json_encode($user->getRoles()) . " | " . $this->session->get('user_role_perms'),
            'date_today' => date('Y-m-d'),
            'date_start' => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d")
        );

        return $this->prepareResponse($data);
    }

    /**
     * @Route("/mumurita", name="mumurita")
     * @Template()
     */
    public function exportAction()
    {
        // get the service container to pass to the closure
        $container = $this->container;
        $response = new StreamedResponse(function () use ($container) {

            $em = $container->get('doctrine')->getManager();

            // The getExportQuery method returns a query that is used to retrieve
            // all the objects (lines of your csv file) you need. The iterate method
            // is used to limit the memory consumption
            $results = $em->getRepository('ObtaoAcmeBundle:Jedi')->getExportQuery()->iterate();
            $handle = fopen('php://output', 'r+');

            while (false !== ($row = $results->next())) {
                // add a line in the csv file. You need to implement a toArray() method
                // to transform your object into an array
                fputcsv($handle, $row[0]->toArray());
                // used to limit the memory consumption
                $em->detach($row[0]);
            }

            fclose($handle);
        });

        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }

    /**
     * View Reports
     *
     * @Route("/reset_user_password", name="reset_user_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reset_user_password.html.twig")
     */
    public function reset_user_passwordAction()
    {

        $data = array(
            'title' => "Reset User Password",
            'title_descr' => "Reset User Password",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reset_user_password_post/{username}/{password}/{timestamp}", name="reset_user_password_post")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reset_user_password.html.twig")
     */
    public function reset_user_password_postAction($username, $password, $timestamp)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $em->createQuery("SELECT p "
            . "from RestApiBundle:User p "
            . " where p.username = '" . $username . "'")
            ->getOneOrNullResult();

        $data['msg'] = "Not successfull !";
        $data['status'] = 3;

        if ($user) {
            //if device exists update user
            /** @var  $user User */
            //$user = $this->get('security.token_storage')->getToken()->getUser();
            $encoder = $this->container->get('security.password_encoder');

            $user->setPassword($password);
            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);

            $em->flush();

            $data['msg'] = "Password Changing Successful";
            $data['status'] = 1;
        } else {
            $data['msg'] = "User Doesnt Exist. Contact your Admin";
        }

        echo json_encode($data);
        exit;

        return array(
            'title' => "Reset User Password",
            'title_descr' => "Reset User Password",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verifyer", name="verifyer")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:index_verifyer.html.twig")
     */
    public function verifyerAction()
    {

        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction(), true));


        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $data = array(
            'title' => "Index",
            'title_descr' => "View Frequently Asked Questions",
            'ddate' => date('Y-m-d'),
            'akuku' => json_encode($user->getRoles()) . " | " . $this->session->get('user_role_perms'),
            'date_today' => date('Y-m-d'),
            'date_start' => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d")
        );

        return $this->prepareResponse($data);
    }


    /**
     * View Reports
     *
     * @Route("/verifyer_summary", name="verifyer_summary")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:index_verifyer_summary.html.twig")
     */
    public function verifyerSummaryAction()
    {

        $this->session->set('user_role_perms', json_encode($this->get_department_perms_arrayAction(), true));


        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $data = array(
            'title' => "Index",
            'title_descr' => "View Frequently Asked Questions",
            'ddate' => date('Y-m-d'),
            'akuku' => json_encode($user->getRoles()) . " | " . $this->session->get('user_role_perms'),
            'date_today' => date('Y-m-d'),
            'date_start' => date('Y-m-d') //date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d")
        );

        return $this->prepareResponse($data);
    }

    function likehood_percent($str1, $str2)
    {
        similar_text($str1, $str2, $percentMatch);
        return round($percentMatch);
    }

    /**
     *
     * @Route("/change_language/{changeToLocale}", name="change_language")
     * @Method("GET")
     */
    public function changeLanguageAction($changeToLocale)
    {

//var_dump($request->request);
//$changeToLocale = $request->request->get('loc');
        $this->get('request')->attributes->set('_locale', null);
        $this->get('session')->set('_locale', $changeToLocale);

        $this->get('request')->setLocale($changeToLocale);

//var_dump($changeToLocale);
//var_dump($this->get('request')->getLocale());

        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * Registration Summary
     *
     * @Route("/anto_test", name="registration_summary")
     * @Method("GET")
     * @Template()
     */
    public function registration_summaryAction()
    {
        $em = $this->getDoctrine()->getManager();

        echo "<p>" . $this->getRequest()->getSchemeAndHttpHost() . $this->getRequest()->getBaseUrl() . "</p>";
        /*
          SELECT count(sreg_dashboard_registrations.id) as count, `state` FROM (`sreg_dashboard_registrations`) GROUP BY `state`
         */

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');

        $result = $em->createQuery("SELECT COUNT(p.id) as ccount, p.state "
            . "FROM RestApiBundle:Registration p "
            . "GROUP BY p.state")
            ->getArrayResult();

        $data = array();

//PURGE DATA IS TO BE FETCHED FROM purge table that hosts initially archived data!! nice idea huh ;)
        $purge['approved'] = $purge['pending'] = $purge['declined'] = $purge['total'] = 0;

        $data['filter'] = //$this->input->post('filter'); $filter;
        $data['start'] = "2015-01-01"; //$this->input->post('start'); $start;
        $data['end'] = "2015-01-12"; //$this->input->post('end'); $end;


        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed
        foreach ($result as $key => $value) {

            $total = $total + $value['ccount'];
            if ($value['state'] == 2 || $value['state'] == 4) {
                $approved = $approved + $value['ccount'];
            }
            if ($value['state'] == 0) {
                $pending = $value['ccount'];
            }
            if ($value['state'] == 3) {
                $declined = $value['ccount'];
            }


            if ($value['state'] == 4) {
                $approved_real = $approved_real + $value['ccount'];
            }
            if ($value['state'] == 2) {
                $pending_real = $pending_real + $value['ccount'];
            }
        }

        $data[] = array(
            'total' => number_format($total + $purge['total']),
            'approved' => number_format($approved + $purge['approved']),
            'approved_real' => number_format($approved_real),
            'pending_real' => number_format($pending_real),
            'pending' => number_format($pending + $purge['pending']),
            'rejected' => number_format($declined + $purge['declined'])
        );

        echo json_encode($data);
        exit;
    }

    /**
     * View User Role Departmenrd
     *
     * @Route("/user_role_department", name="user_role_department")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:user_role_department.html.twig")
     */
    public function view_user_role_departmentsAction()
    {
        if (!in_array(39, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();

        $query = $em->createQueryBuilder()
            ->select("p")
            ->from("RestApiBundle:Role", "p")
            ->getQuery();

        try {
            $results = $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        $data = array(
            'title' => "View User Roles Listing:",
            'title_descr' => "Listing for user permission roles departments ",
            'user_roles' => $results,
            //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/edit_role_department/{id}", name="edit_role_department")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:edit_role_department.html.twig")
     */
    public function showedit_role_departmentAction($id)
    {
        if (!in_array(40, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:Role')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Role entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'dept_id' => $id,
            'title' => "Edit User Role Department Permissions",
            'title_descr' => "Edit User Role Department Permissions",
            'entity_perms' => $entity->getDeptPerms(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Edits an existing user dept perm
     *
     * @Route("/edit_role_department_action/{id}", name="edit_role_department_action")
     * @Method("POST")
     * @Template("DashboardBundle:Faq:edit.html.twig")
     */
    public function update_edit_role_department_actionAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $attributes = $request->request->all();

        $results = $em->createQuery("UPDATE RestApiBundle:Role u SET u.dept_perms = '" . str_replace('"', "", json_encode($attributes['perms'])) . "' WHERE u.id = " . $id . " ")
            ->getArrayResult();

//return $this->redirect($this->generateUrl('admin/agents'));
        return $this->redirect($this->generateUrl('admin'));
    }

    /**
     * hour_graph Summary
     *
     * @Route("/department_perms_json", name="department_perms_json")
     * @Method("GET")
     * @Template()
     */
    public function department_perms_jsonAction()
    {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();

        $result = $em->createQuery("SELECT p "
            . " from DashboardBundle:Department_perms p "
            . " ORDER BY p.parent asc")
            ->getArrayResult(); //getSingleResult();
        $data = array();

        foreach ($result as $key => $value) {
            if ($value['parent'] == 0) {
                $data[$value['id']] = array(
                    'id' => 1,
                    'text' => $value['name'],
                    'children' => array()
                );
            } else {
                $data[$value['parent']]['children'][] = array('text' => $value['name'], 'perm_checked' => 0, 'perm_id' => $value['id']);
            }
        }
        foreach ($data as $ey => $alue) {
            $data_perms[] = $alue;
        }

        echo json_encode($data_perms);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/department_perms_array", name="department_perms_array")
     * @Method("GET")
     * @Template()
     */
    public function get_department_perms_arrayAction()
    {
        $em = $this->getDoctrine()->getManager();
        $pperms = $unique_perms = array();
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($user) {
            $roles = $user->getRoles(); //array of roles
            $perms_str = str_replace('"', "'", str_replace("]", "", str_replace("[", "", json_encode($user->getRoles()))));

            $emConfig = $em->getConfiguration();
            $result = $em->createQuery("SELECT p "
                . " from RestApiBundle:Role p "
                . "WHERE p.role in (" . $perms_str . ")")
                ->getArrayResult(); //getSingleResult();


            foreach ($result as $key => $value) {
                foreach (json_decode($value['dept_perms']) as $kkey => $vvalue) {
                    $pperms[$vvalue] = $vvalue;
                }
            }

            ksort($pperms);
            foreach ($pperms as $kkkkey => $vvvalue) {
                $unique_perms[] = $vvvalue;
            }
        }

        return $unique_perms;
    }

    /**
     * Create New User Role Dept
     *
     * @Route("/newUserDept", name="newUserDept")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:newUserDept.html.twig")
     */
    public function newUserDeptAction()
    {

        $entity = new Role();
        $form = $this->createCreatedForm($entity);

        $data = array(
            'title' => "Create New User Role Department",
            'title_descr' => "Create New User Role Department",
            'entity' => $entity,
            'form' => $form->createView(),
        );
        return $this->prepareResponse($data);
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/newUserDeptaction", name="newUserDeptaction")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:newUserDept.html.twig")
     */
    public function createnewUserDeptAction(Request $request)
    {

        $entity = new Role();
        $form = $this->createCreatedForm($entity);
        $form->handleRequest($request);

        if (true || $form->isValid()) { //echo "11111111"; exit;
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('user_role_department'));
        }

        //echo "222222222"; exit;

        return array(
            'title' => "Create New User Role Department",
            'title_descr' => "Create New User Role Department",
            'entity' => $entity,
            'form' => $form->createView()
        );
    }

    /**
     * Creates a form to create a Role entity.
     *
     * @param Role $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreatedForm(Role $entity)
    {

        $form = $this->createForm(new RoleType(), $entity, array(
            'action' => $this->generateUrl('admin'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Registration Summary
     *
     * @Route("/regbyday_summary/{start_date}/{end_date}", name="regbyday_summary")
     * @Method("GET")
     * @Template()
     */
    public function regbyday_summaryAction($start_date, $end_date)
    {
        $em = $this->getDoctrine()->getManager();

        /*
          SELECT DAYNAME(created_date) as day_name, count(id) as day_count
          from sreg_dashboard_registrations
          " . $where . "
          group by day_name
          order by FIELD(day_name, 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY', 'SUNDAY');
         */

        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(p.createdDate) >= '" . $start_date . "' AND date(p.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DAYNAME', 'DoctrineExtensions\Query\Mysql\DayName');


        $result = $em->createQuery("SELECT DAYNAME(p.createdDate) as day_name, count(p) as day_count "
            . "from RestApiBundle:Registration p "
            . " JOIN p.owner a"
            . " where 1=1 " . $where
            . " group by day_name")
            ->getArrayResult();


        $response_array = array();
        foreach ($result as $key => $value) {
            $response_array[] = array('day_name' => $value['day_name'], 'day_count' => $value['day_count']);
        }

        echo json_encode($response_array);
        exit;
    }

    /**
     * trendlatest_summary Summary
     *
     * @Route("/oldtrendlatest_summary/{start_date}/{end_date}/{timestamp}", name="oldtrendlatest_summary")
     * @Method("GET")
     * @Template()
     */
    public function oldtrendlatest_summaryAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        /*
          SELECT date(created_date) as date, count(sreg_dashboard_registrations.id) as count, `state`
          FROM (`sreg_dashboard_registrations`)
          WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08'
          GROUP BY date(`created_date`), `state`
          ORDER BY date(`created_date`) desc
         */

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }
        if (in_array("ROLE_VODASHOP", $roles)) {
            $where .= " AND a.vodashop_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(e.createdDate) >= '" . $start_date . "' AND date(e.createdDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, DATE(e.createdDate) as ddate, COUNT(p.id) as ccount "
            . " from RestApiBundle:RegistrationStatus p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
            . " JOIN p.registrationId e"
            . " JOIN e.owner a" . $where
            . " GROUP BY e.image_count, ddate, treg, freg "
            . " ORDER BY ddate desc")
            ->getArrayResult(); //getSingleResult();

        $data = $data2 = array();


//        foreach ($result as $key => $value) {
//            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
//            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
//        }

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed
        foreach ($result as $key => $value) {
            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];

            $status_keys = array(
                'success' => 2,
                'declined' => 3,
                'fail' => 4,
                'retry' => 5,
                'success_cap' => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value['image_count'] < 3) {
                $data[$value['ddate']][0]['count'] = @$data[$value['ddate']][0]['count'] + $value['ccount'];
            }


            if (in_array($value['treg'], array(1, 2, 6, 7))) {
                //TREG
                $data[$value['ddate']][1]['count'] = @$data[$value['ddate']][1]['count'] + $value['ccount'];
            } elseif (in_array($value['treg'], array(4, 5, 3))) {
                //FAIL
                $data[$value['ddate']][3]['count'] = @$data[$value['ddate']][3]['count'] + $value['ccount'];
            } else {
                //NOT SENT YET
                //$data[$value['ddate']][5]['count'] = @$data[$value['ddate']][5]['count'] + $value['ccount'];
            }

            if ($value['image_count'] >= 3 && in_array($value['treg'], array(0, '-1'))) {
                $data[$value['ddate']][5]['count'] = @$data[$value['ddate']][5]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(1, 2, 6, 7)) && in_array($value['freg'], array(2, 6, 7))) {
                //FREG
                $data[$value['ddate']][2]['count'] = @$data[$value['ddate']][2]['count'] + $value['ccount'];
            }
            if (in_array($value['treg'], array(1, 2, 6, 7)) && $value['freg'] == 0) {
                //PENDING FREG
                $data[$value['ddate']][4]['count'] = @$data[$value['ddate']][4]['count'] + $value['ccount'];
            }
        }

        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2[] = array(
                'date' => $key4,
                'total' => (@$value4['total']['count']) > 0 ? "<a href=''>" + @$value4['total']['count'] + "</a>" : @$value4['total']['count'],
                'pending_info' => (@$value4[0]['count']) ? @$value4[0]['count'] : "0",
                'pending_tobesent' => (@$value4[5]['count']) ? @$value4[5]['count'] : "0",
                'treg' => (@$value4[1]['count']) ? @$value4[1]['count'] : "0",
                'freg' => (@$value4[2]['count']) ? @$value4[2]['count'] : "0",
                'pending_freg' => (@$value4[4]['count']) ? @$value4[4]['count'] : "0",
                'declined' => (@$value4[3]['count']) ? @$value4[3]['count'] : "0"
            );
        }

        echo json_encode($data2);
        exit;
    }


    /**
     * trendlatest_summary Summary
     *
     * @Route("/trendlatest_summary/{start_date}/{end_date}/{timestamp}", name="trendlatest_summary")
     * @Method("GET")
     * @Template()
     */
    public function trendlatest_summaryAction($start_date, $end_date, $timestamp)
    {
        $em = $this->container->get("doctrine")->getManager();
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }
        if (in_array("ROLE_VODASHOP", $roles)) {
            $where .= " AND a.vodashop_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(e.createdDate) >= '" . $start_date . "' AND date(e.createdDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.verifyState as vstate, DATE(e.createdDate) as ddate, COUNT(p.id) as ccount "
            . " from RestApiBundle:RegistrationStatus p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
            . " JOIN p.registrationId e"
            . " JOIN e.owner a" . $where
            . " GROUP BY ddate, e.image_count, treg, freg, vstate "
            . " ORDER BY ddate desc")
            ->getArrayResult(); //getSingleResult();


        $data = $data2 = array();


//        foreach ($result as $key => $value) {
//            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
//            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
//        }

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed

        foreach ($result as $key => $value) {
            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];

            $status_keys = array(
                'success' => 2,
                'declined' => 3,
                'fail' => 4,
                'retry' => 5,
                'success_cap' => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value['image_count'] < 4) {
                if (!in_array($value['treg'], array(4, 5, 3))) {
                    $data[$value['ddate']][0]['count'] = @$data[$value['ddate']][0]['count'] + $value['ccount'];
                }
            }


            if (in_array($value['treg'], array(2, 6, 7))) {
                //TREG
                $data[$value['ddate']][1]['count'] = @$data[$value['ddate']][1]['count'] + $value['ccount'];
            } elseif (in_array($value['treg'], array(4, 5, 3))) {
                //FAIL
                $data[$value['ddate']][3]['count'] = @$data[$value['ddate']][3]['count'] + $value['ccount'];
            } else {
                //NOT SENT YET
                if ($value['image_count'] >= 4 && $value['vstate'] == 1) {
                    $data[$value['ddate']][5]['count'] = @$data[$value['ddate']][5]['count'] + $value['ccount'];
                }
            }

            if (in_array($value['treg'], array(2, 6, 7)) && in_array($value['freg'], array(2, 6, 7))) {
                //FREG
                $data[$value['ddate']][2]['count'] = @$data[$value['ddate']][2]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(2, 6, 7)) && !in_array($value['freg'], array(2, 6, 7))) {// && $value['vstate'] == 1
                //PENDING FREG
                $data[$value['ddate']][4]['count'] = @$data[$value['ddate']][4]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(0, 2, 6, 4)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))) {
                if (in_array($value['vstate'], array(1, 2, 3, '-2', '-3'))) {
                    $data[$value['ddate']][30]['count'] = @$data[$value['ddate']][30]['count'] + $value['ccount'];

                    if ($value['vstate'] == 1) {
                        //vgood
                        $data[$value['ddate']][32]['count'] = @$data[$value['ddate']][32]['count'] + $value['ccount'];
                    } elseif ($value['vstate'] == 2 || $value['vstate'] == '-2') {
                        //vfair
                        $data[$value['ddate']][33]['count'] = @$data[$value['ddate']][33]['count'] + $value['ccount'];
                    } elseif ($value['vstate'] == 3 || $value['vstate'] == '-3') {
                        //vbad
                        $data[$value['ddate']][34]['count'] = @$data[$value['ddate']][34]['count'] + $value['ccount'];
                    }
                } else {
                    //notverified
                    $data[$value['ddate']][31]['count'] = @$data[$value['ddate']][31]['count'] + $value['ccount'];
                }
            }
        }

        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2[] = array(
                'date' => $key4,
                'total' => (@$value4['total']['count']) > 0 ? "<a href=''>" + @$value4['total']['count'] + "</a>" : @$value4['total']['count'],
                'pending_info' => (@$value4[0]['count']) ? @$value4[0]['count'] : "0",
                'pending_tobesent' => (@$value4[5]['count']) ? @$value4[5]['count'] : "0",
                'treg' => (@$value4[1]['count']) ? @$value4[1]['count'] : "0",
                'freg' => (@$value4[2]['count']) ? @$value4[2]['count'] : "0",
                'verified' => (@$value4[30]['count']) ? @$value4[30]['count'] : "0",
                'notverified' => (@$value4[31]['count']) ? @$value4[31]['count'] : "0",
                'vgood' => (@$value4[32]['count']) ? @$value4[32]['count'] : "0",
                'vfair' => (@$value4[33]['count']) ? @$value4[33]['count'] : "0",
                'vbad' => (@$value4[34]['count']) ? @$value4[34]['count'] : "0",
                'pending_freg' => (@$value4[4]['count']) ? @$value4[4]['count'] : "0",
                'declined' => (@$value4[3]['count']) ? @$value4[3]['count'] : "0"
            );
        }

        echo json_encode($data2);
        exit;
    }


    /**
     * hourly_summary Summary
     *
     * @Route("/ccount_summary/{start_date}/{end_date}/{timestamp}", name="ccount_summary")
     * @Method("GET")
     * @Template()
     */
    public function ccount_summaryAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(x.createdDate) >= '" . $start_date . "' AND date(x.createdDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg, r.fullRegStatus as freg, r.verifyState as vstate FROM RestApiBundle:RegistrationStatus r"
            . " JOIN r.registrationId x JOIN x.owner a " . $where
            . " GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus, r.verifyState"
        )->getArrayResult();


        $data['freg'] = $data['ttopreceived'] = $data['fail'] = $data['treg'] = $data['pendingtreg'] = $data['pending'] = $data['total'] = $data['pending_freg'] = 0;
        foreach ($count_tfreg as $key => $val) {
            $data['total'] = $data['total'] + $val['reg_count'];
            if ($val['image_count'] < 3) {
                $data['pending'] = $data['pending'] + $val['reg_count'];
            } else {
                $data['ttopreceived'] = $data['ttopreceived'] + $val['reg_count'];
            }

            if (in_array($val['treg'], array(1, 2, 6, 7))) {
                $data['treg'] = $data['treg'] + $val['reg_count'];
                $data['pending_freg'] = $data['pending_freg'] + $val['reg_count'];

                if (in_array($val['freg'], array(1, 2, 6, 7))) {
                    $data['freg'] = $data['freg'] + $val['reg_count'];
                }
            } elseif (in_array($val['treg'], array(0, 5)) && $val['image_count'] > 3 && $val['vstate'] == 1) {
                $data['pendingtreg'] = $data['pendingtreg'] + $val['reg_count'];
            } else {
                $data['fail'] = $data['fail'] + $val['reg_count'];
            }
        }


        echo json_encode($data);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/ccount_verifysummary/{start_date}/{end_date}/{timestamp}", name="ccount_verifysummary")
     * @Method("GET")
     * @Template()
     */
    public function ccount_verifysummaryAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND r.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }
        //$where .= " AND date(r.verifyDate) >= '2016-07-01' ";

        if (in_array("ROLE_VERIFYER_SUPERVISOR", $roles)) {
            $where .= " AND r.verifyBy != 3260 ";
        }

        $where .= " AND x.image_count >= 3 ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, r.verifyState as verifyState FROM RestApiBundle:RegistrationStatus r"
            . " JOIN r.registrationId x "
            . $where
            . " GROUP BY r.verifyState"
        )->getArrayResult();


        $data['total'] = $data['pass'] = $data['fail'] = $data['pending'] = 0;
        foreach ($count_tfreg as $key => $val) {
            $data['total'] = $data['total'] + $val['reg_count'];

            if ($val['verifyState'] == 1) {
                $data['pass'] = $data['pass'] + $val['reg_count'];
            } elseif (in_array($val['verifyState'], array(2, 3))) {
                $data['fail'] = $data['fail'] + $val['reg_count'];
            } else {
                $data['pending'] = $data['pending'] + $val['reg_count'];
            }
        }


        echo json_encode($data);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/avarage_verification_time/{start_date}/{end_date}/{timestamp}", name="avarage_verification_time")
     * @Method("GET")
     * @Template()
     */
    public function avarage_verification_timeAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 AND r.verifyState in (1,2,3) AND r.verifyLock > 1 AND r.verifyLock < 1000 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND r.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.verifyDate) >= '" . $start_date . "' AND date(r.verifyDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT AVG(r.verifyLock) as verifyLock FROM RestApiBundle:RegistrationStatus r"
            . $where
        )->getSingleResult();

        $count_tfreg['verifyLock'] = (@$count_tfreg['verifyLock'] ? floor($count_tfreg['verifyLock']) . " Seconds" : 0);

        echo json_encode($count_tfreg);
        exit;
    }

    /**
     * hourly_summary Summary
     *
     * @Route("/hourly_summary/{start_date}/{end_date}/{timestamp}", name="hourly_summary")
     * @Method("GET")
     * @Template()
     */
    public function hourly_summaryAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        if ($start_date == 1) {
            $start_date = $end_date = date('Y-m-d');
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $where .= " AND DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        $result = $em->createQuery("SELECT HOUR(p.createdDate) as hhour, COUNT(p.id) as ccount "
            . " from RestApiBundle:Registration p "
//. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
            . " JOIN p.owner a" . $where
            . " GROUP BY hhour "
            . " ORDER BY hhour desc")
            ->getArrayResult(); //getSingleResult();

        $data = array();

        for ($i = 0; $i <= 23; $i++) {
            $data[$i] = 0;
        }

        foreach ($result as $key => $value) {
            //echo "<pre>"; print_r($value); echo "</pre>";

            $data[$value['hhour']] = $value['ccount'];
        }


        echo json_encode($data);
        exit;
    }

    /**
     * graphreport_region Summary
     *
     * @Route("/graphreport_region/{start_date}/{end_date}/{territory}", name="graphreport_region")
     * @Method("GET")
     * @Template()
     */
    public function graphreport_regionAction($start_date, $end_date, $territory)
    {
        $em = $this->getDoctrine()->getManager();

        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery("SELECT t from DashboardBundle:Territory t ")->getArrayResult(); //getSingleResult();

        foreach ($result as $key => $value) {
            $regionskey[$value['name']] = $value['id'];
            $regionsName[$value['id']] = $value['name'];
        }

        $regionsName['EntireCountry'] = $regionskey['EntireCountry'] = "EntireCountry";
        $regionskey['all_regions'] = "EntireCountry";

        $postedRegions = explode(",", $territory);
        foreach ($postedRegions as $xkey => $xval) {
            $regionscode[] = $regionskey[strtolower($xval)];
        }

        $regionssearch = "";
        foreach ($regionscode as $tkey => $tval) {
            $regionssearch .= $tval . ",";
        }
        $regionssearch = rtrim($regionssearch, ",");


        $group_by = $territory_field = "";
        $territory_field = ", 'EntireCountry' AS  tterritory";
        $territory_list = "'" . str_replace(",", "','", $territory) . "'";
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";
        if ($territory != "all_regions") {
            $where .= " AND p.territory in (" . $regionssearch . ") ";
            $group_by = ", tterritory";
            $territory_field = ", p.territory as tterritory";
        }

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        //* DESIRED: SELECT count(*) as ccount, gender, DATE_FORMAT(createdDate, '%Y%m') as mmonth, territory, EntireCountry' AS  tterritory FROM `registration` group by mmonth, gender order by id desc */
        $result = $em->createQuery("SELECT DATE_FORMAT(p.createdDate, '%Y%m') as mmonth, COUNT(p.id) as ccount, p.gender " . $territory_field
            . " from RestApiBundle:Registration p "
            // . " JOIN p.owner a "
            // . " JOIN p.region b "
            . $where
            . " GROUP BY mmonth, p.gender" . $group_by
            . " ORDER BY mmonth desc")
            ->getArrayResult(); //getSingleResult();
        //echo $em->getSql();

        $data_gender = array();
        $data2[0][0][0] = "[]";
        $data2[0][0][1] = "[]";
        $data2[1]['male'] = 0;
        $data2[1]['female'] = 0;
        $data2[2] = array();


        //get all months and all dates
        $month_list = $territory_list = array();
        $cnt = 1;
        foreach ($result as $key => $value) {

            $antoo2[] = $regionsName[$value['tterritory']] . "|" . $value['tterritory'];
            if (!in_array($value['mmonth'], $month_list)) {
                $month_list[] = $value['mmonth'];
            }
            $territory_list[$regionsName[$value['tterritory']]] = $regionsName[$value['tterritory']];
            $cnt++;
        }

        //create interesting empty array for territory Gender and Registrations
        foreach ($territory_list as $tkey => $tval) {

            foreach ($month_list as $mkey => $mval) {
                $territory_data['data'][$tval][$mval] = 0;
                $territory_data['gender'][$tval]['male'] = 0;
                $territory_data['gender'][$tval]['female'] = 0;
            }
        }

        //Abracadabra!! let the magic happen!! 
        foreach ($result as $kkey => $vvalue) {

            $antoo[] = $regionsName[$vvalue['tterritory']] . "|" . $vvalue['tterritory'];
            $territory_data['data'][$regionsName[$vvalue['tterritory']]][$vvalue['mmonth']] = $territory_data['data'][$regionsName[$vvalue['tterritory']]][$vvalue['mmonth']] + $vvalue['ccount'];

            if (strtolower($vvalue['gender']) == 'male' || strtolower($vvalue['gender']) == 'm') {
                $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['male'] = $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['male'] + $vvalue['ccount'];
            } else {
                $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['female'] = $territory_data['gender'][$regionsName[$vvalue['tterritory']]]['female'] + $vvalue['ccount'];
            }
        }

        $territory_data['month_list'] = $month_list;
        $territory_data['ccount_elements'] = count(@$territory_data['data']);

        echo json_encode($territory_data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/hour_graph/{start_date}/{end_date}/{territory}", name="hour_graph")
     * @Method("GET")
     * @Template()
     */
    public function hour_graphAction($start_date, $end_date, $territory)
    {
        $em = $this->getDoctrine()->getManager();


        //since territory [town names ] isnt left joined plus saving time lets pick code from array instead :(
        //first get territories iterate to form array then pick!
        $result = $em->createQuery("SELECT t from DashboardBundle:Territory t ")->getArrayResult(); //getSingleResult();

        foreach ($result as $key => $value) {
            $regionskey[$value['name']] = $value['id'];
            $regionsName[$value['id']] = $value['name'];
        }

        $regionsName['EntireCountry'] = $regionskey['EntireCountry'] = "EntireCountry";
        $regionskey['all_regions'] = "EntireCountry";

        $postedRegions = explode(",", $territory);
        foreach ($postedRegions as $xkey => $xval) {
            $regionscode[] = $regionskey[strtolower($xval)];
        }

        $regionssearch = "";
        foreach ($regionscode as $tkey => $tval) {
            $regionssearch .= $tval . ",";
        }
        $regionssearch = rtrim($regionssearch, ",");


        $territory = strtolower($territory);
        $group_by = $territory_field = "";
        $territory_field = ", 'EntireCountry' AS  tterritory";
        //echo '[[["[]","[]"]],{"male":0,"female":0},[]]'; exit;
        $territory_list = "'" . str_replace(",", "','", $territory) . "'";

        if ($start_date == 1) {
            $start_date = $end_date = date('Y-m-d');
        }


        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";
        if ($territory != "all_regions") {
            $where .= " AND p.territory in (" . $regionssearch . ") ";
            $group_by = ", tterritory";
            $territory_field = ", p.territory as tterritory";
        } else {
            $territory = "EntireCountry";
        }

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, HOUR(p.createdDate) as hhour " . $territory_field
            . " from RestApiBundle:Registration p "
            . " JOIN p.owner a" . $where
            . " GROUP BY hhour "
            . " ORDER BY hhour desc")
            ->getArrayResult(); //getSingleResult(); //getSql

        $data = array();

        $string_hour = "[";
        $string_data = "[";
        $cnt = 1;
        foreach (explode(",", $territory) as $key => $value) {

            for ($i = 0; $i <= 23; $i++) {
                $data[$value][$i] = 0;
            }
            $cnt++;
        }

        $ccount = 0;
        //get all months and all dates
        $month_list = $territory_list = array();
        foreach ($result as $key => $value) {
            $ccount = $ccount + 1;
            $data[$regionsName[$value['tterritory']]][$value['hhour']] = $value['ccount'];
        }

        $datareturn['ccount_elements'] = $ccount;
        $datareturn['ddata'] = $data;

        echo json_encode($datareturn);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentregistrations/{start_date}/{end_date}/{territory}", name="agentregistrations")
     * @Method("GET")
     * @Template()
     */
    public function agentregistrations_mapAction($start_date, $end_date, $territory)
    {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);

        //echo '[[["[]","[]"]],{"male":0,"female":0},[]]'; exit;
        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.territory as territory "
            . " from RestApiBundle:Registration p " . $where
            . " GROUP BY territory ")
            ->getArrayResult(); //getSingleResult(); //getSql

        /*
          {
          "code": "GE",
          "z": 362
          }
         */

        $regionskey = array(
            "mwanza" => "MW",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR",
            "NT" => "North",
            "CS" => "Coast",
            "ST" => "South",
            "LK" => "Lake Zone"
        );

        $data = array();
        foreach ($regionskey as $key => $value) {
            $data[$key] = array("code" => $value, "z" => 0);
        }

        foreach ($result as $kkey => $vval) {
            $data[$vval['territory']]['z'] = $vval['ccount'];
        }


//        echo "<p><pre>";
//        print_r($data);
//        echo "</pre></p><p> --- --- --</p><br />";

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentlocation_map/{start_date}/{end_date}/{territory}", name="agentlocation_map")
     * @Method("GET")
     * @Template()
     */
    public function agentlocation_mapAction($start_date, $end_date, $territory)
    {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);

        $where = " WHERE DATE(p.createdDate) >= '" . $start_date . "' AND DATE(p.createdDate) <= '" . $end_date . "' ";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.status, a.name as tterritory "
            . " from RestApiBundle:User p "
            . " JOIN p.territory a"
            . " GROUP BY tterritory ")
            ->getArrayResult(); //getSingleResult(); //getSql

        $regionskey = array(
            "mwanza" => "MW",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR",
            "NT" => "North",
            "CS" => "Coast",
            "ST" => "South",
            "LK" => "Lake Zone"
        );

        $data = array();
        foreach ($regionskey as $key => $value) {
            $data[$key] = array("code" => $value, "z" => 0);
        }

        foreach ($result as $kkey => $vval) {
            $data[$vval['tterritory']]['z'] = $vval['ccount'];
        }

        echo json_encode($data);
        exit;
    }

    /**
     * hour_graph Summary
     *
     * @Route("/agentactiveinactive_map/{start_date}/{end_date}/{territory}", name="agentactiveinactive_map")
     * @Method("GET")
     * @Template()
     */
    public function agentactiveinactive_mapAction($start_date, $end_date, $territory)
    {
        $em = $this->getDoctrine()->getManager();

        $territory = strtolower($territory);
        $group_by = $territory_field = "";
        $territory_field = ", 'EntireCountry' AS  tterritory";

        $territory_list = "'" . str_replace(",", "','", $territory) . "'";
        $where = " WHERE 1=1 ";

        if ($territory != "all_regions") {
            $where .= " AND a.name in (" . $territory_list . ") ";
            $group_by = ", tterritory";
            $territory_field = ", a.name as tterritory";
        } else {
            $territory = "EntireCountry";
        }


        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $result = $em->createQuery("SELECT count(p) as ccount, p.status " . $territory_field
            . " from RestApiBundle:User p "
            . " JOIN p.territory a" . $where
            . " GROUP BY tterritory, p.status ")
            ->getArrayResult(); //getSingleResult(); //getSql

        $territory_array = explode(",", $territory);

        $data['ccount_elements'] = count($territory_array);
        foreach ($territory_array as $keyy => $vall) {
            $data['data'][$vall] = array("inactive" => 0, "active" => 0);
        }

        $data['ccount_result'] = 0;
        foreach ($result as $kkey => $vval) {

            $data['ccount_result'] = $data['ccount_result'] + 1;

            $sstatus = ($vval['status'] == 1 ? "active" : "inactive");
            $data['data'][$vval['tterritory']][$sstatus] = $vval['ccount'];
            $data['data'][$vval['tterritory']][$sstatus] = $vval['ccount'];
        }

        echo json_encode($data);
        exit;
    }

    /**
     * graphdate_range Summary
     *
     * @Route("/graphdate_range", name="graphdate_range")
     * @Method("GET")
     * @Template()
     */
    public function graphdate_rangeAction()
    {
        $em = $this->getDoctrine()->getManager();

        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

//$emConfig->addCustomStringFunction($name, 'aimgroup\DashboardBundle\DQL');
        $emConfig->addCustomNumericFunction('FLOOR', 'aimgroup\DashboardBundle\DQL\MysqlFloor');
//$emConfig->addCustomDatetimeFunction($name, 'aimgroup\DashboardBundle\DQL');


        $result = $em->createQuery("SELECT COUNT(p) as ccount, CASE WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) <= 10 THEN '0-10'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >= 10 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <= 20 THEN '11-20'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=21 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=30 THEN '21-30'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=31 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=40 THEN '31-40'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=41 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <= 50 THEN '31-40'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=51 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=60 THEN '51-60'
		
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=61 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=70 THEN '61-70'
		WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=71 AND FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob 
, '%d-%m-%Y' ) ) /365 ) <=80 THEN '71-80'
        WHEN FLOOR( DATEDIFF( DATE( NOW( ) ) , STR_TO_DATE( p.dob , '%d-%m-%Y' ) ) /365 ) >=81 THEN '81+'
	  END AS ageband  from RestApiBundle:Registration p GROUP BY ageband  ORDER BY ageband desc")
            ->getArrayResult(); //getSingleResult();

        echo json_encode($result);
        exit;
    }

    /**
     * @param Request $request
     * @Route("/passwordChange", name="passwordChange")
     * @Method("POST")
     */
    public function passwordChangeAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();

        $resp = new JsonObject();
        $status = false;
        $message = "REQUEST FAILED";
        $attributes = json_decode($request->getContent(), true);

        // $this->get("api.helper")->log("saveUserPasswordAction", $requestString);
        try {

            //echo json_encode($attributes);
            //$username = $attributes['username'];
            $old_password = $attributes["toptop"];
            $password = $attributes["password"];

            $user = $this->get('security.token_storage')->getToken()->getUser();

            /* $em = $this->getDoctrine()->getManager();
              $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
              $user = $queryBuilder->select('u')
              ->from('RestApiBundle:User', 'u')
              ->where('u.username = :username')
              ->setParameter('username', $username)
              ->getQuery()->getOneOrNullResult(); */

            if ($user) {
                //if device exists update user
                /** @var  $user User */
                //$user = $this->get('security.token_storage')->getToken()->getUser();
                $encoder = $this->container->get('security.password_encoder');
                if ($encoder->isPasswordValid($user, $old_password)) {
                    $user->setPassword($password);
                    $encoder = $this->container->get('security.password_encoder');
                    $encoded = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($encoded);

                    $em->flush();
                    $status = true;
                    $message = "SUCCESS";
                } else {
                    $status = false;
                    $message = "FAILED: Mismatching | Incorrect PAssword ";
                }
            } else {
                echo "patee user";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        //$this->get("api.helper")->log("saveUserPasswordAction", array($resp->getMessage(), $username), true);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * top_agents Summary
     *
     * @Route("/top_agents/{start_date}/{end_date}/{timestamp}", name="top_agents")
     * @Method("GET")
     * @Template()
     */
    public function top_agentsAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        /*
         * SELECT `phone`, `users`.`first_name` as first_name, `users`.`last_name` as last_name, count(sreg_dashboard_registrations.id) as count
         * FROM (`sreg_dashboard_registrations`) 
         * LEFT JOIN `users` ON `sreg_dashboard_registrations`.`created_by` = `users`.`id` 
         * GROUP BY `sreg_dashboard_registrations`.`created_by` 
         * ORDER BY `count` desc 
         * LIMIT 10
         */

        $limit = 10;
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $limit = "all";
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $limit = 10;

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "
        $query = $em->createQueryBuilder()
            ->select("u.username, CONCAT(u.firstName , ' ', u.lastName) as agent_names, COUNT(r.id) as registrations_count"
                . " from RestApiBundle:Registration r"
                . " JOIN r.owner u " . $where
                . " GROUP BY r.owner ORDER BY registrations_count DESC")
            ->getQuery();

        if (strtolower($limit) != "all") {
            $query->setMaxResults($limit);
        }

        $data2 = $query->getResult();

        $data = array();
        foreach ($data2 as $key => $value) {
            $data[] = array('name' => $value['agent_names'], 'count' => $value['registrations_count']);
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/active_agents/{start_date}/{end_date}", name="active_agents")
     * @Method("GET")
     * @Template()
     */
    public function active_agentsAction($start_date, $end_date)
    {
        $em = $this->getDoctrine()->getManager();

        /*
         * SELECT `phone`, `users`.`first_name` as first_name, `users`.`last_name` as last_name, count(sreg_dashboard_registrations.id) as count
         * FROM (`sreg_dashboard_registrations`) 
         * LEFT JOIN `users` ON `sreg_dashboard_registrations`.`created_by` = `users`.`id` 
         * GROUP BY `sreg_dashboard_registrations`.`created_by` 
         * ORDER BY `count` desc 
         * LIMIT 10
         */

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }
        $where .= " AND u.roles LIKE '%ROLE_AGENT%' ";
        $where2 .= " AND u.roles LIKE '%ROLE_AGENT%' ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("COUNT(r.id) as sum_regs, DATE_FORMAT(u.createdAt, '%Y-%m-%d') as created_on, u.id as agent_id, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username,  t.name as territory_name, d.name as region_name "
                . " from RestApiBundle:Registration r"
                . " JOIN r.owner u "
                . " JOIN u.region d "
                . " JOIN u.territory t " . $where
                . " GROUP BY r.owner ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $active[] = $value['agent_id'];

            unset($value['agent_id']);
            $data['active'][] = $value;

        }


        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/inactive_agents/{start_date}/{end_date}", name="inactive_agents")
     * @Method("GET")
     * @Template()
     */
    public function inactive_agentsAction($start_date, $end_date)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles


        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $where .= " AND u.roles LIKE '%ROLE_AGENT%' ";
        $where2 .= " AND u.roles LIKE '%ROLE_AGENT%' ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $query = $em->createQueryBuilder()
            ->select("u.id as userId "
                . " from RestApiBundle:Registration r"
                . " JOIN r.owner u " . $where
                . " GROUP BY r.owner ORDER BY r.id DESC")
            ->getQuery();


        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $active[] = $value['userId'];
        }


        if (count($active) > 0) {
            $where2 .= " AND u.id NOT in (" . implode(',', $active) . ")";
        }
        $query2 = $em->createQueryBuilder()
            ->select("DATE_FORMAT(u.createdAt, '%Y-%m-%d') as created_on, CONCAT(u.firstName , ' - ', u.lastName) as agent_names, u.username, d.name as region_name, t.name as territory_name, DATE_FORMAT(u.lastLogin, '%Y-%m-%d') as lastLogin "
                . " from RestApiBundle:User u"
                // . " JOIN u.parent_id p "
                . " JOIN u.region d "
                . " JOIN u.territory t " . $where2
                . " GROUP BY u.id")
            ->getQuery();


        $result2 = $query2->getResult();


        $data['inactive'] = array();
        foreach ($result2 as $key2 => $value2) {
            $data['inactive'][] = $value2;
        }


        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/pullsmsreg_type/{start_date}/{end_date}/{timestamp}", name="pullsmsreg_type")
     * @Method("GET")
     * @Template()
     */
    public function pullsmsreg_typeAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        /*
         * SELECT `phone`, `users`.`first_name` as first_name, `users`.`last_name` as last_name, count(sreg_dashboard_registrations.id) as count
         * FROM (`sreg_dashboard_registrations`) 
         * LEFT JOIN `users` ON `sreg_dashboard_registrations`.`created_by` = `users`.`id` 
         * GROUP BY `sreg_dashboard_registrations`.`created_by` 
         * ORDER BY `count` desc 
         * LIMIT 10
         */

        $where = $where2 = " WHERE 1=1 AND r.isSms = 1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $query = $em->createQueryBuilder()
            ->select("count(r.id) as ccount, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, r.appVersion, r.deviceModel "
                . " from RestApiBundle:Registration r "
                . " JOIN r.owner u " . $where
                . " GROUP BY r.deviceModel, r.appVersion")
            ->getQuery();

        $result1 = $query->getResult();

        echo json_encode($result1);
        exit;

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    /**
     * View Users
     *
     * @Route("/view_agents", name="view_agents")
     * @Method("GET")
     * @Template()
     */
    public function view_agentsAction()
    {
        $em = $this->getDoctrine()->getManager();

//$entities = $em->getRepository('DashboardBundle:Admin')->findAll();
        $entities_user = $em->getRepository('DashboardBundle:User')->findAll();

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_daily_report/{start_date}/{end_date}/{timestamp}", name="audit_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_daily_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, COUNT(r.id) as sum_regs, DATE_FORMAT(s.auditDate, '%Y-%m-%d') as created_on, s.auditState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u " . $where
                . " GROUP BY s.auditDate, s.auditState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['created_on']] = (@$data[$value['created_on']] ? $data[$value['created_on']] : $value);

            $data[$value['created_on']]['sum_total'] = @$data[$value['created_on']]['sum_total'] + $value['sum_regs'];
            $data[$value['created_on']]['sum_good'] = (@$data[$value['created_on']]['sum_good'] ? $data[$value['created_on']]['sum_good'] : 0);
            $data[$value['created_on']]['sum_fair'] = (@$data[$value['created_on']]['sum_fair'] ? $data[$value['created_on']]['sum_fair'] : 0);
            $data[$value['created_on']]['sum_bad'] = (@$data[$value['created_on']]['sum_bad'] ? $data[$value['created_on']]['sum_bad'] : 0);
            $data[$value['created_on']]['sum_unrated'] = (@$data[$value['created_on']]['sum_unrated'] ? $data[$value['created_on']]['sum_unrated'] : 0);

            if ($value['auditState'] == 3) {
                $data[$value['created_on']]['sum_good'] = $data[$value['created_on']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 2) {
                $data[$value['created_on']]['sum_fair'] = $data[$value['created_on']]['sum_fair'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 1) {
                $data[$value['created_on']]['sum_bad'] = $data[$value['created_on']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data[$value['created_on']]['sum_unrated'] = $data[$value['created_on']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_total_report/{start_date}/{end_date}/{timestamp}", name="audit_total_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_total_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, r.image_count, COUNT(r.id) as sum_regs, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.temporaryRegStatus as treg, s.fullRegStatus as freg, s.auditState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u " . $where
                . " GROUP BY s.auditDate, r.image_count, s.temporaryRegStatus, s.auditState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();

        foreach ($result1 as $key => $value) {
            $data[$value['created_on']] = (@$data[$value['created_on']] ? $data[$value['created_on']] : $value);

            $data[$value['created_on']]['sum_total_reg'] = @$data[$value['created_on']]['sum_total_reg'] + $value['sum_regs'];
            $data[$value['created_on']]['sum_total_approved'] = (@$data[$value['created_on']]['sum_total_approved'] ? $data[$value['created_on']]['sum_total_approved'] : 0);
            $data[$value['created_on']]['sum_total_pending'] = (@$data[$value['created_on']]['sum_total_pending'] ? $data[$value['created_on']]['sum_total_pending'] : 0);
            $data[$value['created_on']]['sum_total_declined'] = (@$data[$value['created_on']]['sum_total_declined'] ? $data[$value['created_on']]['sum_total_declined'] : 0);
            $data[$value['created_on']]['sum_total_audited'] = (@$data[$value['created_on']]['sum_total_audited'] ? $data[$value['created_on']]['sum_total_audited'] : 0);
            $data[$value['created_on']]['sum_good'] = (@$data[$value['created_on']]['sum_good'] ? $data[$value['created_on']]['sum_good'] : 0);
            $data[$value['created_on']]['sum_fair'] = (@$data[$value['created_on']]['sum_fair'] ? $data[$value['created_on']]['sum_fair'] : 0);
            $data[$value['created_on']]['sum_bad'] = (@$data[$value['created_on']]['sum_bad'] ? $data[$value['created_on']]['sum_bad'] : 0);
            $data[$value['created_on']]['sum_unrated'] = (@$data[$value['created_on']]['sum_unrated'] ? $data[$value['created_on']]['sum_unrated'] : 0);

            if (in_array($value['auditState'], array(1, 2, 3))) {
                $data[$value['created_on']]['sum_total_audited'] = $data[$value['created_on']]['sum_total_audited'] + $value['sum_regs'];

                if ($value['auditState'] == 3) {
                    $data[$value['created_on']]['sum_good'] = $data[$value['created_on']]['sum_good'] + $value['sum_regs'];
                } elseif ($value['auditState'] == 2) {
                    $data[$value['created_on']]['sum_fair'] = $data[$value['created_on']]['sum_fair'] + $value['sum_regs'];
                } elseif ($value['auditState'] == 1) {
                    $data[$value['created_on']]['sum_bad'] = $data[$value['created_on']]['sum_bad'] + $value['sum_regs'];
                }
            } else {
                $data[$value['created_on']]['sum_unrated'] = $data[$value['created_on']]['sum_unrated'] + $value['sum_regs'];
            }

            if ($value['image_count'] >= 3) {
                $data[$value['created_on']]['sum_total_approved'] = $data[$value['created_on']]['sum_total_approved'] + $value['sum_regs'];
            } else {
                $data[$value['created_on']]['sum_total_pending'] = $data[$value['created_on']]['sum_total_pending'] + $value['sum_regs'];
            }
            if (in_array($value['treg'], array(4, 5, 3))) {
                $data[$value['created_on']]['sum_total_declined'] = $data[$value['created_on']]['sum_total_declined'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_agents_report/{start_date}/{end_date}", name="audit_agents_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_agents_reportAction($start_date, $end_date)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select(" u.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.auditDate, '%Y-%m-%d') as created_on, s.auditState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u "
                . " JOIN u.region d "
                . " JOIN u.territory t " . $where
                . " GROUP BY u.id, s.auditState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $data['active'][$value['sid']] = (@$data['active'][$value['sid']] ? $data['active'][$value['sid']] : $value);


            $data['active'][$value['sid']]['sum_total'] = @$data['active'][$value['sid']]['sum_total'] + $value['sum_regs'];
            $data['active'][$value['sid']]['sum_good'] = (@$data['active'][$value['sid']]['sum_good'] ? $data['active'][$value['sid']]['sum_good'] : 0);
            $data['active'][$value['sid']]['sum_fair'] = (@$data['active'][$value['sid']]['sum_fair'] ? $data['active'][$value['sid']]['sum_fair'] : 0);
            $data['active'][$value['sid']]['sum_bad'] = (@$data['active'][$value['sid']]['sum_bad'] ? $data['active'][$value['sid']]['sum_bad'] : 0);
            $data['active'][$value['sid']]['sum_unrated'] = (@$data['active'][$value['sid']]['sum_unrated'] ? $data['active'][$value['sid']]['sum_unrated'] : 0);
            $data['active'][$value['sid']]['sum_rated'] = (@$data['active'][$value['sid']]['sum_rated'] ? $data['active'][$value['sid']]['sum_rated'] : 0);

            if ($value['auditState'] == 3) {
                $data['active'][$value['sid']]['sum_good'] = $data['active'][$value['sid']]['sum_good'] + $value['sum_regs'];

                $data['active'][$value['sid']]['sum_rated'] = $data['active'][$value['sid']]['sum_rated'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 2) {
                $data['active'][$value['sid']]['sum_fair'] = $data['active'][$value['sid']]['sum_fair'] + $value['sum_regs'];

                $data['active'][$value['sid']]['sum_rated'] = $data['active'][$value['sid']]['sum_rated'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 1) {
                $data['active'][$value['sid']]['sum_bad'] = $data['active'][$value['sid']]['sum_bad'] + $value['sum_regs'];

                $data['active'][$value['sid']]['sum_rated'] = $data['active'][$value['sid']]['sum_rated'] + $value['sum_regs'];
            } else {
                $data['active'][$value['sid']]['sum_unrated'] = $data['active'][$value['sid']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_region_report/{start_date}/{end_date}/{timestamp}", name="audit_region_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_region_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            //$where.= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) { //echo "sdfsdf"; exit;
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, DATE_FORMAT(s.auditDate, '%Y-%m-%d') as created_on, s.auditState as auditState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u "
                . " JOIN u.region d "
                . " JOIN u.territory t " . $where
                . " GROUP BY region_name, territory_name, auditState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['region_name']] = (@$data[$value['region_name']] ? $data[$value['region_name']] : $value);

            //first append total, good faor bad to array as zero
            $data[$value['region_name']]['sum_total'] = @$data[$value['region_name']]['sum_total'] + $value['sum_regs'];
            $data[$value['region_name']]['sum_good'] = (@$data[$value['region_name']]['sum_good'] ? $data[$value['region_name']]['sum_good'] : 0);
            $data[$value['region_name']]['sum_fair'] = (@$data[$value['region_name']]['sum_fair'] ? $data[$value['region_name']]['sum_fair'] : 0);
            $data[$value['region_name']]['sum_bad'] = (@$data[$value['region_name']]['sum_bad'] ? $data[$value['region_name']]['sum_bad'] : 0);
            $data[$value['region_name']]['sum_unrated'] = (@$data[$value['region_name']]['sum_unrated'] ? $data[$value['region_name']]['sum_unrated'] : 0);
            $data[$value['region_name']]['sum_rated'] = (@$data[$value['region_name']]['sum_rated'] ? $data[$value['region_name']]['sum_rated'] : 0);

            //now fill in the data
            if ($value['auditState'] == 3) {
                $data[$value['region_name']]['sum_good'] = $data[$value['region_name']]['sum_good'] + $value['sum_regs'];

                $data[$value['region_name']]['sum_rated'] = $data[$value['region_name']]['sum_rated'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 2) {
                $data[$value['region_name']]['sum_fair'] = $data[$value['region_name']]['sum_fair'] + $value['sum_regs'];

                $data[$value['region_name']]['sum_rated'] = $data[$value['region_name']]['sum_rated'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 1) {
                $data[$value['region_name']]['sum_bad'] = $data[$value['region_name']]['sum_bad'] + $value['sum_regs'];

                $data[$value['region_name']]['sum_rated'] = $data[$value['region_name']]['sum_rated'] + $value['sum_regs'];
            } else {
                $data[$value['region_name']]['sum_unrated'] = $data[$value['region_name']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/audit_auditor_report/{start_date}/{end_date}/{timestamp}", name="audit_auditor_report")
     * @Method("GET")
     * @Template()
     */
    public function audit_auditor_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.auditDate) >= '" . $start_date . "' AND date(s.auditDate) <= '" . $end_date . "' ";
        }

        $where .= " AND s.auditState in (1,2,3)";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("u.id as sid, COUNT(s.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, s.auditState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.auditBy u " . $where
                . " GROUP BY s.auditBy, s.auditState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $data['active'][$value['sid']] = (@$data['active'][$value['sid']] ? $data['active'][$value['sid']] : $value);

            $data['active'][$value['sid']]['sum_total'] = @$data['active'][$value['sid']]['sum_total'] + $value['sum_regs'];
            $data['active'][$value['sid']]['sum_good'] = (@$data['active'][$value['sid']]['sum_good'] ? $data['active'][$value['sid']]['sum_good'] : 0);
            $data['active'][$value['sid']]['sum_fair'] = (@$data['active'][$value['sid']]['sum_fair'] ? $data['active'][$value['sid']]['sum_fair'] : 0);
            $data['active'][$value['sid']]['sum_bad'] = (@$data['active'][$value['sid']]['sum_bad'] ? $data['active'][$value['sid']]['sum_bad'] : 0);
            $data['active'][$value['sid']]['sum_unrated'] = (@$data['active'][$value['sid']]['sum_unrated'] ? $data['active'][$value['sid']]['sum_unrated'] : 0);

            if ($value['auditState'] == 3) {
                $data['active'][$value['sid']]['sum_good'] = $data['active'][$value['sid']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 2) {
                $data['active'][$value['sid']]['sum_fair'] = $data['active'][$value['sid']]['sum_fair'] + $value['sum_regs'];
            } elseif ($value['auditState'] == 1) {
                $data['active'][$value['sid']]['sum_bad'] = $data['active'][$value['sid']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data['active'][$value['sid']]['sum_unrated'] = $data['active'][$value['sid']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/reports", name="reports")
     * @Method("GET")
     * @Template()
     */
    public function reportsAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Generate Reports",
            'title_descr' => "View Reports for",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reports_verifyer", name="reports_verifyer")
     * @Method("GET")
     * @Template()
     */
    public function reports_verifyerAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Generate Reports",
            'title_descr' => "View Reports for",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_reports", name="audit_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_reports.html.twig")
     */
    public function audit_reportsAction()
    {

        if (!in_array(55, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Audit Report",
            'title_descr' => "View Reports for Registration Auditing",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_agent_reports", name="audit_agent_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_agent_reports.html.twig")
     */
    public function audit_agent_reportsAction()
    {

        if (!in_array(55, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Report",
            'title_descr' => "View Reports for Agents Registration Auditing",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_auditor_reports", name="audit_auditor_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_auditor_reports.html.twig")
     */
    public function audit_auditor_reportsAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Auditor's Report",
            'title_descr' => "View Reports for Auditor | Verifier Registration Performance",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_daily_reports", name="audit_daily_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_daily_reports.html.twig")
     */
    public function audit_daily_reportsAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Daily Audit Breakdown Report",
            'title_descr' => "View Agents Daily Audit Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_total_reports", name="audit_total_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_total_reports.html.twig")
     */
    public function audit_total_reportsAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Daily Audit Breakdown Report",
            'title_descr' => "View Agents Daily Audit Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/audit_listview", name="audit_listview")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_listview.html.twig")
     */
    public function audit_listviewAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Audit List View Report & Result",
            'title_descr' => "Audit List View Report & Result",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/__verify_listview", name="__verify_listview")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_listview.html.twig")
     */
    public function __verify_listviewAction()
    {

        $data = array(
            'title' => "Verification List View Report & Result",
            'title_descr' => "Verification List View Report & Result",
            'users' => $this->loadUsers('ROLE_VERIFYER'),
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    private function __loadUsers($userType)
    {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $users = $queryBuilder->select('u')
            ->from('RestApiBundle:User', 'u')
            ->where($queryBuilder->expr()->like('u.roles', ':userType'))//('u.type = :userType ')
            ->setParameter('userType', '%' . $userType . '%')
            ->getQuery()
            ->getArrayResult();

        return $users;
    }

    /**
     * View Reports
     *
     * @Route("/audit_region_reports", name="audit_region_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:audit_region_reports.html.twig")
     */
    public function audit_region_reportsAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Region Report",
            'title_descr' => "View Reports for Regions Registration Auditing",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View rectifyImageCount
     *
     * @Route("/rectifyImageCount", name="rectifyImageCount")
     * @Method("GET")
     * @Template()
     */
    public function rectifyImageCountAction()
    {
        echo "called exit";
        exit;
        $em = $this->getDoctrine()->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $num = 0;
        $limit = 6000;
        $flag = true;
        while ($flag == true) {
            $data2 = $em->createQuery("SELECT count(u.id) as ccount, u.registration "
                . " from RestApiBundle:RegImages u "
                . " WHERE DATE(u.createdOn) >= '2016-09-09' GROUP BY u.registration order by u.id desc")
                ->setMaxResults($limit)
                ->setFirstResult($limit * $num)
                ->getArrayResult();

            if (count($data2) > 1) {
                foreach ($data2 as $key => $value) {

                    $results = $em->createQuery("UPDATE RestApiBundle:Registration u SET u.image_count = '" . $value['ccount'] . "' WHERE u.registrationid = " . $value['registration'] . " ")
                        ->getArrayResult();
                }
            } else {
                $flag = false;
            }
        }

        exit;

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            //'entities' => $entities,
        );
    }

    /**
     * View rectifyImageCount
     *
     * @Route("/updateRecordImage/{times}/{id}/{images}/{image_count}/{ftime}", name="updateRecordImage")
     * @Method("GET")
     * @Template()
     */
    public function updateRecordImageAction($times, $id, $images, $image_count, $ftime)
    {

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            //'entities' => $entities,
        );
        exit;
        $em = $this->getDoctrine()->getManager();


        $results = $em->createQuery("UPDATE RestApiBundle:Registration u SET u.image_count = '" . $image_count . "' WHERE u.registrationid = " . $id . " ")
            ->getArrayResult();

        $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.verifyState = '5', u.auditDescr = '  :: " . date('Y-m-d H:i:s') . " " . $images . "' WHERE u.registrationId = '" . $id . "' ")
            ->getArrayResult();

        $img_arry = explode(",", $images);

        /*foreach($img_arry as $key => $val){
            $query1 = "DELETE from reg_images WHERE imageType = '".$val."' AND registration in (select registrationid from registration where id = ".$id.")";
            $em->getConnection()->exec($query1);
        }*/

        echo json_encode(array('message' => 'true'));
        exit;

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            //'entities' => $entities,
        );
    }


    /**
     * hour_graph Summary
     *
     * @Route("/getRegJourneyData/{start_date}/{end_date}", name="getRegJourneyData")
     * @Method("GET")
     * @Template()
     */
    public function getRegJourneyDataAction($start_date, $end_date)
    {
        $em = $this->getDoctrine()->getManager();

        $query1 = "select avg(timestampdiff(MINUTE,createdDate,allimageDate)) as allimageTime, avg(timestampdiff(MINUTE,allimageDate,verifyDate)) as verifyTime, avg(timestampdiff(minute,verifyDate,tregDate)) as tregTime, hour(createdDate) as theHour from RegistrationStatus where verifyState = 1 and createdDate >= '" . date('Y-m-d', strtotime($start_date)) . " 00:00:00' and createdDate <= '" . date('Y-m-d', strtotime($start_date)) . " 23:59:59' group by theHour;";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $buckets = $statement->fetchAll();


        $journeyArray = array();

        $allimageTime = array();
        $verifyTime = array();
        $tregTime = array();
        $mpesaTime = array();


        foreach ($buckets as $key => $bucket) {
            array_push($allimageTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['allimageTime'] >= 0 ? number_format((float)$bucket['allimageTime'], 2, '.', '') : 0)));
            array_push($verifyTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['verifyTime'] >= 0 ? number_format((float)$bucket['verifyTime'], 2, '.', '') : 0)));
            array_push($tregTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['tregTime'] >= 0 ? number_format((float)$bucket['tregTime'], 2, '.', '') : 0)));

        }

        array_push($journeyArray, array('key' => 'All Images Arrivez', 'values' => $allimageTime));
        array_push($journeyArray, array('key' => 'Verify Time', 'values' => $verifyTime));
        array_push($journeyArray, array('key' => 'Sent Time', 'values' => $tregTime));

        echo json_encode($journeyArray, JSON_NUMERIC_CHECK);
        exit;

        $data = array(
            'title' => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message' => '',
            'histcatexplong' => json_encode($journeyArray, JSON_NUMERIC_CHECK)
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/reg_journey", name="reg_journey")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:reg_journey.html.twig")
     */
    public function reg_journeyAction()
    {

        if (!in_array(54, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        //$query = $this->container->get('fos_elastica.manager')->getRepository('RestApiBundle:RegistrationStatus')->getRegistrationJourney();
        //$result = $this->container->get('fos_elastica.index.vodacom.registration_status')->search($query);


        /*
        select avg(timestampdiff(MINUTE,createdDate,allimageDate)) as allimageTime, avg(timestampdiff(MINUTE,createdDate,verifyDate)) as verifyTime, avg(timestampdiff(minute,createdDate,fregDate)) as frgeTime, avg(timestampdiff(MINUTE,createdDate,mpesaDate)) as mpesaTime, avg(timestampdiff(MINUTE,createdDate,dbmsDate)) as dbmsTime, hour(createdDate) as theHour from RegistrationStatus where verifyState = 1 and createdDate >= '2017-11-16 00:00:00' and createdDate <= '2017-11-16 23:59:59' group by theHour;
        +--------------+------------+----------+-----------+----------+---------+
        | allimageTime | verifyTime | frgeTime | mpesaTime | dbmsTime | theHour |
        +--------------+------------+----------+-----------+----------+---------+
        |      44.1333 |    41.0667 |  44.2667 |   46.2000 |  44.2000 |       0 |
        |       9.0000 |     8.0000 |   9.0000 |   11.0000 |   9.0000 |       1 |
        |       5.0000 |     3.0000 |   5.0000 |    7.0000 |   5.0000 |       4 |
        |       3.0000 |     1.0000 |   3.0000 |    5.0000 |   3.0000 |       5 |
        
        {
            "key" : "Consumer Discretionary" ,
            "values" : [ [ 01 , 27.38478809681] , [ 02 , 27.371377218208] , [ 03 , 26.309915460827] , [ 04 , 26.425199957521] , [ 05 , 26.823411519395] , [ 06 , 23.850443591584] , [ 07 , 23.158355444054] , [ 08 , 22.998689393694]]
        } ,
        {
            "key" : "Consumer Staples" ,
            "values" : [ [ 01 , 7.2800122043237] , [ 02 , 7.1187787503354] , [ 03 , 8.351887016482] , [ 04 , 8.4156698763993] , [ 05 , 8.1673298604231] , [ 06 , 5.5132447126042] , [ 07 , 6.1152537710599] , [ 08 , 6.076765091942]]
        } 
        */


        $query1 = "select avg(timestampdiff(MINUTE,createdDate,allimageDate)) as allimageTime, avg(timestampdiff(MINUTE,allimageDate,verifyDate)) as verifyTime, avg(timestampdiff(minute,verifyDate,tregDate)) as tregTime, hour(createdDate) as theHour from RegistrationStatus where verifyState = 1 and createdDate >= '" . date('Y-m-d') . " 00:00:00' and createdDate <= '" . date('Y-m-d') . " 23:59:59' group by theHour;";


        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $buckets = $statement->fetchAll();


        $journeyArray = array();

        $allimageTime = array();
        $verifyTime = array();
        $tregTime = array();
        $mpesaTime = array();


        foreach ($buckets as $key => $bucket) {
            array_push($allimageTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['allimageTime'] >= 0 ? number_format((float)$bucket['allimageTime'], 2, '.', '') : 0)));
            array_push($verifyTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['verifyTime'] >= 0 ? number_format((float)$bucket['verifyTime'], 2, '.', '') : 0)));

            array_push($tregTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['tregTime'] >= 0 ? number_format((float)$bucket['tregTime'], 2, '.', '') : 0)));

            array_push($mpesaTime, array(number_format((float)$bucket['theHour'], 2, '.', ''), ($bucket['mpesaTime'] >= 0 ? number_format((float)$bucket['mpesaTime'], 2, '.', '') : 0)));
        }

        array_push($journeyArray, array('key' => 'All Images Arrivez', 'values' => $allimageTime));
        array_push($journeyArray, array('key' => 'Verify Time', 'values' => $verifyTime));
        array_push($journeyArray, array('key' => 'Sent Time', 'values' => $tregTime));
        //array_push($journeyArray, array('key' => 'Mpesa Time', 'values' => $mpesaTime));


        $data = array(
            'title' => 'Active & Inactive',
            'title_descr' => 'View Reports for Active Inactive',
            'message' => '',
            'datepick' => date('d/m/Y'), //10/24/1984
            'histcatexplong' => json_encode($journeyArray, JSON_NUMERIC_CHECK)
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/active_inactive", name="active_inactive")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:active_inactive.html.twig")
     */
    public function active_inactiveAction()
    {

        if (!in_array(53, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/smsreg_type", name="smsreg_type")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:smsreg_type.html.twig")
     */
    public function smsreg_typeAction()
    {

        if (!in_array(53, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Registration Type [ SMS / Web ]",
            'title_descr' => "View Reports for Registration Type",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/session_page", name="session_page")
     * @Method("GET")
     */
    public function sessionPageAction()
    {
        $data['active'] = 1;
        echo json_encode($data);
        exit;
        return array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/editUserForm", name="editUserForm")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function editUserFormAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
            ->select("u.id "
                . " from RestApiBundle:User u WHERE u.id = '" . $request->request->get('userid') . "' ")
            ->setMaxResults(1)
            ->getQuery();

        $data2 = $query->getOneOrNullResult();

        if (count($data2) > 0) {

            $ddata = array(
                "firstName" => $request->request->get('firstName'),
                "middleName" => $request->request->get('middleName'),
                "lastName" => $request->request->get('lastName'),
                "username" => $request->request->get('username'),
                "msisdn" => $request->request->get('msisdn'),
                "email" => $request->request->get('email'),
                "adminRole" => $request->request->get('adminRole'),
                "adminDepartment" => $request->request->get('adminDepartment'),
                "dob" => $request->request->get('dob'),
                "user_roles" => $request->request->get('user_roles'),
                "userid" => $request->request->get('userid')
            );

//            echo "<pre>";
//            print_r($ddata);
//            echo "</pre>";

            $user = $em->getRepository('RestApiBundle:User')->findOneBy(array("id" => $request->request->get('userid')));
            if ($user) {


                $user->setFirstName($request->request->get('firstName'));
                $user->setMiddleName($request->request->get('middleName'));
                $user->setLastName($request->request->get('lastName'));
                $user->SetUsername($request->request->get('username'));
                $user->setMobileNumber($request->request->get('msisdn'));
                $user->setEmail($request->request->get('email'));
                $user->setAdminRole($request->request->get('adminRole'));
                $user->setAdminDepartment($request->request->get('adminDepartment'));
                //$user->setDob($request->request->get('dob'));
                $user->setRoles($request->request->get('user_roles'));
                $em->flush();
            }

            return $this->redirect($this->generateUrl('admin'));
        }

        exit;
    }


    /**
     * Creates a new Faq entity.
     *
     * @Route("/newreportspost", name="newreportspost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function newreportspostAction(Request $request)
    {

        global $post;
        $post = $request;

        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $where_clause = " 1=1";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            if ($post->request->get('report_type') == "agent_registration") {
                $where_clause .= " AND u.parent_id = " . $user->getId();
            } else {
                $where_clause .= " AND u.parent_id = " . $user->getId();
            }
        }

        if (in_array("ROLE_VODASHOP", $roles)) {
            $where_clause .= " AND u.vodashop_id = " . $user->getId();
        }

        if ($post->request->get('region')) {
            $where_clause .= " AND p.region = '" . $post->request->get('region') . "'";
        }
        if ($post->request->get('territory')) {
            $where_clause .= " AND p.territory = " . $post->request->get('territory');
        }
        if ($post->request->get('from_date')) {
            if (in_array($post->request->get('report_type'), ['all_agents_download', 'device_report'])) {
                $where_clause .= " AND date(p.createdAt) <= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
            } else {
                $where_clause .= " AND date(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
            }
            //$where_clause .= " AND date(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";

        }
        if ($post->request->get('to_date')) {
            if (in_array($post->request->get('report_type'), ['all_agents_download', 'device_report'])) {
                $where_clause .= " AND date(p.createdAt) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
            } else {
                $where_clause .= " AND date(p.createdDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
            }
            //$where_clause .= " AND date(p.createdDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";

        }
        if ($post->request->get('customer_msisdn')) {//----customer msisdn
            //$where_clause.= " AND p.msisdn = " . $post->request->get('customer_msisdn');
        }
        if ($post->request->get('agent_msisdn')) {//----customer msisdn
            $where_clause .= " AND u.username = '" . $post->request->get('agent_msisdn') . "'";
        }

        $handle = fopen('php://output', 'w+');

        $regionsAll = $this->getRegionsArrayCodeName();
        $teritoriesAll = $this->getTerritoryArrayIdName();

        //echo $post->request->get('report_type').":"; exit;
        switch ($post->request->get('report_type')) {
            case "agent_registration":

                $columnnames = array('CREATEDDATE', 'MSISDN', 'ALTERNATENUMBER', 'CUSTOMERNAME', 'IDNUMBER', 'IDTYPE','TYPE', 'AGENTMOBILE', 'AGENTNAME', 'REGIONNAME', 'TERRITORYNAME', 'DEVICEMODEL','APPVERSION', 'FULLREGSTATUS', 'FULLREGDESCR', 'ADDITIONALINFO', 'IMAGECOUNT');

                $extraQUERY = "";
                if ($post->request->get('verify_info')) {
                    if ($post->request->get('exclude_tigosys')) {
                        $where_clause .= " AND r3_.verifyBy != '3260'";
                    }
                    $columnnames[] = 'VerifiedState';
                    $columnnames[] = 'VerifiedBy';
                    $columnnames[] = 'VerifiedDate';
                    $columnnames[] = 'VerifiedDescr';
                    $columnnames[] = 'TimeToVerify';
                    //$extraQUERY .= ", r3_.verifyState, CONCAT(u3_.first_name, ' ', u3_.last_name) as verifyBy, DATE_FORMAT(r3_.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate, REPLACE(IFNULL(r3_.verifyDescr, ' '), ',', ' AND ') AS VERIFYDESCRIPTION, r3_.verifyLock ";
                    $extraQUERY .= ", CASE r3_.verifyState WHEN '-2' THEN 'Fair' WHEN '-3' THEN 'Bad'  WHEN '1' THEN 'Good' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Bad' WHEN '0' THEN 'NotVerified' ELSE 'NotVerified' END AS verifyState, CONCAT(u3_.first_name, ' ', u3_.last_name) as verifyBy, DATE_FORMAT(r3_.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate, REPLACE(IFNULL(r3_.verifyDescr, ' '), ',', ' ') AS VERIFYDESCRIPTION, r3_.verifyLock ";
                }
                if ($post->request->get('audit_info')) {
                    $columnnames[] = 'AuditState';
                    $columnnames[] = 'AuditedBy';
                    $columnnames[] = 'AuditedDate';
                    $columnnames[] = 'AuditDescr';
                    $extraQUERY .= ", r3_.auditState, CONCAT(u4_.first_name, ' ', u4_.last_name) as auditBy, date(r3_.auditDate) as auditDate, REPLACE(IFNULL(r3_.auditDescr, ' '), ',', ' AND ') AS AUDITDESCRIPTION ";

                }

                $query = "SELECT DATE(p.createdDate) AS CREATEDDATE, p.msisdn AS MSISDN, p.contact AS ALTERNATENUMBER, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, IFNULL(p.identification, ' ') AS IDNUMBER, a1_.name AS IDTYPE, CASE p.registration_type WHEN '2' THEN 'Biometric' ELSE 'Standard'  END AS TYPE, u.username AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL,IFNULL(p.appVersion, ' ') AS APPVERSION, CASE r3_.fullRegStatus WHEN '0' THEN 'pending' WHEN '1' THEN 'pending' WHEN '6' THEN 'success'  ELSE 'declined' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, p.image_count AS IMAGECOUNT " . $extraQUERY . " FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN super_agent s4_ ON u.id = s4_.id LEFT JOIN agent a5_ ON u.id = a5_.id LEFT JOIN user u2_ ON u.parent_id = u2_.id LEFT JOIN user u3_ ON r3_.verifyBy = u3_.id LEFT JOIN user u4_ ON r3_.auditBy = u4_.id LEFT JOIN super_agent s6_ ON u2_.id = s6_.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN agent a7_ ON u2_.id = a7_.id LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory WHERE " . $where_clause;


                break;
            case "id_documents_eport":

                $columnnames = array('ID Type', 'Number of Usage');
                // Add the header of the CSV file
                /*fputcsv($handle, array('ID Type', 'Number of Usage'), ',');
                $query = $em->createQueryBuilder()
                        ->select("p.identificationType, COUNT(p)")
                        ->from("RestApiBundle:Registration", "p")
                        ->leftJoin('p.owner', 'u')
                        ->groupBy('p.identificationType')
                        ->where($where_clause)
                        ->getQuery();*/

                $query = "SELECT Idtype.name, COUNT(p.id) AS sclr_1 FROM registration p LEFT JOIN user u1_ ON p.owner_id = u1_.id LEFT JOIN agent a2_ ON u1_.id = a2_.id LEFT JOIN super_agent s3_ ON u1_.id = s3_.id LEFT JOIN Idtype on Idtype.id = p.identificationType WHERE " . $where_clause . " GROUP BY p.identificationType;";

                break;
            case "registration_region_report":

// Add the header of the CSV file
                fputcsv($handle, array('Region Name', 'Territory Name', 'Registrations Done'), ',');

                $columnnames = array('Registrations Done', 'Region Name', 'Territory Name');

                /*$query = $em->createQueryBuilder()
                        ->select("p.region, p.territory, COUNT(p)")
                        ->from("RestApiBundle:Registration", "p")
                        
                        ->groupBy('p.region, p.territory')
                        ->where($where_clause)
                        ->getQuery();*/


                $query = "SELECT COUNT(p.id) AS sclr_2, Region.name AS region_0, Territory.name AS territory_1 FROM registration p  LEFT JOIN Territory on Territory.id = p.territory LEFT JOIN Region on Territory.region_id = Region.id WHERE " . $where_clause . " GROUP BY region_0, territory_1";

                break;
            case "device_report":

                //select device.msisdn as deviceMSISDN, device.imei as IMEI, device.isActive, date(device.createdOn) as deviceCreatedOn, device.appVersion as AppVersion, auser.username as AgentMobile, concat(auser.first_name, ' ', auser.last_name) as AgentName, Region.name as RegionName, Territory.name as TerritoryName, date(auser.created_at) as AgentCreatedOn, concat(xuser.first_name, ' ', xuser.last_name) as SuperAgentName from device left join user auser on device.user_id = auser.id left join Region on auser.region_id = Region.id left join Territory on auser.territory_id = Territory.id left join user xuser on auser.parent_id  = xuser.id

                $columnnames = array('deviceMSISDN', 'IMEI', 'isActive', 'deviceCreatedOn', 'AppVersion', 'AgentMobile', 'AgentName', 'RegionName', 'TerritoryName', 'AgentCreatedOn');

                fputcsv($handle, $columnnames, ',');
                $query = $em->createQueryBuilder()
                    ->select("d.msisdn as deviceMSISDN, d.imei as IMEI, d.isActive, date(d.createdOn) as deviceCreatedOn, d.appVersion as AppVersion, p.username as AgentMobile, concat(p.firstName, ' ', p.lastName) as AgentName, r.name as RegionName, t.name as TerritoryName, date(p.createdAt) as AgentCreatedOn ")
                    ->from("RestApiBundle:Device", "d")
                    ->leftJoin('d.user', 'p')
                    ->leftJoin('p.region', 'r')
                    ->leftJoin('p.territory', 't')
//                        ->leftJoin('p.parent_id', 'x')
                    ->where($where_clause)
                    ->getQuery();

                break;
            case "all_agents_download":

                $columnnames = array('Created At', 'Agent Name', 'Phone', 'Email', 'Region', 'Territory');
                $query = $em->createQueryBuilder()
                    ->select("DATE_FORMAT(p.createdAt, '%Y-%m-%d %H:%i:%s') as createdDate, CONCAT(p.firstName, ' ', p.lastName) as agentName, p.mobileNumber as phone, p.email, r.name as regionName, t.name as territoryName ")
                    ->from("RestApiBundle:User", "p")
                    ->leftJoin('p.region', 'r')
                    ->leftJoin('p.territory', 't')
                    ->where($where_clause)
                    ->getQuery();

                break;
            case "registrations_gender_report":

// Add the header of the CSV file
                fputcsv($handle, array('Gender', 'Registrations Done'), ',');

                $columnnames = array('Gender', 'Registrations Done');
                $query = $em->createQueryBuilder()
                    ->select("p.gender, COUNT(p)")
                    ->from("RestApiBundle:Registration", "p")
                    ->leftJoin('p.owner', 'u')
                    ->groupBy('p.gender')
                    ->where($where_clause)
                    ->getQuery();
                break;
            case "active_inactive_agents":
//code to be executed if n = label3;
                fputcsv($handle, array('Gender', 'Registrations Done'), ',');
                $query = $this->get_active_inactive($where_clause);
                break;


            case 'registration_journey_report':

                $columnnames = array('CREATEDDATE', 'MSISDN', 'CUSTOMERNAME', 'IDNUMBER', 'IDTYPE', 'TYPE' ,'AGENTMOBILE', 'AGENTNAME', 'REGIONNAME', 'TERRITORYNAME', 'DEVICEMODEL', 'REGTYPE', 'VERIFYSTATE', 'VERIFYDATE', 'TEMPREGSTATUS', 'TEMPREGDESCRIPTION', 'FULLREGSTATUS', 'FULLREGDESCR', 'IMAGECOUNT', 'registrationTime', 'Device Sent Time', 'TREGDATE', 'AllImagesReceived', 'FREGDATE');

                fputcsv($handle, $columnnames, ',');

                $query = "SELECT p.createdDate AS CREATEDDATE, p.msisdn AS MSISDN, CONCAT(p.firstName, ' ', p.lastName) AS CUSTOMERNAME, IFNULL(p.identification, ' ') AS IDNUMBER, IFNULL(a1_.name, ' ') AS IDTYPE,  CASE p.registration_type WHEN '2' THEN 'Biometric' ELSE 'Standard'  END AS TYPE, IFNULL(u.username, ' ') AS AGENTMOBILE, CONCAT(u.first_name, ' ', u.last_name) AS AGENTNAME, IFNULL(r5_.name, ' ') as REGIONNAME, IFNULL(t3_.name, ' ') AS TERRITORYNAME, IFNULL(p.deviceModel, ' ') AS DEVICEMODEL, CASE r3_.reg_type WHEN '0' THEN 'NewReg' WHEN '1' THEN 'NewReg' WHEN '2' THEN 'Re-Reg' ELSE 'New' END AS REGTYPE, CASE r3_.verifyState WHEN '1' THEN 'Good' WHEN '2' THEN 'Fair' WHEN '3' THEN 'Bad' ELSE 'Not Verified' END AS verifyState, DATE_FORMAT(r3_.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate, CASE r3_.temporaryRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS TEMPREGSTATUS, REPLACE(IFNULL(r3_.temporaryRegDesc, ' '), ',', ' ') AS TEMPREGDESCRIPTION, CASE r3_.fullRegStatus WHEN '0' THEN 'Pending' WHEN '1' THEN 'Pending' WHEN '2' THEN 'Success' WHEN '3' THEN 'Declined' WHEN '4' THEN 'Fail' WHEN '5' THEN 'Retry' WHEN '6' THEN 'Success' WHEN '7' THEN 'Success' ELSE 'Pending' END AS FULLREGSTATUS, REPLACE(IFNULL(r3_.fullRegDesc, ' '), ',', ' ') AS FULLREGDESCR, p.image_count AS IMAGECOUNT, p.registrationTime, p.deviceSentTime, r3_.tregDate AS TREGDATE, r3_.allimageDate AS AllImagesReceived, r3_.fregDate AS FREGDATE FROM RegistrationStatus r3_ LEFT JOIN registration p ON r3_.registrationId = p.id LEFT JOIN user u ON p.owner_id = u.id LEFT JOIN Region r5_ on r5_.code = p.region LEFT JOIN Idtype a1_ on a1_.id = p.identificationType LEFT JOIN Territory t3_ on t3_.id = p.territory WHERE " . $where_clause;

                break;

            default:
//code to be executed if n is different from all labels;
                $results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
                    ->getArrayResult();
        }

        try {
            if (in_array($post->request->get('report_type'), array('agent_registration', 'id_documents_eport', 'registration_region_report', 'registration_journey_report'))) {
                $results = $query;
            } else {
                $results = $query->getSql();
            }

            echo " 11 ";
            $kernel = $this->get('kernel');
            echo " 22 ";


            $application = new Application($kernel);
            echo " 33 ";
            $application->setAutoExit(false);
            echo " 44 ";
            $input = new ArrayInput(array('command' => 'dashboard:report_generator_command', 'headings' => implode(",", $columnnames), 'query' => $results, 'email' => $post->request->get('emailAddress'), 'title' => 'ReportGeneration-' . $post->request->get('report_type')));
            echo " 55 ";
            $application->run($input, null);
            echo " 66 ";
        } catch (Exception $e) {
            echo $e;
            return null;
        }

        if ($post->request->get('verifyer') == 1) {
            return $this->redirect($this->generateUrl('verifyer'));
        } else {
            return $this->redirect($this->generateUrl('reports'));
        }
    }


    /**
     * Creates a new Faq entity.
     *
     * @Route("/reportspost", name="reportspost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function reportspostAction(Request $request)
    {

        echo "<pre>";
        echo $request->request->get('report_type');
        echo "</pre>";
        exit;

        global $post;
        $post = $request;

        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $where_clause = " 1=1";

        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where_clause .= " AND u.parent_id = " . $user->getId();
        }

        if (in_array("ROLE_VODASHOP", $roles)) {
            $where_clause .= " AND u.vodashop_id = " . $user->getId();
        }

        if ($post->request->get('region')) {
            $where_clause .= " AND p.region = '" . $post->request->get('region') . "'";
        }
        if ($post->request->get('territory')) {
            $where_clause .= " AND p.territory = " . $post->request->get('territory');
        }
        if ($post->request->get('from_date')) {
            $where_clause .= " AND date(p.createdDate) >= '" . date('Y-m-d', strtotime($post->request->get('from_date'))) . "'";
        }
        if ($post->request->get('to_date')) {
            $where_clause .= " AND date(p.createdDate) <= '" . date('Y-m-d', strtotime($post->request->get('to_date'))) . "'";
        }
        if ($post->request->get('customer_msisdn')) {//----customer msisdn
            //$where_clause.= " AND p.msisdn = " . $post->request->get('customer_msisdn');
        }
        if ($post->request->get('agent_msisdn')) {//----customer msisdn
            $where_clause .= " AND u.username = '" . $post->request->get('agent_msisdn') . "'";
        }

        $handle = fopen('php://output', 'w+');

        $regionsAll = $this->getRegionsArrayCodeName();
        $teritoriesAll = $this->getTerritoryArrayIdName();

        //echo $post->request->get('report_type').":"; exit;
        switch ($post->request->get('report_type')) {
            case "agent_registration":

                $columnnames = array('Customer Number', 'Customer Name', 'ID Number', 'ID Type', 'Registrar MSISDN', 'Registrar Name', 'Regional Manager', 'Region Name', 'Territory Name', 'Created Date', 'AgentPhone', 'Registration State', 'Registration Description', 'Freg Registration Description');
                $additional_fieldsname = $additional_columname = "";
                if ($post->request->get('verify_info')) {
                    if ($post->request->get('exclude_tigosys')) {
                        $where_clause .= " AND x.verifyBy != '3260'";
                    }
                    $columnnames[] = 'Verified State';
                    $columnnames[] = 'Verified By';
                    $columnnames[] = 'Verified Date';
                    $columnnames[] = 'Verified Descr';
                    $columnnames[] = 'TimeToVerify';
                    $additional_columname .= ", x.verifyState, CONCAT(y.firstName, ' ', y.lastName) as verifyBy, DATE_FORMAT(x.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate, x.verifyDescr, x.verifyLock ";
                }
                if ($post->request->get('audit_info')) {
                    $columnnames[] = 'Audit State';
                    $columnnames[] = 'Audited By';
                    $columnnames[] = 'Audited Date';
                    $columnnames[] = 'Audit Descr';
                    $additional_columname .= ", x.auditState, CONCAT(z.firstName, ' ', z.lastName) as auditBy, date(x.auditDate) as auditDate, x.auditDescr";
                }

                fputcsv($handle, $columnnames, ',');
                // Query data from database 
                if ($post->request->get('verify_info') || $post->request->get('audit_info')) {
                    $query = $em->createQueryBuilder()
                        ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, DATE_FORMAT(p.createdDate, '%Y-%m-%d %H:%i%:%s') as createdDate, p.deviceModel as AgentPhone, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr,  x.fullRegDesc as fregDescr " . $additional_columname)
                        ->from("RestApiBundle:RegistrationStatus", "x")
                        ->leftJoin('x.registrationId', 'p')
                        ->leftJoin('x.verifyBy', 'y')
                        ->leftJoin('x.auditBy', 'z')
                        ->leftJoin('p.owner', 'u')
                        ->leftJoin('u.parent_id', 's')
                        ->where($where_clause)
                        ->getQuery();
                } else {
                    $query = $em->createQueryBuilder()
                        ->select("p.msisdn, CONCAT(p.firstName, ' ', p.lastName) as customer_name, p.identification, p.identificationType, u.username, CONCAT(u.firstName, ' ', u.lastName) as registrar_name,  CONCAT(s.firstName, ' ', s.lastName) as superAgent_name, p.region, p.territory, date(p.createdDate), p.deviceModel as AgentPhone, x.temporaryRegStatus as tregStatus, x.temporaryRegDesc as tregDescr,  x.fullRegDesc as fregDescr ")
                        ->from("RestApiBundle:RegistrationStatus", "x")
                        ->leftJoin('x.registrationId', 'p')
                        ->leftJoin('p.owner', 'u')
                        ->leftJoin('u.parent_id', 's')
                        ->where($where_clause)
                        ->getQuery();
                }

                break;
            case "id_documents_eport":

// Add the header of the CSV file
                fputcsv($handle, array('ID Type', 'Number of Usage'), ',');
                $query = $em->createQueryBuilder()
                    ->select("p.identificationType, COUNT(p)")
                    ->from("RestApiBundle:Registration", "p")
                    ->leftJoin('p.owner', 'u')
                    ->groupBy('p.identificationType')
                    ->where($where_clause)
                    ->getQuery();

                break;
            case "registration_region_report":

// Add the header of the CSV file
                fputcsv($handle, array('Region Name', 'Territory Name', 'Registrations Done'), ',');
                $query = $em->createQueryBuilder()
                    ->select("p.region, p.territory, COUNT(p)")
                    ->from("RestApiBundle:Registration", "p")
                    ->groupBy('p.region')
                    ->groupBy('p.territory')
                    ->where($where_clause)
                    ->getQuery();

                break;
            case "device_report":

                //select device.msisdn as deviceMSISDN, device.imei as IMEI, device.isActive, date(device.createdOn) as deviceCreatedOn, device.appVersion as AppVersion, auser.username as AgentMobile, concat(auser.first_name, ' ', auser.last_name) as AgentName, Region.name as RegionName, Territory.name as TerritoryName, date(auser.created_at) as AgentCreatedOn, concat(xuser.first_name, ' ', xuser.last_name) as SuperAgentName from device left join user auser on device.user_id = auser.id left join Region on auser.region_id = Region.id left join Territory on auser.territory_id = Territory.id left join user xuser on auser.parent_id  = xuser.id

                $columnnames = array('deviceMSISDN', 'IMEI', 'isActive', 'deviceCreatedOn', 'AppVersion', 'AgentMobile', 'AgentName', 'RegionName', 'TerritoryName', 'AgentCreatedOn');

                fputcsv($handle, $columnnames, ',');
                $query = $em->createQueryBuilder()
                    ->select("d.msisdn as deviceMSISDN, d.imei as IMEI, d.isActive, date(d.createdOn) as deviceCreatedOn, d.appVersion as AppVersion, a.username as AgentMobile, concat(a.firstName, ' ', a.lastName) as AgentName, r.name as RegionName, t.name as TerritoryName, date(a.createdAt) as AgentCreatedOn ")
                    ->from("RestApiBundle:Device", "d")
                    ->leftJoin('d.user', 'a')
                    ->leftJoin('a.region', 'r')
                    ->leftJoin('a.territory', 't')
                    ->leftJoin('a.parent_id', 'x')
                    ->where($where_clause)
                    ->getQuery();


                break;
            case "all_agents_download":

                fputcsv($handle, array('Created At', 'Agent Name', 'Phone', 'Email', 'Region', 'Territory'), ',');
                $query = $em->createQueryBuilder()
                    ->select("DATE_FORMAT(p.createdAt, '%H:%i:%s') as createdDate, CONCAT(p.firstName, ' ', p.lastName) as agentName, p.mobileNumber as phone, p.email, r.name as regionName, t.name as territoryName ")
                    ->from("RestApiBundle:User", "p")
                    ->leftJoin('p.region', 'r')
                    ->leftJoin('p.territory', 't')
                    ->where($where_clause)
                    ->getQuery();

                break;
            case "registrations_gender_report":

// Add the header of the CSV file
                fputcsv($handle, array('Gender', 'Registrations Done'), ',');
                $query = $em->createQueryBuilder()
                    ->select("p.gender, COUNT(p)")
                    ->from("RestApiBundle:Registration", "p")
                    ->leftJoin('p.owner', 'u')
                    ->groupBy('p.gender')
                    ->where($where_clause)
                    ->getQuery();
                break;
            case "active_inactive_agents":
//code to be executed if n = label3;
                fputcsv($handle, array('Gender', 'Registrations Done'), ',');
                $query = $this->get_active_inactive($where_clause);
                break;
            default:
//code to be executed if n is different from all labels;
                $results = $em->createQuery("SELECT p.firstName, p.lastName, p.region, p.territory from RestApiBundle:Registration p ")
                    ->getArrayResult();
        }

        try {
            $results = $query->getSql();

            $kernel = $this->get('kernel');
            $application = new Application($kernel);
            $application->setAutoExit(true);
            $input = new ArrayInput(array('command' => 'dashboard:report_generator_command', 'query' => $results, 'email' => 'Report Generation '));
            $application->run($input, null);
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $this->redirect($this->generateUrl('reports'));
    }

    function get_active_inactive($where_clause)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        // Add the header of the CSV file
        $query = $em->createQueryBuilder()
            ->select("p.gender, COUNT(p)")
            ->from("RestApiBundle:Registration", "p")
            ->leftJoin('p.owner', 'u')
            ->groupBy('p.gender')
            ->where($where_clause)
            ->getQuery();
        return $query;
    }

    private function getGkycRegStatusDesc($tstatus, $fstatus)
    {
        $statusLabel = "Not Sent";

        if ($fstatus == ImagesOperatorStatus::SUCCESS) {
            $statusLabel = 'Completed';
        } elseif ($tstatus == PlainTextOperatorStatus::SUCCESS & !in_array($fstatus, [ImagesOperatorStatus::SUCCESS])) {
            if ($tstatus == PlainTextOperatorStatus::SUCCESS) {
                $statusLabel = ('Partial');
            } else {
                $statusLabel = ('Submitted');
            }
        } elseif ($tstatus == PlainTextOperatorStatus::SUBMITTED) {
            $statusLabel = 'Submitted';
        } elseif ($tstatus == PlainTextOperatorStatus::DECLINED) {
            $statusLabel = 'Declined';
        } elseif ($tstatus == PlainTextOperatorStatus::FAILED) {
            $statusLabel = 'Failed';
        }

        return $statusLabel;
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/registrations_filter", name="registrations_filter")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:registrations.html.twig")
     */
    public function registrations_filterAction(Request $request)
    {

        $post = $request;
        $post->request->get('customer_msisdn');


        $results = "empty";
        $searchResult = new SearchResult();

        $dao = new SearchDao($this->container);
        $results = $dao->searchAgent($post->request->get('first_name'));

//        $cnt = 1;
//        foreach ($results as $key => $value) {
//            echo "<p style='color:#fff;'>" . $cnt . " | " . $value->getFirstName() . " | " . $value->getlastName() . "</p>";
//            $cnt++;
//        }

        $em = $this->getDoctrine()->getManager();
        $entities = $em->getRepository('RestApiBundle:Registration')->findAll();

        $data = array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report | All registrations",
            'registrations' => $results,
            'message' => json_encode($request->getContent()) //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/registration_info_edit/{id}/{timestamp}", name="registration_info_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info_edit.html.twig")
     */
    public function registrationInfoEditAction($id, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        $reg_status['fullRegStatus'] = $result['fullRegDesc'] = "";
        $result = $em->createQuery("SELECT  p from RestApiBundle:RegistrationStatus p "
            . "WHERE p.registrationId = '" . $entity->getId() . "' ")
            ->getArrayResult();

        if (count($result) == 1) {
            $reg_status = $result[0];
            $reg_status['fullRegStatus'] = $this->getRegStatusDesc($reg_status['fullRegStatus']);
            $reg_status['tregDate'] = (@$reg_status['tregDate'] ? date("F j, Y, g:i a", strtotime($reg_status['tregDate']->format('Y-m-d H:i:s'))) : "Invalid TIme");
            $reg_status['allimageDate'] = (@$reg_status['allimageDate'] ? date("F j, Y, g:i a", strtotime($reg_status['allimageDate']->format('Y-m-d H:i:s'))) : "Invalid TIme");
        }

        $id_types = array();
        $idtypes_list = $em->getRepository('DashboardBundle:Idtype')->findAll();
        foreach ($idtypes_list as $key => $value) {
            $id_types[$value->getName()] = $value->getName();
        }


        $data = array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'id' => $id,
            'datetime' => strtotime(date('Y-m-d H:i:s')),
            'entity' => $entity,
            'region' => $this->get_regionName_fromKey($entity->getRegion()),
            'reg_status' => $reg_status,
            'gender' => $this->getGender($entity->getGender()),
            'idtypes' => json_encode($id_types),
            'idtype' => $this->getIDType($entity->getIdentificationType())
        );

        return $this->prepareResponse($data);
    }


    /**
     * View Reports
     *
     * @Route("/registration_info/{id}", name="registration_info")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info.html.twig")
     */
    public function registrationInfoAction($id)
    {
        $em = $this->getDoctrine()->getManager();


        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        $reg_status['fullRegStatus'] = $result['fullRegDesc'] = "";
        $result = $em->createQuery("SELECT  p from RestApiBundle:RegistrationStatus p "
            . "WHERE p.registrationId = '" . $entity->getId() . "' ")
            ->getArrayResult();


        if (count($result) == 1) {
            $reg_status = $result[0];


            $verify_state = $reg_status['verifyState'];
            $verify_description = $reg_status['verifyDescr'];
            $verify_date = $reg_status['verifyDate'];


            // var_dump($reg_status['verifyDescr']);exit;
            // exit(var_dump($verify_description));

            $reg_status['aimStatus'] = $this->getAimRegStatusDesc($reg_status['temporaryRegStatus'], $reg_status['fullRegStatus'], $entity->getImage_count());

            $reg_status['icapStatus'] = $this->getIcapRegStatusDesc($reg_status['temporaryRegStatus'], $reg_status['fullRegStatus']);

            $reg_status['temporaryRegStatus'] = $this->getRegStatusDesc($reg_status['temporaryRegStatus']);
            $reg_status['fullRegStatus'] = $this->getRegStatusDesc($reg_status['fullRegStatus']);
        }


        $registrationStatus = $em
            ->getRepository('RestApiBundle:RegistrationStatus')
            ->findOneBy(['registrationId' => $id]);

        if (is_null($registrationStatus)) {
            throw new \InvalidArgumentException("Unable to find Registration entity with an ID of {$id}.");
        }

        $registrationStatus->ekycStatus = $this->geEkycRegStatusDesc($registrationStatus->getTemporaryRegStatus(), $registrationStatus->getFullRegStatus(), $registrationStatus->getRegistrationId()->getImageCount());

        if ($registrationStatus->getTemporaryRegStatus() == PlainTextOperatorStatus::FAILED) {

            $reasons = \GuzzleHttp\json_decode($registrationStatus->getTemporaryRegDesc());
            $registrationStatus->ekycDescription = implode(', ', $reasons);
        } else {
            $registrationStatus->ekycDescription = EkycStatus::getStatusMessages($registrationStatus->ekycStatus);
        }

        $registrationStatus->gkycStatus = $this->getGkycRegStatusDesc($registrationStatus->getTemporaryRegStatus(), $registrationStatus->getFullRegStatus());

        //Overwrite status for display
        $registrationStatus->setTemporaryRegStatus($this->getRegStatusDesc($registrationStatus->getTemporaryRegStatus()));
        $registrationStatus->setFullRegStatus($this->getRegStatusDesc($registrationStatus->getFullRegStatus()));


//        echo "<pre>"; print_r($reg_status); echo "</pre>"; 
//        echo ($reg_status['allimageDate'] ? date('F j,Y  H:i:s', strtotime($reg_status['allimageDate']->format('Y-m-d H:i:s'))) : "Not yet arrived" );
//        exit;

        $data = array(
            'title' => "View Registrations Report",
            'pageSubTitle' => 'Customer Registration Information',
            'territory' => @$territory,
            'registrationStatus' => $registrationStatus,

            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'entity' => $entity,	
            'region' => $this->get_regionName_fromKey($entity->getRegion()),
            'reg_status' => $reg_status,
            'allImageDate' => ($reg_status['allimageDate'] ? date('F j,Y  H:i:s', strtotime($reg_status['allimageDate']->format('Y-m-d H:i:s'))) : "No All Images"),
            'tregDate' => ($reg_status['tregDate'] ? date('F j,Y  H:i:s', strtotime($reg_status['tregDate']->format('Y-m-d H:i:s'))) : "Not yet TREGd"),
            'fregDate' => ($reg_status['fregDate'] ? date('F j,Y  H:i:s', strtotime($reg_status['fregDate']->format('Y-m-d H:i:s'))) : "Not Yet FREGd"),
            'gender' => $this->getGender($entity->getGender()),
            'idtype' => $this->getIDType($entity->getIdentificationType()),
            'verification_time' => ($verify_date == NULL) ? "NOT SET" : $verify_date->format('Y-m-d H:i:s'),
            'verification_state' => $this->getVerifyStates($verify_state),
            'verification_description' => $verify_description,
            'verify_date' => ($reg_status['verifyDate'] ? date('F j,Y  H:i:s', strtotime($reg_status['verifyDate']->format('Y-m-d H:i:s'))) : "Not Yet Verified"),
            'registrationStatus' => $registrationStatus
        );

        return $this->prepareResponse($data);
    }


    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agent_edit/{id}", name="agent_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $query = $em->createQuery("SELECT d FROM RestApiBundle:Device d WHERE d.user = 1")//'".$entity->getId()."'")
        ->getResult();


        $editForm = $this->createEditForm($entity);

        $data = array(
            'title' => "Create New User",
            'title_descr' => "Create New Agent",
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'devices' => $query,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('agent_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Creates a form to delete a User entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('agent_deactivate', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Deactivate'))
            ->getForm();
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="agent_deactivate")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $results = $em->createQuery("UPDATE RestApiBundle:User u SET u.enabled = 0 WHERE u.id = " . $id . " ")
                ->getArrayResult();

//$em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin/agents'));
    }

    /**
     * Edits an existing Faq entity.
     *
     * @Route("/agent_edit/{id}", name="agent_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Admin:agent_edit.html.twig")
     * @param Request $request
     * @param $id
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('agent_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * View Reports
     *
     * @Route("/registration_images/{id}", name="registration_images")
     * @Method("GET")
     *
     */
    public function registrationImagesAction($id)
    {
        $resp = new JsonObject();
        $status = false;
        $message = "";
        try {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:RegImages')->findBy(array("registration" => $id));
            if ($entity) {
                $resp->setItem($entity);
                $status = true;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);


//        $entity = $em->getRepository('RestApiBundle:RegImages')->findBy(
//            array("registration"=>$id)
//        );
//        if (!$entity) {
//            throw $this->createNotFoundException('Unable to find Registration entity.');
//        }
//        var_dump($entity);
    }

    private function getRegionsArrayCodeName()
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Region p ")
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $regionArray[$val['code']] = $val['name'];
        }

        return $regionArray;
    }

    private function getTerritoryArrayIdName()
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Territory p ")
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $territoryArray[$val['id']] = $val['name'];
        }

        return $territoryArray;
    }

    /**
     * Finds and displays a Faq entity.
     *
     * @Route("/{id}", name="faq_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Faq')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Faq entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a new User entity.
     *
     * @Route("/", name="user_create")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:useragent_new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('view_agents', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('user_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new User entity.
     *
     * @Route("/useragent_new", name="useragent_new")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:useragent_new.html.twig")
     */
    public function newAction()
    {
        $entity = new User();
        $form = $this->createCreateForm($entity);

        $data = array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'entity' => $entity,
            'form' => $form->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * .
     *
     * @Route("/user_account", name="user_account")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:my_account.html.twig")
     */
    public function userAccountAction(Request $request)
    {

        $user = $this->get('security.context')->getToken()->getUser();
        $user->getUsername();

        $data = array('first_name' => $user->getFirstName(),
            'middle_name' => $user->getMiddleName(),
            'last_name' => $user->getLastName(),
            'email' => $user->getEmail(),
            'mobile' => $user->getMobileNumber(),
            'organization' => $user->getLastName());

        $data = array(
            'title' => "My Account",
            'title_descr' => "List, create, delete, activate System Admins",
            'userInfo' => $data
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list_registrations",name="list_registrations")
     * @Method({"POST","GET"})
     *
     * @return JsonResponse
     */
    public function listRegistrationsAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = " WHERE 1=1 ";

            if (strlen(@$attributes["msisdn"]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes["msisdn"] . "%'";
            }
            if (strlen(@$attributes["first_name"]) >= 2) {
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (strlen(@$attributes["last_name"]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["identificationType"] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes["identificationType"] . "'";
            }
            if (strlen(@$attributes["id_number"]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes["id_number"] . "%'";
            }
            if (strlen(@$attributes["agent_firstname"]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes["agent_firstname"] . "%'";
            }
            if (strlen(@$attributes["agent_lastname"]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes["agent_lastname"] . "%'";
            }
            if (strlen(@$attributes["agent_phone"]) >= 4) {
                $where .= " AND a.username = '" . $attributes["agent_phone"] . "'";
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = '" . $attributes["territory"] . "'";
            }
            if (strlen(@$attributes["sim_serial"]) > 10) {
                $where .= " AND u.simSerial like '%" . $attributes["sim_serial"] . "%'";
            }
            if (strlen(@$attributes["id_number"]) >= 3) {
                $where .= " AND u.identification = '" . $attributes["id_number"] . "'";
            }


            $dateCheck = false;
            if (strlen(@$attributes["from_date"]) > 4) {
                $dateCheck = true;
                $where .= " AND DATE(u.createdDate) >= '" . date('Y-m-d', strtotime($attributes["from_date"])) . "'";
            }
            if (strlen(@$attributes["to_date"]) > 4) {
                $dateCheck = true;
                $where .= " AND u.createdDate <= '" . date('Y-m-d', strtotime($attributes["to_date"])) . " 23:59:59'";
            }


            if (strlen(@$attributes["icap_status"]) > 2) {
                $where .= $this->getWhereForICAPSatus($attributes["icap_status"]);
            }
            if (strlen(@$attributes["ereg_status"]) > 2) {
                $where .= $this->getWhereForEREGSatus($attributes["ereg_status"]);
            }
            //echo @$attributes["date"];
            if (strlen(@$attributes["date"]) > 4) {
                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "treg") {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.createdDate >= '" . @$attributes["date"] . " 00:00:00' AND x.createdDate <= '" . @$attributes["date"] . " 23:59:59'";

                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "tobesent") {
                    //NOT sent to TREG
                    $where .= "AND x.verifyState = 1 AND x.temporaryRegStatus = 0 AND x.createdDate >= '" . @$attributes["date"] . " 00:00:00' AND x.createdDate <= '" . @$attributes["date"] . " 23:59:59'";

                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "declined") {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4) AND x.createdDate >= '" . @$attributes["date"] . " 00:00:00' AND x.createdDate <= '" . @$attributes["date"] . " 23:59:59'";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "freg") {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7) AND x.createdDate >= '" . @$attributes["date"] . " 00:00:00' AND x.createdDate <= '" . @$attributes["date"] . " 23:59:59'";
                }

                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "pending") {
                    $where .= " AND u.image_count < 4 AND x.createdDate >= '" . @$attributes["date"] . " 00:00:00' AND x.createdDate <= '" . @$attributes["date"] . " 23:59:59'";
                }
            } else {
                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "treg") {
                    //sent to TREG NOT sent to FREG
                    $where .= " AND x.temporaryRegStatus IN (1,2,6)"; // AND x.fullRegStatus = 0";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "tobesent") {
                    //NOT sent to TREG
                    $where .= " AND x.temporaryRegStatus = 0 AND x.verifyState = 1 ";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "declined") {
                    //DECLINED or FAILED
                    $where .= " AND x.temporaryRegStatus IN (3,4)";
                } else if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "freg") {
                    $where .= " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7)";
                }

                if (strlen(@$attributes["path"]) > 2 && @$attributes["path"] == "pending") {
                    $where .= " AND u.image_count < 4";
                }
            }

            if (!$dateCheck) {
                $where .= " AND u.createdDate >= '" . date('Y-m-d', strtotime('-90 days')) . " 00:00:00'";
            }

            $queryAttrib = $request->query->all();


            $queryString = " SELECT u.id,u.msisdn, x.temporaryRegStatus as tregStatus, x.fullRegStatus as fregStatus, x.auditState, x.verifyState, concat(u.firstName, ' ', u.lastName) as customerName, DATE_FORMAT(u.createdDate, '%Y-%m-%d %H:%i:%s') as created_on, DATE_FORMAT(u.createdDate, '%H:%i:%s') as created_time, u.image_count as image_count, u.identificationType as identificationType, u.identification, a.username, concat(a.firstName,' ', a.lastName) as agentName, u.region as region, u.territory as territory,  CASE u.registrationType WHEN '2' THEN 'Biometric' ELSE 'Standard'  END AS type"
                . " FROM RestApiBundle:RegistrationStatus x "
                . "LEFT OUTER JOIN x.registrationId u "
                . "LEFT OUTER JOIN u.owner a " . $where
                . " ORDER BY u.id desc ";

            $query = $em->createQuery($queryString);

            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            $agents = $query->getResult();

            $returnData['Result'] = "OK";
            $rowData = array();
            if ($agents) {
                foreach ($agents as $kkey => $vval) {

                    if (isset($vval['verifyState'])) {
                        $vval['verifyState'] = $this->getVerifyStates($vval['verifyState']);
                    }

                    if (isset($vval['identificationType'])) {
                        $idType = $em->getRepository('DashboardBundle:Idtype')->find($vval['identificationType']);
                        if (isset($idType)) {
                            $vval['identificationType'] = $idType->getName();
                        } else {
                            $vval['identificationType'] = "Deleted ID";
                        }

                    }

                    if (isset($vval['territory']) && NULL != $vval['territory']) {
                        if (is_numeric($vval['territory'])) {
                            $entityManager = $this->getDoctrine()->getManager();
                            $territory = $entityManager->getRepository("DashboardBundle:Territory")->findOneBy(['id' => $vval['territory']]);
                        } else {
                            $entityManager = $this->getDoctrine()->getManager();
                            $territory = $entityManager->getRepository("DashboardBundle:Territory")->findOneBy(['name' => $vval['territory']]);
                        }
                        $vval['territory'] = !is_null($territory) ?  $territory->getName() : null;
                        $vval['region'] = !is_null($territory) ? $territory->getRegion()->getName() : null;
                    }

                    $vval['temporaryRegStatus'] = $this->getRegStatusDesc($vval['tregStatus']);

                    $vval['aimStatus'] = $this->getAimRegStatusDesc($vval['tregStatus'], $vval['fregStatus'], $vval['image_count']);

                    $vval['icapStatus'] = $this->getIcapRegStatusDesc($vval['tregStatus'], $vval['fregStatus']);
                    $rowData[] = $vval;
                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['Message'] = null;
            $returnData['Options'] = null;


            return new JsonResponse($returnData);

        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    function curl_message($url, $data)
    {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
            )
        );

        return curl_exec($ch);
    }

    private function verificationKeys()
    {
        return array(
            "invalid_name" => AbstractFilter::INVALID_NAME,
            "incorrect_gender" => AbstractFilter::INVALID_GENDER,
            "incorrect_dOB" => AbstractFilter::INVALID_DOB,
            "blur_portrait" => AbstractFilter::INVALID_POTRAIT,
            "invalid_portrait" => AbstractFilter::INVALID_POTRAIT,
            "blur_id_images" => AbstractFilter::INVALID_FRONT_ID,
            "invalid_id_images" => AbstractFilter::INVALID_FRONT_ID,
            "invalid_customer_signature" => AbstractFilter::INVALID_SIGNATURE
        );
    }

    /**
     * View Reports
     *
     * @Route("/verifyaction_registrations/{timestamp}/{recordID}/{aaction}/{descr}", name="verifyaction_registrations")
     * @Method("GET")
     */
    public function verifyaction_registrationsAction($timestamp, $recordID, $aaction, $descr)
    {
        $apiHelper = $this->get("api.helper");
        $data['recid'] = $recordID;
        $data['message'] = "Sent Successfully: " . $aaction;


        $em = $this->getDoctrine()->getManager();
        $registration = $em->getRepository('RestApiBundle:Registration')->find($recordID);
        if (!$registration) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }


        $query = $em->createQueryBuilder()
            ->select("u.id as sid, DATE_FORMAT(u.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate,  DATE_FORMAT(u.verifyLock, '%Y-%m-%d %H:%i%:%s') as verifyLock"
                . " from RestApiBundle:RegistrationStatus u "
                . " WHERE u.registrationId = " . $registration->getId())
            ->getQuery();

        echo $result1 = $query->getSql();
        exit; //Result();

        $timetaken = 0;
        if ($result1) {
            $timetaken = strtotime(date('Y-m-d H:i:s')) - strtotime($result1[0]['verifyLock']);
        }


        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($aaction == "approved") {


            $this->updateVerificationStatus(
                $registration, 0, 0, "", "Live Verification :: Approved ", 1, $descr, $user, $timetaken
            );

            /*
            $apiHelper->logInfo("VERIFYregistrationAction", "registerCustomerAction", array('start' => 'start dispatcher'));
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
            $event = new PlainTextEvent($registration);
            $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
            */
        } else {

            $errorArray = array();
            $descr_sent = explode(",", trim($descr, ","));


            $failedMsg = "";
            foreach ($descr_sent as $key => $val) {
                $failedMsg .= " " . $this->container->get('translator')->trans(
                        'verify.status.fail.' . $val, array('%msisdn%' => $user->getMobileNumber()), 'messages', "sw"
                    );
                array_push($errorArray, $this->verificationKeys()[$val]);
            }

            $failedMsg . " for number " . $registration->getMsisdn();

            $msg = str_replace("_", " ", $failedMsg);
            //$message = new Message();
            //$message->setDeviceId($registration->getDeviceId());
            //$message->setMessage($msg);
            //$message->setTopic($this->container->getParameter("mqtt_notify_msg"));
            //$this->container->get('api.helper')->sendMessage($message);
            //$apiHelper->notifyRegStatusDetail($registration, str_replace("_", " ", $failedMsg),$errorArray);
            $status = false;
            $this->updateVerificationStatus(
                $registration, 3, 3, $descr, "Live Verification :: Declined ", 3, $descr, $user, $timetaken
            );

            //$registration->setSimSerial($registration->getSimSerial() . "_" . date("Ymdhisa"));
            $em->flush();
        }

        $this->logUserEvent(UserLog::REGISTRATION_VERIFY, "registration verification", array("action" => $aaction, 'message' => $descr, "recordID" => $recordID));

        echo json_encode($data);

        exit;

        return array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }


    /**
     * @param Request $request
     * @Route("/list_verify_registrations/{list}/{max_result}",name="list_verify_registrations")
     * @Method({"POST","GET"})
     */
    public function list_verify_registrationsAction($list, $max_result, Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

            $attributes = $request->request->all();
            $where = " WHERE 1=1 ";

            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array("ROLE_SUPERAGENT", $roles)) {
                $where .= " AND a.parent_id = " . $user->getId();
            }
            if (in_array("ROLE_VODASHOP", $roles)) {
                $where .= " AND a.vodashop_id = " . $user->getId();
            }

            if (strlen(@$attributes["msisdn"]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes["msisdn"] . "%'";
            }
            if (strlen(@$attributes["first_name"]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (strlen(@$attributes["last_name"]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["identificationType"] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes["identificationType"] . "'";
            }
            if (strlen(@$attributes["id_number"]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes["id_number"] . "%'";
            }
            if (strlen(@$attributes["agent_firstname"]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes["agent_firstname"] . "%'";
            }
            if (strlen(@$attributes["agent_lastname"]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes["agent_lastname"] . "%'";
            }
            if (strlen(@$attributes["agent_phone"]) >= 4) {
                $where .= " AND a.username = '" . $attributes["agent_phone"] . "'";
            }
            if (strlen(@$attributes["region"]) > 1) {
                $where .= " AND u.region = '" . $attributes["region"] . "'";
            }
            if (@$attributes["audit_state"] >= 1) {
                $where .= " AND x.auditState = '" . $attributes["audit_state"] . "'";
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = '" . $attributes["territory"] . "'";
            }


            $dateCheck = false;
            if (strlen(@$attributes["from_date"]) > 4) {
                $dateCheck = true;
                $where .= " AND u.createdDate >= '" . date('Y-m-d', strtotime($attributes["from_date"])) . " 00:00:00'";
            }
            if (strlen(@$attributes["to_date"]) > 4) {
                $dateCheck = true;
                $where .= " AND u.createdDate <= '" . date('Y-m-d', strtotime($attributes["to_date"])) . " 23:59:59'";
            }

            if (strlen(@$attributes["verifyfrom_date"]) > 4) {
                $dateCheck = true;
                $where .= " AND x.verifyDate >= '" . date('Y-m-d', strtotime($attributes["verifyfrom_date"])) . " 00:00:00'";
            }
            if (strlen(@$attributes["verifyto_date"]) > 4) {
                $dateCheck = true;
                $where .= " AND x.verifyDate <= '" . date('Y-m-d', strtotime($attributes["verifyto_date"])) . " 23:59:59'";
            }


            if (!$dateCheck) {
                $where .= " AND u.createdDate >= '" . date('Y-m-d') . " 00:00:00'";
            }


            if (isset($attributes["verifyDescr"]) && !empty($attributes["verifyDescr"])) {
                $where .= " AND x.verifyDescr LIKE '%" . $attributes["verifyDescr"] . "%'";
            }
            if (isset($attributes["verifyState"]) && !empty($attributes["verifyState"])) {
                if (in_array($attributes["verifyState"], array(2, 3))) {
                    $where .= " AND (x.verifyState = '" . $attributes["verifyState"] . "' OR x.verifyState = '-" . $attributes["verifyState"] . "')";
                } else {
                    $where .= " AND x.verifyState = '" . $attributes["verifyState"] . "'";
                }
            }
            if (isset($attributes["verifyBy"]) && @$attributes["verifyBy"] >= 1) {
                $where .= " AND v.id = " . $attributes["verifyBy"];
            }

            $where .= " AND x.verifyBy != '541'";

            $additional_fields = "";
            $verifier_query_join = "";
            if ($list == "search") {
                //$where .= " AND x.temporaryRegStatus in ('NULL', 0)";
                //$where .= " AND u.image_count >= 3";
                //$where .= " AND x.verifyState NOT IN (1,2,3)";
            } elseif ($list == "list") {
                $where .= " AND x.verifyState IN ('1','2','3','-2','-3')";
                $additional_fields = ", DATE_FORMAT(x.verifyDate, '%Y-%m-%d') as Date, x.verifyState as State, v.username as Verifier, x.verifyDescr as Description ";
                $verifier_query_join = "LEFT OUTER JOIN x.verifyBy v ";
            } else {
                //multiple
                $additional_fields = ", DATE_FORMAT(x.auditDate, '%Y-%m-%d') as Date, x.auditState as State, x.auditDescr as Description ";
            }

            $selectFields = "u.id,u.msisdn as MSISDN,u.registrationid, u.firstName as FirstName,u.lastName as LastName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as CreatedOn, u.dob as DoB, u.nationality as Nationality, u.identificationType as IDType, u.identification as IDNumber, a.username as AgentMobile, a.firstName as agentFirstName, a.lastName as agentLastName, u.region as Region, u.territory as Territory, u.deviceModel as DeviceModel ";

            $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
                ->getQuery();
            $configMaster = $query->getOneOrNullResult();
            if ($configMaster && $list == "search") {
                $selectFields = str_replace(":::", ", ", $configMaster->getConfig());
            }

            $queryAttrib = $request->query->all();
            $queryString = " SELECT u.id as regId, u.id, u.registrationid, " . $selectFields . " " . $additional_fields
                . "FROM RestApiBundle:RegistrationStatus x "
                . "LEFT OUTER JOIN x.registrationId u "
                . $verifier_query_join
                . "LEFT OUTER JOIN u.owner a " . $where
                . " ORDER BY u.id desc ";
            $query = $this->getDoctrine()->getEntityManager()
                ->createQuery($queryString);

            if (isset($queryAttrib["jtPageSize"])) {
                $query->setMaxResults($queryAttrib["jtPageSize"]);
            } else {
                $query->setMaxResults($max_result);
            }
            if (isset($queryAttrib["jtStartIndex"])) {
                $query->setFirstResult($queryAttrib["jtStartIndex"]);
            }


            //echo $agents = $query->getSql(); exit; //Result();
            $agents = $query->getResult();

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[$aval->getId()] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[$bval->getId()] = $bval->getName();
            }

//            echo "<pre>";
//            print_r($territory_array);
//            echo "<pre>";

            $user = $this->get('security.token_storage')->getToken()->getUser();


            $returnData['Result'] = "OK";
            $rowData = array();
            $rowData_send = array();
            if ($agents) {
                foreach ($agents as $kkey => $vval) {
                    if (@$vval["Territory"] > 1) {
                        $vval['Territory'] = @$territory_array[$vval['Territory']];
                    }
                    if (isset($vval['Region'])) {
                        $vval['Region'] = $this->getRegionFromCode($vval['Region']);
                    }
                    if (isset($vval['IDType'])) {
                        $vval['IDType'] = @$id_array[$vval['IDType']];
                    }

                    if (isset($vval['State'])) {
                        if ($list == "list") {
                            $vval['State'] = $this->getVerifyStates($vval['State']);
                        } else {
                            $vval['State'] = $this->getAuditStates($vval['State']);
                        }
                    }
                    if (isset($vval['Description'])) {
                        $vval['Description'] = trim(str_replace("_", " ", str_replace("descr", " ", $vval['Description'])), ",");
                    }

                    $rowData_send['id'] = $vval['registrationid'];
                    unset($vval['registrationid']);
                    unset($vval['regId']);
                    $rowData_send['jsonddata'] = json_encode($vval);
                    $rowData[] = $rowData_send;
                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['Message'] = null;
            $returnData['Options'] = null;
            $returnData['TotalRecordCount'] = 3434;


//            echo "<pre>";
//            print_r($returnData);
//            echo "<pre>";
            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    private function getRegionFromCode($regionCode = null)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Region p ")
            ->getArrayResult();

        $regionName = "";

        foreach ($result as $key => $val) {
            $regionArray[$val['code']] = $val['name'];
        }

        if (isset($regionArray[$regionCode])) {
            $regionName = @$regionArray[$regionCode];
        }

        return $regionName;
    }


    public function lockVerifytatus($registration_id, $verifyState, $verifyDesc, $user)
    {
        $em = $this->getDoctrine()->getManager();

        $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . date('Y-m-d H:i:s') . "', verifyBy = '" . $user->getId() . "' WHERE registrationid = " . $registration_id;
        $em->getConnection()->exec($query1);

    }

    public function updateVerificationStatus(Registration $registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken)
    {


        $em = $this->getDoctrine()->getManager();

        $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . $timetaken . "', verifyBy = '" . $user->getId() . "', verifyDescr = '" . $verifyDesc . "' WHERE registrationid = " . $registration->getId();
        $em->getConnection()->exec($query1);

    }


    /**
     * @param Request $request
     * @Route("/antolist_verify_registrations/{list}/{max_result}",name="antolist_verify_registrations")
     * @Method({"POST","GET"})
     */
    public function antolist_verify_registrationsAction($list, $max_result, Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

            $attributes = $request->request->all();
            $where = " WHERE 1=1 ";

            if (strlen(@$attributes["msisdn"]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes["msisdn"] . "%'";
            }
            if (strlen(@$attributes["first_name"]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (strlen(@$attributes["last_name"]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["identificationType"] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes["identificationType"] . "'";
            }
            if (strlen(@$attributes["id_number"]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes["id_number"] . "%'";
            }
            if (strlen(@$attributes["agent_firstname"]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes["agent_firstname"] . "%'";
            }
            if (strlen(@$attributes["agent_lastname"]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes["agent_lastname"] . "%'";
            }
            if (strlen(@$attributes["agent_phone"]) >= 4) {
                $where .= " AND a.username = '" . $attributes["agent_phone"] . "'";
            }
            if (@$attributes["region"] > 0) {
                $where .= " AND u.region = '" . $attributes["region"] . "'";
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = '" . $attributes["territory"] . "'";
            }
            if (strlen(@$attributes["from_date"]) > 4) {
                $where .= " AND DATE(u.createdDate) >= '" . date('Y-m-d', strtotime($attributes["from_date"])) . "'";
            }
            if (strlen(@$attributes["to_date"]) > 4) {
                $where .= " AND DATE(u.createdDate) <= '" . date('Y-m-d', strtotime($attributes["to_date"])) . "'";
            }

            if (strlen(@$attributes["verifyfrom_date"]) > 4) {
                $where .= " AND DATE(x.verifyDate) >= '" . date('Y-m-d', strtotime($attributes["verifyfrom_date"])) . "'";
            }
            if (strlen(@$attributes["verifyto_date"]) > 4) {
                $where .= " AND DATE(x.verifyDate) <= '" . date('Y-m-d', strtotime($attributes["verifyto_date"])) . "'";
            }
            if (isset($attributes["verifyDescr"]) && strlen($attributes["verifyDescr"]) > 4) {
                $where .= " AND x.verifyDescr LIKE '%" . $attributes["verifyDescr"] . "%'";
            }
            if (isset($attributes["verifyState"]) && $attributes["verifyState"] >= 1) {
                $where .= " AND x.verifyState = '" . $attributes["verifyState"] . "'";
            }
            if (isset($attributes["verifyBy"]) && $attributes["verifyBy"] >= 1) {
                $where .= " AND x.verifyBy = '" . $attributes["verifyBy"] . "'";
            }

            // $where .= " AND x.verifyState = 9 ";

            $join = "";
            $additional_fields = "";
            if ($list == "search") {
                //$where .= " AND x.temporaryRegStatus in ('NULL', 0)";
                $where .= " AND u.image_count >= 3";
                $where .= " AND x.verifyState NOT IN (1,2,3,'-1')";
                $where .= " AND date(x.createdDate) > '2016-07-01' ";
            } elseif ($list == "list") {
                $where .= " AND x.verifyState IN (1,2,3)";
                $additional_fields = ", CONCAT(b.firstName, ' ', b.lastName) as verifyByName, DATE_FORMAT(x.verifyDate, '%Y-%m-%d') as Date, x.verifyState as State, x.verifyDescr as Description ";
                $join .= " LEFT OUTER JOIN x.verifyBy b ";
            } else {
                //multiple
                $additional_fields = ", DATE_FORMAT(x.verifyDate, '%Y-%m-%d') as Date, x.verifyState as State, x.verifyDescr as Description ";
            }
            echo "here ";
            exit;

            $selectFields = "u.id,u.msisdn as MSISDN,u.registrationid, u.firstName as FirstName,u.lastName as LastName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as CreatedOn, u.dob as DoB, u.nationality as Nationality, u.identificationType as IDType, u.identification as IDNumber, a.username as AgentMobile, a.firstName as agentFirstName, a.lastName as agentLastName, u.region as Region, u.territory as Territory, u.deviceModel as DeviceModel, u.msisdnOccuranceCount as TimesReg ";

            $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
                ->getQuery();
            $configMaster = $query->getOneOrNullResult();
            if ($configMaster && $list == "search") {
                $selectFields = str_replace(":::", ", ", $configMaster->getConfig());
            }

            $queryAttrib = $request->query->all();
            $queryString = " SELECT u.id as regId, u.id, u.registrationid, " . $selectFields . " " . $additional_fields . ", x.verifyLock as TimeTaken "
                . "FROM RestApiBundle:RegistrationStatus x "
                . "LEFT OUTER JOIN x.registrationId u "
                . "LEFT OUTER JOIN u.owner a " . $join . $where
                . " ORDER BY u.id desc ";
            $query = $this->getDoctrine()->getEntityManager()
                ->createQuery($queryString);

            if (isset($queryAttrib["jtPageSize"])) {
                $query->setMaxResults($queryAttrib["jtPageSize"]);
            } else {
                $query->setMaxResults($max_result);
            }
            if (isset($queryAttrib["jtStartIndex"])) {
                $query->setFirstResult($queryAttrib["jtStartIndex"]);
            }

            //echo $agents = $query->getSql(); exit; //Result();
            $agents = $query->getResult();

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[$aval->getId()] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[$bval->getId()] = $bval->getName();
            }

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $returnData['Result'] = "OK";
            $rowData = array();
            $rowData_send = array();
            if ($agents) {
                foreach ($agents as $kkey => $vval) {

                    if (isset($vval["Territory"])) {
                        $vval['Territory'] = $territory_array[$vval['Territory']];
                    }
                    if (isset($vval['Region'])) {
                        $vval['Region'] = $this->get_regionName_fromKey($vval['Region']);
                    }
                    if (isset($vval['IDType'])) {
                        $vval['IDType'] = $id_array[$vval['IDType']];
                    }

                    if (isset($vval['State'])) {
                        $vval['State'] = $this->getVerifyStates($vval['State']);
                    }
                    if (isset($vval['Description'])) {
                        $vval['Description'] = trim(str_replace("_", " ", str_replace("descr", " ", $vval['Description'])), ",");
                    }

                    if ($list == "search") {

                        //lock record
                        $em->getRepository("RestApiBundle:RegistrationStatus")->lockVerifytatus($vval['id'], '-1', 'lock', $user
                        );
                    }

                    $rowData_send['id'] = $vval['registrationid'];
                    unset($vval['registrationid']);
                    unset($vval['regId']);
                    $rowData_send['jsonddata'] = json_encode($vval);
                    $rowData[] = $rowData_send;
                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['Message'] = null;
            $returnData['Options'] = null;


//            echo "<pre>";
//            print_r($returnData);
//            echo "<pre>";
            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/audit_registrations/{recordID}/{rateValue}/{rateDescr}/{timestamp}", name="audit_registrations")
     * @Method("GET")
     */
    public function audit_registrationsAction($recordID, $rateValue, $rateDescr, $timestamp)
    {

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $em = $this->getDoctrine()->getManager();
        $query1 = "UPDATE RegistrationStatus SET auditDate = '" . date('Y-m-d H:i:s') . "', "
            . " auditState = '" . $rateValue . "', "
            . " auditDescr = '" . trim(str_replace("___", "", $rateDescr), ',') . "', "
            . " auditBy = '" . $user->getId() . "' "
            . " WHERE registrationId = '" . $recordID . "'";
        $em->getConnection()->exec($query1);


        $data['response_msg'] = "Audited Successfully";
        echo json_encode($data);
        exit;
        return array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }

    /**
     * @param Request $request
     * @Route("/list_audit_registrations",name="list_audit_registrations")
     * @Method({"POST","GET"})
     *
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function list_audit_registrationsAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = " WHERE 1=1 ";

            if (strlen(@$attributes["msisdn"]) >= 4) {
                $where .= " AND u.msisdn like '%" . $attributes["msisdn"] . "%'";
            }
            if (strlen(@$attributes["first_name"]) >= 2) {//echo "<<<<<<<<<<<<<<";
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (strlen(@$attributes["last_name"]) >= 2) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["identificationType"] >= 1) {
                $where .= " AND u.identificationType = '" . $attributes["identificationType"] . "'";
            }
            if (strlen(@$attributes["id_number"]) >= 4) {
                $where .= " AND u.identification like '%" . $attributes["id_number"] . "%'";
            }
            if (strlen(@$attributes["agent_firstname"]) >= 3) {
                $where .= " AND a.firstName like '%" . $attributes["agent_firstname"] . "%'";
            }
            if (strlen(@$attributes["agent_lastname"]) >= 3) {
                $where .= " AND a.lastName like '%" . $attributes["agent_lastname"] . "%'";
            }
            if (strlen(@$attributes["agent_phone"]) >= 4) {
                $where .= " AND a.username = '" . $attributes["agent_phone"] . "'";
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = '" . $attributes["territory"] . "'";
            }
            if (strlen(@$attributes["from_date"]) > 4) {
                $where .= " AND date(u.createdDate) >= '" . date('Y-m-d', strtotime($attributes["from_date"])) . "'";
            }
            if (strlen(@$attributes["to_date"]) > 4) {
                $where .= " AND date(u.createdDate) <= '" . date('Y-m-d', strtotime($attributes["to_date"])) . "'";
            }

            $where .= " AND x.auditState NOT IN (1,2,3,'-1')";
            $where .= " AND u.image_count >= 3";


            $queryAttrib = $request->query->all();
            $queryString = " SELECT u.id,u.msisdn,u.registrationid, u.firstName,u.lastName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as created_on, u.dob, u.nationality,i.name as identificationType, u.identification, a.username, a.firstName as agentfname, a.lastName as agentlname, r.name as region, t.name as territory,u.deviceModel, x.temporaryRegStatus "
                . "FROM RestApiBundle:RegistrationStatus x "
                . " LEFT OUTER JOIN x.registrationId u "
                . " LEFT OUTER JOIN u.identificationType i "
                . " LEFT OUTER JOIN u.territory t "
                . " LEFT OUTER JOIN t.region r "
                . " LEFT OUTER JOIN u.owner a " . $where
                . " ORDER BY u.id desc ";
            $query = $this->getDoctrine()->getEntityManager()
                ->createQuery($queryString);

            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            $agents = $query->getResult();

            $user = $this->get('security.token_storage')->getToken()->getUser();

            $returnData['Result'] = "OK";
            $rowData = array();
            $rowData_send = array();
            if (@$agents) {
                foreach ($agents as $kkey => $vval) {

                    //update lock
                    $em->getRepository("RestApiBundle:RegistrationStatus")->updateAuditStatus($vval['id'], '-1', 'lock', $user
                    );

                    $vval['temporaryRegStatus'] = $this->getRegStatusDesc($vval['temporaryRegStatus']);

                    $rowData_send['id'] = $vval['registrationid'];
                    $rowData_send['jsonddata'] = json_encode($vval);
                    $rowData[] = $rowData_send;
                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['Message'] = null;
            $returnData['Options'] = null;

            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a idverificationConfig.
     *
     * @Route("/idverificationConfig", name="idverificationConfig")
     * @Method("POST")
     */
    public function idverificationConfigAction(Request $request)
    {

//        $logger = $this->container->get("monolog.logger.ereg");
//        /** @var  $loggedIn User */
//        $loggedIn = $this->get('security.token_storage')->getToken()->getUser();

        $resp = new JsonObject();
        $status = false;
        $message = "ERROR SUBMITING REQUEST";
        /* try { */
        $attributes = json_decode($request->getContent(), true);
        if ($attributes) {

            $attributes['idtype'] = (is_array($attributes['idtype']) ? $attributes['idtype'] : array($attributes['idtype']));

            $em = $this->getDoctrine()->getManager();
            $result = $em->createQuery("SELECT p "
                . " from DashboardBundle:ConfigMaster p "
                . " WHERE p.name = 'id_verification_list' ")
                ->getOneOrNullResult();

            if ($result) {
                $query1 = "UPDATE ConfigMaster SET config = '" . json_encode($attributes['idtype']) . "' WHERE name = 'id_verification_list'";
                $em->getConnection()->exec($query1);
            } else {
                $query1 = "INSERT INTO `ConfigMaster` (`name`, `config`, `version`, `createdOn`) VALUES ('id_verification_list', '" . json_encode($attributes['idtype']) . "', 1, '" . date('Y-m-d H:i:s') . "');";
                $em->getConnection()->exec($query1);
            }

            $status = true;
            $message = "Successfully Saved ID";
        }
        /* } catch (\Exception $e) {
          $message = $e->getMessage();
          } */
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Get verifiers idle time
     *
     * @Route("/verifier_idle_time", name="verifier_idle_time")
     * @Method({"POST","GET"})
     */
    public function verifierIdleTime()
    {
        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('RestApiBundle:user', 'u');
        $rsm->addScalarResult('first_name', 'first_name');
        $rsm->addScalarResult('last_name', 'last_name');
        $rsm->addScalarResult('counter', 'counter');
        $rsm->addScalarResult('idletime', 'idletime');


        $queryString = "select u.first_name, u.last_name, count(r2.id) as counter, r2.verifyBy, r2.id as maxid, "
            . "r2.verifyDate, MAX(r2.verifyDate) as maxdate, TIMEDIFF('" . date('Y-m-d H:i:s') . "', MAX(r2.verifyDate)) as idletime FROM RegistrationStatus r2, user u WHERE r2.verifyBy = u.id AND date(r2.verifyDate) = '" . date('Y-m-d') . "' AND r2.verifyBy != 3260 GROUP BY r2.verifyBy ORDER BY maxdate";

        $query = $this->getDoctrine()->getEntityManager()->createNativeQuery($queryString, $rsm);

        $agents = $query->getResult();

        return $this->buildResponse($agents, Response::HTTP_OK);
    }

    /**
     * Get verifiers idle time
     *
     * @Route("/verifier_today_count", name="verifier_today_count")
     * @Method({"POST","GET"})
     */
    public function verifierTodayCounts()
    {
        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('RestApiBundle:user', 'u');
        $rsm->addScalarResult('first_name', 'first_name');
        $rsm->addScalarResult('last_name', 'last_name');
        $rsm->addScalarResult('counter', 'counter');

        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter, r2.verifyBy, r2.id as maxid, r2.verifyDate, MAX(r2.verifyDate) as maxdate, TIMEDIFF(now(), MAX(r2.verifyDate)) as idletime FROM RegistrationStatus r2, user u WHERE r2.verifyBy = u.id AND DATE(r2.verifyDate) = CURDATE() GROUP BY r2.verifyBy ORDER BY maxdate";

        $queryString = "select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u "
            . "WHERE r2.verifyBy = u.id AND r2.verifyBy != 3260 AND DATE(r2.verifyDate) = CURDATE() AND r2.verifyState > 0 GROUP BY r2.verifyBy ORDER BY counter";


        $query = $this->getDoctrine()->getEntityManager()->createNativeQuery($queryString, $rsm);

        $agents = $query->getResult();

        return $this->buildResponse($agents, Response::HTTP_OK);
    }

    /**
     * get a list of all the territoires
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list_id_types",name="list_id_types")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function listIdTypes(Request $request)
    {

        $idtype = array();
        $attribs = $request->query->all();
        $regionId = isset($attribs['regionId']) ? $attribs['regionId'] : 0;


        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p "
            . " from DashboardBundle:ConfigMaster p "
            . " WHERE p.name = 'id_verification_list' ")
            ->getArrayResult();
        if ($result) {
            foreach ($result as $kkey => $vval) {
                $ids_used = json_decode($vval['config']);
            }
        }

        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT t FROM DashboardBundle:Idtype t');

        $idtype = $query->getArrayResult();

        foreach ($idtype as $key => $val) {
            $id_used_flag = 0;
            $id_array[$val['name']] = array("Value" => $val['id'], "DisplayText" => $val['name']);
            if (in_array($val['name'], $ids_used)) {
                $id_used_flag = 1;
            }

            $id_array[$val['name']]['used'] = $id_used_flag;
        }

        $return_array['Options'] = $id_array;

        echo json_encode($return_array);
        exit;
    }

    function get_regionName_fromKey($region_key)
    {
        $regionsArray = array(
            "MW" => "mwanza",
            "KG" => "Kigoma",
            "KR" => "kagera",
            "PW" => "pwani",
            "MO" => "morogoro",
            "NJ" => "njombe",
            "ZS" => "zanzibar",
            "ZW" => "zanzibar",
            "ZN" => "zanzibar",
            "KM" => "kigoma",
            "MT" => "mtwara",
            "RV" => "ruvuma",
            "PN" => "pemba",
            "PS" => "pemba",
            "SD" => "singida",
            "SH" => "shinyanga",
            "AS" => "arusha",
            "MY" => "manyara",
            "MA" => "mara",
            "SI" => "simiyu",
            "MB" => "mbeya",
            "RK" => "rukwa",
            "DS" => "dar-es-salaam",
            "DO" => "dodoma",
            "TB" => "tabora",
            "LI" => "lindi",
            "GE" => "Geita",
            "KL" => "kilimanjaro",
            "TN" => "tanga",
            "KA" => "kahama",
            "IR" => "iringa",
            "NT" => "North",
            "CS" => "Coast",
            "ST" => "South",
            "LK" => "Lake Zone"
        );
        return $regionsArray[$region_key];
    }

    function get_regionKey_fromName($region_name = NULL)
    {
        $regionsArray = array(
            "mwanza" => "MW",
            "kagera" => "KR",
            "pwani" => "PW",
            "morogoro" => "MO",
            "njombe" => "NJ",
            "zanzibar" => "ZS",
            "zanzibar" => "ZW",
            "zanzibar" => "ZN",
            "kigoma" => "KM",
            "mtwara" => "MT",
            "ruvuma" => "RV",
            "pemba" => "PN",
            "pemba" => "PS",
            "singida" => "SD",
            "shinyanga" => "SH",
            "arusha" => "AS",
            "manyara" => "MY",
            "mara" => "MA",
            "simiyu" => "SI",
            "mbeya" => "MB",
            "rukwa" => "RK",
            "dar-es-salaam" => "DS",
            "dodoma" => "DO",
            "tabora" => "TB",
            "lindi" => "LI",
            "Geita" => "GE",
            "kilimanjaro" => "KL",
            "tanga" => "TN",
            "kahama" => "KA",
            "iringa" => "IR",
            "NT" => "North",
            "CS" => "Coast",
            "ST" => "South",
            "LK" => "Lake Zone"
        );

        return $regionsArray[$region_name];
    }

    function getGender($gender = null)
    {
        $gender_array = array(
            'm' => 'Male',
            'f' => 'female',
        );

        return $gender_array[strtolower($gender)];
    }

    /**
     * @deprecated
     *
     * @param integer $status
     *
     * @return mixed
     */
    function getRegStatusDesc($status = null)
    {
        $regstatus = array(
            '0' => 'Pending',
            '1' => 'Sent',
            '2' => 'success',
            '3' => 'declined',
            '4' => 'fail',
            '5' => 'retry',
            '6' => 'success_cap',
            '7' => 'success_dbms'
        );

        return $regstatus[strtolower($status)];
    }

    private function getIcapRegStatusDesc($tstatus, $fstatus)
    {
        $ret = "Not Sent";
        if (in_array($tstatus, array('1', '2', '6')) && in_array($fstatus, array('1', '0'))) {
            $ret = "Successful";
        } else if (in_array($fstatus, array('2', '6', '7'))) {
            $ret = "SUCCESSFUL";
        } else if (in_array($tstatus, array('3', '4')) || in_array($fstatus, array('3', '4'))) {
            $ret = "Declined";
        }

        return $ret; // .$tstatus . $fstatus;
    }

    private function geEkycRegStatusDesc($tstatus, $fstatus, $imageCount)
    {
        $ret = "Pending";
        if (in_array($tstatus, array('1', '2', '6')) || in_array($fstatus, array('1', '2', '6', '7'))) {
            $ret = "Submitted";
        } else if (in_array($tstatus, array('3', '4')) || in_array($fstatus, array('3', '4'))) {
            $ret = "Declined";
        } else if ($imageCount >= 3) {
            $ret = "Received";
        }

        return $ret; // .$tstatus . $fstatus;
    }

    private function getAimRegStatusDesc($tstatus, $fstatus, $imageCount)
    {
        $ret = "Pending";
        if (in_array($tstatus, array('1', '2', '6')) || in_array($fstatus, array('1', '2', '6', '7'))) {
            $ret = "Submitted";
        } else if (in_array($tstatus, array('3', '4')) || in_array($fstatus, array('3', '4'))) {
            $ret = "Declined";
        } else if ($imageCount >= 3) {
            $ret = "Received";
        }

        return $ret; // .$tstatus . $fstatus;
    }

    private function getWhereForICAPSatus($status)
    {
        if ($status != null) {
            if ($status == 'notsent') {
                return " AND x.temporaryRegStatus = 0";
            } else if ($status == 'declined') {
                return " AND x.temporaryRegStatus IN (3,4) AND x.fullRegStatus = 0";
            } else if ($status == 'freg') {
                return " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus IN (1,6,7)";
            } else if ($status == 'treg') {
                return " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0";
            }
        }
        return "";
    }

    private function getWhereForEREGSatus($status)
    {
        if ($status != null) {
            if ($status == 'pending') {
                return " AND x.temporaryRegStatus = 0";
            } else if ($status == 'submitted') {
                return " AND x.temporaryRegStatus IN (1,2,6)";
            } else if ($status == 'declined') {
                return " AND x.temporaryRegStatus IN (3, 4) OR x.fullRegStatus IN (3, 4)";
            } else if ($status == 'received') {
                return " AND u.image_count >= 3";
            } else if ($status == 'audited') {
                return " AND x.auditState IN (1,2,3)";
            }
            return " AND x.temporaryRegStatus IN (1,2,6) AND x.fullRegStatus = 0";
        }
        return "";
    }

    function getVerifyStates($status = null)
    {
        $statename = "Bad";

        $verifyStates = array(
            '1' => 'Good',
            '2' => 'Fair',
            '3' => 'Bad',
            '5' => 'Pending',
            '-1' => 'Pending',
            '0' => 'Pending'
        );
        if (isset($verifyStates[strtolower($status)])) {
            $statename = $verifyStates[strtolower($status)];
        }
        return $statename;
    }

    private function getIDType($type = null)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Idtype p ")
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $idArray[$val['id']] = $val['name'];
        }

        return $idArray[$type];
    }

    private function getTerritoryFromId($type = null)
    {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p from DashboardBundle:Territory p ")
            ->getArrayResult();

        foreach ($result as $key => $val) {
            $territoryArray[$val['id']] = $val['name'];
        }

        if ($type != null) {
            return $territoryArray[$type];
        } else {
            return "";
        }
    }

    /**
     * Makes a Post to intergration box
     * @Get("/tigosend_api/{id}", name="/tigosend_api")
     */
    public function tigoSendApiAction($id)
    {


        $current_directory = date('Ymd');
        $uploadsDirectory = "./api_logs/{$current_directory}/";

        if (!file_exists($uploadsDirectory)) {
            mkdir($uploadsDirectory, 0777, true);
        }
        file_put_contents(
            $uploadsDirectory . date("dMY H:00") . '_log.txt', date('Y-m-d H:i:s') . " APICustomer] \n----------------\n.\n\n", FILE_APPEND | LOCK_EX
        );


        $soapUrl = "https://accessgw.tigo.co.tz:8443/registrationlive";

        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);


        //--------
        $apiHelper = $this->container->get('api.helper');
        $apiHelper->logInfo("antoIntegration", "onReceivePlainText", array('start' => 'entered'));

        try {
            echo $this->container->get('tigo.endpoint')->processText($entity);
        } catch (\Exception $e) {
            $apiHelper->logE("antoIntegration", "onReceivePlainText", $e);
        }


//        $apiHelper->logInfo("antoIntegration2", "registerCustomerAction", array('start' => 'start dispatcher'));
//        $dispatcher = $this->get('event_dispatcher');
//        $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
//        $event = new PlainTextEvent($entity);
//        $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);


        $apiHelper->logInfo("antoIntegration", "onReceivePlainText", array('end' => 'entered'));

        return array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }

    /**
     * Summary
     *
     * @Route("/__verify_agents_report/{start_date}/{end_date}/{timestamp}", name="__verify_agents_report")
     * @Method("GET")
     * @Template()
     */
    public function __verify_agents_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select(" u.id as sid, COUNT(r.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.verifyState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u " . $where
                . " GROUP BY u.id, s.verifyState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $data['active'][$value['sid']] = (@$data['active'][$value['sid']] ? $data['active'][$value['sid']] : $value);


            $data['active'][$value['sid']]['sum_total'] = @$data['active'][$value['sid']]['sum_total'] + $value['sum_regs'];
            $data['active'][$value['sid']]['sum_good'] = (@$data['active'][$value['sid']]['sum_good'] ? $data['active'][$value['sid']]['sum_good'] : 0);
            $data['active'][$value['sid']]['sum_bad'] = (@$data['active'][$value['sid']]['sum_bad'] ? $data['active'][$value['sid']]['sum_bad'] : 0);
            $data['active'][$value['sid']]['sum_unrated'] = (@$data['active'][$value['sid']]['sum_unrated'] ? $data['active'][$value['sid']]['sum_unrated'] : 0);

            if ($value['verifyState'] == 1) {
                $data['active'][$value['sid']]['sum_good'] = $data['active'][$value['sid']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 2 || $value['verifyState'] == 3) {
                $data['active'][$value['sid']]['sum_bad'] = $data['active'][$value['sid']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data['active'][$value['sid']]['sum_unrated'] = $data['active'][$value['sid']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/__verify_region_report/{start_date}/{end_date}/{timestamp}", name="__verify_region_report")
     * @Method("GET")
     * @Template()
     */
    public function __verify_region_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.verifyState as verifyState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u "
                . " JOIN u.region d "
                . " JOIN u.territory t " . $where2
                . " GROUP BY region_name, territory_name, verifyState ORDER BY sum_regs DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['region_name']][$value['territory_name']] = (@$data[$value['region_name']][$value['territory_name']] ? $data[$value['region_name']][$value['territory_name']] : $value);

            //first append total, good faor bad to array as zero
            $data[$value['region_name']][$value['territory_name']]['sum_total'] = @$data[$value['region_name']][$value['territory_name']]['sum_total'] + $value['sum_regs'];
            $data[$value['region_name']][$value['territory_name']]['sum_good'] = (@$data[$value['region_name']][$value['territory_name']]['sum_good'] ? $data[$value['region_name']][$value['territory_name']]['sum_good'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_fair'] = (@$data[$value['region_name']][$value['territory_name']]['sum_fair'] ? $data[$value['region_name']][$value['territory_name']]['sum_fair'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_bad'] = (@$data[$value['region_name']][$value['territory_name']]['sum_bad'] ? $data[$value['region_name']][$value['territory_name']]['sum_bad'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_unrated'] = (@$data[$value['region_name']][$value['territory_name']]['sum_unrated'] ? $data[$value['region_name']][$value['territory_name']]['sum_unrated'] : 0);

            //now fill in the data
            if ($value['verifyState'] == 1) {
                $data[$value['region_name']][$value['territory_name']]['sum_good'] = $data[$value['region_name']][$value['territory_name']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == 2) {
                $data[$value['region_name']][$value['territory_name']]['sum_bad'] = $data[$value['region_name']][$value['territory_name']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data[$value['region_name']][$value['territory_name']]['sum_unrated'] = $data[$value['region_name']][$value['territory_name']]['sum_unrated'] + $value['sum_regs'];
            }
        }
//        echo "<pre>";
//        print_r($data);
//        echo "<pre>";
        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/__getverify_auditor_reports/{start_date}/{end_date}/{timestamp}", name="__getverify_auditor_reports")
     * @Method("GET")
     * @Template()
     */
    public function __getverify_auditor_reportsAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }
        //$where .= " AND s.verifyState in (1,3,2)";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("u.id as sid, COUNT(s.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.verifyBy u " . $where2
                . " GROUP BY s.verifyBy, s.verifyState")
            ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $data['active'][$value['sid']] = (@$data['active'][$value['sid']] ? $data['active'][$value['sid']] : $value);

            $data['active'][$value['sid']]['sum_total'] = @$data['active'][$value['sid']]['sum_total'] + $value['sum_regs'];
            $data['active'][$value['sid']]['sum_good'] = (@$data['active'][$value['sid']]['sum_good'] ? $data['active'][$value['sid']]['sum_good'] : 0);
            $data['active'][$value['sid']]['sum_fair'] = (@$data['active'][$value['sid']]['sum_fair'] ? $data['active'][$value['sid']]['sum_fair'] : 0);
            $data['active'][$value['sid']]['sum_bad'] = (@$data['active'][$value['sid']]['sum_bad'] ? $data['active'][$value['sid']]['sum_bad'] : 0);
            $data['active'][$value['sid']]['sum_unrated'] = (@$data['active'][$value['sid']]['sum_unrated'] ? $data['active'][$value['sid']]['sum_unrated'] : 0);

            if ($value['verifyState'] == 1) {
                $data['active'][$value['sid']]['sum_good'] = $data['active'][$value['sid']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == 2) {
                $data['active'][$value['sid']]['sum_bad'] = $data['active'][$value['sid']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data['active'][$value['sid']]['sum_unrated'] = $data['active'][$value['sid']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/__verify_daily_report/{start_date}/{end_date}/{timestamp}/{pull_all_records}", name="__verify_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function __verify_daily_reportAction($start_date, $end_date, $timestamp, $pull_all_records)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.createdDate) >= '" . $start_date . "' AND date(s.createdDate) <= '" . $end_date . "' ";
        }

        //if (in_array("ROLE_VERIFYER_SUPERVISOR", $roles)) {
        if ($pull_all_records == 1) {
            // To do nothing :)) smile
        } else {
            $where .= " AND s.verifyBy != 3260 OR s.verifyBy IS NULL ";
        }

        //$where.=" AND r.image_count >= 3 ";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.createdDate, '%Y-%m-%d') as created_on, s.verifyState "
                . " from RestApiBundle:RegistrationStatus s"
                . " JOIN s.registrationId r "
                . " JOIN r.owner u " . $where
                . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
            ->getQuery();

        //echo $result1 = $query->getSql(); exit; //Result();
        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['created_on']] = (@$data[$value['created_on']] ? $data[$value['created_on']] : $value);

            $data[$value['created_on']]['sum_total'] = @$data[$value['created_on']]['sum_total'] + $value['sum_regs'];
            $data[$value['created_on']]['sum_good'] = (@$data[$value['created_on']]['sum_good'] ? $data[$value['created_on']]['sum_good'] : 0);
            $data[$value['created_on']]['sum_fair'] = (@$data[$value['created_on']]['sum_fair'] ? $data[$value['created_on']]['sum_fair'] : 0);
            $data[$value['created_on']]['sum_bad'] = (@$data[$value['created_on']]['sum_bad'] ? $data[$value['created_on']]['sum_bad'] : 0);
            $data[$value['created_on']]['sum_unrated'] = (@$data[$value['created_on']]['sum_unrated'] ? $data[$value['created_on']]['sum_unrated'] : 0);

            if ($value['verifyState'] == 1) {
                $data[$value['created_on']]['sum_good'] = $data[$value['created_on']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == 2) {
                $data[$value['created_on']]['sum_bad'] = $data[$value['created_on']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data[$value['created_on']]['sum_unrated'] = $data[$value['created_on']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * active_agents Summary
     *
     * @Route("/__verify_timebreakdown_report/{start_date}/{end_date}/{timestamp}", name="__verify_timebreakdown_report")
     * @Method("GET")
     * @Template()
     */
    public function __verify_timebreakdown_reportAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }

        $where .= " AND s.verifyBy NOT IN (3260) ";


        $where .= " AND s.verifyState IN (1,2)";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

//$emConfig->addCustomStringFunction($name, 'aimgroup\DashboardBundle\DQL');
        $emConfig->addCustomNumericFunction('FLOOR', 'aimgroup\DashboardBundle\DQL\MysqlFloor');
//$emConfig->addCustomDatetimeFunction($name, 'aimgroup\DashboardBundle\DQL');

        $query = $em->createQuery("SELECT COUNT(s.id) as RegistrationsCount, (CASE WHEN s.verifyLock <=20 THEN '0-20' WHEN s.verifyLock >=20 AND s.verifyLock <=40 THEN '21-40' WHEN s.verifyLock >=41 AND s.verifyLock <=60 THEN '41-60' WHEN s.verifyLock >=61 AND s.verifyLock <=80 THEN '61-80' WHEN s.verifyLock >=81 AND s.verifyLock <=100 THEN '81-100' WHEN s.verifyLock >=101 AND s.verifyLock <=120 THEN '101-120' WHEN s.verifyLock >=121 AND s.verifyLock <=140 THEN '121-140' WHEN s.verifyLock >=141 AND s.verifyLock <=160 THEN '141-160' WHEN s.verifyLock >=161 THEN '161+' ELSE '1+' END) as verifyTimeRange "
            . " from RestApiBundle:RegistrationStatus s " . $where
            . " GROUP BY verifyTimeRange")
            ->getArrayResult();

        $data = array('0-20' => 0, '21-40' => 0, '41-60' => 0, '61-80' => 0, '81-100' => 0, '101-120' => 0, '121-140' => 0, '141-160' => 0, '161+' => 0);
        foreach ($query as $key => $value) {
            $data[$value['verifyTimeRange']] = $value['RegistrationsCount'];
        }

        //ksort($data);
        echo json_encode($data);
        exit;
    }


    /**
     * View Reports
     *
     * @Route("/__verify_agent_reports", name="__verify_agent_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_agent_reports.html.twig")
     */
    public function __verify_agent_reportsAction()
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Verification Report",
            'title_descr' => "View Reports for Agents Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/__verify_region_reports", name="__verify_region_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_region_reports.html.twig")
     */
    public function __verify_region_reportsAction()
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Verification Region Report",
            'title_descr' => "View Reports for Regions Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/__verify_auditor_reports", name="__verify_auditor_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_auditor_reports.html.twig")
     */
    public function __verify_auditor_reportsAction()
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Auditor | Verifier Report",
            'title_descr' => "View Reports for Auditor | Verifier Registration Performance",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

   /**
     * active_agents Summary
     *
     * @Route("/verify_daily_report/{start_date}/{end_date}/{timestamp}/{pull_all_records}", name="verify_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_daily_reportAction($start_date, $end_date, $timestamp, $pull_all_records) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = ' WHERE 1=1 ';
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if ($start_date != 1) {
            $where .= " AND DATE(r0_.verifyDate) >= '" . date('Y-m-d', strtotime($start_date)) . "' AND DATE(r0_.verifyDate) <= '" . date('Y-m-d', strtotime($end_date)) . "' ";
        }
        $where .= " AND r0_.verifyState in (1, 2, 3, '-2', '-3') ";

        $where .= " AND DATE(r0_.createdDate) >= '" . date('Y-m-d', strtotime('-3 months')) . "'";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        /* $query = $em->createQueryBuilder()
          ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
          . " from RestApiBundle:RegistrationStatus s" . $where
          . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
          ->getQuery();
          $result1 = $query->getResult(); */

        $query1 = "SELECT r0_.id AS sid, COUNT(r0_.id) AS sum_regs, DATE_FORMAT (r0_.verifyDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState "
            . ' FROM RegistrationStatus r0_ '
            . $where . ' GROUP BY created_on, r0_.verifyState ORDER BY created_on DESC';

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'verifyState' ] == 1) {
                $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/__verify_daily_reports", name="__verify_daily_reports")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_daily_reports.html.twig")
     */
    public function __verify_daily_reportsAction()
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Daily Verify Breakdown Report",
            'title_descr' => "View Agents Daily Verify Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }


    /**
     * View Reports
     *
     * @Route("/__verify_daily_reports_system", name="__verify_daily_reports_system")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_daily_reports_system.html.twig")
     */
    public function __verify_daily_reports_systemAction()
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Daily Verify Breakdown Report :: Includes System Verified",
            'title_descr' => "View Agents Daily Verify Breakdown Report :: Includes System Verified",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/__verify_timebreakdown", name="__verify_timebreakdown")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_timebreakdown.html.twig")
     */
    public function __verify_timebreakdownAction()
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Verification Report TimeTaken Breakdown Report",
            'title_descr' => "Verification Report TimeTaken Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/thirdparty_verifications", name="thirdparty_verifications")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:thirdparty_verifications.html.twig")
     */
    public function thirdparty_verificationsAction()
    {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Verification Report",
            'title_descr' => "View Reports for Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/listthirdparty_verifications", name="listthirdparty_verifications")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:thirdparty_verificationslist.html.twig")
     */
    public function listthirdparty_verificationsAction()
    {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Third Party Verification Report",
            'title_descr' => "View Reports for Third Party Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verifyerviewlist/{numRows}/{fields}/{timestamp}", name="verifyerviewlist")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verifyerviewlist.html.twig")
     */
    public function verifyerviewlistAction($numRows, $fields, $timestamp)
    {

        $data['success'] = 1;
        $data['message'] = $fields;

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
            ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
            ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        try {
            if ($configMaster) {
                $configMaster->setName("verifierFieldList");
                $configMaster->setConfig(trim($fields, ":::"));
                $configMaster->setConfigType($numRows);
                $configMaster->setCreatedOn(new \DateTime(date('Y-m-d H:i:s')));
                $em->flush();
            } else {
                $configMaster = new ConfigMaster();
                $configMaster->setName("verifierFieldList");
                $configMaster->setConfig(trim($fields, ":::"));
                $configMaster->setConfigType($numRows);
                $configMaster->setCreatedOn(new \DateTime(date('Y-m-d H:i:s')));

                $em->persist($configMaster);
                $em->flush();
            }
            $data['message'] = "Settings Saved Successfully. Time: " . date('Y-m-d H:i:s');
        } catch (Exception $ex) {
            $data['success'] = 0;
            $data['message'] = "Issue saving config" . $ex;
        }


        echo json_encode($data);
        exit;

        $data = array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verifyRecordsCount/{userId}/{timeStamp}/{randomNumb}", name="verifyRecordsCount")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:active_inactive.html.twig")
     */
    public function verifyRecordsCountAction($userId, $timeStamp, $randomNumb)
    {
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $query = $em->createQueryBuilder()
            ->select("COUNT(s.id) as sum_regs from RestApiBundle:RegistrationStatus s WHERE s.verifyBy = '" . $userId . "' and s.verifyState NOT IN (0,'-1') AND DATE(s.verifyDate) = '" . date('Y-m-d') . "' ")
            ->getQuery();

        $result1 = $query->getResult();

        $data['ccount'] = "";
        $data['ccount'] = 0;

        if ($result1) {
            $data['ccount'] = $result1[0]['sum_regs'];
        }

        echo json_encode($data);
        exit;

        $data = array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/auditRecordsCount/{userId}/{timeStamp}/{randomNumb}/{gettype}", name="auditRecordsCount")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:active_inactive.html.twig")
     */
    public function auditRecordsCountAction($userId, $timeStamp, $randomNumb, $gettype)
    {
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $timefilter = "and DATE(s.auditDate) = '" . date('Y-m-d') . "'";

        if ($gettype == "month") {
            $query_date = date('Y-m-d');

            $timefilter = "and DATE(s.auditDate) >= '" . date('Y-m-01', strtotime($query_date)) . "' AND DATE(s.auditDate) <= '" . date('Y-m-t', strtotime($query_date)) . "'";
        }


        $query = $em->createQueryBuilder()
            ->select("COUNT(s.id) as sum_regs from RestApiBundle:RegistrationStatus s WHERE s.auditBy = '" . $userId . "' AND s.auditState in (1,2,3) " . $timefilter . " ")
            ->getQuery();

        $result1 = $query->getResult();

        $data['ccount'] = "";
        $data['ccount'] = 0;

        if ($result1) {
            $data['ccount'] = $result1[0]['sum_regs'];
        }

        echo json_encode($data);
        exit;

        $data = array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     *  Summary
     *
     * @Route("/thirdPartyVerification/{start_date}/{end_date}/{timestamp}", name="thirdPartyVerification")
     * @Method("GET")
     * @Template()
     */
    public function thirdPartyVerificationAction($start_date, $end_date, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";

        if ($start_date != 1) {
            $where .= " AND date(u.createdDate) >= '" . $start_date . "' AND date(u.createdDate) <= '" . $end_date . "' ";
        }

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
            ->select(" u.id, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as verifiedOn, u.idType, u.resultType, u.timeTaken, u.customerRecordNames, u.customerMsisdn "
                . " from DashboardBundle:Verifications u" . $where . " ORDER BY u.id DESC")
            ->getQuery();

        $result1 = $query->getResult();

        $data = array();
        foreach ($result1 as $key => $value) {
            $data[] = $value;
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verify_daily_reports_detail/{dday}/{sstate}", name="verify_daily_reports_detail")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_daily_reports_detail.html.twig")
     */
    public function verify_daily_reports_detailAction($dday, $sstate)
    {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Records For State " . $this->getVerifyStates($sstate) . " Handled on " . $dday,
            'dday' => $dday,
            'sstate' => $sstate,
            'title_descr' => "View Agents Daily Verify Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/list_verify_daily_report_detail/{dday}/{sstate}/{ccount}/{timestamp}", name="list_verify_daily_report_detail")
     * @Method({"POST","GET"})
     * @Template()
     */
    public function list_detail_verify_daily_reportAction($dday, $sstate, $ccount, $timestamp, Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $attributes = $request->request->all();
            $where = " WHERE 1=1 ";

            $where .= " AND date(x.verifyDate) = '" . $dday . "' ";

            if (in_array($sstate, array('1', '2', '3'))) {
                $where .= " AND x.verifyState = '" . $sstate . "' ";
            }

            $queryAttrib = $request->query->all();

            $queryString = " SELECT u.id,u.msisdn, x.temporaryRegStatus as tregStatus, x.fullRegStatus as fregStatus, x.auditState, x.verifyState, concat(u.firstName, ' ', u.lastName) as customerName, DATE_FORMAT(u.createdDate, '%Y-%m-%d') as created_on, DATE_FORMAT(u.createdDate, '%H:%i:%s') as created_time, u.image_count as image_count, u.identificationType, u.identification, a.username, concat(a.firstName,' ', a.lastName) as agentName, m.mobileNumber as superAgentPhone, concat(m.firstName,' ', m.lastName) as superAgentName, u.region, u.territory "
                . "FROM RestApiBundle:RegistrationStatus x "
                . "LEFT OUTER JOIN x.registrationId u "
                . "LEFT OUTER JOIN u.owner a "
                . "LEFT OUTER JOIN a.parent_id m  " . $where
                . " ORDER BY u.id desc ";
            $query = $em->createQuery($queryString);

            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            //echo $agents = $query->getSql(); exit;
            $agents = $query->getResult();


            if ($ccount == 0) {
                $queryString = " SELECT count(x.id) as ccount "
                    . "FROM RestApiBundle:RegistrationStatus x "
                    . "LEFT OUTER JOIN x.registrationId u "
                    . "LEFT OUTER JOIN u.owner a "
                    . "LEFT OUTER JOIN a.parent_id m  " . $where
                    . " ORDER BY u.id desc ";
                $query = $em->createQuery($queryString);
                //echo $agents = $query->getSql(); exit;
                $resultCount = $query->getResult();

                $ccount = $resultCount[0]['ccount'];
            }

            $entities = $em->getRepository('DashboardBundle:Idtype')->findAll();

            $id_array = array();
            foreach ($entities as $akey => $aval) {
                $id_array[$aval->getId()] = $aval->getName();
            }

            $territory_entiry = $em->getRepository('DashboardBundle:Territory')->findAll();

            $territory_array = array();
            $territory_array['none'] = "";
            foreach ($territory_entiry as $bkey => $bval) {
                $territory_array[$bval->getId()] = $bval->getName();
            }


            $returnData['Result'] = "OK";
            $rowData = array();

            if ($agents) {
                foreach ($agents as $kkey => $vval) {
                    if ($vval["territory"]) {
                        $vval['territory'] = @$territory_array[$vval['territory']];
                    }
                    $vval['temporaryRegStatus'] = $this->getRegStatusDesc($vval['tregStatus']);

                    $vval['aimStatus'] = $this->getAimRegStatusDesc($vval['tregStatus'], $vval['fregStatus'], $vval['image_count']);

                    $vval['icapStatus'] = $this->getIcapRegStatusDesc($vval['tregStatus'], $vval['fregStatus']);

                    $vval['region'] = $this->get_regionName_fromKey($vval['region']);
                    $vval['identificationType'] = $id_array[$vval['identificationType']];
                    $rowData[] = $vval;
                }
            }
            $returnData['Records'] = $rowData;
            $returnData['Record'] = null;
            $returnData['TotalRecordCount'] = $ccount;
            $returnData['Message'] = null;
            $returnData['Options'] = null;

            echo json_encode($returnData);
            exit;

            $resp->setRecords($agents);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/getNidaStatus/{id}/{ttime}", name="getNidaStatus")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registration_info.html.twig")
     */
    public function getNidaStatusAction($id, $ttime)
    {
        $em = $this->getDoctrine()->getManager();
        //----------
        $result = $em->createQuery("SELECT v from DashboardBundle:Verifications v "
            . "WHERE v.customerMsisdn = '" . $id . "' ")
            ->getArrayResult();

        if (count($result) >= 1) {
            $data['success'] = 1;
            $data['data'] = $result[0];
        } else {
            $data['success'] = 0;
        }

        echo json_encode($data);

        exit;
        //-----------
        $data = array(
            'title' => "View Registrations Report:"
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/app_verion_report", name="app_verion_report")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:app_verion_report.html.twig")
     */
    public function app_verion_reportAction()
    {

        $data = array(
            'title' => "App Version Report",
            'title_descr' => "View Day Summary Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/pullapp_verion_report/{region}/{territory}/{timestamp}", name="pullapp_verion_report")
     * @Method("GET")
     * @Template()
     */
    public function pullapp_verion_reportAction($region, $territory, $timestamp)
    {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 AND r.appVersion IS NOT NULL ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }

        if ($region != 'ALL') {
            $where .= " AND u.region = " . $region;
        }
        if ($territory != 'ALL') {
            $where .= " AND u.territory = " . $territory;
        }

        $query = $em->createQueryBuilder()
            ->select("count(r.id) as ccount, r.appVersion "
                . " from RestApiBundle:Device r "
                . " JOIN r.user u " . $where
                . " GROUP BY r.appVersion")
            ->getQuery();

        $result1 = $query->getResult();

        echo json_encode($result1);
        exit;

        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }


    /**
     * active_agents Summary
     *
     * @Route("/pullimage_matrix/{start_date}/{end_date}/{timestamp}", name="pullimage_matrix")
     * @Method("GET")
     * @Template()
     */
    public function pullimage_matrixAction($start_date, $end_date, $timestamp)
    {

        $where = " WHERE 1=1 ";
        $em = $this->getDoctrine()->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

        $where .= " AND DATE(createdOn) >= '" . $start_date . "' ";
        $where .= " AND DATE(createdOn) <= '" . $end_date . "' ";


        $sql = "select count(*) as RecordsCount, PotraitImage, IDFrontImage, IDBackImage, SignatureImage, concat(PotraitImage, IDFrontImage, IDBackImage, SignatureImage) as conc from (select CASE MAX(IF(imageType = 'potrait', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END  AS PotraitImage, CASE MAX(IF(imageType = 'front-pic', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END AS IDFrontImage, CASE MAX(IF(imageType = 'rear-pic', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END AS IDBackImage, CASE MAX(IF(imageType = 'signature', webPath, NULL)) WHEN ISNULL(webPath) THEN '1' ELSE '0' END AS SignatureImage FROM reg_images " . $where . " group by registration) as t group by conc;";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $count = $stmt->fetchAll();

        echo json_encode($count);
        exit;


        return array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
            'entities_user' => $entities_user,
            //'entities' => $entities,
        );
    }

    /**
     * View Reports
     *
     * @Route("/image_matrix", name="image_matrix")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:image_matrix.html.twig")
     */
    public function image_matrixAction()
    {

        $data = array(
            'title' => "Registration Image Matrix",
            'title_descr' => "View Registration Image Matrix",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }


    /**
     * View Reports
     *
     * @Route("/adminuserupload", name="adminuserupload")
     * @Method("GET")
     * @Template()
     */
    public function adminuseruploadAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            //return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Generate Reports",
            'title_descr' => "View Reports for",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }


    /**
     * Add Agents using CVS file.
     *
     * @Route("/admin_users_bulk_upload", name="admin_users_bulk_upload")
     * @Method("POST")
     */
    public function admin_users_bulk_uploadAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        $msg = "";
        try {


            $file = $request->files->get('fileLocation');

            $fileName = $file->getClientOriginalName();

            $ext = pathinfo($fileName)["extension"];
            if (strcmp($ext, "csv") == 0) {
                $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "agent_uploads" . DIRECTORY_SEPARATOR . date("Ymd");
                $newFileName = date("YmdHi") . $fileName;
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $newFileName);

                $apiHelper = $this->container->get('api.helper');
                //echo "filename: " . $uploadedURL . "/" . $newFileName;
                $data = $apiHelper->parseCSV($uploadedURL . "/" . $newFileName);

                foreach ($data as $key => $val) {
                    $status = $this->container->get('users.helper')->createAdminUser($val);
                    if ($status == UsersHelper::EXISTS) {
                        $msg .= $val['msisdn'] . " ALREADY EXITS <br />";
                    } elseif ($status == UsersHelper::NOSUPERAGENT) {
                        $msg .= $val['msisdn'] . " NOSUPERAGENT: " . $val['superagent'] . " <br />";
                    } elseif ($status == UsersHelper::NOREGION) {
                        $msg .= $val['msisdn'] . " NOREGION: " . $val['region'] . " <br />";
                    } elseif ($status == UsersHelper::NOPOSNAME) {
                        $msg .= $val['msisdn'] . " NOPOSNAME <br />";
                    } elseif ($status == UsersHelper::NOROUTENAME) {
                        $msg .= $val['msisdn'] . " NOROUTENAME <br />";
                    } elseif ($status == UsersHelper::NOOWNERNAME) {
                        $msg .= $val['msisdn'] . " NOOWNERNAME <br />";
                    } elseif ($status == UsersHelper::NOMANAGERNAME) {
                        $msg .= $val['msisdn'] . " NOMANAGERNAME <br />";
                    } elseif ($status == UsersHelper::NOTERRITORY) {
                        $msg .= $val['msisdn'] . " NOTERRITORY: " . $val['territory'] . " <br />";
                    } elseif ($status == UsersHelper::SUCCESS) {
                        $msg .= $val['msisdn'] . " CREATED SUCCESSFULLY <br />";
                    } else {
                        $msg .= $val['msisdn'] . " FAILED. ERROR OCCURED <br />";
                    }
                }

                $resp->setMessage($msg);

                $this->logUserEvent(UserLog::BULK_AGENTS_UPLOAD, "bulk agents upload", array("file" => $fileName, 'status' => 'success'));
            } else {
                $resp->setMessage("Invalid File Type");
                $this->logUserEvent(UserLog::BULK_AGENTS_UPLOAD, "bulk agents upload", array("file" => $fileName, 'status' => 'failed'));
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }


}
