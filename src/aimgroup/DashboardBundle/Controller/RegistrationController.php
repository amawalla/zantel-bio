<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\RestApiBundle\Dao\ImagesOperatorStatus;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class RegistrationController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("admin/registration")
 */
class RegistrationController extends AbstractController
{

    var $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * View Reports
     *
     * @Route("/", name="admin/registration")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registrations.html.twig")
     */
    public function indexAction()
    {
        //var_dump(json_decode($this->session->get('user_role_perms')));
        if (!in_array(2, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        //$attributes = json_decode($request->getContent(), true);
        $request = $this->getRequest();

        $path = $request->get('path');
        $date = $request->get('date');

        $title = $this->getTitle($path, $date);
        $data = array(
            'title' => $title . ":",
            'title_descr' => $title,
            'idtype' => $idtype,
            'hide_search_class' => ($path != null ? 'hide_search' : '')
        );

        return $this->prepareResponse($data);
    }

    private function getTitle($path, $date)
    {
        if ($path) {
            if ($path == "treg") {
                return "Temporary Registrations" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "freg") {
                return "Fully Registrations" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "tobesent") {
                return "Waiting to be Sent" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "pending") {
                return "Pending Registrations" . ($date ? " (" . $date . ")" : "");
            } else if ($path == "declined") {
                return "Declined Registrations" . ($date ? " (" . $date . ")" : "");
            }
        }
        return "View Registrations Report:";
    }

    /**
     * View User Role verify_registrations
     *
     * @Route("/verify_registrations", name="admin/verify_registrations")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:verify_registrations.html.twig")
     */
    public function verify_registrationsAction()
    {
        if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
            ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
            ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(":::", $configMaster->getConfig());

            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }
        //--

        $data = array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'idtype' => $idtype,
            'numRows' => $numRows,
            'secsCount' => $secsCount,
            'user_id' => $user->getId()
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/registrationaudit", name="admin/registrationaudit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:registrationaudit.html.twig")
     */
    public function registrationauditAction()
    {
        if (!in_array(50, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        $data = array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'idtype' => $idtype,
            'user_id' => $user->getId()
        );

        return $this->prepareResponse($data);
    }

    /**
     * get a list of all the Regions
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list",name="list_registrations_elastic")
     *
     * @return JsonResponse
     */
    public function listRegistrationsAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            $attributes = $attributes + $request->query->all();

            $results = $this->container->get("fos_elastica.manager")
                ->getRepository("RestApiBundle:RegistrationStatus")->listRegistrationStatus($attributes, $attributes["jtPageSize"], $attributes["jtStartIndex"] + 1);

            $registrationsArray = array();
            /** @var  $result RegistrationStatus */
            foreach ($results["results"] as $result) {
                $regOut = array();
                /** @var  $registration Registration */
                $registration = $result->getRegistrationid();
                $user = $registration->getOwner();
                $regOut["tregStatus"] = $result->getTemporaryRegStatus();
                $regOut["fullRegStatus"] = $result->getFullRegStatus();
                $regOut["customerName"] = $registration->getFirstName() . " " . $registration->getLastName();
                $regOut["image_count"] = $registration->getImage_count();
                $regOut["region"] = $registration->getTerritory()->getRegion()->getName();
                $regOut["territory"] = $registration->getTerritory()->getName();
                $regOut["created_on"] = $registration->getCreatedDate()->format('Y-m-d H:i:s');
                $regOut["msisdn"] = $registration->getMsisdn();
                $regOut["type"] = $registration->getRegistrationType() == 1 ? 'Standard' : 'Biometric';
                $regOut["mobileNumber"] = $registration->getAgentMsisdn();
                $regOut["identificationType"] = $registration->getIdentificationType()->getName();
                $regOut["id"] = $registration->getId();
                $regOut["agentName"] = $user->getFirstName() . " " . $user->getLastName();
                $regOut["aimStatus"] = $this->getAimRegStatusDesc($result->getTemporaryRegStatus(), $result->getFullRegStatus(), $registration->getImageCount());
                $regOut["gkycStatus"] = ImagesOperatorStatus::getErrorDescription($result->getFullRegStatus());
                array_push($registrationsArray, $regOut);
            }
            $resp->setRecords($registrationsArray);
            if (isset($attributes["jtPageSize"])) {
                $resp->setTotalRecordCount(round($results["count"] / $attributes["jtPageSize"]));
            } else {
                $resp->setTotalRecordCount($results["count"]);
            }

        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    private function getAimRegStatusDesc($tstatus, $fstatus, $imageCount)
    {
        $ret = "Pending";
        if (in_array($tstatus, array('1', '2', '6')) || in_array($fstatus, array('1', '2', '6', '7'))) {
            $ret = "Submitted";
        } else if (in_array($tstatus, array('3', '4')) || in_array($fstatus, array('3', '4'))) {
            $ret = "Declined";
        } else if ($imageCount >= 3) {
            $ret = "Received";
        }
        return $ret; // .$tstatus . $fstatus;
    }

}
