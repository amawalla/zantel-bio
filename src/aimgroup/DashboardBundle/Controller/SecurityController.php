<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Security;

/**
 * This SystemsConfigurationsController is used to controll how system users login into the system
 *
 * @author Michael Tarimo
 *
 */
class SecurityController extends Controller {

    /**
     * @Route("/admin/login", name="login_route")
     */
    public function loginAction(Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

       
        
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        
        $forgot_passurl = $this->container->get('router')->generate('forgot_password');

        return $this->render('security/login.html.twig', array('forgot_passurl' => $forgot_passurl, 'last_username' => $lastUsername, 'error' => $error,));
    }
    
    
    
    /**
     * Registration Summary
     *
     * @Route("/admin/forgotPass", name="forgotPass_route")
     */
    public function forgotPassAction(Request $request) {
        $authenticationUtils = $this->get('security.authentication_utils');

       
        
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/forgotPass.html.twig', array('last_username' => $lastUsername, 'error' => $error,));
    }
    

    /**
     * @Route("/admin/login_check", name="login_check")
     */
    public function loginCheckAction() {
        // this controller will not be executed,
        // as the route is handled by the Security system
    }

}
