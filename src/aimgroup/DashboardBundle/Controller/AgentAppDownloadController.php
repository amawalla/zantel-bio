<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Entity\AppUpload;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AgentAppDownloadController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("/appdownload")
 *
 */
class AgentAppDownloadController extends Controller {

    public function indexAction($name) {
        return $this->render('', array('name' => $name));
    }

    /**
     * Android App download Page
     *
     * @Route("/", name="app_download_")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:app_download.html.twig")
     */
    public function app_downloadAction(Request $request) {


        $em = $this->container->get('doctrine')->getManager();
        try {
            $query = $em->createQueryBuilder()->select("p")->from("RestApiBundle:Role", "p")->getQuery();
            $results = $query->getArrayResult();

            /** @var $result AppUpload */
            $result = $em->getRepository("DashboardBundle:AppUpload")->createQueryBuilder("a")
                    ->select("a.fileLocation")
                    ->orderBy("a.createdDate", "DESC")
                    ->getQuery()
                    ->setMaxResults(1)
                    ->getSingleResult();
            $url = "";
            if ($result) {
                $this->container->get("api.helper")->logInfo("AgentAppDownloadController", "app_downloadAction", array("result" => $result));
                $url = str_replace($this->container->getParameter("resource_path"), $this->container->getParameter("hostname") . "/euploads", $result["fileLocation"]);
            }
        } catch (\Doctrine\ORM\NoResultException $e) {
	  echo $e->getMessage();
            return null;
        }

        return array(
            'title' => "View User Roles Listing:",
            'title_descr' => "Listing for user permission roles departments ",
            'user_roles' => $results,
            'entities' => $url,
        );
    }

    /**
     * View resetrecordlock
     *
     * @Route("/resetrecordlock/{type}/{timestamp}", name="resetrecordlock")
     * @Method("GET")
     * @Template()
     */
    public function rectifyImageCountAction($type, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('NOW', 'DoctrineExtensions\Query\Mysql\Now');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('TIMESTAMPDIFF', 'DoctrineExtensions\Query\Mysql\TimestampDiff');

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
                ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(":::", $configMaster->getConfig());

            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }
        //--

        $data2 = $em->createQuery("SELECT r.id, r." . $type . "State, DATE_FORMAT(r." . $type . "Date, '%Y-%m-%d %H:%i:%s') as ddate "
                        . " from RestApiBundle:RegistrationStatus r "
                        . " WHERE r." . $type . "State = '-1'")
                ->getArrayResult(); //getSingleResult();

        $reset = $not_reset = array();
        foreach ($data2 as $key => $value) {
            if ($type == "audit") {
                if ((strtotime(date('Y-m-d H:i:s')) - strtotime(date($value['ddate']))) > 90) {
                    $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.auditState = 0, u.auditBy = NULL, u.auditDescr = '', u.auditDate = '0000-00-00 00:00:00' WHERE u.id = " . $value['id'] . " ")
                            ->getArrayResult();
                    $reset[] = $value;
                } else {
                    $not_reset[] = $value;
                }
            }

            if ($type == "verify") {
                if ((strtotime(date('Y-m-d H:i:s')) - strtotime(date($value['ddate']))) > 90) {
                    $results = $em->createQuery("UPDATE RestApiBundle:RegistrationStatus u SET u.verifyState = 0, u.verifyBy = NULL, u.verifyDescr = '', u.verifyDate = '0000-00-00 00:00:00' WHERE u.id = " . $value['id'] . " ")
                            ->getArrayResult();
                    $reset[] = $value;
                } else {
                    $not_reset[] = $value;
                }
            }
        }

        echo "---" . count($reset) . "----RESET-----<br /> <pre>";
        print_r($reset);
        echo "</pre>";

        echo "---" . count($not_reset) . "----NOT RESET-----<br /> <pre>";
        print_r($not_reset);
        echo "</pre>";

        exit;

        $data = array(
            'title' => "View Reports for:",
            'title_descr' => "View Reports for",
                //'entities' => $entities,
        );

        return $this->prepareResponse($data);
    }

}
