<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Territory;
use aimgroup\DashboardBundle\Form\TerritoryType;

/**
 * Territory controller.
 *
 * @Route("/territory")
 */
class TerritoryController extends Controller
{

    /**
     * Lists all Territory entities.
     *
     * @Route("/", name="territory")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Territory')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Territory entity.
     *
     * @Route("/", name="territory_create")
     * @Method("POST")
     * @Template("DashboardBundle:Territory:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Territory();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('territory_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Territory entity.
     *
     * @param Territory $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Territory $entity)
    {
        $form = $this->createForm(new TerritoryType(), $entity, array(
            'action' => $this->generateUrl('territory_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Territory entity.
     *
     * @Route("/new", name="territory_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Territory();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Territory entity.
     *
     * @Route("/{id}", name="territory_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Territory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Territory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Territory entity.
     *
     * @Route("/{id}/edit", name="territory_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Territory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Territory entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Territory entity.
    *
    * @param Territory $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Territory $entity)
    {
        $form = $this->createForm(new TerritoryType(), $entity, array(
            'action' => $this->generateUrl('territory_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Territory entity.
     *
     * @Route("/{id}", name="territory_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Territory:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:Territory')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Territory entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('territory_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Territory entity.
     *
     * @Route("/{id}", name="territory_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:Territory')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Territory entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('territory'));
    }

    /**
     * Creates a form to delete a Territory entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('territory_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
