<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\UserType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;
use aimgroup\RestApiBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use aimgroup\RestApiBundle\Form\UserTypes;

/**
 * This ManageUsersController is used to manage system users.
 *
 *
 * @Route("/admin/users")
 */
class ManageUsersController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * get a list of all the User Types
     * @param Request $request
     * @Method("POST")
     * @Route("/get_accountTypes",name="get_accountTypes")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function getAccountTypes(Request $request) {

        $resp = new JsonObject();
        $resp->setMessage("SUCCESS");
        $status = false;
        $resp->setItem("{}");

        try {
            $attributes = json_decode($request->getContent(), true);
            $msisdn = $attributes['msisdn'];
            $access_token = $attributes['access_token'];
            $account_type = $attributes['account_type'];

            $em = $this->getDoctrine()->getManager();
            $query = $em->createQuery('SELECT u FROM RestApiBundle:User u WHERE u.mobileNumber = :mobileNumber')
                    //->setParameter('mobileNumber', $msisdn)
                    ->setParameter('mobileNumber', $msisdn)//accessToken
                    ->setMaxResults(1);

            $user = $query->getOneOrNullResult(); //getSingleResult();

            if (null != $user) {
                $device = $em->getRepository("RestApiBundle:Device")->findOneBy(['user' => $user->getId(), 'accessToken' => $access_token]);

                if (NULL != $device) {
                    $resp->setItem($device->getAccessToken());
                } else {
                    $resp->setMessage("DEVICE NOT FOUND");
                }
            } else {
                $resp->setMessage("USER NOT FOUND");
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     *
     *
     * @Route("/", name="admin/users")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $data = array(
            'title' => "Manage System Admin Users",
            'title_descr' => "List, create, delete, activate System Admins",
            'users' => $this->loadUsers(''),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/sys_admin", name="sys_admin")
     * @Method("GET")
     * @Template("DashboardBundle:ManageUsers:index.html.twig")
     */
    public function loadSystemAdminsAction(Request $request) {
        if (!in_array(36, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $result = $em->createQuery("SELECT p.role as role_name "
                        . "from RestApiBundle:Role p")
                ->getArrayResult();

        $data = array(
            'title' => "Manage System Admin Users",
            'title_descr' => "List, create, delete, activate System Admins",
            'users' => $this->loadUsers('1'),
            'role_names' => $result
        );

        return $this->prepareResponse($data);
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/managers", name="managers")
     * @Method("GET")
     * @Template("DashboardBundle:ManageUsers:index.html.twig")
     */
    public function loadManagerUsersAction(Request $request) {
        return array(
            'title' => "Manage Managers",
            'title_descr' => "List, create, delete, activate System Admins",
            'users' => $this->loadUsers('ROLE_ADMIN'),
        );
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/super_agents", name="super_agents")
     * @Method("GET")
     * @Template("DashboardBundle:ManageUsers:index.html.twig")
     */
    public function loadSuperAgentsAction(Request $request) {
        if (!in_array(37, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $result = $em->createQuery("SELECT p.name as role_name "
                        . "from RestApiBundle:Role p")
                ->getArrayResult();

        return array(
            'title' => "Manage Super Agents",
            'title_descr' => "List, create, delete, activate System Admins",
            'users' => $this->loadUsers('ROLE_SUPERAGENT'),
            'role_names' => $result
        );
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/qa", name="qa")
     * @Method("GET")
     * @Template("DashboardBundle:ManageUsers:index.html.twig")
     */
    public function loadQAUsersAction(Request $request) {
        return array(
            'title' => "Manage QA Users",
            'title_descr' => "List, create, delete, activate System Admins",
            'users' => $this->loadUsers(''),
        );
    }

    private function loadUsersOrg($userType) {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $users = $queryBuilder->select('u')
                ->from('RestApiBundle:User', 'u')
                ->where($queryBuilder->expr()->like('u.roles', ':userType'))//('u.type = :userType ')
                ->setParameter('userType', '%' . $userType . '%')
                ->getQuery()
                ->getArrayResult();

        return $users;
    }

    private function loadUsers($userType) {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $users = $queryBuilder->select('u')
                ->from('RestApiBundle:User', 'u')
                ->where($queryBuilder->expr()->like('u.roles', ':userType'))//('u.type = :userType ')
                ->setParameter('userType', '%' . $userType . '%')
                ->getQuery()
                ->getArrayResult();

        return $users;
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/edit", name="user_edit")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:useredit.html.twig")
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $query = $em->createQueryBuilder()
                ->select("p")
                ->from("RestApiBundle:Role", "p")
                ->getQuery();
        try {
            $results = $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        $data = array(
            'title' => "Edit User",
            'title_descr' => "Create New Frequently Asked Questions",
            'userid' => $id,
            'user_roles' => $results,
            'entity' => $entity
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to edit a User entity.
     *
     * @param User $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(User $entity) {
        $form = $this->createForm(new UserTypes(), $entity, array(
            'action' => $this->generateUrl('user_update', array('id' => $entity->getId())),
            'method' => 'PUT'
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing User entity.
     *
     * @Route("/{id}", name="user_update")
     * @Method("PUT")
     * @Template("DashboardBundle:Admin:useredit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('user_edit', array('id' => $id)));
        }

        $data = array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Deletes a User entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:User')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find User entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('faq'));
    }

    /**
     * get a list of all the User Types
     * @param Request $request
     * @Method({"GET","POST"})
     * @Route("/list_userTypes",name="list_userTypes")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function listUserTypes(Request $request) {

        $resp = new JTableResponse();
        try {
            $query = $this->getDoctrine()->getEntityManager()
                    ->createQuery(
                    'SELECT u.id as Value,u.description as DisplayText
                     FROM DashboardBundle:UserType u'
            );
            $userTypes = $query->getResult();
            $resp->setOptions($userTypes);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Load existing login configurations for the mobile App. This incluses password regex
     *
     * @Route("/load_login", name="load_login")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function createUserAction(Request $request) {
        
    }

    /**
     * Load existing messages configured for the mobile App.
     *
     * @Route("/load_messages", name="load_messages")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function updateUserAction(Request $request) {
        
    }

    /**
     * Load ID Configured and allowed  to be used in the mobile App.
     *
     * @Route("/load_messages", name="load_messages")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function activateUserAction(Request $request) {
        
    }

    /**
     * Load images configurations to be used in the mobile App.
     *
     * @Route("/load_messages", name="load_messages")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function deactivateUserAction(Request $request) {
        
    }

    /**
     * Add a new Manager.
     *
     * @Route("/createManagers", name="createManagers")
     * @Method("POST")
     */
    public function createManagersAction(Request $request) {

        $resp = new JsonObject();
        $status = false;
        $message = "ERROR SUBMITING REQUEST";
        try {
            $attributes = json_decode($request->getContent(), true);

//            echo "<pre>";
//            print_r($attributes);
//            echo "</pre>"; exit;           

            if ($attributes) {

                $em = $this->getDoctrine()->getManager();

                $user = new User();
                $user->setFirstName($attributes['firstName']);
                $user->setLastName($attributes['lastName']);
                $user->setMobileNumber(substr($attributes['msisdn'], -9));
                $user->setAdminRole($attributes['adminRole']);
                $user->setAdminDepartment($attributes['adminDepartment']);


                $isExists = $em->getRepository("RestApiBundle:User")->isMobileExists($user->getMobileNumber());

                //echo "1213123 123 12312"; exit(); 

                if ($isExists) {
                    $status = false;
                    $message = "MOBILE NUMBER ALREADY EXITS";
                } else {
                    $user->setUserType($attributes["userType"]);
                    $user->setUsername($attributes["username"]); //$user->getMobileNumber()
                    $user->setEnabled(true);
                    $user->setSalt(rand(1000, 9999));
                    $user->setNumberDevices(1); //$attributes["number_devices"]
                    $user->setAgentExclusivity($attributes["agent_exclusivity"]);
                    $user->setDob(new \DateTime($attributes["dob"]));
                    $user->setStatus(true);
                    //$user->setRoles(array("ROLE_ADMIN"));
                    $user->setRoles(array($attributes['user_roles']));

                    /** optional fields */
                    if (isset($attributes["agentCode"]))
                        $user->setAgentCode($attributes['agentCode']);
                    if (isset($attributes['email'])) {
                        $user->setEmail($attributes['email']);
                        $user->setEmailCanonical($user->getEmail());
                    }

                    /** end optional fields */
                    /*                     * *******
                      $region = $em->getRepository('DashboardBundle:Region')->findOneBy(array("id" => $attributes["region"]));
                      if ($region) {
                      $user->setRegion($region);
                      }

                      $territory = $em->getRepository('DashboardBundle:Territory')->findOneBy(array("id" => $attributes["territory"]));
                      if ($territory) {
                      $user->setTerritory($territory);
                      }
                     * ********* */

                    $user->setPassword($attributes['password']);

                    $encoder = $this->container->get('security.password_encoder');
                    $encoded = $encoder->encodePassword($user, $user->getPassword());
                    $user->setPassword($encoded);
                    $user->setStatus(1);

                    $em->persist($user);

                    $em->flush();
                    $status = true;
                    $message = "SUCCESS ful";
                }
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Load a list of system administrators.
     *
     * @Route("/admin_users/{userGroup}", name="admin_users")
     * @Method("GET")
     * @Template("DashboardBundle:ManageUsers:admin_users.html.twig")
     */
    public function AdminUsersAction($userGroup) {
        if (!in_array(36, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $result = $em->createQuery("SELECT p.name as role_name "
                        . "from RestApiBundle:Role p")
                ->getArrayResult();

        $data = array(
            'title' => "Manage " . $userGroup,
            'title_descr' => "List, create, delete, activate ",
            'users' => $this->loadUsers($userGroup),
            'role_names' => $result
        );

        return $this->prepareResponse($data);
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/{id}/view_agents", name="view_agents")
     * @Method("GET")
     * @Template("DashboardBundle:ManageUsers:view_agents.html.twig")
     */
    public function viewAgentsAction($id, Request $request) {

        $em = $this->getDoctrine()->getManager();

        $users = $em->createQuery("SELECT u FROM RestApiBundle:User u"
                        . " WHERE u.parent_id = " . $id)->getResult();

        $data = array(
            'title' => "View Agents: " . $request->get('agentfName') . " " . $request->get('agentlName'),
            'title' => "View Agents: " . $request->get('agentfName') . " " . $request->get('agentlName'),
            'title_descr' => "Agents List",
            'users' => $users,
            'id' => $id
        );

        return $this->prepareResponse($data);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/addVodashopAgent", name="addVodashopAgent")
     * @Method("POST")
     */
    public function addVodashopAgentAction(Request $request) {

        $resp = new JsonObject();
        $status = false;
        $message = array();
        try {
            $attributes = json_decode($request->getContent(), true);

            if (@$attributes['agent_msisdn'] || @$attributes['agent_name']) {

                $where = "WHERE 1=1";

                //{"agent_msisdn":"11111","agent_name":"2222","rec_id":13}:
                if (@$attributes['agent_msisdn']) {
                    $where.=" AND u.mobileNumber = '" . $attributes['agent_msisdn'] . "'";
                }
                if (@$attributes['agent_name']) {
                    $where.= " AND u.firstName = '" . $attributes['agent_name'] . "'";
                    $where.= " OR u.lastName = '" . $attributes['agent_name'] . "'";
                }

                $em = $this->getDoctrine()->getManager();
                $result = $em->createQuery("SELECT u FROM RestApiBundle:User u " . $where)
                        ->getArrayResult();

                $removeKeys = array('password', 'salt', 'dob', 'lastLogin', 'usernameCanonical', 'passwordRequestedAt', 'confirmationToken', 'credentialsExpireAt', 'credentialsExpired');

                foreach ($removeKeys as $key) {
                    unset($result[0][$key]);
                }

                echo $message = json_encode($result);
                exit;
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * assign agent to superagent/vodashop/teamleader 
     * and return agentInfo in json to be appended in table if need be
     *
     * @Route("/assignToLeader/{agent_id}/{super_agent}/{field}/{timestamp}", name="assignToLeader")
     * @Method("GET")
     * @Template()
     */
    public function assignToLeaderAction($agent_id, $super_agent, $field, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $superAgent = $em->getRepository('RestApiBundle:User')->find($super_agent);

        if (!$superAgent) {
            throw $this->createNotFoundException('Unable to find Territory entity.');
        }

        $user = $em->getRepository('RestApiBundle:User')->find($agent_id);

        if (!$user) {
            throw $this->createNotFoundException('Unable to find Territory entity.');
        }

        $user->setParentId($superAgent);
        $em->flush();

        //kwanza get the user
        $query = $em->createQueryBuilder()
                ->select("u from RestApiBundle:User u WHERE u.id = '" . $agent_id . "' ")
                ->setMaxResults(1)
                ->getQuery();

        $data = $query->getArrayResult();
        $data2 = $data[0];
        
        $removeKeys = array('password', 'salt', 'dob', 'lastLogin', 'usernameCanonical', 'passwordRequestedAt', 'confirmationToken', 'credentialsExpireAt', 'credentialsExpired');

        foreach ($removeKeys as $key) {
            unset($data2[$key]);
        }

        echo json_encode($data2);
        exit;
    }

}
