<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
//use Mosquitto\Client;
use Mosquitto\Client;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;
use Doctrine\ORM\Query\ResultSetMapping;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * This AppConfigurationsController is used to manage configuration which are supposed to be sent to mobile devices.
 *
 * @Route("admin/app_config")
 */
class AppConfigurationsController extends AbstractController {

    var $session;
    public function __construct() {
        $this->session = new Session();
    }
    
    /**
     * Lists all Anto entities.
     *
     * @Route("/", name="admin/app_config")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Anto')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Load existing Dynamic Form for the mobile App.
     *
     * @Route("/load_form", name="load_form")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:dynamic_form.html.twig")
     */
    public function loadDynamicFormAction(Request $request) {

        $em = $this->getDoctrine()->getManager();

        //$rsm = new ResultSetMapping();
        //$query = $em->createNativeQuery("select Column_name from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='registration'", $rsm);

        $dataColumns = $em->getConnection()->getSchemaManager()->listTableColumns('registration');

        //$dataColumns = $query->getResult();
        //var_dump($dataColumns);

        return array(
            'title' => "Dynamic Form Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'dataColumns' => $dataColumns
        );
    }

    /**
     * Save existing Dynamic Form ready for publishing
     * @param Request $request
     * @Route("/save_form",name="save_form")
     * @Method("POST")
     *
     */
    public function saveDynamicFormAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $parameters = json_decode($request->getContent(), true);
            $em = $this->getDoctrine()->getManager();

            $configMaster = new ConfigMaster();
            $configMaster->setConfig($request->getContent());
            $configMaster->setName("forms");
            $configMaster->setCreatedOn(new \DateTime());
            $configMaster->setConfigType("formConfig");
            $em = $this->getDoctrine()->getManager();
            /** @var  $configuration ConfigMaster */
            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                    array("configType" => "formConfig")
            );
            if (!$configuration) {
                $em->persist($configMaster);
            } else {
                $configuration->setConfig($configMaster->getConfig());
                $configuration->setVersion($configuration->getVersion() + 1);
            }
            $em->flush();
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/display_form",name="display_form")
     * @Method("GET")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function displaySavedDynamicFormAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findBy(
                    array("configType" => "formConfig")
            );

            $resp->setItem($configurations);
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish_configuration",name="publish_configuration")
     */
    public function publishConfiguration(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $json = json_decode($request->getContent(), true);
            $topic = $json['topic'];
            $qos = 2;


            \Mqtt_Mqtt::load();
            $conn = new \Mqtt();
            $conn->Connect(SAM_MQTT, array(SAM_HOST => $this->container->getParameter("mqtt"), SAM_PORT => 1883));

            /** @var  $message \SAMMessage */
            $message = new \SAMMessage($request->getContent());
            $sendMsg = $conn->Send("topic://" . $topic, $message, array(SAM_MQTT_QOS => $qos));
            $resp->setMessage("SUCCESFULLY PUBLISHED");
            $status = true;
            $conn->Disconnect();

            $resp->setItem($sendMsg);
            $resp->setMessage("SUCCESFULLY PUBLISHED");
            $status = true;
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/tpublish",name="tpublish")
     */
    public function testPublish(Request $request) {
        $resp = new JsonObject();
        $status = false;
//        try{
        $topic = "moat";
        $qos = 2;

        \Mqtt_Mqtt::load();
        $conn = new \Mqtt();
        $conn->Connect(SAM_MQTT, array(SAM_HOST => $this->container->getParameter("mqtt"), SAM_PORT => 1883));

        /** @var  $message \SAMMessage */
        $message = new \SAMMessage("hello");
        $sendMsg = $conn->Send("topic://moat", $message, array());
        $resp->setMessage("SUCCESFULLY PUBLISHED");
        $status = true;
//        } catch (\Exception $e) {
//            var_dump($e->);
//            $resp->setMessage($e->getMessage());
//        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * get a list of all the topics you can publish too, generic and each device id
     * @param Request $request
     * @Method("GET")
     * @Route("/get_topics",name="get_topics")
     * @return \aimgroup\DashboardBundle\Controller\JsonResponse
     */
    public function getTopicList(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();
            $results = $em->getRepository("RestApiBundle:Device")
                    ->createNamedQuery("deviceWithDeviceId")
                    ->getResult();
            $resp->setItem($results);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Load existing login configurations for the mobile App. This incluses password regex
     *
     * @Route("/load_login_config", name="load_login_config")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:login_configurations.html.twig")
     */
    public function loadLoginConfigAction(Request $request) {

        if (!in_array(30, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        $configurations = array(); // pull from db

        $data = array(
            'title' => "Login Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'configurations' => $configurations
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * Load existing messages configured for the mobile App.
     *
     * @Route("/load_messages", name="load_messages")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function loadMessagesConfigAction(Request $request) {
        
    }

    /**
     * Load ID Configured and allowed  to be used in the mobile App.
     *
     * @Route("/load_id_config", name="load_id_config")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:id_configurations.html.twig")
     */
    public function loadIDsConfigAction(Request $request) {
        if (!in_array(31, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $em = $this->getDoctrine()->getManager();
        $ids = $em->getRepository('DashboardBundle:Idtype')->findAll(); // pull from db

        return array(
            'title' => "ID Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'ids' => $ids
        );
    }

    /**
     * Load images configurations to be used in the mobile App.
     *
     * @Route("/load_image_config", name="load_image_config")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:image_configurations.html.twig")
     */
    public function loadImagesConfigAction(Request $request) {

        if (!in_array(32, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $configurations = array(); // pull from db

        $data = array(
            'title' => "Image Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'configurations' => $configurations
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * Save existing Dynamic Form ready for publishing
     * @param Request $request
     * @Route("/save_image_config",name="save_image_config")
     * @Method("POST")
     *
     */
    public function saveImageConfigAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {

            $configMaster = new ConfigMaster();
            $configMaster->setConfig($request->getContent());
            $configMaster->setName("imageConfig");
            $configMaster->setCreatedOn(new \DateTime());
            $configMaster->setConfigType("imageConfig");
            $em = $this->getDoctrine()->getManager();
            /** @var  $configuration ConfigMaster */
            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                    array("configType" => "imageConfig")
            );
            if (!$configuration) {
                $em->persist($configMaster);
            } else {
                $configuration->setConfig($configMaster->getConfig());
                $configuration->setVersion($configuration->getVersion() + 1);
            }
            $em->flush();
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Save existing LOgin Form ready for publishing
     * @param Request $request
     * @Route("/save_login_config",name="save_login_config")
     * @Method("POST")
     *
     */
    public function saveLoginConfigAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {

            $configMaster = new ConfigMaster();
            $configMaster->setConfig($request->getContent());
            $configMaster->setName("loginConfig");
            $configMaster->setCreatedOn(new \DateTime());
            $configMaster->setConfigType("loginConfig");
            $em = $this->getDoctrine()->getManager();
            /** @var  $configuration ConfigMaster */
            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                    array("configType" => "loginConfig")
            );
            if (!$configuration) {
                $em->persist($configMaster);
            } else {
                $configuration->setConfig($configMaster->getConfig());
                $configuration->setVersion($configuration->getVersion() + 1);
            }
            $em->flush();
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * List configurations
     *
     * @Route("/list_image_config", name="list_image_config")
     * @Method("GET")
     *
     */
    public function listImageConfigurationsAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findBy(
                    array("configType" => "imageConfig")
            );

            $resp->setItem($configurations);
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * List Login configurations
     *
     * @Route("/list_login_config", name="list_login_config")
     * @Method("GET")
     *
     */
    public function listLoginConfigurationsAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $em = $this->getDoctrine()->getManager();

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findBy(
                    array("configType" => "loginConfig")
            );

            $resp->setItem($configurations);
            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

//
//    /**
//     * Load existing Dynamic Form for the mobile App.
//     *
//     * @Route("/update_form", name="update_form")
//     * @Method("POST")
//     * @Template("DashboardBundle:Anto:new.html.twig")
//     */
//    public function updateDynamicFormAction(Request $request)
//    {
//
//    }

    /**
     * Load existing login configurations for the mobile App. This incluses password regex
     *
     * @Route("/update_login", name="update_login")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function updateLoginConfigAction(Request $request) {
        
    }

    /**
     * Load existing messages configured for the mobile App.
     *
     * @Route("/load_messages", name="load_messages")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function updateMessagesConfigAction(Request $request) {
        
    }

    /**
     * Load ID Configured and allowed  to be used in the mobile App.
     *
     * @Route("/update_id", name="update_id")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function updateIDsConfigAction(Request $request) {
        
    }

    /**
     * Load images configurations to be used in the mobile App.
     *
     * @Route("/update_images", name="update_images")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function updateImagesConfigAction(Request $request) {
        
    }
    
    
    /**
     * Load ID Configured and allowed  to be used in the mobile App.
     *
     * @Route("/id_verification", name="id_verification")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:id_verification.html.twig")
     */
    public function id_verificationAction(Request $request) {
        if (!in_array(48, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $ids = array(); // pull from db

        $data = array(
            'title' => "ID Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'ids' => $ids
        );
        
        return $this->prepareResponse($data);
    }
    
    
    /**
     * Load ID Configured and allowed  to be used in the mobile App.
     *
     * @Route("/verifier_view_fields", name="verifier_view_fields")
     * @Method("GET")
     * @Template("DashboardBundle:AppConfigurations:verifier_view_fields.html.twig")
     */
    public function verifier_view_fieldsAction(Request $request) {
        if (!in_array(48, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $configArray = array();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'verifierFieldList' ")
                ->getQuery();

        $configMaster = $query->getOneOrNullResult();
        
        $numRows = 1;
        if($configMaster){
            $configArray = explode(":::", $configMaster->getConfig());
            
            $settingMerged = explode(":::", $configMaster->getConfigType());
            
            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }
        
        $data = array(
            'configArray' => json_encode($configArray),
            'numRows' => $numRows,
            'secsCount' => $secsCount,
            'title' => "Verifier View Fields",
            'title_descr' => "List, create, delete, activate System Admins"
        );
        return $this->prepareResponse($data);
    }
    
    
    


}
