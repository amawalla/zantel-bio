<?php
/**
 * Created by PhpStorm.
 * User: Roby
 * Date: 1/18/18
 * Time: 2:34 PM
 */

namespace aimgroup\DashboardBundle\Controller;

use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class BiometricController
 *
 * @Route("/admin/biometric/")
 */
class BiometricController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Show Biometric Dashboard
     * @Route("", name="biometric_index")
     * @Method("Get")
     * @Template("DashboardBundle:Biometric:index.html.twig")
     */
    public function indexAction() {

        $user_roles = json_decode($this->get('session')->get('user_role_perms'), true);

        if (is_null($user_roles) || !in_array(2, $user_roles)) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $this->container->get("api.helper")->logInfo("BiometricController", "GET", array( "request" => $user_roles, true ));
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->prepareResponse([
            'title'       => 'Biometric',
            'title_descr' => 'Biometric',
            'ddate'       => date('Y-m-d'),
            'akuku'       => json_encode($user->getRoles()) . ' | ' . (new Session())->get('user_role_perms'),
            'date_today'  => date('Y-m-d'),
            'date_start'  => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string('10 days')), 'Y-m-d')
        ]);
    }


    /**
     * hourly_summary Summary
     *
     * @Route("get_count_summary/{start_date}/{end_date}", name="biometric_summary")
     * @Method("Get")
     */
    public function count_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE r.registryType = 2 ";

        $where .= " AND 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        $leftJOIN = "";
        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
            $leftJOIN = " JOIN x.owner a ";
        }
        if ($start_date != 1) {
            $where .= " AND date(x.createdDate) >= '" . $start_date . "' AND date(x.createdDate) <= '" . $end_date . "' ";
        } else {
            $end_date = date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d");
            //$where .= " AND date(x.createdDate) >= '" . $end_date . "' ";
            if (true) {
                $where .= " AND date(x.createdDate) >= '" . $end_date . "' AND date(x.createdDate) <= '" . date('Y-m-d H:i:s') . "' ";
            }
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg,r.verifyState as verifyState, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
            . " JOIN r.registrationId x " . $leftJOIN . $where
            . " GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus"
        )->getArrayResult();


        $data[ 'freg' ] = $data[ 'unverified' ] = $data[ 'pending_info' ] = $data[ 'verified' ] =  $data[ 'declined' ]  = $data[ 'total' ] = 0;

        foreach ($count_tfreg as $key => $val) {

            $data[ 'total' ] = $data[ 'total' ] + $val[ 'reg_count' ];

            if ($val[ 'image_count' ] != 1) {

                $data[ 'pending_info' ] = $data[ 'pending' ] + $val[ 'reg_count' ];

            } else {

                $data[ 'received' ] = $data[ 'received' ] + $val[ 'reg_count' ];
            }

            if ($val[ 'verifyState' ] == 1) {

                $data[ 'verified' ] = $data[ 'verified' ] + $val[ 'reg_count' ];
            }

            if ($val[ 'verifyState' ] == 0) {

                $data[ 'unverified' ] = $data[ 'unverified' ] + $val[ 'reg_count' ];
            }

            if ($val[ 'verifyState' ] == 4) {

                $data[ 'declined' ] = $data[ 'declined_nida' ] + $val[ 'reg_count' ];
            }

        }


        if ($start_date == 1 && !in_array("ROLE_SUPERAGENT", $roles)) {
            $result = $em->createQuery("SELECT s from DashboardBundle:Summary s "
                . "WHERE s.dataType = 'top_count' ")
                ->getArrayResult();

            if (count($result) >= 1) {
                $result = $result[ 0 ];

                foreach (json_decode($result[ 'dataString' ]) as $key => $val) {
                    $data[ $key ] = $data[ $key ] + $val;
                }
            }
        }

        echo json_encode($data);
        exit;
    }


    /**
     * hourly_summary Summary
     *
     * @Route("get_hourly_summary/{start_date}/{end_date}", name="biometric_hourly_summary")
     * @Method("GET")
     * @Template()
     */
    public function hourly_summaryAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();


        $where = " WHERE r0_.registration_type = 2 ";

        $where .= " AND 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        $join_superagent = "";
        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u1_.parent_id = " . $user->getId();
            $join_superagent = "LEFT JOIN user u1_ ON r0_.owner_id = u1_.id";
        }


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('HOUR', 'DoctrineExtensions\Query\Mysql\Hour');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        if ($start_date == 1) {

            $start_date = $end_date = date('Y-m-d');
        }


        $where .= " AND r0_.createdDate >= " . date('Ymd', strtotime($end_date)) . "000000 AND r0_.createdDate <= " . date('Ymd', strtotime($end_date)) . "235959 ";

        $query1 = "SELECT HOUR(r0_.createdDate) AS hhour, COUNT(r0_.id) AS ccount FROM registration r0_ " . $join_superagent . " " . $where . " GROUP BY hhour ORDER BY hhour DESC";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result = $statement->fetchAll();

        //20170410


        $data = array();

        for ($i = 0; $i <= 23; $i++) {
            $data[ $i ] = 0;
        }

        foreach ($result as $key => $value) {
            //echo "<pre>"; print_r($value); echo "</pre>";

            $data[ $value[ 'hhour' ] ] = $value[ 'ccount' ];
        }


        echo json_encode($data);
        exit;
    }


    /**
     * Get verifiers idle time
     *
     * @Route("get_daily_count", name="biometric_daily_count")
     * @Method({"POST","GET"})
     */
    public function dailySummaryCounts() {
        $rsm = new \Doctrine\ORM\Query\ResultSetMapping();
        $rsm->addEntityResult('RestApiBundle:user', 'u');
        $rsm->addScalarResult('first_name', 'first_name');
        $rsm->addScalarResult('last_name', 'last_name');
        $rsm->addScalarResult('counter', 'counter');

        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter, r2.verifyBy, r2.id as maxid, r2.verifyDate, MAX(r2.verifyDate) as maxdate, TIMEDIFF(now(), MAX(r2.verifyDate)) as idletime FROM RegistrationStatus r2, user u WHERE r2.verifyBy = u.id AND DATE(r2.verifyDate) = CURDATE() GROUP BY r2.verifyBy ORDER BY maxdate";
        //$queryString = "select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u "
        //        . "WHERE r2.verifyBy = u.id AND r2.verifyBy != 3260 AND DATE(r2.verifyDate) = CURDATE() AND r2.verifyState > 0 GROUP BY r2.verifyBy ORDER BY counter";

        $queryString = 'select u.first_name, u.last_name, count(r2.id) as counter FROM RegistrationStatus r2, user u '
            . 'WHERE (r2.registry_type = 2 AND r2.verifyBy = u.id OR r2.originalVerifyBy = u.id) AND r2.verifyBy != 3260 AND r2.verifyDate = ' . date('Ymd') . " AND r2.verifyState in (1, 2, 3, '-2', '-3') GROUP BY r2.verifyBy ORDER BY counter";


        $query = $this->getDoctrine()->getManager()->createNativeQuery($queryString, $rsm);

        $agents = $query->getResult();

        return $this->buildResponse($agents, Response::HTTP_OK);
    }

    /**
     * active_agents Summary
     *
     * @Route("verify_daily_report/{start_date}/{end_date}", name="biometric_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_daily_reportAction($start_date, $end_date) {
        $em = $this->getDoctrine()->getManager();

        $where = ' WHERE r0_.registry_type = 2 ';

        $where .= " AND 1=1 ";
        $where2 = $where;
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if ($start_date != 1) {
            $where .= " AND DATE(r0_.verifyDate) >= '" . date('Y-m-d', strtotime($start_date)) . "' AND DATE(r0_.verifyDate) <= '" . date('Y-m-d', strtotime($end_date)) . "' ";
        }
        $where .= " AND r0_.verifyState in (1, 2, 3, '-2', '-3') ";

        $where .= " AND DATE(r0_.createdDate) >= '" . date("Y-m-d", strtotime("-3 months")) . "'";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        /* $query = $em->createQueryBuilder()
          ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
          . " from RestApiBundle:RegistrationStatus s" . $where
          . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
          ->getQuery();
          $result1 = $query->getResult(); */

        $query1 = "SELECT r0_.id AS sid, COUNT(r0_.id) AS sum_regs, DATE_FORMAT (r0_.verifyDate,'%Y-%m-%d') AS created_on, r0_.verifyState AS verifyState "
            . " FROM RegistrationStatus r0_ "
            . $where . " GROUP BY created_on, r0_.verifyState ORDER BY created_on DESC";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[ $value[ 'created_on' ] ] = (@$data[ $value[ 'created_on' ] ] ? $data[ $value[ 'created_on' ] ] : $value);

            $data[ $value[ 'created_on' ] ][ 'sum_total' ] = @$data[ $value[ 'created_on' ] ][ 'sum_total' ] + $value[ 'sum_regs' ];
            $data[ $value[ 'created_on' ] ][ 'sum_good' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_good' ] ? $data[ $value[ 'created_on' ] ][ 'sum_good' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_fair' ] ? $data[ $value[ 'created_on' ] ][ 'sum_fair' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_bad' ] ? $data[ $value[ 'created_on' ] ][ 'sum_bad' ] : 0);
            $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = (@$data[ $value[ 'created_on' ] ][ 'sum_unrated' ] ? $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] : 0);

            if ($value[ 'verifyState' ] == 1) {
                $data[ $value[ 'created_on' ] ][ 'sum_good' ] = $data[ $value[ 'created_on' ] ][ 'sum_good' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 2 || $value[ 'verifyState' ] == -2) {
                $data[ $value[ 'created_on' ] ][ 'sum_fair' ] = $data[ $value[ 'created_on' ] ][ 'sum_fair' ] + $value[ 'sum_regs' ];
            } elseif ($value[ 'verifyState' ] == 3 || $value[ 'verifyState' ] == -3) {
                $data[ $value[ 'created_on' ] ][ 'sum_bad' ] = $data[ $value[ 'created_on' ] ][ 'sum_bad' ] + $value[ 'sum_regs' ];
            } else {
                $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] = $data[ $value[ 'created_on' ] ][ 'sum_unrated' ] + $value[ 'sum_regs' ];
            }
        }

        echo json_encode($data);
        exit;
    }


    /**
     * Verification Time
     *
     * @Route("verification_time/{start_date}/{end_date}", name="biometric_verification_time")
     * @Method("GET")
     * @Template()
     */
    public function avarage_verification_timeAction($start_date, $end_date) {

        try {

            $em = $this->getDoctrine()->getManager();

            $where = " r0_.registry_type = 2 AND 1=1 AND r0_.verifyState in (1, 2, 3, '-2', '-3') AND r0_.verifyLock > 1 AND r0_.verifyLock < 1000 ";
            /** @var  $user User */
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $roles = $user->getRoles(); //array of roles

            if (in_array('ROLE_SUPERAGENT', $roles)) {
                $where .= ' AND r.parent_id = ' . $user->getId();
            }
            if ($start_date != 1) {
                $where .= ' AND r0_.date_stored_verifyDate >= ' . date('Ymd', strtotime($start_date)) . ' AND r0_.date_stored_verifyDate <= ' . date('Ymd', strtotime($end_date)) . ' ';
            }


            $emConfig = $em->getConfiguration();
            $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
            $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
            $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
            $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


            $treg = $freg = $fail = $pending = 0;

            $query1 = 'SELECT AVG(r0_.verifyLock) AS verifyLock '
                . ' FROM RegistrationStatus r0_ ' . $where;

            $connection = $em->getConnection();
            $statement = $connection->prepare($query1);
            $statement->execute();
            $count_tfreg = $statement->fetchAll();


            $count_tfreg2[ 'verifyLock' ] = (@$count_tfreg[ 0 ][ 'verifyLock' ] ? floor($count_tfreg[ 0 ][ 'verifyLock' ]) . ' Seconds' : 0);

            echo json_encode($count_tfreg2);

        }catch (\Exception $e) {

            echo $e->getMessage();
        }
        exit;
    }


    /**
     * Verification Time
     *
     * @Route("reports", name="biometric_report")
     * @Method("GET")
     * @Template("DashboardBundle:Biometric/report:index.html.twig")
     */
    public function reportsAction() {

        return $this->prepareResponse([]);
    }

    /**
     * Region reports
     *
     * @Route("reports/region", name="biometric_region_report")
     * @Method("GET")
     * @Template("DashboardBundle:Biometric/report:region.html.twig")
     */
    public function getRegionReport(Request $request) {

        return $this->prepareResponse([]);
    }

    /**
     * Agent reports
     *
     * @Route("reports/agent_score", name="biometric_agent_report")
     * @Method("GET")
     * @Template("DashboardBundle:Biometric/report:agent.html.twig")
     */
    public function getAgentScoreReport(Request $request) {

        return $this->prepareResponse([]);
    }


}
