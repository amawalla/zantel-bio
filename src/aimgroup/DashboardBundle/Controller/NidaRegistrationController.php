<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\RestApiBundle\Entity\NidaRegistration;
use aimgroup\DashboardBundle\Form\NidaRegistrationType;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * NidaRegistration controller.
 *
     * @Route("/nidaregistration")
 */
class NidaRegistrationController extends AbstractController
{

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * Lists all NidaRegistration entities.
     *
     * @Route("/", name="nidaregistration")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        if (!in_array(23, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('RestApiBundle:NidaRegistration')->findAll();

        $data = array(
            'title'       => "View NidaRegistrations",
            'title_descr' => "",
            'entities'    => $entities,
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a new NidaRegistration entity.
     *
     * @Route("/", name="nidaregistration_create")
     * @Method("POST")
     * @Template("RestApiBundle:NidaRegistration:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new NidaRegistration();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('nidaregistration_show', array('id' => $entity->getId())));
        }

        $data = array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to create a NidaRegistration entity.
     *
     * @param NidaRegistration $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(NidaRegistration $entity)
    {
        $form = $this->createForm(
            new NidaRegistrationType(),
            $entity,
            array(
                'action' => $this->generateUrl('nidaregistration_create'),
                'method' => 'POST',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new NidaRegistration entity.
     *
     * @Route("/new", name="nidaregistration_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new NidaRegistration();
        $form = $this->createCreateForm($entity);

        $data = array(
            'title'       => "View NidaRegistrations",
            'title_descr' => "",
            'entity'      => $entity,
            'form'        => $form->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Finds and displays a NidaRegistration entity.
     *
     * @Route("/{id}", name="nidaregistration_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:NidaRegistration')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NidaRegistration entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title'       => "View NidaRegistrations",
            'title_descr' => "",
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Displays a form to edit an existing NidaRegistration entity.
     *
     * @Route("/{id}/edit", name="nidaregistration_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:NidaRegistration')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NidaRegistration entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = array(
            'title'       => "Edit NidaRegistrations",
            'title_descr' => "",
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a form to edit a NidaRegistration entity.
     *
     * @param NidaRegistration $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(NidaRegistration $entity)
    {
        $form = $this->createForm(
            new NidaRegistrationType(),
            $entity,
            array(
                'action' => $this->generateUrl('nidaregistration_update', array('id' => $entity->getId())),
                'method' => 'PUT',
            )
        );

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing NidaRegistration entity.
     *
     * @Route("/{id}", name="nidaregistration_update")
     * @Method("PUT")
     * @Template("RestApiBundle:NidaRegistration:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('RestApiBundle:NidaRegistration')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find NidaRegistration entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('nidaregistration_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a NidaRegistration entity.
     *
     * @Route("/{id}", name="nidaregistration_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('RestApiBundle:NidaRegistration')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find NidaRegistration entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('nidaregistration'));
    }

    /**
     * Creates a form to delete a NidaRegistration entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('nidaregistration_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm();
    }

}
