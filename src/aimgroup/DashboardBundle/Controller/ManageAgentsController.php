<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\UserLog;
use aimgroup\DashboardBundle\Util\UsersHelper;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use Doctrine\Common\Persistence\AbstractManagerRegistry;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;
use Symfony\Component\HttpFoundation\Response;
//use FOS\ElasticaBundle\Repository;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\Util\SecureRandom;
//use aimgroup\DashboardBundle\Dao\SearchDao;
use aimgroup\DashboardBundle\Entity\SearchResult;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;

/**
 * This ManageAgentsController is used to manage agents in the system.
 *
 *
 * @Route("admin/agents")
 */
class ManageAgentsController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     *
     * @Route("/", name="admin/agents")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {

        if (!in_array(13, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }


        //$searchResult = new SearchResult();
        //$dao = new SearchDao($this->container);


        $em = $this->getDoctrine()->getManager();
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $data = array(
            'title' => "Manage Agents",
            'title_descr' => "List, create, delete, activate Agents",
            'user_role' => json_encode($user->getRoles()),
            'posts' => "",
            'mqttMessageTopic' => $this->container->getParameter("mqtt_notify_msg"),
            "operator" => strtolower($this->container->getParameter("operator_name"))
        );

        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list_agents",name="list_agents")
     * @Cache(expires="now", smaxage="30", maxage="3x0")
     * @Method({"POST","GET"})
     *
     * @return JsonResponse
     */
    public function listAgentAction(Request $request) {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();
            $where = " WHERE u.roles LIKE :roles ";

            if (isset($attributes["msisdn"])) {
                $where .= " AND u.username like '%" . $attributes["msisdn"] . "%'";
            }
            if (isset($attributes["first_name"])) {
                $where .= " AND u.firstName like '%" . $attributes["first_name"] . "%'";
            }
            if (isset($attributes["last_name"])) {
                $where .= " AND u.lastName like '%" . $attributes["last_name"] . "%'";
            }
            if (@$attributes["region"] > 0) {
                $where .= " AND u.region = " . $attributes["region"];
            }
            if (@$attributes["territory"] > 0) {
                $where .= " AND u.territory = " . $attributes["territory"];
            }
            if (isset($attributes["from_date"])) {
                //    $where.= " AND u.msisdn like '%" . $attributes["from_date"]."%'";
            }
            if (isset($attributes["to_date"])) {
                //    $where.= " AND u.msisdn like '%" . $attributes["to_date"]."%'";
            }

            $queryAttrib = $request->query->all();

            $queryString = "
SELECT u.id,concat(u.firstName, ' ',u.lastName) as agentName,DATE_FORMAT(u.createdAt, '%Y-%m-%d') as createdAt,u.username as mobileNumber,u.status,u.type as userType,u.username,u.idNumber,u.agentCode,
u.email, u.agent_exclusivity as exclusive,u.number_devices,r.id as region,t.id as territory,u.firstName,u.lastName, u.isEnrolledToTest as isEnrolledToTest 
                     FROM RestApiBundle:User u
                     LEFT OUTER JOIN u.region r
                     LEFT OUTER JOIN u.territory t " . $where . "order by u.id desc";

            $query = $this->getDoctrine()->getEntityManager()
                    ->createQuery($queryString)
                    ->setParameter("roles", '%ROLE_AGENT%');

            $query->setMaxResults($queryAttrib["jtPageSize"]);
            $query->setFirstResult($queryAttrib["jtStartIndex"]);
            $agents = $query->getResult();
            $resp->setRecords($agents);
        } catch (\Exception $e) {
            
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/createUser", name="createUser")
     * @Method("POST")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request) {

        $resp = new JsonObject();
        $status = false;
        $message = "ERROR SUBMITTING REQUEST";
        try {
            $attributes = json_decode($request->getContent(), true);

            if ($attributes) {
                $status = $this->container->get('users.helper')->createUser($attributes);
                if ($status == UsersHelper::EXISTS) {
                    $status = false;
                    $message = "MOBILE NUMBER ALREADY EXITS";
                } else if ($status == UsersHelper::SUCCESS) {
                    $status = true;
                    $message = "SUCCESS";
                }
                $this->logUserEvent(UserLog::CREATE_USER, "create user", array("attributes" => $attributes, 'status' => $status));
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
           // throw $e;
        }

        $resp->setMessage($message);
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add a new Agent.
     *
     * @Route("/updateUser", name="updateUser")
     * @Method("POST")
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function updateAction(Request $request) {
        $resp = new JTableResponse();
        try {
            $attributes = $request->request->all();

            $em = $this->getDoctrine()->getManager();

            if (isset($attributes["id"])) {
                /** @var  $user User */
                $user = $em->getRepository('RestApiBundle:User')->findOneBy(array("id" => $attributes["id"]));
                if ($user) {
                    //$superagent = $em->getRepository('RestApiBundle:User')->find($attributes['superagent_id']);
                    //if ($superagent) {
                    //    $user->setParentId($superagent);
                    //}
                    
                    
                    if(@$attributes['territory']){
                        $territory = $em->getRepository('DashboardBundle:Territory')->find(@$attributes['territory']);

                        if (!$territory) {
                            throw $this->createNotFoundException('Unable to find Territory entity.');
                        }
                        
                        $user->setTerritory($territory);
                    }
                    if(@$attributes['region']){
                        $region = $em->getRepository('DashboardBundle:Region')->find(@$attributes['region']);

                        if (!$region) {
                            throw $this->createNotFoundException('Unable to find Region entity.');
                        }
                        
                        $user->setRegion($region);
                    }

                    $user->setFirstName($attributes['firstName']);
                    $user->setLastName($attributes['lastName']);
                    $user->setIsEnrolledToTest($attributes['isEnrolledToTest']);

                    if (count(@$attributes['number_devices']) > 4) {
                        $user->setNumberDevices($attributes['number_devices']);
                    }
                    $user->setStatus($attributes['status']);
                    //$user->setAgentCode($attributes['agentCode']);

                    if (count(@$attributes['exclusive']) > 4) {
                        $user->setAgentExclusivity($attributes['exclusive']);
                    }


                    $em->flush();
                    $resp->setMessage("SUCCESSFULLY");
                    $resp->setRecord(
                            array(
                                "agentName" => $user->getFirstName() . " " . $user->getLastName(),
                                "status" => $user->getStatus(),
                                "agentCode" => $user->getAgentCode(),
                                "type" => $user->getType(),
                                "exclusive" => $user->getAgentExclusivity(),
                                "id" => $user->getId(),
                                "number_devices" => $user->getNumberDevices(),
                            )
                    );
                } else {
                    $resp->setResult("ERROR");
                    $resp->setMessage("USER NOT FOUND");
                }
            } else {
                $resp->setResult("ERROR");
                $resp->setMessage("INVALID REQUEST,SERVER ERROR");
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * View Reports
     *
     * @Route("/create_access_token/{id}", name="create_access_token")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:create_access_token.html.twig")
     */
    public function CreateAccessTokenAction($id) {

        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('RestApiBundle:User')->findAll();

        return array(
            'title' => "View Registrations Report:",
            'title_descr' => "View Registrations Report",
            'mobileNumber' => $id,
            'accessToken' => rand(10000000, 99999999),
            'agents' => $users,
        );
    }

    /**
     * Activate an Agent.
     *
     * @Route("/activate", name="activate")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function activateAction(Request $request) {
        
    }

    /**
     * Deactivate an Agent.
     *
     * @Route("/deactivate", name="deactivate")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function deactivateAction(Request $request) {
        
    }

    /**
     * Assign an Agent to a network.
     *
     * @Route("/assign", name="assign")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function assignNetworkAction(Request $request) {
        
    }

    /**
     * Activate Agents using CVS file.
     *
     * @Route("/activate_group", name="activate_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchActivateAction(Request $request) {
        
    }

    /**
     * Deactivate Agents using CVS file.
     *
     * @Route("/deactivate_group", name="deactivate_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchDeactivateAction(Request $request) {
        
    }

    /**
     * Assign Agents to a network
     *
     * @Route("/assign_group", name="assign_group")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchAssignNetworkAction(Request $request) {
        
    }

    /**
     * Approve Agent
     *
     * @Route("/approve", name="approve")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function approveAgentAction(Request $request) {
        
    }

    /**
     * Add Agents using CVS file.
     *
     * @Route("/bulk_upload", name="bulk_upload")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchCreateAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();
            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');

            $fileName = $file->getClientOriginalName();
            $ext = pathinfo($fileName)["extension"];
            if (strcmp($ext, "csv") == 0) {
                $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "agent_uploads" . DIRECTORY_SEPARATOR . date("Ymd");
                $newFileName = date("YmdHi") . $fileName;
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $newFileName);
                //command to publish message
                $kernel = $this->get('kernel');
                $application = new Application($kernel);
                $application->setAutoExit(false);

                $input = new ArrayInput(array(
                    'command' => 'dashboard:bulk_agent_upload_command',
                    'file_path' => $uploadedURL . "/" . $newFileName
                ));
                // You can use NullOutput() if you don't need the output
                $application->run($input, null);
                $resp->setMessage("SUCCESS");
                $status = true;

                $this->logUserEvent(UserLog::BULK_AGENTS_UPLOAD, "bulk agents upload", array("file" => $fileName, 'status' => 'success'));
            } else {
                $resp->setMessage("Invalid File Type");

                $this->logUserEvent(UserLog::BULK_AGENTS_UPLOAD, "bulk agents upload", array("file" => $fileName, 'status' => 'failed'));
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Add Agents using CVS file.
     *
     * @Route("/bulk_agent_delete", name="bulk_agent_delete")
     * @Method("POST")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function batchAgentDeleteAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();
            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');

            $fileName = $file->getClientOriginalName();
            $ext = pathinfo($fileName)["extension"];
            if (strcmp($ext, "csv") == 0) {
                $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "agent_delete" . DIRECTORY_SEPARATOR . date("Ymd");
                $newFileName = date("YmdHi") . $fileName;
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $newFileName);
                //command to publish message
                $kernel = $this->get('kernel');
                $application = new Application($kernel);
                $application->setAutoExit(false);

                $input = new ArrayInput(array(
                    'command' => 'dashboard:bulk_delete_command',
                    'file_path' => $uploadedURL . "/" . $newFileName
                ));
                // You can use NullOutput() if you don't need the output
                $application->run($input, null);
                $resp->setMessage("SUCCESS");
                $status = true;

                $this->logUserEvent(UserLog::BULK_AGENTS_DELETE, "bulk agents upload", array("file" => $fileName));
            } else {
                $resp->setMessage("Invalid File Type");

                $this->logUserEvent(UserLog::BULK_AGENTS_DELETE, "bulk agents upload", array("file" => $fileName));
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish",name="publish_agent")
     *
     */
    public function publishMessageAction(Request $request) {
        $apiHelper = $this->container->get('api.helper');
        $status = false;
        try {
            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');
            $fileName = $file->getClientOriginalName();

            $apiHelper->logInfo("publishMessageAction", "publishMessageAction", array('name' => $fileName));

            $ext = pathinfo($fileName)["extension"];
            if (strcmp($ext, "csv") == 0 || strcmp($ext, "txt") == 0) {
                $uploadedURL = $this->container->getParameter(
                                "resource_path"
                        ) . DIRECTORY_SEPARATOR . "CSV" . DIRECTORY_SEPARATOR . date("Ymd");
                //Move the file to the directory where brochures are stored
                $file->move($uploadedURL, $fileName);
                $attributes = $request->request->all();


                $apiHelper->logInfo("publishMessageAction", "publishMessageAction", array('arttributes' => $attributes));

                $deviceId = $attributes["device"];
                $message = $attributes["message"];
                $type = $attributes["type"];

                $script = $this->container->getParameter("publishing_script");

                $apiHelper->logInfo("publishMessageAction", "publishMessageAction", array('script' => $script));

                $apiHelper->logInfo("publishMessageAction", "publishMessageAction", array('params' => $script . ' "' . $deviceId . '" "' . $message . '" "' . $type . '" "' . $uploadedURL . '" "' . $fileName . '"'));

                $process = new Process(
                        $script . ' "' . $deviceId . '" "' . $message . '" "' . $type . '" "' . $uploadedURL . '" "' . $fileName . '"'
                );

                $process->run();
                if ($process->isSuccessful()) {
                    $status = true;
                }
            }
        } catch (\Exception $e) {
            
        }
        if ($status == true) {
            return $this->buildResponse(array(), Response::HTTP_OK);
        } else {
            return $this->buildResponse(array(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
