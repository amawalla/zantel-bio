<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;


/**
 * Class AgentMobileReportController
 * @package aimgroup\DashboardBundle\Controller
 * @Route("/mreport")
 * @Cache(expires="now", smaxage="0", maxage="0")
 *
 */
class AgentMobileReportController extends Controller {

    public function indexAction($name) {
        return $this->render('', array('name' => $name));
    }

    /**
     * Displays a form to edit an existing User entity.
     * @Cache(expires="now", smaxage="0", maxage="0")
     * @Route("/agent/{id}", name="magent")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function agentdetailsAction(Request $request, $id) {

        $request = $this->getRequest();
        $pagetitle = "View Reports from Beginning";
        $where = "";

        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");

        $em = $this->get("doctrine")->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        $em = $this->get("doctrine")->getManager();

	/*
        * todo
        * check if language is set from language url
        * if language parameter is set, use it
        * Else read language from db
        */
       	if(null !== $request->query->get('language') && strlen($request->query->get('language')) == 2) {
           	$language = $request->query->get('language');
    	} else {
        	$count_tfreg = $em->createQuery("SELECT x.language as language FROM RestApiBundle:Registration x"
                           . " WHERE x.agentMsisdn='" . $id . "' "
                           . " ORDER BY x.id DESC "
                   )->setMaxResults(1)->getArrayResult();
               	$language = $count_tfreg[0]['language'];
	}


       	$request = $this->getRequest();
       	$pagetitle = "Ripoti toka Mwanzo";
       	$where = "";

        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");

        try {

             switch ($request->get('timefilter')) {

                case "day":
                    	$pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.today', array(), 'messages', $language) . date('Y-m-d');
                    	$where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d") . "' ";
                    break;
                case "week":
			$pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.thisweek', array(), 'messages', $language) . date("Y-m-d", strtotime("-1 week")) . " - " . date('Y-m-d');
                    	$where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 week")) . "' ";

                    break;
                case "month":
                    	$pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.thismonth', array(), 'messages', $language) . date('F Y');
			$where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 month")) . "' ";

                    break;
                case "lastmonth":
                    	$month = date("m", strtotime("-1 month"));
                    	$month2 = date("m");
                    	$year = date('Y');
			$pagetitle = "'" . $year . "-" . $month . "-01' AND '" . $year . "-" . $month2 . $this->container->get('translator')->trans('agent.mreport.pagetitle.lastmonth', array(), 'messages', $language) . date("F Y", strtotime("-1 month"));
                    	$where .= " AND DATE(r.createdDate) >= '" . $year . "-" . $month . "-01' AND DATE(r.createdDate) < '" . $year . "-" . $month2 . "-01' ";

                    break;
                default:
                    	$pagetitle = $this->container->get('translator')->trans('agent.mreport.pagetitle.all', array(), 'messages', $language);
			$where .= " ";
            }


            $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, x.image_count, r.temporaryRegStatus as treg, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
                            . " JOIN r.registrationId x  WHERE x.agentMsisdn='".$id."' " . $where
                            . " GROUP BY x.image_count, r.temporaryRegStatus, r.fullRegStatus"
                    )->getArrayResult();
        } catch (\Exception $e) {
            $result = null;
        }

        $data['total'] = $data['full_info'] = $data['treg_reg'] = $data['pending_info'] = $data['full_reg'] = $data['declined_reg'] = $data['pending_reg'] = 0;
        if ($count_tfreg != null) {

            $data['full_info'] = $data['pending_info'] = $data['full_reg'] = $data['declined_reg'] = $data['pending_reg'] = 0;
            foreach ($count_tfreg as $key => $val) {

                $data['total'] = $data['total'] + 1;
                //pending
                if ($val['image_count'] >= 3) {
                    $data['full_info'] = $data['full_info'] + $val['reg_count'];
                } else {
                    $data['pending_info'] = $data['pending_info'] + $val['reg_count'];
                }


                if (in_array($val['treg'], array(2, 6, 7))) {
                    //treg
                    $data['treg_reg'] = $data['treg_reg'] + $val['reg_count'];
                } elseif (in_array($val['treg'], array(4, 5, 3))) {
                    //declined
                    $data['declined_reg'] = $data['declined_reg'] + $val['reg_count'];
                } else {
                    //NOT SENT YET
                    $data['pending_reg'] = $data['pending_reg'] + $val['reg_count'];
                }

                if (in_array($val['treg'], array(1, 2, 6, 7)) && in_array($val['freg'], array(2, 6, 7))) {
                    //FREG
                    $data['full_reg'] = $data['full_reg'] + $val['reg_count'];
                }
            }

            $data['full_reg'] = @$data['treg_reg'];
        }

        return array(
	        'device_language' => $language,
            'title' => "Create New Userx",
            'id' => $id,
            'title_descr' => "Create New Agent",
            'agent_id' => $id,
            'report_type' => $pagetitle,
            'reg_data' => $data,
	        'timefilter' => $request->get('timefilter'),
            'ttitlependinginfo' => $this->container->get('translator')->trans('agent.mreport.title.pendinginfo', array(), 'messages', $language),
            'ttitlereceived' => $this->container->get('translator')->trans('agent.mreport.title.received', array(), 'messages', $language),
            'ttitlependingsent' => $this->container->get('translator')->trans('agent.mreport.title.pendingsent', array(), 'messages', $language),
            'ttitlesent' => $this->container->get('translator')->trans('agent.mreport.title.sent', array(), 'messages', $language),
            'ttitledeclined' => $this->container->get('translator')->trans('agent.mreport.title.declined', array(), 'messages', $language),
            'subtitlependinginfo' => $this->container->get('translator')->trans('agent.mreport.subtitle.pendinginfo', array(), 'messages', $language),
            'subtitlereceived' => $this->container->get('translator')->trans('agent.mreport.subtitle.received', array(), 'messages', $language),
            'subtitlependingsent' => $this->container->get('translator')->trans('agent.mreport.subtitle.pendingsent', array(), 'messages', $language),
            'subtitlesent' => $this->container->get('translator')->trans('agent.mreport.subtitle.sent', array(), 'messages', $language),
            'subtitledeclined' => $this->container->get('translator')->trans('agent.mreport.subtitle.declined', array(), 'messages', $language),
            'menutoday' => $this->container->get('translator')->trans('agent.menu.today', array(), 'messages', $language),
            'menuthisweek' => $this->container->get('translator')->trans('agent.menu.thisweek', array(), 'messages', $language),
            'menuthismonth' => $this->container->get('translator')->trans('agent.menu.thismonth', array(), 'messages', $language),
            'menulastmonth' => $this->container->get('translator')->trans('agent.menu.lastmonth', array(), 'messages', $language),
            'menutotal' => $this->container->get('translator')->trans('agent.menu.all', array(), 'messages', $language)
        );
    }




      /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agentReg/{id}", name="agentReg")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_registration_details.html.twig")
     */
    public function agentRegistrationdetailsAction($id) {
        //declined  submitted   waitingSubmission   received   pending
        $request = $this->getRequest();
        $pagetitle = "Orodha ya Usajili : ";
        $where = "";

        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");
        $em = $this->get("doctrine")->getManager();
        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

        try {

            switch ($request->get('statusfilter')) {
                case "declined":
                    $pagetitle.= " Zilizokataliwa ";
                    $where.= " AND r.temporaryRegStatus IN (4,5,3) ";
                    break;
                case "submitted":
                    $pagetitle.= " Zillizotumwa ";
                    $where.= " AND r.temporaryRegStatus IN (1,2,6,7)";
                    break;
                case "waitingSubmission":
                    $pagetitle.= " Bado Hazijatumwa ";
                    $where.= " AND r.temporaryRegStatus IN (0,1) ";
                    break;
                case "received":
                    $pagetitle.= " Zilizopokelewa ";
                    $where.= " AND x.image_count >= 3 ";
                    break;
                default:
                    $pagetitle.= " Zinazosubiri";
                    $where.= " AND x.image_count < 3 ";
            }

             switch ($request->get('timefilter')) {

                case "day":
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d") . "' ";
                    break;
                case "week":
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 week")) . "' ";

                    break;
                case "month":
                    $where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-1 month")) . "' ";

                    break;
                case "lastmonth":
                    $month = date("m", strtotime("-1 month"));
                    $month2 = date("m");
                    $year = date('Y');

                    $where .= " AND DATE(r.createdDate) >= '" . $year . "-" . $month . "-01' AND DATE(r.createdDate) < '" . $year . "-" . $month2 . "-01' ";

                    break;
                default:
                    $where .= " ";
            }

            #######$where .= " AND DATE(r.createdDate) >= '" . date("Y-m-d", strtotime("-2 DAY")) . "' ";

            $count_tfreg = $em->createQuery("SELECT x.msisdn, x.image_count, CONCAT(x.firstName, ' ', x.lastName) as customerName, x.nationality, x.registrationid as regid, r.temporaryRegStatus as treg, r.fullRegStatus as freg FROM RestApiBundle:RegistrationStatus r"
                            . " JOIN r.registrationId x JOIN x.owner a WHERE x.agentMsisdn='" . $id . "' " . $where
                            . " ORDER BY r.id DESC "
                    )->getArrayResult();
        } catch (\Exception $e) {
            $result = null;
        }

        if ($count_tfreg != null) {
            foreach ($count_tfreg as $key => $val) {
                //potrait  front-pic  rear-pic signature
                $count_tfreg[$key]['potrait'] = "";
                $count_tfreg[$key]['frontpic'] = "";
                $count_tfreg[$key]['rearpic'] = "";
                $count_tfreg[$key]['signature'] = "";

                $imageReg = $em->createQuery("SELECT i.webPath, i.imageType"
                                . " FROM RestApiBundle:RegImages i"
                                . " WHERE i.registration='" . $val['regid'] . "' "
                                . " ORDER BY i.id DESC "
                        )->getArrayResult();

                if ($imageReg != null) {
                    foreach ($imageReg as $xkey => $xval) {
                        $count_tfreg[$key][str_replace("-", "", $xval['imageType'])] = $xval['webPath'];
                    }
                }
            }
        }

        return array(
            'title' => "Create New Userx",
            'title_descr' => "Create New Agent",
            'agent_id' => $id,
            'report_type' => $pagetitle,
            'entities' => $count_tfreg,
            'isAdministrator' => FALSE,
        );
    }







    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/help", name="mHelp")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     */
    public function agentHelpAction() {
        $data = array("pending" => "0", "approved" => "0", "sent" => "0", "declined" => "0", "delete_req" => "0");
        return array(
            'title' => "Create New Userx",
            'title_descr' => "Create New Agent",
            'reg_data' => $data,
        );
    }

    /**
     * Displays a form to create a new Pass entity.
     *
     * @Route("/set_forgot_password/{code}", name="set_forgot_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:set_forgot_password.html.twig")
     */
    public function set_forgot_passwordAction($code) {
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p "
                        . "from DashboardBundle:SecurityCodes p "
                        . " where p.actionCode = " . $code)
                ->getOneOrNullResult();

        if (!$result) {
            return $this->redirect($this->generateUrl('admin'));
        }

        return array(
            'title' => "Create New FAQ",
            'code' => $code,
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/set_forgot_password_post/{code}", name="set_forgot_password_post")
     * @Method("POST")
     * @Template("DashboardBundle:Faq:new.html.twig")
     */
    public function set_forgot_password_postAction($code, Request $request) {

        $attributes = json_decode($request->getContent(), true);

        
        
        $em = $this->getDoctrine()->getManager();
        $result = $em->createQuery("SELECT p "
                        . "from DashboardBundle:SecurityCodes p "
                        . " where p.actionCode = " . $code)
                ->getOneOrNullResult();

        if (!$result) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $password = $request->request->get('password');
        $user = $em->getRepository('RestApiBundle:User')->find($result->getUserId());

        try {

            if ($user) {
                //if device exists update user
                /** @var  $user User */
                //$user = $this->get('security.token_storage')->getToken()->getUser();
                $encoder = $this->container->get('security.password_encoder');

                $user->setPassword($password);
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);
                
               
                
                $em->flush();
            } else {
                echo " patee user";
            }
            
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        
        

        return $this->redirect($this->generateUrl('admin'));
        exit;

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Displays a form to create a new Faq entity.
     *
     * @Route("/forgot_password", name="forgot_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:forgot_password.html.twig")
     */
    public function forgot_passwordAction() {

        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    /**
     * Displays a form to create a new Faq entity.
     *
     * @Route("/sendforgot_password/{email}/{timestamp}", name="sendforgot_password")
     * @Method("GET")
     * @Template("DashboardBundle:Admin:forgot_password.html.twig")
     */
    public function sendforgot_passwordAction($email, $timestamp) {

        $req['email'] = $email;
        $data['status'] = 1;
        $data['msg'] = "SENDING EMAIL FAIL";

        $code = random_int(123456789012345, 5432109876543210);

        
        
        $em = $this->getDoctrine()->getManager();
       $result = $em->createQuery("SELECT p "
                        . " from RestApiBundle:User p "
                        . " where p.email = '" . $email . "' ORDER BY p.id DESC")->setMaxResults(1)
                ->getOneOrNullResult();
        
        
        if (!$result) {
            $data['msg'] = "User Doesnt Exist. Contact your Admin";
            echo json_encode($data);
            exit;
        }

        

        $em = $this->getDoctrine()->getManager();
        $query1 = "INSERT INTO `SecurityCodes` (action_name, action_code, email_address,user_id) VALUES ('reset_pass', '" . $code . "', '" . $email . "', '" . $result->getId() . "');";
        $em->getConnection()->exec($query1);

        $this->sendmailAction($email, $code);
        $data['msg'] = "SENDING EMAIL SUCCEEDED";

        echo json_encode($data);
        exit;


        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    function sendmailAction($to, $code) {
        
        
        
        //http://b2b.efulusi.com/api/send_email_function/mutisya06____gmail.com/atotiCome/AtotiGO
        $url = "http://b2b.efulusi.com/api/send_email_function";
        $data = array(
            'to' => $to,
            'title' => 'eKYC Password Reset',
            'message' => $this->renderView('DashboardBundle:Admin:resetPassEmail.html.twig', array('ccode' => $code, 'base_url' => $this->get('request')->getSchemeAndHttpHost()))
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public function curl_to_emailAPI($email, $code) { //echo "Email: ".$email."   Code: " . $code;
        $message = \Swift_Message::newInstance()
                ->setSubject('eKYC Password Reset')
                ->setFrom('eams.notify@gmail.com')
                ->setTo($email)
                ->setBody(
                $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        'DashboardBundle:Admin:resetPassEmail.html.twig', array('ccode' => $code)
                ), 'text/html'
                )
        /*
         * If you also want to include a plaintext version of the message
          ->addPart(
          $this->renderView(
          'Emails/registration.txt.twig',
          array('name' => $name)
          ),
          'text/plain'
          )
         */
        ;
        $this->get('mailer')->send($message);

        return array(
            'title' => "Create New FAQ",
            'title_descr' => "Create New Frequently Asked Questions",
            'error' => 'fucktard'
        );
    }

    
    
    
    
    /**
     * trendlatest_summary Summary
     *
     * @Route("/appappsummary/{start_date}/{end_date}/{timestamp}", name="appappsummary")
     * @Method("GET")
     * @Template()
     */
    public function appappsummaryAction($start_date = "", $end_date = "", $timestamp = "") {
         $start_date = date('Y-m-d')." 00:00:00";
        $end_date = date('Y-m-d')." 23:59:59";
            
        $em = $this->container->get("doctrine")->getManager();
        $where = " WHERE 1=1 ";
        /** @var  $user User */
        
        
        
        
        $query1 = "SELECT r0_.image_count AS image_count, r1_.temporaryRegStatus AS treg, r1_.fullRegStatus AS freg, r1_.verifyState AS vstate, DATE(r0_.createdDate) AS ddate, COUNT(r1_.id) AS ccount FROM RegistrationStatus r1_ INNER JOIN registration r0_ ON r1_.registrationId = r0_.id INNER JOIN user u2_ ON r0_.owner_id = u2_.id WHERE 1 = 1 AND r0_.createdDate >= '".$start_date."' AND r0_.createdDate <= '".$end_date."' GROUP BY ddate, image_count, treg, freg, vstate ORDER BY ddate DESC;";
        

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result = $statement->fetchAll();

        $data = $data2 = array();


//        foreach ($result as $key => $value) {
//            $data[$value['ddate']][$value['state']]['count'] = @$data[$value['ddate']][$value['state']]['count'] + $value['ccount'];
//            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];
//        }

        $total = $declined = $approved = $synched = $pending = $approved_real = $pending_real = 0;
//assigned_entries|approved_entries|declined_entries|delete_request|synched_entries|tickets_assigned|tickets_actioned|tickets_pending	tickets_closed

        foreach ($result as $key => $value) {
            $data[$value['ddate']]['total']['count'] = @$data[$value['ddate']]['total']['count'] + $value['ccount'];

            $status_keys = array(
                'success' => 2,
                'declined' => 3,
                'fail' => 4,
                'retry' => 5,
                'success_cap' => 6,
                'success_dbms' => 7,
            );

            //images not received so pending
            if ($value['image_count'] < 4) {
                if (!in_array($value['treg'], array(4, 5, 3))) {
                    $data[$value['ddate']][0]['count'] = @$data[$value['ddate']][0]['count'] + $value['ccount'];
                }
            }


            if (in_array($value['treg'], array(2, 6, 7))) {
                //TREG
                $data[$value['ddate']][1]['count'] = @$data[$value['ddate']][1]['count'] + $value['ccount'];
            } elseif (in_array($value['treg'], array(4, 5, 3))) {
                //FAIL
                $data[$value['ddate']][3]['count'] = @$data[$value['ddate']][3]['count'] + $value['ccount'];
            } else {
                //NOT SENT YET
                if ($value['image_count'] >= 4 && $value['vstate'] == 1) {
                    $data[$value['ddate']][5]['count'] = @$data[$value['ddate']][5]['count'] + $value['ccount'];
                }
            }

            if (in_array($value['treg'], array(2, 6, 7)) && in_array($value['freg'], array(2, 6, 7))) {
                //FREG
                $data[$value['ddate']][2]['count'] = @$data[$value['ddate']][2]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(2, 6, 7)) && !in_array($value['freg'], array(2, 6, 7))) {// && $value['vstate'] == 1
                //PENDING FREG
                $data[$value['ddate']][4]['count'] = @$data[$value['ddate']][4]['count'] + $value['ccount'];
            }

            if (in_array($value['treg'], array(0, 2, 6,4)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))) {
                if (in_array($value['vstate'], array(1, 2, 3, '-2', '-3'))) {
                    $data[$value['ddate']][30]['count'] = @$data[$value['ddate']][30]['count'] + $value['ccount'];

                    if ($value['vstate'] == 1) {
                        //vgood
                        $data[$value['ddate']][32]['count'] = @$data[$value['ddate']][32]['count'] + $value['ccount'];
                    } elseif ($value['vstate'] == 2 || $value['vstate'] == '-2') {
                        //vfair
                        $data[$value['ddate']][33]['count'] = @$data[$value['ddate']][33]['count'] + $value['ccount'];
                    } elseif ($value['vstate'] == 3 || $value['vstate'] == '-3') {
                        //vbad
                        $data[$value['ddate']][34]['count'] = @$data[$value['ddate']][34]['count'] + $value['ccount'];
                    }
                } else {
                    //notverified
                    $data[$value['ddate']][31]['count'] = @$data[$value['ddate']][31]['count'] + $value['ccount'];
                }
            }
        }

        foreach ($data as $key4 => $value4) {
            //echo "<pre>"; print_r($value4); echo "</pre>";
            $data2 = array(
                'Time' => date('H:i:s'),
                'Total' => (@$value4['total']['count']) > 0 ? "<a href=''>" + @$value4['total']['count'] + "</a>" : @$value4['total']['count'],
                'Pending Info' => (@$value4[0]['count']) ? @$value4[0]['count'] : "0",
                'Pending toBe Sent' => (@$value4[5]['count']) ? @$value4[5]['count'] : "0",
                'Sent' => (@$value4[1]['count']) ? @$value4[1]['count'] : "0",
               // 'freg' => (@$value4[2]['count']) ? @$value4[2]['count'] : "0",
                'Verified' => (@$value4[30]['count']) ? @$value4[30]['count'] : "0",
                'Not Verified' => (@$value4[31]['count']) ? @$value4[31]['count'] : "0",
                'Good' => (@$value4[32]['count']) ? @$value4[32]['count'] : "0",
                'Fair' => (@$value4[33]['count']) ? @$value4[33]['count'] : "0",
                'Bad' => (@$value4[34]['count']) ? @$value4[34]['count'] : "0",
                //'pending_freg' => (@$value4[4]['count']) ? @$value4[4]['count'] : "0",
                'Declined' => (@$value4[3]['count']) ? @$value4[3]['count'] : "0"
            );
        }
        
        foreach($data2 as $kk=>$vv){
            $ress[] = array('itemName' => $kk, 'itemVal' => $vv);
        }
        echo json_encode($ress);
        exit;
    }


    
}
