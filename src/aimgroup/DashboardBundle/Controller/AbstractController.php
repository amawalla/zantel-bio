<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\LogsDao;
use aimgroup\DashboardBundle\Util\JTableNormalizer;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

class AbstractController extends FOSRestController
{

    /**
     *
     * @param type $dataObject The data you wish to put in the response object
     * @param type $responseStatus The status of the response
     * @param type $format This can be 'json' or 'xml'. The daufault is 'json'
     * @return JsonResponse
     */
    protected function buildResponse($dataObject, $responseStatus = Response::HTTP_OK, $format = 'json')
    {
        $response = new Response();

        $response->headers->set('Content-Type', 'application/json');
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new JTableNormalizer(), new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);

        $serializedProduct = $serializer->serialize($dataObject, $format);

        return $response->create($serializedProduct, $responseStatus);
    }

    /**
     * This method includes common data to be merged with desired data to compose response
     *
     * @param array $data
     * @return type merged array
     */
    protected function prepareResponse(array $data)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles();

        $result = array_merge(array('date_today' => date('Y-m-d'),
            'user' => $user,
            'roles' => $roles,
            'isAdministrator' => $this->isRoleAdministrator($roles),
	        'isAdmin' => $this->isRoleAdmin($roles),
            'inRoleVerifier' => $this->inRoleVerifierGroup($roles),
            'inRoleEditorVerifier' => $this->inRoleEditorVerifier($roles),
            'isRoleEditorVerifier' => $this->isRoleEditorVerifier($roles),
            'isRoleVerifierSupervisor' => $this->isRoleVerifierSupervisor($roles),
            'isRoleVerifier' => $this->isRoleVerifier($roles)), $data);

        return $result;
    }

    private function isRoleAdministrator($roles)
    {
        if ($roles != null) {
            return in_array('ROLE_SYSTEM_ADMIN', $roles);
        }
        return false;
    }
    
    private function isRoleAdmin($roles) {
        if ($roles != null) {
            return in_array('ROLE_ADMIN', $roles);
        }
        return false;
    }

    private function isRoleVerifier($roles)
    {

        if ($roles != null) {
            return in_array('ROLE_VERIFYER', $roles);
        }
        return false;
    }

    private function inRoleVerifierGroup($roles)
    {

        if ($roles != null) {
            return in_array('ROLE_VERIFYER', $roles) || in_array('ROLE_VERIFYER_SUPERVISOR', $roles);
        }
        return false;
    }

    private function isRoleVerifierSupervisor($roles)
    {

        if ($roles != null) {
            return in_array('ROLE_VERIFYER_SUPERVISOR', $roles);
        }
        return false;
    }

    public function getUser()
    {
        return $this->get('security.token_storage')->getToken()->getUser();
    }

    public function logUserEvent($action, $description, $metadata = array())
    {
        $logsDao = new LogsDao($this->getDoctrine()->getManager());
        $logsDao->addUserLogs($this->getUser(), $action, $description, $metadata);
    }

    public function logSystemEvent($tag, $method, $content, $isError = FALSE)
    {
        $apiHelper = $this->container->get('api.helper');
        if ($isError) {
            $apiHelper->logE($tag, $method, $content);
        }
        $apiHelper->logInfo($tag . ":" . $method, $content);
    }

    function checkSessionPermission($check) {
        return true;
        $permsArray = json_decode($this->session->get('user_role_perms'), true);
        foreach ($permsArray as $key => $val) {
            if ($check == $val) {
                return TRUE;
            }
        }

        return FALSE;
    }
    
    private function isRoleEditorVerifier($roles) {

        if ($roles != null) {
            return in_array('ROLE_EDITOR_VERIFYER', $roles);
        }
        return false;
    }
    
    private function inRoleEditorVerifier($roles) {

        if ($roles != null) {
            return in_array('ROLE_EDITOR_VERIFYER', $roles) || in_array('ROLE_VERIFYER_SUPERVISOR', $roles);
        }
        return false;
    }
}
