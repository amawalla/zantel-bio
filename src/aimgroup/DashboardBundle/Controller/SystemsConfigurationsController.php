<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
use aimgroup\DashboardBundle\Entity\LogMessage;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Anto;
use aimgroup\DashboardBundle\Form\AntoType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
/**
 * This SystemsConfigurationsController is used to manage system configurations.
 *
 * @author Michael Tarimo
 *
 * @Route("admin/systems_config")
 */
class SystemsConfigurationsController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Lists all Anto entities.
     *
     * @Route("/", name="admin/systems_config")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:Anto')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Create configurations
     *
     * @Route("/createSysConfig", name="createSysConfig")
     * @Method("POST")
     *
     */
    public function createConfigurationsAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try{

            $configMaster = new ConfigMaster();
            $configMaster->setName("endpoints");
            $configMaster->setConfig($request->getContent());
            $configMaster->setConfigType("endpointConfig");
            $configMaster->setCreatedOn(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            /** @var  $configuration ConfigMaster */
            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                array("configType"=>"endpointConfig")
            );
            if(!$configuration){
                $em->persist($configMaster);
            }else{
                $configuration->setConfig($configMaster->getConfig());
                $configuration->setVersion($configuration->getVersion()+1);
            }
            $em->flush();

            $status=true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * List configurations
     *
     * @Route("/listSysConfig", name="listSysConfig")
     * @Method("GET")
     *
     */
    public function listConfigurationsAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        try{
            $em = $this->getDoctrine()->getManager();

            $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findBy(
                array("configType"=>"endpointConfig")
            );

            $resp->setItem($configurations);
            $status=true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Save configurations
     *
     * @Route("/update", name="update")
     * @Method("GET")
     * @Template("DashboardBundle:Anto:new.html.twig")
     */
    public function updateConfigurationsAction(Request $request) {

    }

    /**
     * Save configurations
     *
     * @Route("/endpoints", name="endpoints")
     * @Method("GET")
     * @Template("DashboardBundle:SystemsConfigurations:endpoints_configurations.html.twig")
     */
    public function endpointsConfigurationsAction(Request $request) {

        if(!in_array(26, json_decode($this->session->get('user_role_perms')))){
            return $this->redirect($this->generateUrl('admin'));
        }

        $ids = array(); // pull from db

        $data = array(
            'title' => "Endpoints Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'ids' => $ids,
	    "operator" => strtolower($this->container->getParameter("operator_name"))
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * Save configurations
     *
     * @Route("/partners", name="partners")
     * @Method("GET")
     * @Template("DashboardBundle:SystemsConfigurations:partners.html.twig")
     */
    public function partnersAction(Request $request) {

        if(!in_array(27, json_decode($this->session->get('user_role_perms')))){
            return $this->redirect($this->generateUrl('admin'));
        }

        $partners = array(); // pull from db

        $data = array(
            'title' => "Partners Configurations",
            'title_descr' => "List, create, delete, activate System Admins",
            'partners' => $partners
        );
        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish",name="publish")
     *
     */
    public function publishMessageAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $requestJson = json_decode($request->getContent(), true);
            if ($requestJson) {

                //migrate all to operatorId
                $requestJson["operator"] = $this->container->getParameter("operator_id");
                $requestJson["operatorId"] = $this->container->getParameter("operator_id");
                $requestJson["id"] = $this->container->getParameter("operator_id");
                $requestJson["operatorName"] = $this->container->getParameter("operator_name");
                $deviceId = $requestJson["deviceId"];
                $topic = $requestJson["type"];
                if (isset($deviceId)) {
                    unset($requestJson["deviceId"]);
                    unset($requestJson["type"]);
                    $respond = $this->get("api.helper")->publish($deviceId, $topic, json_encode($requestJson));
                    if ($respond) {
                        $resp->setMessage("SUCCESS");
                        $status = true;
                    } else {
                        $resp->setMessage("PUBLISHED HAS FAILED");
                    }
                }
                $em = $this->get('doctrine')->getManager();

                /** @var  $user User */
                $user = $this->get('security.token_storage')->getToken()->getUser();

                $logMessage = new LogMessage();
                $logMessage->setContent(json_encode($requestJson));
                $logMessage->setMessageType($topic);
                $logMessage->setCreatedBy($user->getId());
                $em->persist($logMessage);
                $em->flush();

            }

        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }
  /**
     * @param Request $request
     * @Method("POST")
     * @Route("/publish_file",name="publish_file")
     *
     */
    public function publishMessageWithFileAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = $request->request->all();

            $deviceId = $attributes["deviceId"];
            $topic = $attributes["type"];

            $attributes["operator"] = $this->container->getParameter("operator_id");
            $attributes["operatorId"] = $this->container->getParameter("operator_id");
            $attributes["id"] = $this->container->getParameter("operator_id");
            $attributes["operatorName"] = $this->container->getParameter("operator_name");

            /** @var  $file UploadedFile */
            $file = $request->files->get('fileLocation');
            if ($file != null) {
                $fileName = $file->getClientOriginalName();
                $ext = pathinfo($fileName)["extension"];
                if (strcmp($ext, "csv") == 0) {
                    $uploadedURL = $this->container->getParameter("resource_path") . DIRECTORY_SEPARATOR . "mqtt_uploads" . DIRECTORY_SEPARATOR . date("Ymd");
                    //Move the file to the directory where brochures are stored
                    $file->move($uploadedURL, $fileName);

                    //command to publish message
                    $kernel = $this->get('kernel');
                    $application = new Application($kernel);
                    $application->setAutoExit(false);


                    unset($attributes["deviceId"]);
                    unset($attributes["type"]);

                    $input = new ArrayInput(array(
                        'command' => 'dashboard:mqtt_csv_publish_command',
                        'file_path' => $uploadedURL . "/" . $fileName,
                        "message" => json_encode($attributes),
                        "topic" => $topic
                    ));
                    // You can use NullOutput() if you don't need the output
                    $application->run($input, null);

                    $resp->setMessage("SUCCESS");
                    $status = true;
                    $this->logUserEvent(\aimgroup\DashboardBundle\Entity\UserLog::PUBLISH_MESSAGE_WITH_FILE, 
                            "Published message with file: ", (array) $attributes);
                } else {
                    $resp->setMessage("Invalid File Type");
                }
            } else {
                unset($attributes["deviceId"]);
                unset($attributes["type"]);
                $respond = $this->get("api.helper")->publish($deviceId, $topic, json_encode($attributes));
                if ($respond) {
                    $resp->setMessage("SUCCESS");
                    $status = true;
                } else {
                    $resp->setMessage("PUBLISHED HAS FAILED");
                }
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $this->logSystemEvent("", "publishMessageWithFileAction", $e);
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }


}
