<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use aimgroup\DashboardBundle\Entity\Service;
use aimgroup\DashboardBundle\Entity\Step;
use aimgroup\DashboardBundle\Entity\StepField;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\RestApiBundle\Entity\NidaRegistration;
use aimgroup\DashboardBundle\Form\NidaRegistrationType;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Service controller.
 *
 * @Route("admin/services")
 */
class ServiceController extends AbstractController {

    /**
     * Lists all Services
     *
     * @Route("", name="dashboard.service.list")
     * @Method("GET")
     * @Template("DashboardBundle:Services:index.html.twig")
     */
    public function indexAction() {
        $services = $this->getDoctrine()->getRepository('DashboardBundle:Service')->findBy(
                array(), array('ordering' => 'ASC')
        );

        return $this->prepareResponse(
                        array(
                            'title' => 'Manage Services',
                            'subtitle' => 'Services list',
                            'operator' => $this->container->getParameter('operator_name'),
                            'services' => $services,
                        )
        );
    }

    /**
     * @Route("/create", name="dashboard.service.create")
     * @Method("GET")
     * @Template("DashboardBundle:Services:new.html.twig")
     */
    public function createAction() {


        $post = Request::createFromGlobals();

        if ($post->request->has('submit')) {

            $entityManager = $this->getDoctrine()->getManager();

            $service = new Service();
            $service->setName($post->request->get('serviceName'));
            $service->setNameAlt($post->request->get('serviceNameAlt'));
            $service->setServiceCode($post->request->get('serviceNCode'));
            $service->setDescription($post->request->get('serviceDescription'));
            $service->setStatus($post->request->get('status'));
            $service->setOrdering($post->request->get('ordering'));

            $entityManager->persist($service);
            $entityManager->flush();
        }

        return $this->prepareResponse(
                        array(
                            'title' => "Add New Service Form",
                            'subtitle' => 'Services list'
                        )
        );
    }

    /**
     * @Route("/update", name="dashboard_service_update")
     * @Method("GET")
     */
    public function updateAction() {
        
    }

    /**
     * @param Service $service
     *
     * @Route("/forms/{id}", name="dashboard.service.forms")
     * @Template("DashboardBundle:Services:forms.html.twig")
     * @return type
     */
    public function serviceFormAction(Service $service) {

        $em = $this->getDoctrine()->getManager();

        $forms = $em->getRepository('DashboardBundle:ServiceForm')->findBy(
                array(
                    'serviceId' => $service,
                )
        );

        return $this->prepareResponse(
                        array(
                            'title' => "Manage Service Form - {$service->getName()}",
                            'subtitle' => 'Services list',
                            'service' => $service,
                            'operator' => $this->getParameter('operator_name'),
                            'forms' => $forms,
                        )
        );
    }

    /**
     *
     * Save existing Dynamic Form ready for publishing
     *
     * @param Service $service
     *
     * @Route("/forms/{id}/save",name="dashboard.service.form.save")
     * @Method("GET")
     *
     * @return Response
     */
    public function saveServiceFormAction(Service $service) {

        $resp = new JsonObject();
        $status = false;

        try {

            $entityManager = $this->getDoctrine()->getManager();

            $primaryLanguage = $this->container->getParameter("primary_language");
            $secondaryLanguage = $this->container->getParameter("secondary_language");

            $steps = $entityManager->getRepository("DashboardBundle:Step")->findBy(
                    array(
                        'serviceId' => $service,
                    )
            );

            $output = array();

            foreach ($steps as $key => $step) {

                $stepName = 'step' . ($key + 1);

                $output[$stepName] = array(
                    'next' => $step->getNext(),
                    'title' => array(
                        $primaryLanguage => $step->getTitle(),
                        $secondaryLanguage => $step->getTitleAlt(),
                    ),
                );

                //populating step fields
                $stepFields = $entityManager->getRepository('DashboardBundle:StepField')->findBy(
                        array(
                            'step' => $step,
                        )
                );

                $fields = array();
                foreach ($stepFields as $index => $stepField) {

                    $skey = $stepField->getSkey()->getName();

                    $fields[$index] = array(
                        'key' => $skey,
                        'type' => $stepField->getType()->getName(),
                        'required' => $stepField->getRequired(),
                        'hint' => array(
                            $primaryLanguage => $stepField->getHint(),
                            $secondaryLanguage => $stepField->getHintLangAlt(),
                        ),
                    );

                    if ($stepField->getRegex()) {
                        $fields[$index]['regex'] = array(
                            'value' => $stepField->getRegex(),
                            'err' => array(
                                $primaryLanguage => $stepField->getRegexError(),
                                $secondaryLanguage => $stepField->getRegexErrorAlt(),
                            ),
                        );
                    }

                    if (strcmp($skey, "gender") == 0) {

                        $fields[$index]['options'] = array();

                        $genders = $entityManager->getRepository("DashboardBundle:Gender")->findAll();

                        foreach ($genders as $gender) {
                            $fields[$index]['options'][] = array(
                                'value' => $gender->getShortHand(),
                                'key' => array(
                                    $primaryLanguage => $gender->getName(),
                                    $secondaryLanguage => $gender->getNameAlt(),
                                ),
                            );
                        }
                    }

                    if (strcmp($skey, "id-type") == 0) {

                        $fields[$index]['options'] = array();

                        $identificationTypes = $entityManager->getRepository("DashboardBundle:Idtype")->findBy(
                                array("isActive" => 1)
                        );

                        foreach ($identificationTypes as $identificationType) {
                            $fields[$index]['options'][] = array(
                                'value' => $identificationType->getId(),
                                'key' => array(
                                    $primaryLanguage => $identificationType->getName(),
                                    $secondaryLanguage => $identificationType->getNameAlt(),
                                ),
                            );
                        }
                    }

                    if (strcmp($skey, "region") == 0) {

                        $fields[$index]['child'] = 'territory';

                        $fields[$index]['options'] = array(
                            array(
                                'value' => 'Select Region',
                                'key' => '',
                            ),
                        );

                        $regions = $entityManager->getRepository('DashboardBundle:Region')->findAll();

                        foreach ($regions as $region) {
                            $fields[$index]['options'][] = array(
                                'value' => $region->getCode(),
                                'key' => $region->getName(),
                            );
                        }
                    }

                    if (strcmp($skey, "territory") == 0) {

                        $regions = $entityManager->getRepository('DashboardBundle:Region')->findAll();

                        foreach ($regions as $region) {

                            $territories = $region->getTerritories();

                            $v_options = array(
                                array(
                                    'value' => 'Select Territory',
                                    'key' => '',
                                ),
                            );

                            foreach ($territories as $index => $territory) {
                                $v_options[$region->getCode()][] = array(
                                    'value' => $territory->getId(),
                                    'key' => $territory->getName(),
                                );
                            }

                            $fields[$index]['v_options'] = $v_options;
                        }
                    }

                    //add fields node to the output array
                    $output[$stepName]['fields'] = $fields;
                }
            }

            $output['count'] = count($output);

            $formJsonString = json_encode(
                    array(
                        'form' => $output,
                    )
            );

            $service->setFormJson($formJsonString);

            $entityManager->persist($service);
            $entityManager->flush();

            $status = true;
            $resp->setMessage("SUCCESS");
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Service $service
     *
     * @return Response
     *
     * @Route("/forms/{id}/steps",name="dashboard.service.form.steps")
     * @Method({"POST","GET"})
     */
    public function listStepAction(Service $service) {
        $response = new JTableResponse();
        try {

            $entityManager = $this->getDoctrine()->getManager();

            $steps = $entityManager->createQuery(
                            "SELECT s.id, s.title,s.titleAlt,s.next,concat('step',s.id) as step FROM DashboardBundle:Step s WHERE s.serviceId = {$service->getId()}"
                    )
                    ->getResult();

            $steps = $entityManager->getRepository('DashboardBundle:Step')->findBy(
                    array(
                        'serviceId' => $service,
                    )
            );

            $output = array();
            foreach ($steps as $index => $step) {
                $output[] = array(
                    'id' => $step->getId(),
                    'next' => $step->getNext(),
                    'step' => 'step' . ($index + 1),
                    'title' => $step->getTitle(),
                    'titleAlt' => $step->getTitleAlt(),
                );
            }


            $response->setRecords($output);
        } catch (Exception $e) {
            $response->setMessage($e->getMessage());
            $response->setResult("ERROR");
        }

        return $this->buildResponse($response, Response::HTTP_OK);
    }

    /**
     * @param Service $service
     * @param Request $request
     *
     * @return Response
     * @Route("/forms/{id}/step/create",name="dashboard.service.form.step.create")
     * @Method({"POST","GET"})
     */
    public function createStepAction(Service $service, Request $request) {
        $response = new JTableResponse();

        try {

            $entityManager = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $step = new Step();
            $step->setNext($attributes["next"]);
            $step->setTitle($attributes["title"]);
            $step->setTitleAlt($attributes['titleAlt']);
            $step->setServiceId($service);

            $entityManager->persist($step);
            $entityManager->flush();

            $response->setRecord(
                    array(
                        'id' => $step->getId(),
                        'title' => $step->getTitle(),
                        'next' => $step->getNext(),
                        'titleAlt' => $step->getTitleAlt(),
                    )
            );
        } catch (Exception $e) {
            $response->setMessage($e->getMessage());
            $response->setResult("ERROR");
        }

        return $this->buildResponse($response, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @Route("/forms/step/update",name="dashboard.service.form.step.update")
     * @Method({"POST","GET"})
     *
     */
    public function updateStepAction(Request $request) {
        $resp = new JTableResponse();
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $step = $entityManager->getRepository('DashboardBundle:Step')->find($attributes['id']);

            if ($step) {

                $step->setNext($attributes["next"]);
                $step->setTitle($attributes["title"]);
                $step->setTitleAlt($attributes['titleAlt']);
                $entityManager->flush();

                $resp->setRecord(
                        array(
                            'id' => $step->getId(),
                            'title' => $step->getTitle(),
                            'next' => $step->getNext(),
                            'titleAlt' => $step->getTitleAlt(),
                        )
                );
            }
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @Route("/forms/step/delete",name="dashboard.service.form.step.delete")
     * @Method({"POST","GET"})
     *
     */
    public function deleteStepAction(Request $request) {

        $response = new JsonResponse();

        try {

            $entityManager = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $step = $entityManager->getRepository('DashboardBundle:Step')->find($attributes['id']);

            if (!$step) {
                $response->setData(
                        array(
                            "Result" => "ERROR",
                            "Message" => 'No Record found..',
                        )
                );

                return $response;
            }

            $entityManager->remove($step);
            $entityManager->flush();

            $response->setData(
                    array(
                        "Result" => "OK",
                    )
            );
        } catch (\Exception $exception) {
            $response->setData(
                    array(
                        "Result" => "ERROR",
                        "Message" => $exception->getMessage(),
                    )
            );
        }

        return $response;
    }

    /**
     * @param Request $request
     * @Route("/forms/step/field/list",name="dashboard.service.step.field.list")
     * @Method({"POST","GET"})
     *
     * @return Response
     */
    public function listStepFieldAction(Request $request) {
        $resp = new JTableResponse();

        try {

            $em = $this->getDoctrine()->getManager();

            $attributes = $request->query->all();

            $fields = $em->getRepository('DashboardBundle:StepField')->findBy(
                    array(
                        'step' => $attributes['stepId'],
                    )
            );

            $output = array();
            foreach ($fields as $index => $field) {
                $output[] = array(
                    'fieldId' => $field->getId(),
                    'skey' => $field->getSkey()->getId(),
                    'required' => $field->getRequired(),
                    'regex' => $field->getRegex(),
                    "hint" => $field->getHint(),
                    "hintLangAlt" => $field->getHintLangAlt(),
                    "regexErrorAlt" => $field->getRegexErrorAlt(),
                    "type" => $field->getType()->getId(),
                    "regexError" => $field->getRegexError(),
                    "action" => $field->getAction(),
                );
            }

            $resp->setRecords($output);
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/forms/step/field/create",name="dashboard.service.step.field.create")
     * @Method({"POST","GET"})
     *
     * @return Response
     */
    public function createStepFieldAction(Request $request) {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $step = $em->getRepository('DashboardBundle:Step')->findOneBy(array('id' => $attributes['id']));

            if ($step) {

                $field = new StepField();

                $field->setSkey($em->getRepository('DashboardBundle:FieldNames')->find($attributes['skey']));
                $field->setHint($attributes['hint']);
                $field->setRequired($attributes['required']);
                $field->setType($em->getRepository('DashboardBundle:FieldTypes')->find($attributes['type']));

                if (isset($attributes['action'])) {
                    $field->setAction($attributes['action']);
                }
                if (isset($attributes["regex"])) {
                    $field->setRegex($attributes['regex']);
                }
                if (isset($attributes["regexError"])) {
                    $field->setRegexError($attributes['regexError']);
                }
                if (isset($attributes["hintLangAlt"])) {
                    $field->setHintLangAlt($attributes["hintLangAlt"]);
                }
                if (isset($attributes["regexErrorAlt"])) {
                    $field->setRegexErrorAlt($attributes["regexErrorAlt"]);
                }
                //maxLengthErrorAlt
                $field->setStep($step);
                $em->persist($field);
                $em->flush();

                $resp->setRecord(
                        array(
                            "fieldId" => $field->getId(),
                            "hint" => $field->getHint(),
                            "skey" => $field->getSkey()->getName(),
                            "required" => $field->getRequired(),
                            "type" => $field->getType()->getName(),
                            "regex" => $field->getRegex(),
                            "regexError" => $field->getRegexError(),
                            "hintLangAlt" => $field->getHintLangAlt(),
                            "regexErrorAlt" => $field->getRegexErrorAlt(),
                        )
                );
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/forms/step/field/update",name="dashboard.service.step.field.update")
     * @Method({"POST","GET"})
     *
     * @return Response
     */
    public function updateStepFieldAction(Request $request) {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();
            $stepField = $em->getRepository('DashboardBundle:StepField')->findOneBy(
                    array('id' => $attributes['fieldId'])
            );
            if ($stepField) {

                $stepField->setSkey($em->getRepository('DashboardBundle:FieldNames')->find($attributes['skey']));
                $stepField->setHint($attributes['hint']);
                $stepField->setRequired($attributes['required']);
                $stepField->setType($em->getRepository('DashboardBundle:FieldTypes')->find($attributes['type']));

                if (isset($attributes['action'])) {
                    $stepField->setAction($attributes['action']);
                }
                if (isset($attributes["regex"])) {
                    $stepField->setRegex($attributes['regex']);
                }
                if (isset($attributes["regexError"])) {
                    $stepField->setRegexError($attributes['regexError']);
                }


                if (isset($attributes["action"])) {
                    $stepField->setAction($attributes['action']);
                }
                if (isset($attributes["hintLangAlt"])) {
                    $stepField->setHintLangAlt($attributes['hintLangAlt']);
                }
                if (isset($attributes["regexErrorAlt"])) {
                    $stepField->setRegexErrorAlt($attributes["regexErrorAlt"]);
                }


                $em->flush();

                $resp->setRecord(
                        array(
                            "fieldId" => $stepField->getId(),
                            "skey" => $stepField->getSkey()->getName(),
                            "required" => $stepField->getRequired(),
                            "type" => $stepField->getType()->getName(),
                            "regex" => $stepField->getRegex(),
                            "regexError" => $stepField->getRegexError(),
                            "hintLangAlt" => $stepField->getHintLangAlt(),
                            "regexErrorAlt" => $stepField->getRegexErrorAlt(),
                        )
                );
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    private function _buildForm() {
        $entityManager = $this->getDoctrine()->getManager();

        $services = $entityManager->getRepository('DashboardBundle:Service')->findBy(
                array(
            'status' => true,
                ), array('ordering' => 'asc')
        );

        $test = array();
        foreach ($services as $index => $service) {
            if ($service->getFormJson()) {
                $savedForm = json_decode($service->getFormJson());
                $test[] = array(
                    'name' => $service->getName(),
                    'order' => $service->getOrdering(),
                    'form' => $savedForm->form
                );
            }
        }
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     * @Route("/agentservice/{id}", name="agentservice")
     * @Method("GET")
     * @Template("DashboardBundle:Services:agentservice.html.twig")
     */
    public function agentserviceAction($id) {
        $em = $this->getDoctrine()->getManager();

        $query = $em->createQueryBuilder()
                ->select(" s.name, s.id"
                        . " from DashboardBundle:Service s")
                ->getQuery();

        $result1 = $query->getResult();

        $data = array(
            'title' => "Edit Agent Services",
            'title_descr' => "Edit agent services",
            'recordID' => $id,
            'services' => $result1
        );

        return $this->prepareResponse($data);
    }

    /**
     * Creates a new Faq entity.
     *
     * @Route("/agentservicePOST/{id}", name="agentservicePOST")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function agentservicePOSTAction($id, Request $request) {
        $em = $this->getDoctrine()->getManager();

        $query1 = "DELETE FROM AgentService where agentID = '" . $id . "'";
        $em->getConnection()->exec($query1);

        foreach ($request->request->get('services') as $key => $val) {
            $query1 = "INSERT INTO AgentService (agentID, serviceID) values(".$id.", ".$val.")";
            $em->getConnection()->exec($query1);
        }
        
        return $this->redirect($this->generateUrl('admin'));
        
        exit;
    }

}
