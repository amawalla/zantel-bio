<?php

namespace aimgroup\DashboardBundle\Controller;

use aimgroup\DashboardBundle\Dao\JTableResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Form\IdtypeType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Idtype controller.
 *
 * @Route("admin/idtype")
 */
class IdtypeController extends AbstractController
{

    /**
     * Lists all Idtype entities.
     *
     * @Route("", name="idtype_index")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {

        $data = array(
            'title' => "Identification Types",
            'title_descr' => "Id Types in Use",
        );
        
        return $this->prepareResponse($data);
    }

    /**
     * @param Request $request
     * @Route("/list",name="idtype_list")
     * @Method({"POST","GET"})
     *
     * @return JsonResponse
     */
    public function listIdTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {
            $em = $this->getDoctrine()->getEntityManager();
            $IdTypes = $em->getRepository("DashboardBundle:Idtype")
                ->createQueryBuilder('e')
                ->select('e.id, e.name, e.nameAlt, e.isActive')
                ->getQuery()
                ->getResult();
            $resp->setRecords($IdTypes);
        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Route("/create",name="idtype_create")
     * @Method({"POST","GET"})
     *
     * @return JsonResponse
     */
    public function createIdTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            $idType = new Idtype();
            $idType->setName($attributes["name"]);
            $idType->setNameAlt($attributes["nameAlt"]);
            $idType->setIsActive($attributes["isActive"]);
            $idType->setUseCount(0);
            $idType->setTrainingName('');
            $idType->setRegexString($attributes['regexString']);

            $em->persist($idType);
            $em->flush();

            $resp->setRecord(array(
                'id' => $idType->getId(),
                'name' => $idType->getName(),
                'nameAlt' => $idType->getNameAlt(),
                'isActive' => $idType->getIsActive()
            ));

        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @Route("/update",name="idtype_update")
     * @Method({"POST","GET"})
     *
     * @return JsonResponse
     */
    public function updateIdTypeAction(Request $request)
    {
        $resp = new JTableResponse();
        try {

            $em = $this->getDoctrine()->getManager();
            $attributes = $request->request->all();

            /** @var Idtype $idType */
            $idType = $em->getRepository('DashboardBundle:Idtype')->find($attributes['id']);
            if ($idType) {
                $idType->setName($attributes["name"]);
                $idType->setNameAlt($attributes["nameAlt"]);
                $idType->setIsActive($attributes["isActive"]);

                $em->persist($idType);
                $em->flush();

                $resp->setRecord(array(
                    'id' => $idType->getId(),
                    'name' => $idType->getName(),
                    'nameAlt' => $idType->getNameAlt(),
                    'isActive' => $idType->getIsActive()
                ));
            } else {
                $resp->setMessage("NO ID TYPE WITH ID ".$attributes['id']." FOUND");
                $resp->setResult("ERROR");
            }


        } catch (Exception $e) {
            $resp->setMessage($e->getMessage());
            $resp->setResult("ERROR");
        }

        return $this->buildResponse($resp, Response::HTTP_OK);
    }
}
