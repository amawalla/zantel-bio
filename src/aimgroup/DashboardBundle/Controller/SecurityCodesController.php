<?php

namespace aimgroup\DashboardBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use aimgroup\DashboardBundle\Entity\SecurityCodes;
use aimgroup\DashboardBundle\Form\SecurityCodesType;

/**
 * SecurityCodes controller.
 *
 * @Route("/securitycodes")
 */
class SecurityCodesController extends Controller {

    /**
     * Lists all SecurityCodes entities.
     *
     * @Route("/", name="securitycodes")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('DashboardBundle:SecurityCodes')->findAll();

        return array(
            'entities' => $entities,
        );
    }

    /**
     * Creates a new SecurityCodes entity.
     *
     * @Route("/", name="securitycodes_create")
     * @Method("POST")
     * @Template("DashboardBundle:SecurityCodes:new.html.twig")
     */
    public function createAction(Request $request) {
        $entity = new SecurityCodes();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('securitycodes_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Creates a form to create a SecurityCodes entity.
     *
     * @param SecurityCodes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SecurityCodes $entity) {
        $form = $this->createForm(new SecurityCodesType(), $entity, array(
            'action' => $this->generateUrl('securitycodes_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SecurityCodes entity.
     *
     * @Route("/new", name="securitycodes_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction() {
        $entity = new SecurityCodes();
        $form = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form' => $form->createView(),
        );
    }

    /**
     * Finds and displays a SecurityCodes entity.
     *
     * @Route("/{id}", name="securitycodes_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:SecurityCodes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SecurityCodes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SecurityCodes entity.
     *
     * @Route("/{id}/edit", name="securitycodes_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:SecurityCodes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SecurityCodes entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a form to edit a SecurityCodes entity.
     *
     * @param SecurityCodes $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createEditForm(SecurityCodes $entity) {
        $form = $this->createForm(new SecurityCodesType(), $entity, array(
            'action' => $this->generateUrl('securitycodes_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Edits an existing SecurityCodes entity.
     *
     * @Route("/{id}", name="securitycodes_update")
     * @Method("PUT")
     * @Template("DashboardBundle:SecurityCodes:edit.html.twig")
     */
    public function updateAction(Request $request, $id) {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('DashboardBundle:SecurityCodes')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SecurityCodes entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('securitycodes_edit', array('id' => $id)));
        }

        return array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a SecurityCodes entity.
     *
     * @Route("/{id}", name="securitycodes_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id) {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('DashboardBundle:SecurityCodes')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SecurityCodes entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('securitycodes'));
    }

    /**
     * Creates a form to delete a SecurityCodes entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('securitycodes_delete', array('id' => $id)))
                        ->setMethod('DELETE')
                        ->add('submit', 'submit', array('label' => 'Delete'))
                        ->getForm()
        ;
    }

}
