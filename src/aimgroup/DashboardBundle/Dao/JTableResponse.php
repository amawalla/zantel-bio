<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/2/15
 * Time: 2:17 PM
 */

namespace aimgroup\DashboardBundle\Dao;


class JTableResponse
{
    /**
     * @var string
     */
    private $Result = "OK";

    /**
     * @var Object
     */
    private $Records;
    /**
     * @var Object
     */
    private $TotalRecordCount;

    /**
     * @var Object
     */
    private $record;
    /**
     * @var string
     */
    private $Message;

    /**
     * @var Object
     */
    private $options;

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->Result;
    }

    /**
     * @param string $Result
     */
    public function setResult($Result)
    {
        $this->Result = $Result;
    }

    /**
     * @return Object
     */
    public function getRecords()
    {
        return $this->Records;
    }

    /**
     * @param Object $Records
     */
    public function setRecords($Records)
    {
        $this->Records = $Records;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->Message;
    }

    /**
     * @param string $Message
     */
    public function setMessage($Message)
    {
        $this->Message = $Message;
    }

    /**
     * @return Object
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param Object $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getRecord()
    {
        return $this->record;
    }

    /**
     * @param mixed $record
     */
    public function setRecord($record)
    {
        $this->record = $record;
    }


    /**
     * @return Object
     */
    public function getTotalRecordCount()
    {
        return $this->TotalRecordCount;
    }

    /**
     * @param Object $TotalRecordCount
     */
    public function setTotalRecordCount($TotalRecordCount)
    {
        $this->TotalRecordCount = $TotalRecordCount;
    }


}
