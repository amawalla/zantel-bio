<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Dao;

use aimgroup\RestApiBundle\Entity\Product;

/**
 * Description of PriceDao
 *
 * @author Michael Tarimo
 */
class PriceDao extends AbstractDao {
    
    public function addPrice(Product $product, $price) {
        
        $em = $this->getDoctrine()->getEntityManager();
        
        $em->persist($product);
        $em->flush();

        // how to fetch
        //$product = $em->find('Product', $id);
        
    }
}
