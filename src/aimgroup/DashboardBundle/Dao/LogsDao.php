<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Dao;

use aimgroup\DashboardBundle\Entity\UserLog;
use aimgroup\RestApiBundle\Dao\AbstractDao;
use Doctrine\ORM\EntityManager;

/**
 * Description of LogsDao
 *
 * @author Michael Tarimo
 */
class LogsDao extends AbstractDao
{

    public $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function addUserLogs($user, $action, $description, $metadata)
    {

        $log = new UserLog();
        $log->setUser($user);
        $log->setAction($action);
        $log->setDescription($description);
        $log->setMetadata($metadata);
        $log->setTimestamp(new \DateTime("now"));
        $this->em->persist($log);
        $this->em->flush();

        $data = array(
            'success' => true
        );

        return $data;
    }

    public function getAllLogs()
    {
        $queryString = "SELECT * FROM DashboardBundle:UserLog, DashboardBundle:UserLog";
        $query = $this->getDoctrine()->getEntityManager()->createQuery($queryString);
        $usertLogs = $query->getArrayResult();

        return $usertLogs;
    }

    public function getUserLogs($username)
    {
        $queryString = "SELECT * FROM DashboardBundle:UserLog l, DashboardBundle:UserLog u WHERE u.username = " . $username;
        $query = $this->getDoctrine()->getEntityManager()->createQuery($queryString);
        $usertLogs = $query->getArrayResult();
        return $usertLogs;
    }

    public function clearLogs($startDate, $endDate)
    {


    }
}