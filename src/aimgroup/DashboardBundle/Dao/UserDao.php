<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Dao;

use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Entity\UserType;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Util\SecureRandomInterface;

/**
 * Description of UserDao
 *
 * @author Robert Makoye
 */
class UserDao extends AbstractDao {
    
    public $entityManager;
    public $secureRandom;

    public function __construct(EntityManager $entityManager,SecureRandomInterface $secureRandom){
    	$this->entityManager = $entityManager;
    	$this->secureRandom = $secureRandom;
    }

    public function createUser(User $user){
        $confirmationCode = $this->secureRandom->nextByte(4);
        $user->setConfirmationCode(substr(md5($confirmationCode),0,6));
        $this->entityManager->persist($user);
        $this->entityManager->flush();
        $this->entityManager->clear();

        return $user;
    }
    
    public function setAttributes(User $user,$attributes = array()){
        $user->setUsername($attributes['username']);
        $user->setPassword($attributes['password']);
        if(isset($attributes['email'])) {
            $user->setEmail($attributes['email']);
            $user->setContactEmail($attributes['email']);
        }
        if(isset($attributes['additional_data']['uid'])) {
            $user->setUid($attributes['additional_data']['uid']);
            $user->setContactEmail(@$attributes['additional_data']['email']);
        }

        if(isset($attributes['additional_data']['country'])){
             $countryId = $attributes['additional_data']['country'];
             $country = $this->entityManager->getRepository('RestApiBundle:Country')->find($countryId);
             $user->setCountry($country);
        }
       
        $user->setEmailCanonical($attributes['email']);
        $user->setFirstName($attributes['additional_data']['firstname']);
        $user->setMiddleName($attributes['additional_data']['middlename']);
        $user->setLastName($attributes['additional_data']['lastname']);
        $user->setMobileNumber($attributes['additional_data']['phone']);
        $user->setGender($attributes['additional_data']['gender']);
        $user->setJoinedThrough($attributes['additional_data']['channel']);
        $user->setUserType(UserType::NORMAL_USER);
        if(isset($attributes['additional_data']['birthday'])) {
            $user->setDob(new \DateTime($attributes['additional_data']['birthday']));
        }
        $user->setEnabled(true);
        $user->setSalt(rand(1000, 9999));
        $user->setLocked(false);
        $user->setExpired(true);
        $user->setCredentialsExpired(true);
        $confirmationCode = $this->secureRandom->nextBytes(4);
        $user->setConfirmationToken(substr(md5($confirmationCode),0,6));
        return $user;
    }

    public function setSystemUserAttributes(User $user,$attributes = array()){
        $user->setUsername('mkito');
        $user->setPassword('123wsx');
        $user->setEmail('mkito@mkito.com');
        $user->setUid('MK1000000');
        $user->setEmailCanonical('mkito@mkito.com');
        $user->setFirstName('Mkito');
        $user->setMiddleName(' ');
        $user->setLastName('System');
        $user->setMobileNumber('NA');
        $user->setGender('N');
        $user->setUserType(UserType::SYSTEM_USER);
        $user->setDob(new \DateTime(date('Y-m-d H:i:s')));
        $user->setEnabled(true);
        $user->setSalt(rand(1000, 9999));
        $user->setLocked(false);
        $user->setExpired(true);
        $user->setCredentialsExpired(true);
        $confirmationCode = $this->secureRandom->nextBytes(4);
        $user->setConfirmationToken(substr(md5($confirmationCode),0,6));
        return $user;
    }
}
