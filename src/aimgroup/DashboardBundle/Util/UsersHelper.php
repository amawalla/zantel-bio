<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 17/05/2016
 * Time: 12:50 PM
 */

namespace aimgroup\DashboardBundle\Util;


use aimgroup\DashboardBundle\Entity\LogMessage;
use aimgroup\DashboardBundle\Entity\Territory;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UsersHelper {

    const EXISTS  = 1;
    const SUCCESS = 2;
    const FAIL    = 3;
    const NOSUPERAGENT = 4;
    const NOREGION = 5;
    const NOTERRITORY = 6;
    const NOPOSNAME = 7;
    const NOROUTENAME = 8;
    const NOOWNERNAME = 9;
    const NOMANAGERNAME = 10;

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function createUser($attributes, $isCommand = false)
    {
        $em = $this->container->get('doctrine')->getManager();

        try {

            $isExists = $em->getRepository("RestApiBundle:User")->isMobileExists(substr($attributes['msisdn'], -9));
            if ($isExists) {
                return UsersHelper::EXISTS;
            }

            $user = new User();
            $user->setFirstName($attributes['firstName']);
            $user->setMiddleName($attributes["middleName"]);
            $user->setLastName($attributes['lastName']);
            $user->setMobileNumber(substr($attributes['msisdn'], -9));
            $user->setIsEnrolledToTest(0);
            $user->setUserType($attributes["userType"]);
            $user->setUsername($user->getMobileNumber());
            $user->setEnabled(true);
            $user->setSalt(rand(1000, 9999));
            $user->setNumberDevices($attributes["number_devices"]);
            $user->setAgentExclusivity($attributes["agent_exclusivity"]);
            $user->setDob(new \DateTime($attributes["dob"]));
            $user->setStatus(true);
            $user->setGender(strtoupper(substr($attributes["gender"], 0, 1)));
            $user->setRoles(array("ROLE_AGENT"));

            /** optional fields */
            if (isset($attributes["agentCode"]))
                $user->setAgentCode($attributes['agentCode']);
            if (isset($attributes['email'])) {
                $user->setEmail($attributes['email']);
                $user->setEmailCanonical($user->getEmail());
            }

            $territory = $em->getRepository('DashboardBundle:Territory')->findOneBy(array("id" => $attributes["territory"]));
            if ($territory instanceof Territory) {
                $user->setTerritory($territory);
                $user->setRegion($territory->getRegion());
            }

            $encoder = $this->container->get('security.password_encoder');
            $encoded = $encoder->encodePassword($user, $user->getPassword());
            $user->setPassword($encoded);

            $confirmationCode = rand(10000000, 99999999); //md5($secureRandom->nextBytes(4));
            $user->setConfirmationToken($confirmationCode);

            if ($isCommand) {
                $parent = $em->getRepository('RestApiBundle:User')->findOneBy(array("username" => $attributes["superagent"]));
            } else {
                $thisuser = $this->container->get('security.context')->getToken()->getUser();
                $uid = $thisuser->getId();
                $parent = $em->getRepository('RestApiBundle:User')->find($uid);
            }

            $user->setParentId($parent);
            $em->persist($user);
            $em->flush();

            //generate secret and clientid
            $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
            $client = $clientManager->createClient();
            $client->setRedirectUris(array($this->container->getParameter("hostname")));
            $client->setAllowedGrantTypes(array("authorization_code", "password", "refresh_token", "token", "client_credentials"));
            $clientManager->updateClient($client);

            $device = new Device();
            $device->setUser($user);
            $device->setMsisdn($user->getMobileNumber());
            $device->setPrivateKey($client->getSecret());
            $device->setPublicKey($client->getPublicId());

            $device->setAccessToken($confirmationCode);
            $em->persist($device);

            $em->flush();

            //send

            $activateMessage = sprintf(
                "Ndugu wakala, namba yako ya kuwezesha aplikesheni yako ya EKYC ni %s",
                $device->getAccessToken()
            );

            $this->container->get("aimgroup.rest.messaging.sms")->send($device->getMsisdn(), $activateMessage);

            return UsersHelper::SUCCESS;

        }
        catch (\Exception $e) {
            $this->container->get("api.helper")->logE("UsersHelper", "createUser", $e);

            return UsersHelper::FAIL;
        }
    }

    /**
     *
     *
     *
     */
    public function logUserAction()
    {
        try {
            $em = $this->container->get('doctrine')->getEntityManager();
            $log = new LogMessage();

        }
        catch (\Exception $e) {

        }

    }

    public function deleteUsers($attributes, $true)
    {
        try {
            $shouldDelete = true;
            $em = $this->container->get('doctrine')->getEntityManager();
            /** @var  $user User */
            $user = $em->getRepository("RestApiBundle:User")->findByMsisdn(substr($attributes['msisdn'], -9));
            if ($user) {
                $devices = $em->getRepository("RestApiBundle:Device")->findBy(array("user" => $user));
                if ($devices) {
                    foreach ($devices as $device) {
                        if ($device->getIsActive() == 1) {
                            $shouldDelete = false;
                            break;
                        }
                        $em->remove($device);
                    }
                }
                if ($shouldDelete) {
                    $em->remove($user);
                    $em->flush();

                    return UsersHelper::SUCCESS;
                }

                return UsersHelper::FAIL;
            }

        } catch (\Exception $e) {
            $this->container->get("api.helper")->logE("UsersHelper", "deleteUsers", $e);

            return UsersHelper::FAIL;
        }
    }
    
    
    
    
    
    
    public function createAdminUser($attributes, $iscommand = false) {
        try {
            $em = $this->container->get('doctrine')->getManager();

            $isExists = $em->getRepository("RestApiBundle:User")->isUsernameExists($attributes['username']);
            if ($isExists) {
                return UsersHelper::EXISTS;
            } else {
                $user = new User();
                $user->setFirstName($attributes['firstName']);
                $user->setMiddleName($attributes["middleName"]);
                $user->setLastName($attributes['lastName']);
                $user->setMobileNumber(substr($attributes['msisdn'], -9));
                $user->setIdNumber($attributes['identification_number']);

                if (false && isset($attributes['identification_type']) && is_numeric($attributes['identification_type'])) {
                    $user->setType($attributes['identification_type']);  //($em->getRepository('DashboardBundle:Idtype')->find($attributes['identification_type']));
                }
                
                

                //$user->setUserType($attributes["userType"]);
                $userType = $em->getRepository('DashboardBundle:UserType')->findOneBy(array("description" => $attributes["userType"]));

                if ($userType) {
                    $user->setUserType($userType->getId());
                }else{
                    $user->setUserType(1);
                }

                if(array_key_exists('username', $attributes)) {
                    $user->setUsername($attributes["username"]);
                } else {
                    $user->setUsername(substr($attributes['msisdn'], -9));
                }

                $user->setEnabled(true);
                $user->setSalt(rand(1000, 9999));
                $user->setNumberDevices(1);
                $user->setAgentExclusivity(1);
                $user->setDob(new \DateTime($attributes["dob"]));
                $user->setStatus(true);
                $user->setGender($attributes["gender"]);
                $user->setRoles( (isset($attributes["user_role"]) || strlen(@$attributes["user_role"]) >= 4 ? array($attributes["user_role"]) : array("ROLE_AGENT"))  );

                
                /** optional fields */
                if (isset($attributes['email'])) {
                    $user->setEmail($attributes['email']);
                    $user->setEmailCanonical($user->getEmail());
                }
                /** end optional fields */
                
                $region = $em->getRepository('DashboardBundle:Region')->findOneBy(array("name" => $attributes["region"]));
                if ($region) {
                    $user->setRegion($region);
                }else{
                    return UsersHelper::NOREGION;
                }
                
                $territory = $em->getRepository('DashboardBundle:Territory')->findOneBy(array("name" => $attributes["territory"]));
                if ($territory) {
                    $user->setTerritory($territory);
                }else{
                    return UsersHelper::NOTERRITORY;
                }
                
                
                
                $user->setPassword($attributes['password']);

                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                //generate secret and clientid
                $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
                $client = $clientManager->createClient();
                $client->setRedirectUris(array($this->container->getParameter("hostname")));
                $client->setAllowedGrantTypes(array("authorization_code", "password", "refresh_token", "token", "client_credentials"));
                $clientManager->updateClient($client);

                $confirmationCode = rand(10000000, 99999999); //md5($secureRandom->nextBytes(4));
                $user->setConfirmationToken($confirmationCode);

                $em->persist($user);

                
                $em->flush();
                
                

                //send token
                //$this->container->get('api.helper')->sendSms($user->getUsername(), $user->getConfirmationToken());
                return UsersHelper::SUCCESS;
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            return UsersHelper::FAIL . " MESSAGE:: " . json_encode($e->getMessage());
        }
    }
    
    


}
