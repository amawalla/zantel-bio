<?php

namespace aimgroup\DashboardBundle\Util;
use aimgroup\DashboardBundle\Dao\JTableResponse;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/2/15
 * Time: 3:15 PM
 */
class JTableNormalizer implements NormalizerInterface
{

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object object to normalize
     * @param string $format format the normalization result will be encoded as
     * @param array $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'Result'     => $object->getResult(),
            'Records'   => $object->getRecords(),
            'Record'   => $object->getRecord(),
            'Message' => $object->getMessage(),
            'Options' => $object->getOptions()
        ];
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed $data Data to normalize.
     * @param string $format The format being (de-)serialized from or into.
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof JTableResponse;
    }
}
