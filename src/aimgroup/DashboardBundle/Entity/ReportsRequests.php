<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReportsRequests
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifeCycleCallbacks()
 */
class ReportsRequests {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="report_type", type="string", length=255)
     */
    private $reportType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from_date", type="datetime")
     */
    private $fromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to_date", type="datetime")
     */
    private $toDate;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="territory", type="string", length=255)
     */
    private $territory;

    /**
     * @var string
     *
     * @ORM\Column(name="agent_msisdn", type="string", length=255)
     */
    private $agentMsisdn;

    /**
     * @var string
     *
     * @ORM\Column(name="customer_msisdn", type="string", length=255)
     */
    private $customerMsisdn;

    /**
     * @var string
     *
     * @ORM\Column(name="email_receipient", type="string", length=255)
     */
    private $emailReceipient;

    /**
     * @var string
     *
     * @ORM\Column(name="email_message", type="text")
     */
    private $emailMessage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set createdDate
     *
     * @param \DateTime $createdDate
     * @ORM\PrePersist
     * @return ReportsRequests
     */
    public function setCreatedDate() {
        if (!isset($this->createdDate)) {
            $this->createdDate = new \DateTime;
        }
        return $this;
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * Set reportType
     *
     * @param string $reportType
     * @return ReportsRequests
     */
    public function setReportType($reportType) {
        $this->reportType = $reportType;

        return $this;
    }

    /**
     * Get reportType
     *
     * @return string 
     */
    public function getReportType() {
        return $this->reportType;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return ReportsRequests
     */
    public function setFromDate($fromDate) {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate() {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return ReportsRequests
     */
    public function setToDate($toDate) {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime 
     */
    public function getToDate() {
        return $this->toDate;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return ReportsRequests
     */
    public function setRegion($region) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set territory
     *
     * @param string $territory
     * @return ReportsRequests
     */
    public function setTerritory($territory) {
        $this->territory = $territory;

        return $this;
    }

    /**
     * Get territory
     *
     * @return string 
     */
    public function getTerritory() {
        return $this->territory;
    }

    /**
     * Set agentMsisdn
     *
     * @param string $agentMsisdn
     * @return ReportsRequests
     */
    public function setAgentMsisdn($agentMsisdn) {
        $this->agentMsisdn = $agentMsisdn;

        return $this;
    }

    /**
     * Get agentMsisdn
     *
     * @return string 
     */
    public function getAgentMsisdn() {
        return $this->agentMsisdn;
    }

    /**
     * Set customerMsisdn
     *
     * @param string $customerMsisdn
     * @return ReportsRequests
     */
    public function setCustomerMsisdn($customerMsisdn) {
        $this->customerMsisdn = $customerMsisdn;

        return $this;
    }

    /**
     * Get customerMsisdn
     *
     * @return string 
     */
    public function getCustomerMsisdn() {
        return $this->customerMsisdn;
    }

    /**
     * Set emailReceipient
     *
     * @param string $emailReceipient
     * @return ReportsRequests
     */
    public function setEmailReceipient($emailReceipient) {
        $this->emailReceipient = $emailReceipient;

        return $this;
    }

    /**
     * Get emailReceipient
     *
     * @return string 
     */
    public function getEmailReceipient() {
        return $this->emailReceipient;
    }

    /**
     * Set emailMessage
     *
     * @param string $emailMessage
     * @return ReportsRequests
     */
    public function setEmailMessage($emailMessage) {
        $this->emailMessage = $emailMessage;

        return $this;
    }

    /**
     * Get emailMessage
     *
     * @return string 
     */
    public function getEmailMessage() {
        return $this->emailMessage;
    }

}
