<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Verifications
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Verifications {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdDate", type="datetime")
     */
    private $createdDate;

    /**
     * @var string
     *
     * @ORM\Column(name="idType", type="string", length=255)
     */
    private $idType;

    /**
     * @var string
     *
     * @ORM\Column(name="resultType", type="string", length=255,nullable=true)
     */
    private $resultType;

    /**
     * @var string
     *
     * @ORM\Column(name="timeTaken", type="string", length=255,nullable=true)
     */
    private $timeTaken;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255,nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="territory", type="string", length=255,nullable=true)
     */
    private $territory;

    /**
     * @var string
     *
     * @ORM\Column(name="cardDistrict", type="string", length=255,nullable=true)
     */
    private $cardDistrict;

    /**
     * @var text
     *
     * @ORM\Column(name="customerRecordNames", type="text", nullable=true)
     */
    private $customerRecordNames;

    /**
     * @var text
     *
     * @ORM\Column(name="customerMsisdn", type="text", nullable=true)
     */
    private $customerMsisdn;

    /**
     * @var text
     *
     * @ORM\Column(name="nidaResponseString", type="text", nullable=true)
     */
    private $nidaResponseString;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @ORM\PrePersist
     * @param \Datetime $createdDate
     * @return Verifications
     */
    public function setCreatedDate() {
        if (!isset($this->createdDate)) {
            $this->createdDate = new \DateTime;
        }
    }

    /**
     * Get createdDate
     *
     * @return \DateTime 
     */
    public function getCreatedDate() {
        return $this->createdDate;
    }

    /**
     * Set idType
     *
     * @param string $idType
     * @return Verifications
     */
    public function setIdType($idType) {
        $this->idType = $idType;

        return $this;
    }

    /**
     * Get idType
     *
     * @return string 
     */
    public function getIdType() {
        return $this->idType;
    }

    /**
     * Set resultType
     *
     * @param string $resultType
     * @return Verifications
     */
    public function setResultType($resultType) {
        $this->resultType = $resultType;

        return $this;
    }

    /**
     * Get resultType
     *
     * @return string 
     */
    public function getResultType() {
        return $this->resultType;
    }

    /**
     * Set timeTaken
     *
     * @param string $timeTaken
     * @return Verifications
     */
    public function setTimeTaken($timeTaken) {
        $this->timeTaken = $timeTaken;

        return $this;
    }

    /**
     * Get timeTaken
     *
     * @return string 
     */
    public function getTimeTaken() {
        return $this->timeTaken;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Verifications
     */
    public function setRegion($region) {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion() {
        return $this->region;
    }

    /**
     * Set territory
     *
     * @param string $territory
     * @return Verifications
     */
    public function setTerritory($territory) {
        $this->territory = $territory;

        return $this;
    }

    /**
     * Get territory
     *
     * @return string 
     */
    public function getTerritory() {
        return $this->territory;
    }

    /**
     * Set cardDistrict
     *
     * @param string $cardDistrict
     * @return Verifications
     */
    public function setCardDistrict($cardDistrict) {
        $this->cardDistrict = $cardDistrict;

        return $this;
    }

    /**
     * Get cardDistrict
     *
     * @return string 
     */
    public function getCardDistrict() {
        return $this->cardDistrict;
    }

    /**
     * Set nidaResponseString
     *
     * @param string $nidaResponseString
     * @return Verifications
     */
    public function setNidaResponseString($nidaResponseString) {
        $this->nidaResponseString = $nidaResponseString;

        return $this;
    }

    /**
     * Get nidaResponseString
     *
     * @return string 
     */
    public function getNidaResponseString() {
        return $this->nidaResponseString;
    }

    /**
     * Set customerRecordNames
     *
     * @param string $customerRecordNames
     * @return Verifications
     */
    public function setCustomerRecordNames($customerRecordNames) {
        $this->customerRecordNames = $customerRecordNames;

        return $this;
    }

    /**
     * Get customerRecordNames
     *
     * @return string 
     */
    public function getCustomerRecordNames() {
        return $this->customerRecordNames;
    }

    /**
     * Set customerMsisdn
     *
     * @param string $customerMsisdn
     * @return Verifications
     */
    public function setCustomerMsisdn($customerMsisdn) {
        $this->customerMsisdn = $customerMsisdn;

        return $this;
    }

    /**
     * Get customerMsisdn
     *
     * @return string 
     */
    public function getCustomerMsisdn() {
        return $this->customerMsisdn;
    }

}
