<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */


namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\DashboardBundle\Entity\SuperAgent;
use Doctrine\Common\Collections\ArrayCollection;

/**
 *
 * @author Michael Tarimo
 *
 * @ORM\Entity
 * @ORM\Table(name="agent")
 *
 */
class Agent extends User {

    /**
     *  @ORM\Column(type="array",name="assigned_networks")
     */
    private $assignedNetworks = array();

    /**
     * @ORM\Column(name="token", type="string", nullable=true)
     */
    private $token;

    /**
     * @ORM\ManyToOne(targetEntity="aimgroup\DashboardBundle\Entity\SuperAgent", inversedBy="agents")
     * @ORM\JoinColumn(name="super_agent_id", referencedColumnName="id")
     */
    private $superAgent;

    /**
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    private $adress;

    /**
     * @ORM\Column(name="picture_path", type="string", nullable=true)
     */
    private $picturePath;

    /**
     *  @ORM\Column(type="array")
     */
    private $devices = array();

    /**
     * @return mixed
     */
    public function getAssignedNetworks()
    {
        return $this->assignedNetworks;
    }

    /**
     * @param mixed $assignedNetworks
     */
    public function setAssignedNetworks($assignedNetworks)
    {
        $this->assignedNetworks = $assignedNetworks;
    }

    /**
     * @return mixed
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param mixed $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return mixed
     */
    public function getSuperAgent()
    {
        return $this->superAgent;
    }

    /**
     * @param mixed $superAgent
     */
    public function setSuperAgent($superAgent)
    {
        $this->superAgent = $superAgent;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;
    }

    /**
     * @return mixed
     */
    public function getPicturePath()
    {
        return $this->picturePath;
    }

    /**
     * @param mixed $picturePath
     */
    public function setPicturePath($picturePath)
    {
        $this->picturePath = $picturePath;
    }

    /**
     * @return mixed
     */
    public function getDevices()
    {
        return $this->devices;
    }

    /**
     * @param mixed $devices
     */
    public function setDevices($devices)
    {
        $this->devices = $devices;
    }
}