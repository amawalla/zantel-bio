<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of UserLog
 *
 * @author Michael Tarimo
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class UserLog
{

    const CREATE_USER = 0;
    const EDIT_USER = 1;
    const ACTIVATE_USER = 2;
    const DEACTIVATE_USER = 3;
    const GENERATE_TOKEN = 4;
    const VERIFY_REGISTRATION = 5;
    const AUDIT_REGISTRATION = 6;
    const EDIT_REGISTRATION = 7;
    const APPROVE_REGISTRATION = 8;
    const DECLINE_REGISTRATION = 9;
    const GENRATE_REPORT = 10;
    const PUBLISH_MESSAGE = 11;
    const PUBLISH_FORM = 12;
    const CHANGE_ENDPOINTS = 13;
    const CHANGE_CONFIGS = 14;
    const UPDATE_USER_PERMISSIONS = 15;
    const VIEW_REGISTRATION = 16;
    const DOWNLOAD_APP = 17;
    const CHANGE_LOGIN_CONFIG = 18;
    const CHANGE_IMAGE_CONFIG = 19;
    const BLACKLIST_NUMBER = 20;
    const TRACK_REGISTRATION = 21;
    const CHECK_USER_ACTIVITIES = 22;
    const CREATE_TERRITORY = 23;
    const PUBLISH_MESSAGE_WITH_FILE = 24;
    const EDIT_USER_ROLE = 25;
    const UPDATE_USER = 26;
    const BULK_AGENTS_UPLOAD = 27;
    const BULK_AGENTS_DELETE = 28;
    const ASSIGN_USER = 29;
    const REGISTRATION_VERIFY = 30;
    const REGISTRATION_VERIFYOVERIDE = 31;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     */
    private $user;
    /**
     * @ORM\Column(type="smallint")
     */
    private $action;
    /**
     * @ORM\Column(type="string")
     */
    private $description;
    /**
     * @ORM\Column(type="array")
     */
    private $metadata;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $timestamp;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param integer $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return integer
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set action
     *
     * @param smallint $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * Get action
     *
     * @return smallint
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set description
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set metadata
     *
     * @param string $metadata
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
    }

    /**
     * Get metadata
     *
     * @return string
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set timestamp
     *
     * @param datetime $timestamp
     */
    public function setTimestamp($timestamp)
    {
        $this->timestamp = $timestamp;
    }

    /**
     * Get timestamp
     *
     * @return datetime
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

}