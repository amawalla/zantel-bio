<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * StepField
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="aimgroup\DashboardBundle\Repository\StepFieldRepository")
 */
class StepField
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="skey", type="string", length=10)
     */
    private $skey;

    /**
     * @var string
     *
     * @ORM\Column(name="hint", type="text")
     */
    private $hint;


    /**
     * @var string
     *
     * @ORM\Column(name="hint_lang_alt", type="text", nullable=true)
     */
    private $hintLangAlt;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20)
     */
    private $type;

    /**
     * @var boolean
     *
     * @ORM\Column(name="required", type="boolean")
     */
    private $required;

    /**
     * @var string
     *
     * @ORM\Column(name="regex", type="string", length=255,nullable=true)
     */
    private $regex;


    /**
     * @var
     * @ORM\Column(name="regex_error", type="string", length=255,nullable=true)
     */
    private $regexError;


    /**
     * @var
     * @ORM\Column(name="regex_error_alt", type="string", length=255,nullable=true)
     */
    private $regexErrorAlt;

    /**
     * @var string
     *
     * @ORM\Column(name="maxlength", type="string", length=255,nullable=true)
     */
    private $maxLength;

    /**
     * @var string
     *
     * @ORM\Column(name="maxlength_error", type="string", length=255,nullable=true)
     */
    private $maxLengthError;


    /**
     * @var string
     *
     * @ORM\Column(name="options", type="string", length=255,nullable=true)
     */
    private $options;

    /**
     * @var integer
     *
     * @ORM\Column(name="action", type="integer",nullable=true)
     */
    private $action;


    /**
     * @ORM\ManyToOne(targetEntity="Step")
     * @ORM\JoinColumn(name="step_id", referencedColumnName="id")
     */
    private $step;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set skey
     *
     * @param string $skey
     * @return StepField
     */
    public function setSkey($skey)
    {
        $this->skey = $skey;

        return $this;
    }

    /**
     * Get skey
     *
     * @return string 
     */
    public function getSkey()
    {
        return $this->skey;
    }

    /**
     * Set hint
     *
     * @param string $hint
     * @return StepField
     */
    public function setHint($hint)
    {
        $this->hint = $hint;

        return $this;
    }

    /**
     * Get hint
     *
     * @return string 
     */
    public function getHint()
    {
        return $this->hint;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return StepField
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set required
     *
     * @param boolean $required
     * @return StepField
     */
    public function setRequired($required)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Get required
     *
     * @return boolean 
     */
    public function getRequired()
    {
        return $this->required;
    }

    /**
     * Set regex
     *
     * @param string $regex
     * @return StepField
     */
    public function setRegex($regex)
    {
        $this->regex = $regex;

        return $this;
    }

    /**
     * Get regex
     *
     * @return string 
     */
    public function getRegex()
    {
        return $this->regex;
    }

    /**
     * Set options
     *
     * @param string $options
     * @return StepField
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return string 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set action
     *
     * @param integer $action
     * @return StepField
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return integer 
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return mixed
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param mixed $step
     */
    public function setStep($step)
    {
        $this->step = $step;
    }

    /**
     * @return mixed
     */
    public function getRegexError()
    {
        return $this->regexError;
    }

    /**
     * @param mixed $regexError
     */
    public function setRegexError($regexError)
    {
        $this->regexError = $regexError;
    }

    /**
     * @return string
     */
    public function getMaxLength()
    {
        return $this->maxLength;
    }

    /**
     * @param string $maxLength
     */
    public function setMaxLength($maxLength)
    {
        $this->maxLength = $maxLength;
    }

    /**
     * @return string
     */
    public function getMaxLengthError()
    {
        return $this->maxLengthError;
    }

    /**
     * @param string $maxLengthError
     */
    public function setMaxLengthError($maxLengthError)
    {
        $this->maxLengthError = $maxLengthError;
    }

    /**
     * @return string
     */
    public function getHintLangAlt()
    {
        return $this->hintLangAlt;
    }

    /**
     * @param string $hintLangAlt
     */
    public function setHintLangAlt($hintLangAlt)
    {
        $this->hintLangAlt = $hintLangAlt;
    }

    /**
     * @return mixed
     */
    public function getRegexErrorAlt()
    {
        return $this->regexErrorAlt;
    }

    /**
     * @param mixed $regexErrorAlt
     */
    public function setRegexErrorAlt($regexErrorAlt)
    {
        $this->regexErrorAlt = $regexErrorAlt;
    }







}
