<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TerritoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TerritoryRepository extends EntityRepository {

    public function getTerritoryCodeFromName($territoryname) {
        $result = $this->_em->createQuery("SELECT t from DashboardBundle:Territory t ")->getArrayResult(); //getSingleResult();

        $territoryCode = "";
        foreach ($result as $key => $value) {
            $territoryArray[$value['name']] = $value['Code'];
        }

        if (isset($territoryArray[$territoryname])) {
            $territoryCode = $territoryArray[$territoryname];
        }
        
        return $territoryCode;
    }

}
