<?php

namespace aimgroup\DashboardBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Service
 *
 * @ORM\Table(name="services")
 * @ORM\Entity
 */
class Service
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(name="created_date", type="datetime")
     */
    private $createdDate;


    /**
     * @var string
     *
     * @ORM\Column(name="service_code", type="string", unique=true)
     */
    private $serviceCode;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_alt", type="string", length=50)
     */
    private $nameAlt;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="ordering", type="smallint")
     */
    private $ordering;

    /**
     * @var integer
     *
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = false;


    /**
     * @ORM\Column(name="form_json", type="text")
     */
    private $formJson = '';

     /**
     * @ORM\Column(name="version", type="string", length=20, nullable=true)
     */
    private $version;    

    /**
     * Service constructor.
     */
    public function __construct()
    {
        $this->setCreatedDate(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    private function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return string
     */
    public function getServiceCode()
    {
        return $this->serviceCode;
    }

    /**
     * @param string $serviceCode
     */
    public function setServiceCode($serviceCode)
    {
        $this->serviceCode = $serviceCode;
    }


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Service
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameAlt
     *
     * @param string $nameAlt
     *
     * @return Service
     */
    public function setNameAlt($nameAlt)
    {
        $this->nameAlt = $nameAlt;

        return $this;
    }

    /**
     * Get nameAlt
     *
     * @return string
     */
    public function getNameAlt()
    {
        return $this->nameAlt;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Service
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set ordering
     *
     * @param integer $ordering
     *
     * @return Service
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;

        return $this;
    }

    /**
     * Get ordering
     *
     * @return integer
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getFormJson()
    {
        return $this->formJson;
    }

    /**
     * @param mixed $formJson
     */
    public function setFormJson($formJson)
    {
        $this->formJson = $formJson;
    }

    /**
     * @param $string
     */
    public function setVersion($string) {

        $this->version = $string;
    }

    /**
     * @return mixed
     */
    public function getVersion() {

        return $this->version;
    }

}
