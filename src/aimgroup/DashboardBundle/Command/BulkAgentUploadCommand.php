<?php

namespace aimgroup\DashboardBundle\Command;

use aimgroup\DashboardBundle\Entity\Region;
use aimgroup\DashboardBundle\Entity\Territory;
use aimgroup\DashboardBundle\Entity\UserType;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class BulkAgentUploadCommand extends ContainerAwareCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('dashboard:bulk_agent_upload_command')
            ->addArgument("file_path")
            ->setDescription('Hello PhpStorm');
    }

    protected $territoryIdBag = array();
    protected $regionBag = array();
    protected $userTypeBag = array();

    /**
     * {@inheritdoc}
     * 1. Ensure the csv file has the following columns. mandatory
     *  firstname,middlename,lastname,mobile,email,dob(),gender,usertype,region,territory
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $apiHelper = $this->getContainer()->get("api.helper");
        $userHelper = $this->getContainer()->get("users.helper");

        $logger = $this->getContainer()->get('monolog.logger.api');
        $filePath = $input->getArgument('file_path');


        $logger->info("executing agent bulk upload command: ".$filePath);
        $data = $apiHelper->parseCSV($filePath);

        $em = $this->getContainer()->get('doctrine')->getManager();
        foreach ($data as $row) {
            $attributes = array();
            $attributes["firstName"] = $row["firstname"];
            $attributes["middleName"] = $row["middlename"];
            $attributes["lastName"] = $row["lastname"];
            $attributes["msisdn"] = $row["mobile"];
            $attributes["email"] = $row["email"];
            $attributes["dob"] = $row["dob"];
            $attributes["gender"] = $row["gender"];
            $attributes["userType"] = strtolower($row["usertype"]);
            $attributes["region"] = strtolower($row["region"]);
            $attributes["territory"] = strtolower($row["territory"]);
            $attributes["superagent"] = $row["superagent"];
#           $apiHelper->logInfo("bulkagent", "uploading", $attributes);
            if (strlen($attributes["firstName"]) > 0 && strlen($attributes["lastName"]) > 0 &&
                strlen($attributes["gender"]) > 0 && strlen($attributes["region"]) > 0 && strlen($attributes["territory"]) > 0
                && strlen($attributes["msisdn"]) > 5 && strlen($attributes["superagent"]) > 0
            ) {
                $isOk = false;
                $attributes["number_devices"] = 1;
                $attributes["agent_exclusivity"] = 1;

                if (array_key_exists($attributes["userType"], $this->userTypeBag)) {
                    $attributes['userType'] = $this->userTypeBag[$attributes['userType']];
                } else {
                    $userTypes = $em->getRepository("DashboardBundle:UserType")->findAll();
                    /** @var  $userType UserType */
                    foreach ($userTypes as $userType) {
                        $this->userTypeBag[$userType->getName()] = $userType->getId();
                    }
                    $attributes['userType'] = $this->userTypeBag[$attributes['userType']];
                }

                if (count($this->regionBag) == 0) {
                    $regions = $em->getRepository("DashboardBundle:Region")->findAll();
                    /** @var  $region Region */
                    foreach ($regions as $region) {
                        $this->regionBag[strtolower($region->getName())] = $region->getId();
                    }
                }

                if (array_key_exists($attributes["region"], $this->regionBag)) {
                    $attributes['region'] = $this->regionBag[$attributes['region']];
                    if (array_key_exists($attributes["territory"], $this->territoryIdBag)) {
                        $attributes['territory'] = $this->territoryIdBag[$attributes['territory']];
                        $isOk = true;
                    } else {
                        $territories = $em->getRepository("DashboardBundle:Territory")->findBy(array("region" => $attributes["region"]));
                        if ($territories) {
                            foreach ($territories as $territory) {
                                $this->territoryIdBag[strtolower($territory->getName())] = $territory->getId();
                            }
                            if (array_key_exists($attributes["territory"], $this->territoryIdBag)) {
                                $attributes['territory'] = $this->territoryIdBag[$attributes['territory']];
                                $isOk = true;
                            }
                        }
                    }
                }

		        $apiHelper->logInfo("bulkagent", "uploading", $attributes);
                if ($isOk) {
                    $status = $userHelper->createUser($attributes, true);
                    $logger->info("user created with status (".$status.") details: ".json_encode($attributes));
                }
            }
        }
    }
}
