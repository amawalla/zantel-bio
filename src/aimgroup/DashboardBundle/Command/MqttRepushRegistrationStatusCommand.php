<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MqttRepushRegistrationStatusCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:mqtt_repush_registration_status_command')
            ->addArgument("id")
            ->addArgument("status")
            ->setDescription('Mqtt repush registration status');
    }

    /**
     *
     * Please optimize. top bash quer runs the same
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        $id = $input->getArgument('id');
        $status = $input->getArgument("status");
        $registration = $em->getRepository("RestApiBundle:Registration")->find($id);
        if ($registration) {
            $this->getContainer()->get("api.helper")->notifyRegStatus($registration, $status);
        }
    }
}
