<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\ProcessBuilder;

class ReportGeneratorCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:report_generator_command')
            ->addArgument("query")
            ->addArgument("email")
            ->addArgument("title")
            ->addArgument("headings")
            ->setDescription('Report Generator');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queryString = $input->getArgument('query');
        $email = $input->getArgument('email');
        $title = $input->getArgument('title');
        $heading = $input->getArgument('headings');
	//$heading = implode($input->getArgument('headings'), ",");
        //execute bash script
        $output->writeln('starting');
        $script = $this->getContainer()->getParameter("base_path") . "/scripts/ReportGenerator.sh";
        $output->writeln("path of script-" . $script);

        exec($script . " \"" . $queryString . "\" \"" . $email . "\" \"" . $title . "\" \"" . $heading . "\" > /dev/null &");


    }
}

