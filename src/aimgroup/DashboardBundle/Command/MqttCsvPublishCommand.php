<?php

namespace aimgroup\DashboardBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MqttCsvPublishCommand extends ContainerAwareCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('dashboard:mqtt_csv_publish_command')
            ->addArgument("file_path")
            ->addArgument("message")
            ->addArgument("topic")
            ->setDescription('Hello PhpStorm');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $logger = $this->getContainer()->get('monolog.logger.api');
        $filePath = $input->getArgument('file_path');
        $message = $input->getArgument("message");
        $topic = $input->getArgument("topic");

        $logger->info("executing mqtt csv publish command: " . $filePath . " message: " . $message . " topic: " . $topic);

        $data = $this->parseCSV($filePath);
        $msisdnArray = array();
        $em = $this->getContainer()->get('doctrine')->getEntityManager();
        foreach ($data as $row) {
            if (strlen($row['msisdn']) > 8) {
                $msisdn = $this->getContainer()->get('api.helper')->formatMsisdn($row['msisdn']);
                array_push($msisdnArray, $msisdn);
            }
        }

        $builder = $em->getRepository("RestApiBundle:Device")->createQueryBuilder('d');
        $builder->select('d.deviceId');
        $builder->where('d.msisdn in (:msisdn)');
        $builder->setParameter('msisdn', $msisdnArray, \Doctrine\DBAL\Connection::PARAM_STR_ARRAY);
        $results = $builder->getQuery()->getResult();

        foreach ($results as $result) {
            $deviceId = $result["deviceId"];
            $respond = $this->getContainer()->get("api.helper")->publish($deviceId, $topic, $message);
            $logger->info("mqtt status for :" . $deviceId . " is:" . $respond);
        }


    }

    /**
     * Parse a csv file
     *
     * @return array
     */
    private function parseCSV($csv)
    {
        $header = NULL;
        $rows = array();
        if (($handle = fopen($csv, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",")) !== FALSE) {
                if (!$header) {
                    $header = $data;
                } else {
                    $rows[] = array_combine($header, $data);
                }
            }
            fclose($handle);
        }
        return $rows;
    }
}
