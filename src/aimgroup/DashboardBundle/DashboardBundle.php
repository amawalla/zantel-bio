<?php

namespace aimgroup\DashboardBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DashboardBundle extends Bundle
{
    public function getParent()
    {
        return "FOSUserBundle";
    }
}