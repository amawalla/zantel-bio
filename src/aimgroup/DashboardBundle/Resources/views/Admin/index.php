<div class="main white">


    <div class="row" style="padding-left: 10px;">

        <div class="col-sm-12">

            <div class=" float-e-margins">

                <div class="row">
                    <div class="col-md-8">
                        <h2 class="page-title">Dashboard Snapshot</h2>
                    </div>
                    <div class="col-md-4" style="float:right; padding-top: 28px; ">
                    <div class="" style="overflow: auto">
                        <form>
                            <div id="reportrange" class="pull-right" style="z-index: 1004; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                <span id="reports_search">May 6, 2016 - June 12, 2016</span> <b class="caret"></b>
                            </div>
                            <input name="dashboard_range" id="dashboard_range" hidden="">
                            <input name="dashboard_filter" id="dashboard_range" value="12" hidden="">
                        </form>
                    </div>
                </div>
                </div>
                <div class="separator-2"></div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="row">

                                <style>
                                    #canvas_Bar1 {
                                        width: 100%;
                                        height: 300px !important;
                                    }

                                    .dropdown-menu:before {
                                        position: absolute;
                                        top: -7px;
                                        right: 9px;
                                        display: inline-block;
                                        border-right: 7px solid transparent;
                                        border-bottom: 7px solid #ccc;
                                        border-left: 7px solid transparent;
                                        border-bottom-color: rgba(0, 0, 0, 0.2);
                                        content: '';
                                    }

                                    .dropdown-menu:after {
                                        position: absolute;
                                        top: -6px;
                                        right: 10px;
                                        display: inline-block;
                                        border-right: 6px solid transparent;
                                        border-bottom: 6px solid #5F5F5F;
                                        border-left: 6px solid transparent;
                                        content: '';
                                    }

                                    .widget table.summary,
                                    .widget table.summary2 {
                                        width: 100%;
                                        font-weight: 200;
                                    }

                                    .widget table.summary {
                                        margin: 30px 0;
                                    }

                                    .widget table.summary2 {
                                        margin: 15px 0;
                                    }

                                    .widget table.summary td:first-child {
                                        width: 65%;
                                    }

                                    .widget table.summary2 td:first-child {
                                        width: 85%;
                                    }

                                    .widget table.summary2 td:last-child {
                                        text-align: right;
                                    }

                                    .widget table.summary td,
                                    .widget table.summary2 td {
                                        padding: 9px;
                                    }

                                    .widget .add_widget {
                                        margin: 47% auto;
                                        display: block;
                                        color: #8C8C8C;
                                        width: 50px;
                                        font-size: 60px;
                                    }

                                    .widget .add_widget:hover {
                                        color: #0171A9;
                                    }

                                    .delete {
                                        display: none;
                                    }
                                </style>
                                <div class="demo-container" style="height:280px">
                                    <div id="placeholder33x"  style="height:280px" class="demo-placeholder"></div>
                                </div>
                            </div>

                        </div>
                        
                    </div>
                </div>
            </div>

        </div>

        <style>
            .separator {
                width: 100%;
                margin: 1px auto 1px;
                position: relative;
                height: 1px;
                -webkit-backface-visibility: hidden;
                -webkit-transform: translateZ(0) scale(1, 1);
            }

            .separator:after {
                height: 1px;
                background: #e8e8e8;
                background: -moz-linear-gradient(left, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.09) 35%, rgba(0, 0, 0, 0.19) 70%, rgba(0, 0, 0, 0) 100%);
                background: -webkit-gradient(linear, left top, right top, color-stop(0%, rgba(0, 0, 0, 0)), color-stop(35%, rgba(0, 0, 0, 0.09)), color-stop(70%, rgba(0, 0, 0, 0.09)), color-stop(100%, rgba(0, 0, 0, 0)));
                background: -webkit-linear-gradient(left, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.09) 35%, rgba(0, 0, 0, 0.19) 70%, rgba(0, 0, 0, 0) 100%);
                background: -o-linear-gradient(left, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.09) 35%, rgba(0, 0, 0, 0.19) 70%, rgba(0, 0, 0, 0) 100%);
                background: -ms-linear-gradient(left, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.09) 35%, rgba(0, 0, 0, 0.19) 70%, rgba(0, 0, 0, 0) 100%);
                background: linear-gradient(to right, rgba(0, 0, 0, 0) 0%, rgba(0, 0, 0, 0.09) 35%, rgba(0, 0, 0, 0.19) 70%, rgba(0, 0, 0, 0) 100%);
                position: absolute;
                bottom: -1px;
                content: "";
                width: 100%;
                left: 0;
            }

            .widget .w_descr {
                margin: 0;
                font-size: 12px;
            }

            .widget .figure {
                margin: 6px 0;
                font-size: 27px;
                color: #31BA85;
                font-weight: 400;
            }

            .widget .figure.big {
                font-size: 32px;
            }






            /***********************/
            /*       Widget        */
            /***********************/
            .widget {
                position: relative;
                margin-bottom: 30px;
                padding: 15px 20px;
                border-radius: 3px;
            }
            .widget > header {
                margin: -15px -20px;
                padding: 15px 20px;
                border-bottom-width: 0px;
                box-shadow: none;
                background: none;
            }
            .widget > header h1, .widget > header h2, .widget > header h3, .widget > header h4, .widget > header h5, .widget > header h6 {
                margin: 0;
            }
            .widget > header + .widget-body,
            .widget > header + .widget-body.no-padding {
                margin-top: 15px;
            }
            .widget > .widget-body.no-padding {
                margin: -15px -20px;
            }
            .widget > .widget-body.no-padding + footer {
                margin-top: 15px;
            }
            .widget > footer {
                margin: 0 -20px -15px;
                padding: 15px 20px;
            }
            .widget:hover .widget-controls-hover {
                opacity: 1;
            }
            .widget .loader {
                position: absolute;
                top: 0;
                bottom: 0;
                left: 0;
                right: 0;
            }
            .widget .loader .spinner {
                position: absolute;
                top: 50%;
                width: 100%;
                margin-top: -10px;
                font-size: 20px;
                text-align: center;
            }
            .widget .widget-table-overflow {
                margin: 0 -20px -15px;
            }

            .widget-controls {
                position: absolute;
                z-index: 1;
                top: 0;
                right: 0;
                padding: 14px;
                font-size: 12px;
            }
            .widget-controls.left {
                left: 0;
                right: auto;
            }
            .widget-controls.widget-controls-hover {
                opacity: 0;
                -webkit-transition: opacity 0.15s;
                -o-transition: opacity 0.15s;
                transition: opacity 0.15s;
            }
            .widget-controls > a {
                padding: 1px 4px;
                border-radius: 4px;
                color: rgba(0, 0, 0, 0.4);
                -webkit-transition: color 0.15s ease-in-out;
                -o-transition: color 0.15s ease-in-out;
                transition: color 0.15s ease-in-out;
            }
            .widget-controls > a:hover {
                color: rgba(0, 0, 0, 0.1);
            }
            .widget-controls > a > .fa {
                vertical-align: middle;
            }
            .widget-controls > a > .glyphicon {
                vertical-align: -1px;
            }
            .widget-controls > .btn-group {
                vertical-align: baseline;
            }
            .widget-controls .fa-refresh {
                position: relative;
                top: -1px;
            }

            .widget-icon {
                opacity: .5;
                font-size: 42px;
            }

            .widget-image {
                position: relative;
                overflow: hidden;
                margin: -15px -20px;
                border-radius: 4px;
            }
            .widget-image > img {
                max-width: 100%;
                border-radius: 4px 4px 0 0;
                -webkit-transition: -webkit-transform 0.15s ease;
                -moz-transition: -moz-transform 0.15s ease;
                -o-transition: -o-transform 0.15s ease;
                transition: transform 0.15s ease;
            }
            .widget-image:hover > img {
                -webkit-transform: scale(1.1, 1.1);
                -ms-transform: scale(1.1, 1.1);
                -o-transform: scale(1.1, 1.1);
                transform: scale(1.1, 1.1);
            }
            .widget-image .title {
                position: absolute;
                top: 0;
                left: 0;
                margin: 20px;
            }
            .widget-image .info {
                position: absolute;
                top: 0;
                right: 0;
                margin: 20px;
            }

            .widget-footer-bottom {
                position: absolute;
                bottom: 0;
                width: 100%;
            }

            .widget-sm {
                height: 230px;
            }

            .widget-md {
                height: 373px;
            }

            .windget-padding-md {
                padding: 15px 20px;
            }

            .windget-padding-lg {
                padding: 30px 40px;
            }

            .widget-body-container {
                position: relative;
                height: 100%;
            }

            .widget-top-overflow,
            .widget-middle-overflow {
                position: relative;
                margin: 0 -20px;
            }
            .widget-top-overflow > img,
            .widget-middle-overflow > img {
                max-width: 100%;
            }

            .widget-top-overflow {
                margin-top: -15px;
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
            }
            .widget-top-overflow > img {
                border-top-left-radius: 4px;
                border-top-right-radius: 4px;
            }

            /***********************************/

            /***********************/
            /*    Part:Stats Row   */
            /***********************/
            .stats-row {
                margin-bottom: 0;
            }

            .stat-item {
                display: inline-block;
                padding-right: 15px;
            }
            .stat-item + .stat-item {
                padding-left: 15px;
                border-left: 1px solid #eeeeee;
            }
            .stat-item .name {
                margin-bottom: 2px;
            }
            .stat-item .value {
                font-weight: bold;
            }
            .stat-item.stat-item-mini-chart {
                position: relative;
                top: -12px;
                padding-left: 0;
                border-left: none;
            }

            .progress-xs {
                height: 5px;
                margin-bottom: 10px;
            }
            .stat-item .name {
                margin-bottom: 2px; font-weight: 100;
            }
            h6, .h6 {
                font-size: 12px;
            }
            header h5{
                text-transform: uppercase;
            }
        </style>



        <div class="col-md-4">
            <section class="widget ggray">
                <!-- .widget>header is generally a place for widget title and widget controls. see .widget in _base.scss -->
                <header>
                    <h5>
                        Units Purchased
                    </h5>
                    <div class="widget-controls">
                        <a href="#"><i class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" data-widgster="close"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </header>
                <div class="widget-body">
                    <div class="stats-row">
                        <div class="stat-item">
                            <h4 style="margin:0">12000</h4>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="widget ggray">
                <!-- .widget>header is generally a place for widget title and widget controls. see .widget in _base.scss -->
                <header>
                    <h5>
                        Units Used
                    </h5>
                    <div class="widget-controls">
                        <a href="#"><i class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" data-widgster="close"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </header>
                <div class="widget-body">
                    <div class="stats-row">
                        <div class="stat-item">
                            <h4 style="margin:0">12000</h4>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-4">
            <section class="widget ggray">
                <!-- .widget>header is generally a place for widget title and widget controls. see .widget in _base.scss -->
                <header>
                    <h5>
                        Current Balance
                    </h5>
                    <div class="widget-controls">
                        <a href="#"><i class="glyphicon glyphicon-cog"></i></a>
                        <a href="#" data-widgster="close"><i class="glyphicon glyphicon-remove"></i></a>
                    </div>
                </header>
                <div class="widget-body">
                    <div class="stats-row">
                        <div class="stat-item">
                            <h4 style="margin:0">12000</h4>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        
        
        
        <?php for($i = 1; $i <= 3; $i++){ ?>
        
        <div class="col-sm-4">

            <div class="ibox float-e-margins ggray">
                <div class="ibox-title">
                    <h5 class="text-default"><i class="fa fa-history"></i> Report Widget</h5>
                    <div class="ibox-tools">
                        <a class="more-link">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                </div>
                <div class="separator"></div>
                <div class="ibox-content">
                    <div style="display:none" class="activity_list">
                        <p>Company activities &amp; Notifications</p>

                        
                    </div>

                    <div class="expense_ph2" style="display:none">
                        <div class="overlay" style="opacity: 0.3;">
                            <img src="http://localhost/personal/pesamakini/assets2/img/activity_ph.png">
                        </div>
                        <div class="widget_intro">
                            <h3>Company Activities</h3>
                            <p>We automatically track income and expense activities and notify you on looming payments and other activities</p>
                            <div class="text-center">
                                <a href="http://localhost/personal/pesamakini/home/invoice" class="btn btn-default btn-xs" style=" margin:0">Get Started</a>                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        
        <?php } ?>
        
        




    </div>

</div>





<!-- flot js -->
    <!--[if lte IE 8]><script type="text/javascript" src="js/excanvas.min.js"></script><![endif]-->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.orderBars.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.time.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/date.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.spline.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.stack.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/curvedLines.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/flot/jquery.flot.resize.js"></script>

<!-- flot -->
<script type="text/javascript">
    //define chart clolors ( you maybe add more colors if you want or flot will add it automatic )
    var chartColours = ['#47B591', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'];

    //generate random number for charts
    randNum = function () {
        return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
    }

    $(function () {
        var d1 = [];
        //var d2 = [];

        //here we generate data for chart
        for (var i = 0; i < 30; i++) {
            d1.push([new Date(Date.today().add(i).days()).getTime(), randNum() + i + i + 10]);
            //    d2.push([new Date(Date.today().add(i).days()).getTime(), randNum()]);
        }

        var chartMinDate = d1[0][0]; //first day
        var chartMaxDate = d1[20][0]; //last day

        var tickSize = [1, "day"];
        var tformat = "%d/%m/%y";

        //graph options
        var options = {
            grid: {
                show: true,
                aboveData: true,
                color: "#3f3f3f",
                labelMargin: 10,
                axisMargin: 0,
                borderWidth: 0,
                borderColor: null,
                minBorderMargin: 5,
                clickable: true,
                hoverable: true,
                autoHighlight: true,
                mouseActiveRadius: 100
            },
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2,
                    steps: false
                },
                points: {
                    show: true,
                    radius: 4.5,
                    symbol: "circle",
                    lineWidth: 3.0
                }
            },
            legend: {
                position: "ne",
                margin: [0, -25],
                noColumns: 0,
                labelBoxBorderColor: null,
                labelFormatter: function (label, series) {
                    // just add some space to labes
                    return label + '&nbsp;&nbsp;';
                },
                width: 40,
                height: 1
            },
            colors: chartColours,
            shadowSize: 0,
            tooltip: true, //activate tooltip
            tooltipOpts: {
                content: "%s: %y.0",
                xDateFormat: "%d/%m",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
            },
            yaxis: {
                min: 0
            },
            xaxis: {
                mode: "time",
                minTickSize: tickSize,
                timeformat: tformat,
                min: chartMinDate,
                max: chartMaxDate
            }
        };
        var plot = $.plot($("#placeholder33x"), [{
                label: "SMS Sent",
                data: d1,
                lines: {
                    fillColor: "rgba(71, 181, 145, 0.0)"
                }, //#96CA59 rgba(150, 202, 89, 0.42)
                points: {
                    fillColor: "#fff"
                }
            }], options);
    });
</script>
<!-- /flot -->