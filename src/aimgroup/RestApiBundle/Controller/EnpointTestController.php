<?php

namespace aimgroup\RestApiBundle\Controller;

use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Event\ImagesEvent;
use aimgroup\RestApiBundle\Event\ImagesEvents;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use aimgroup\RestApiBundle\Event\PlainTextProcessEvents;
use aimgroup\RestApiBundle\EventListener\ImagesEventListener;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use aimgroup\RestApiBundle\Messaging\PushNotification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use Symfony\Component\EventDispatcher\EventDispatcher;

/**
 * Class ConfigurationController
 * @package aimgroup\RestApiBundle\Controller
 * @Route("/endpoint")
 * @Prefix("endpoint")
 */
class EnpointTestController extends AbstractController
{

    /**
     * @Get("/testendpoint", name="testendpoint")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function testEndpointAction()
    {
        $em = $this->getDoctrine()->getManager();
        $dispatcher = $this->get('event_dispatcher');

        $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));

        $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
            array("registrationid" => "1458228441578")
        );
        if ($registration) {

            $event = new PlainTextEvent($registration);
            $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
        }

        return $this->buildResponse(true);
    }

    /**
     * @Get("/rerun_treg/{registrationId}", name="rerun_treg")
     *
     * @param $registrationId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function rerunTregEndpointAction($registrationId)
    {
        $confirmationMessage = $this->container->get('translator')->trans('registration.message.successful');


        $this->get('api.helper')->sendSMS('774021449', $confirmationMessage);
        exit;
        $em = $this->getDoctrine()->getManager();

        $registration = $em->getRepository("RestApiBundle:Registration")->find($registrationId);

        if ($registration) {

            $this->container->get('registration.processor.endpoint')->processText($registration);

        }

        return $this->buildResponse(false);
    }

    /**
     * @Get("/rerun_freg/{registrationId}", name="rerun_freg")
     *
     * @param $registrationId
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function rerunFregEndpointAction($registrationId)
    {
        $em = $this->getDoctrine()->getManager();

        $registration = $em->getRepository("RestApiBundle:Registration")->find($registrationId);

        if ($registration) {

            $receivedImages = $em->getRepository("RestApiBundle:RegImages")->findBy([
                'registration' => $registration->getRegistrationid()
            ]);

            if (count($receivedImages) >= $this->container->getParameter('images.required.total')) {
                $this->container->get('registration.processor.endpoint')->processImages($registration, $receivedImages);
            }
        }

        return $this->buildResponse(true);
    }

}
