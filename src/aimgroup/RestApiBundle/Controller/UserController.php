<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\RestApiBundle\Entity\Device;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Controller\Annotations\Route;
use aimgroup\RestApiBundle\Dao\UserDao;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Entity\Group;
use aimgroup\RestApiBundle\Entity\GroupType;
use aimgroup\RestApiBundle\Entity\Region;
use aimgroup\RestApiBundle\Entity\UserType;
use aimgroup\RestApiBundle\Entity\UserStatus;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations\View;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Util\SecureRandom;
use aimgroup\RestApiBundle\Event\UserEvent;
use aimgroup\RestApiBundle\Event\UserEvents;
use aimgroup\RestApiBundle\EventListener\CreateAccountListener;

/**
 * Description of UserController
 *
 * @author Michael Tarimo
 *
 * @Route("/user")
 *
 * @Prefix("/user")
 */
class UserController extends AbstractController
{

    /**
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *
     *   }
     * )
     * 1. check to see if the accesstoken exists
     * 2. check if IMEI is NULL, then its first time setup
     * 3. if IMEI is NOT NULL, then check if its the same as device. then this is a returning user
     * 4. if IMEI is not the same,then check if he is allowed for multiple devices.just to be sure
     *
     * @param Request $request
     * @Post("/verifyToken", name="verifyToken")
     */
    public function verifyAccessTokenAction(Request $request)
    {
       

        $resp = new JsonObject();
        $status = false;
        $message = "";
        $username = "";
        $requestString = $request->getContent();
        $this->get("api.helper")->log("agentLoginAction", $requestString);
        try {
              
            $json = json_decode($requestString, true);

            $username = $json['username'];
            $imei = $json["imei"];
            $accessToken = $json["accessToken"];
            $deviceId = $json["deviceId"];

            $em = $this->get("doctrine")->getManager();

            /** @var Device $device */
            $device = null;
            try {
                $responseObj['setupPassword'] = false;
                $shouldSendConfig = false;
                $device = $em->getRepository("RestApiBundle:Device")->findByTokenAndMsisdn($accessToken, $username);
                if (!$device->getStatus()) {//if has not been enabled on dashboard
                    $message = "DEVICE IS NOT ACTIVATED FOR USE, CONTACT ADMIN";
                } else {
                    if ($device->getIsActive()) {
                        if (strcmp(
                                $imei,
                                $device->getImei()
                            ) != 0
                        ) {//check if imei are different ,if different  add a new device
                            /** @var User $user */
                            $user = $device->getUser();
                            $totalDevices = $em->getRepository("RestApiBundle:Device")->countDevices($username);
                            if ($totalDevices >= $user->getNumberDevices()) {
                               
                                $message = "YOU CAN NOT ADD ANY DEVICE";
                            } else {
                                $newDevice = new Device();
                                $newDevice->setDeviceId($deviceId);

                                //generate secret and clientid
                                $clientManager = $this->container->get('fos_oauth_server.client_manager.default');
                                $client = $clientManager->createClient();
                                $client->setRedirectUris(array($this->container->getParameter("hostname")));
                                $client->setAllowedGrantTypes(
                                    array(
                                        "authorization_code",
                                        "password",
                                        "refresh_token",
                                        "token",
                                        "client_credentials",
                                    )
                                );
                                $clientManager->updateClient($client);

                                $confirmationCode = rand(10000000, 99999999); //md5($secureRandom->nextBytes(4));

                                $newDevice->setUser($user);
                                $newDevice->setMsisdn(substr($username, -9));
                                $newDevice->setPrivateKey($client->getSecret());
                                $newDevice->setPublicKey($client->getPublicId());
				$newDevice->setAppVersion($json['appVersion']);
                                $newDevice->setAccessToken($confirmationCode);

                                try {
                                    $em->getRepository('RestApiBundle:Device')->save($newDevice);
                                    $shouldSendConfig = true;
                                }
                                catch (\Exception $e) {
                                     $message = "FAILED TO ADD NEW DEVICE";
                                    // $message = $e->getMessage();
                                }
                            }
                        } else {
                            $device->setDeviceId($deviceId);
                            $device->setImei($imei);
                            $em->flush();

                            $shouldSendConfig = true;
                            /** @var User $user */
                            $user = $device->getUser();
                            $responseObj["credentials"]["publickey"] = $device->getPublicKey();
                            $responseObj["credentials"]["secretKey"] = $device->getPrivateKey();
                            $responseObj["credentials"]["username"] = $user->getUsername();
                            $responseObj["credentials"]["full_name"] = $user->getFirstName() . " " . $user->getLastName();
                            $responseObj["credentials"]["user_id"] = $user->getId();
                        }
                    } else {
                        if (!$device->getIsActive()) {
                            $responseObj['setupPassword'] = true;

                            $device->setDeviceId($deviceId);
                            $device->setImei($imei);
                            $em->flush();
                            $shouldSendConfig = true;
                        }
                    }
                }
                if ($shouldSendConfig) {
                    $cmnfigurations = $em->getRepository("DashboardBundle:ConfigMaster")->findAll();
                    if ($configurations) {
                        foreach ($configurations as $configuration) {
                            $responseObj[$configuration->getConfigType()] = json_decode($configuration->getConfig(), true);
                        }
                    }



		   $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                        array("configType" => "loginConfig")
                    );
                    if ($configuration) {
                        $responseObj["loginConfig"] = json_decode($configuration->getConfig(), true);
                        $responseObj["loginConfig"]["version"] = $configuration->getVersion();
                    }
                    $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                        array("configType" => "endpointConfig")
                    );



		
                    //aweful shortcut pls clean this
//                  unset($responseObj['formConfig']);
                    $responseObj['formConfig'] = $this->form_buildForm($user, $json['appVersion']);

                    if ($configuration) {
                        $responseObj["endpointConfig"] = json_decode($configuration->getConfig(), true);
                        $responseObj["endpointConfig"]["version"] = $configuration->getVersion();
                    }
                    $resp->setItem($responseObj);
                    $message = "SUCCESS";
                    $status = true;
                }
            }
            catch (NoResultException $e) {
                   $message = "USER OR TOKEN NOT FOUND";
	        //  $message = $e->getMessage();
            }
        }
        catch (\Exception $e) {
	     $message = 'USER OR TOKEN NOT FOUND';	
           // $message = $e->getMessage();
        }

        $resp->setMessage($message);
        $resp->setStatus($status);
        $this->get("api.helper")->log("agentLoginAction", array($resp->getMessage(), $username), true);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


  
    /**
     * Get json forms
     * 
     * @param null $user
     * @param string $version
     * @return array
     */
    function form_buildForm($user = null, $version = '2.2.12') {

        $em = $this->getDoctrine()->getManager();
      
	// This is ONLY supposed to be a quick fix on the app version problem that we have on Zantel
       //  Just remove this block once we release a new app prior to 2.2.18       

        $appVersion = $version == '2.2.18' ? '2.2.18' : '2.2.12';
       
        $result = $em->createQuery("SELECT s.id, s.name, s.formJson from DashboardBundle:Service s WHERE s.version = '".$appVersion."'")
            ->getArrayResult();

        $forms = [ ];

        foreach ($result as $key => $val) {
            // Comment this if there are no test releases or we can add a column to identify if the form is for test trial or not

            if (in_array($val[ 'id' ], [ 2, 4 ])) {

                if (is_null($user)) {

                    continue;

                } elseif ($user->getIsEnrolledToTest() == false) {

                    continue;
                }
            }

            $forms[] = array( 'name' => $val[ 'name' ], 'form' => json_decode($val[ 'formJson' ], true) );

        }

        return $forms;
    }



    /**
     * This method creates a user on the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The user to be created."}
     *   }
     * )
     *
     * @View()
     * @param type Request
     * @Post("/createUser", name="createUser")
     *
     */
    public function createUserAction(Request $request)
    {

        $parameters = $request->request->all();
        //print_r($parameters);exit;
        $dao = new UserDao($this->getDoctrine()->getManager(), new SecureRandom());
        $user = $dao->setAttributes(new User(), $parameters);

        $em = $this->getDoctrine()->getManager();
        //$this->container->getParameter('identity_column') == 'email'
        if (!empty($user->getEmail()) &&
            $this->email_check($user->getEmail())
        ) {
            return $this->buildResponse("Error duplicate email", Response::HTTP_NOT_ACCEPTABLE);
        } elseif (!empty($user->getUsername()) &&
            $this->username_check($user->getUsername())
        ) {
            return $this->buildResponse("Error duplicate username", Response::HTTP_NOT_ACCEPTABLE);
        }
//        elseif (empty($user->getGroups())) {
//            return $this->buildResponse("Error no user group(s) defined", Response::HTTP_NOT_ACCEPTABLE);
//        }

        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($encoded);
        $user->setStatus(1);
        $user->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        //add to artist group
        $group = $em->getRepository('RestApiBundle:Group')->findOneBy(array('groupCode' => GroupType::CUSTOMER));
        if (isset($group)) {
            $user->addGroup($group);
        }
        //$user->setStatus($status);
        $user->setCreatedAt(new \DateTime);
        $user->setUpdatedAt(new \DateTime);
        $em->persist($user);
        $em->flush();

        $event = new UserEvent($user);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->addSubscriber(new CreateAccountListener($em));
        $dispatcher->dispatch(UserEvents::NEW_USER_CREATED, $event);

        return $this->buildResponse($user->getIdentity());

    }

    /**
     * @param Request $request
     * @GET()
     */
    public function getIdConfigurationsAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        $message = "REQUEST FAILED";
        try {

            $em = $this->getDoctrine()->getManager();

            $idTypes = $em->getRepository("DashboardBundle:Idtype")->findAll();
            if ($idTypes) {
                $resp->setItem($idTypes);
                $em->flush();
                $status = true;
                $message = "SUCCESS";
            }
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        //$this->get("api.helper")->log("saveUserPasswordAction", array($resp->getMessage(), $username), true);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @Post("/authenticatePasswordResetToken", name="authenticatePasswordResetToken")
     */
    public function authenticatePasswordResetTokenAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        $message = "FAILED";
        try {
            $attributes = json_decode($request->getContent(), true);
            if ($attributes) {
                $em = $this->get("doctrine")->getManager();
                $device = $em->getRepository("RestApiBundle:Device")->findOneBy(
                    array(
                        "accessToken" => $attributes["accessToken"],
                        "msisdn"      => $attributes["username"],
                    )
                );
                if ($device) {
                    $message = "SUCCESSFULL AUTHENTICATE TOKEN";
                    $status = true;
                } else {
                    $message = "DEVICE OR TOKEN NOT FOUND";
 		   // $message =  $attributes["username"];	
                    $status = false;
                }
            }
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @Post("/passwordReset", name="passwordReset")
     */
    public function passwordResetAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        $message = "REQUEST FAILED";

        $requestString = $request->getContent();
        // $this->get("api.helper")->log("saveUserPasswordAction", $requestString);
        try {
            $json = json_decode($requestString, true);

            $username = $json['username'];
            $imei = $json["imei"];
            $password = $json["password"];
            $em = $this->getDoctrine()->getManager();
            /** @var  $device Device */
            $device = $em->getRepository("RestApiBundle:Device")->findOneBy(
                array("msisdn" => $username, "accessToken" => $json["accessToken"])
            );
            if ($device) {
                //if device exists update user
                /** @var  $user User */
                $user = $device->getUser();
                $user->setPassword($password);
                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);

                $device->setUser($user);
                $em->flush();
                $status = true;
                $message = "SUCCESS";

            }
        }
        catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        //$this->get("api.helper")->log("saveUserPasswordAction", array($resp->getMessage(), $username), true);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * This method creates a user on the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The user to be created."}
     *   }
     * )
     *
     * @View()
     * @param type Request
     * @Post("/saveUserPassword", name="saveUserPassword")
     *
     */
    public function saveUserPasswordAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        $message = "REQUEST FAILED";

        $requestString = $request->getContent();
        // $this->get("api.helper")->log("saveUserPasswordAction", $requestString);
        try {
            $json = json_decode($requestString, true);

            $username = $json['username'];
            $imei = $json["imei"];
            $password = $json["password"];
            $em = $this->getDoctrine()->getManager();
            /** @var  $device Device */
            $device = $em->getRepository("RestApiBundle:Device")->findOneBy(
                array("msisdn" => $username, "imei" => $imei)
            );
            if ($device) {
                //if device exists update user
                /** @var  $user User */
                $user = $device->getUser();
                $user->setPassword($password);

                $encoder = $this->container->get('security.password_encoder');
                $encoded = $encoder->encodePassword($user, $user->getPassword());
                $user->setPassword($encoded);
                $user->setStatus(1);

                $device->setUser($user);
                $device->setIsActive(1);
                $em->flush();

                $responseObj["credentials"]["publickey"] = $device->getPublicKey();
                $responseObj["credentials"]["secretKey"] = $device->getPrivateKey();
                $responseObj["credentials"]["username"] = $user->getUsername();
                $responseObj["credentials"]["full_name"] = $user->getFirstName() . " " . $user->getLastName();
                $responseObj["credentials"]["user_id"] = $user->getId();
                $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
                    array("configType" => "imageConfig")
                );
                if ($configuration) {
                    $responseObj["imageConfig"] = json_decode($configuration->getConfig(), true);
                    $responseObj["imageConfig"]["version"] = $configuration->getVersion();
                    $resp->setItem($responseObj);
                }

                $status = true;
                $message = "SUCCESS";

            } else {
                $message = "DEVICE NOT FOUND OR ACTIVE";
            }

        }
        catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        //$this->get("api.helper")->log("saveUserPasswordAction", array($resp->getMessage(), $username), true);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    /**
     * This method edits the user information.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The user to be edited"}
     *   }
     * )
     *
     * @param type User
     *
     * @Post("/editUser", name="editUser")
     *
     */
    public function editUserAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();

        $databaseUser = $em->find('User', $user->getId());

        if ($databaseUser !== null) {

            $user->setPassword($databaseUser->getPassword());

            $em->persist($user);
            $em->flush();
            $em->clear();

            return $this->buildResponse($user);
        }

        return $this->buildResponse("Error editing the user", Response::HTTP_NOT_ACCEPTABLE);
    }


    /**
     * This method deactive a user in the system. Deactivated user can not access the system, untill activated again.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The User to be deactivated"}
     *   }
     * )
     *
     * @param type User
     *
     * @Post("/deactivateUser", name="deactivateUser")
     *
     */
    public function deactivateUserAction(Request $request)
    {
        $userId = $request->request->get('userId');
        if (!empty($userId)) {
            $em = $this->getDoctrine()->getManager();

            $user = $em->find('RestApiBundle:User', $userId);
            if ($user !== null) {
                return $this->buildResponse($user);
                if ($user->getIsActive()) {
                    $user->setIsActive(false);
                    $em->persist($user);
                    $em->flush();
                    $em->clear();

                    return $this->buildResponse($user);
                } else {
                    return $this->buildResponse(
                        "Error use has already been deactivated",
                        Response::HTTP_NOT_ACCEPTABLE
                    );
                }
            }
        }

        return $this->buildResponse("Error fail to deactivate the user", Response::HTTP_NOT_ACCEPTABLE);
    }

    public function clear_forgotten_password_code($code)
    {

        if (empty($code)) {
            return false;
        }

        $this->db->where('forgotten_password_code', $code);

        if ($this->db->count_all_results($this->tables['users']) > 0) {
            $data = array(
                'forgotten_password_code' => null,
                'forgotten_password_time' => null,
            );

            $this->db->update($this->tables['users'], $data, array('forgotten_password_code' => $code));

            return true;
        }

        return false;
    }

    /**
     * This method active a user in the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The User to be deactivated"}
     *   }
     * )
     *
     * @param type User
     *
     * @Post("/activateUser", name="activateUser")
     *
     */
    public function activateUserAction($userId)
    {
        if (!empty($userId)) {
            $em = $this->getDoctrine()->getManager();

            $user = $em->find('User', $userId);

            if ($user !== null) {
                if (!$user->getIsActive()) {
                    $user->setIsActive(false);
                    $em->persist($user);
                    $em->flush();
                    $em->clear();

                    return $this->buildResponse($user);
                } else {
                    return $this->buildResponse("Error user is already active", Response::HTTP_NOT_ACCEPTABLE);
                }
            }
        }

        return $this->buildResponse("Error fail to activate the user", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method authenticate the user to the system using username and password.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful authenticated",
     *     406 = "Returned when the user fails to authenticate",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="username", "dataType"="String", "required"=true, "description"="The username of the user"},
     *      {"name"="password", "dataType"="String", "required"=true, "description"="Hashed password of the user"},
     *      {"name"="remember", "dataType"="boolean", "required"=false, "description"="Wheather the user should be remembered"}
     *   },
     *   output="UserCapability"
     * )
     *
     * @param type Request
     *
     * @return UserCapability The User with a list of capabilities
     *
     * @Post("/authenticateUser", name="authenticateUser")
     *
     */
    public function authenticateUserAction(Request $request)
    {
        $parameters = $request->request->all();
        if (empty($parameters['username']) || empty($parameters['password'])) {
            return $this->buildResponse('Please provide username and/or password', Response::HTTP_NOT_ACCEPTABLE);
        }
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $user = $queryBuilder->select('u')
            ->from('RestApiBundle:User', 'u')
            ->where('u.username = :username OR u.email = :email OR u.uid = :uid')
            ->setParameter('username', $parameters['username'])
            ->setParameter('email', $parameters['username'])
            ->setParameter('uid', $parameters['username'])
            ->getQuery()->getOneOrNullResult();

        if (null != $user && $user->isLocked()) {
            return $this->buildResponse('Account time locked', Response::HTTP_NOT_ACCEPTABLE);
        }
        if (null !== $user) {
            $user->setStatus(true);
            if ($user->getStatus() == false) {
                return $this->buildResponse('Account not active', Response::HTTP_NOT_ACCEPTABLE);
            }

            $encoder = $this->container->get('security.password_encoder');
            $isAuthenticated = false;
            if ($encoder->isPasswordValid($user, $parameters['password'])) {
                //perform  post auth actions
                $isAuthenticated = true;

                return $this->buildResponse($isAuthenticated, Response::HTTP_OK);
            }


        }

        //$this->increase_login_attempts($username);
        return $this->buildResponse('Unable to login', Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method authenticate the user to the system using third party providers such as facebook, google+ and tweeter.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The user object with user details fetched from the provider"}
     *   }
     * )
     *
     * @param type User
     *
     * @Post("/authenticateWithProviderUser", name="authenticateWithProviderUser")
     *
     */
    public function authenticateWithProviderAction(User $user)
    {

        // return count
    }

    /**
     * This method authenticate the user to the system using username and password.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="email", "dataType"="String", "required"=true, "description"="The username of the user"}
     *   },
     *   output="UserCapability"
     * )
     *
     * @param type User
     *
     * @return UserCapability The User with a list of capabilities
     *
     * @Post("/forgotPassword", name="forgotPassword")
     *
     */
    public function forgotPasswordAction(Request $request)
    {
        $email = $request->request->get('email');
        //echo $email;exit;
        if (empty($email)) {
            return $this->buildResponse("Error no email supplied", Response::HTTP_NOT_ACCEPTABLE);
        }
        $em = $this->getDoctrine()->getManager();

        $user = $em->getRepository('RestApiBundle:User')->findOneBy(array('email' => $email));

        if ($user !== null) {
            // generate random password
            $temp_password = "fgfg";
            // set

            // send random password to the email
            $message = \Swift_Message::newInstance()
                ->setSubject('Hello Email')
                ->setFrom($this->container->getParameter('system_email_address'))
                ->setTo($email)
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        $user->getLanguage() == $this->container->getParameter(
                            'default_language'
                        ) ? 'emails/forgot_password.html.twig' : 'emails/forgot_password_other_lang.html.twig',
                        array('user' => $user, 'temporaryPassword' => $temp_password)
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);

            return $this->buildResponse($user);
        }

        return $this->buildResponse("Error reseting user", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method is used to change the password of the user.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="username", "dataType"="String", "required"=true, "description"="The username of the user"},
     *      {"name"="oldPassword", "dataType"="String", "required"=true, "description"="Hashed password of the user"},
     *      {"name"="newPassword", "dataType"="String", "required"=true, "description"="Hashed password of the user"}
     *   },
     *   output="Boolean"
     * )
     *
     * @param type User
     *
     * @return boolean
     *
     * @Post("/changePassword", name="changePassword")
     *
     */
    public function changePasswordAction($username, $oldPassword, $newPassword)
    {
        $em = $this->getDoctrine()->getManager();

        $encoder = $this->container->get('security.password_encoder');
        //  $encodedOld = $encoder->encodePassword($user, $oldPassword);

        $encodedOld = "";
        $user = $em->getRepository('User')->findOneBy(array('username' => $username, 'password' => $encodedOld));

        if ($user !== null) {
            $encodedNew = $encoder->encodePassword($user, $newPassword);

            $user->setPassword($encodedNew);
            $em->persist($user);
            $em->flush();
            $em->clear();

            // send random password to the email
            $message = \Swift_Message::newInstance()
                ->setSubject('Hello Email')
                ->setFrom($this->container->getParameter('system_email_address'))
                ->setTo($user->getEmail())
                ->setBody(
                    $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                        $user->getLanguage() == $this->container->getParameter(
                            'default_language'
                        ) ? 'emails/change_password.html.twig' : 'emails/change_password_other_lang.html.twig',
                        array('user' => $user)
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);

            return $this->buildResponse($user);
        }

        return $this->buildResponse("Error changing user password", Response::HTTP_NOT_ACCEPTABLE);
    }


    /**
     * This method gets the details of the user.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="userId", "dataType"="Integer", "required"=true, "description"="The ID of the user"}
     *   }
     * )
     *
     * @param type Integer
     *
     * @Post("/getUserDetails", name="getUserDetails", requirements={"price" = "\d+"})
     *
     */
    public function getUserDetailsAction(Request $request)
    {

        $email = $request->request->get('email');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('RestApiBundle:User')->findOneBy(array('email' => $email));

        if (null != $user) {
            $identity = $user->getIdentity();

            return $this->buildResponse($identity);
        }

        return $this->buildResponse("User not found", Response::HTTP_NOT_ACCEPTABLE);
        // return User
    }

    /**
     * This method gets the details of the user.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="The ID of the user"}
     *   }
     * )
     *
     * @param type Integer
     *
     * @Post("/getUser", name="getUser", requirements={"price" = "\d+"})
     *
     */

    public function getUserAction(Request $request)
    {

        $email = $request->request->get('email');
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('RestApiBundle:User')->findOneBy(array('email' => $email));

        if (null != $user) {
            return $this->buildResponse($user->getIdentity(), Response::HTTP_OK);
        }

        return $this->buildResponse("User not found", Response::HTTP_NOT_ACCEPTABLE);
    }


    /**
     * This method gets a list of users. The default $page is page 0, which is the first page of the list. Default pageCount is 100.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="userType", "dataType"="UserType", "required"=true, "description"="The type of user to list"},
     *      {"name"="page", "dataType"="Integer", "required"=false, "description"="The page number you wish to list"},
     *      {"name"="pageCount", "dataType"="Integer", "required"=false, "description"="The count per page"}
     *   }
     * )
     *
     * @param type Integer
     * @param type Integer
     * @param type Integer
     *
     * @Get("/getUsers", name="getUsers", requirements={"offset" = "\d+", "limit" = "\d+"})
     *
     */
    public function getUsersAction(Request $request)
    {
        $users = array();
        $userType = !empty($request->query->get('userType')) ? $request->query->get('userType') : 100;
        $offset = !empty($request->query->get('offset')) ? $request->query->get('offset') : 0;
        $limit = !empty($request->query->get('limit')) ? $request->query->get('limit') : 100;

        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->createQueryBuilder();

        $q = $queryBuilder->select('u')
            ->from('RestApiBundle:User', 'u')
            ->where('u.type = :type')
            ->setParameter('type', $userType)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();

        $userEntities = $q->execute();
        foreach ($userEntities as $entity) {
            array_push($users, $entity->getIdentity());
        }

        return $this->buildResponse($users, Response::HTTP_OK);
        // return User
    }

    /**
     * This method gets a list of active users. The default $page is page 0, which is the first page of the list. Default pageCount is 100.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="userType", "dataType"="UserType", "required"=true, "description"="The type of user to list"},
     *      {"name"="page", "dataType"="Integer", "required"=false, "description"="The page number you wish to list"},
     *      {"name"="pageCount", "dataType"="Integer", "required"=false, "description"="The count per page"}
     *   }
     * )
     *
     * @param type UserType
     * @param type Integer
     * @param type Integer
     *
     * @Post("/getActiveUsers", name="getActiveUsers", requirements={"page" = "\d+", "pageCount" = "\d+"})
     *
     */
    public function getActiveUsersAction(Request $request)
    {

        $users = array();
        $userType = !empty($request->query->get('userType')) ? $request->query->get('userType') : 100;
        $offset = !empty($request->query->get('offset')) ? $request->query->get('offset') : 0;
        $limit = !empty($request->query->get('limit')) ? $request->query->get('limit') : 100;

        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $em->createQueryBuilder();

        $q = $queryBuilder->select('u')
            ->from('RestApiBundle:User', 'u')
            ->where('u.type = :type')
            ->andWhere('u.isActive = :active')
            ->setParameter('type', $userType)
            ->setParameter('active', true)
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery();

        $userEntities = $q->execute();
        foreach ($userEntities as $entity) {
            array_push($users, $entity->getIdentity());
        }

        return $this->buildResponse($users, Response::HTTP_OK);
    }

    /**
     * This method gets the count of users of a specific type. To get all users, pass in UserType::ALL
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="userType", "dataType"="UserType", "required"=true, "description"="The type of user to count"},
     *      {"name"="userStatus", "dataType"="UserStatus", "required"=false, "description"="The of the users"}
     *   }
     * )
     *
     * @param type UserType
     *
     * @Post("/getUsersCount", name="getActiveUsers", requirements={"page" = "\d+", "pageCount" = "\d+"})
     *
     */
    public function getUsersCountAction(Request $request)
    {

        $parameters = $request->request->all();
        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $count = 0;
        if (empty($parameters['userType'])) {
            $count = $queryBuilder->select('count(u.id)')
                ->from('RestApiBundle:User', 'u')
                ->where('u.isActive = :active')
                ->setParameter('active', $parameters['isActive'])
                ->getQuery()->getSingleScalarResult();
        } else {
            $count = $queryBuilder->select('count(u.id)')
                ->from('RestApiBundle:User', 'u')
                ->where('u.type = :type')
                ->andWhere('u.isActive = :active')
                ->setParameter('type', $parameters['userType'])
                ->setParameter('active', $parameters['isActive'])
                ->getQuery()->getSingleScalarResult();
        }

        return $this->buildResponse($count, Response::HTTP_OK);


        // return count
    }

    /**
     * This method get user's available  account credit.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="id", "dataType"="Integer", "required"=true, "description"="user ID"}
     *   }
     * )
     *
     * @param Request $request
     *
     * @Post("/balance", name="balance", requirements={"id" = "\d+"})
     *
     */
    public function balanceAction(Request $request)
    {
        $id = $request->request->get('id');
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();
        $userAccount = $qb->select('a')
            ->from('AccountingBundle:Account', 'a')
            ->where('a.accountHolder = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getSingleResult();

        return $this->buildResponse($userAccount->getBalance(), Response::HTTP_OK);
    }

    /**
     * This method sets the the number of seconds a user should be locked out after exceding login attempts count.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$seconds", "dataType"="Integer", "required"=true, "description"="The number of seconds"}
     *   }
     * )
     *
     * @param type $seconds
     *
     * @Post("/setLockoutTime", name="setLockoutTime", requirements={"seconds" = "\d+"})
     *
     */
    public function setLockoutTimeAction($seconds)
    {

        // returns boolean
    }

    /**
     * This method sets the maximum number of login attempts a user can try on the system before locked out for a period of time. @see lockoutTimeAction
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$attemptsCount", "dataType"="Integer", "required"=true, "description"="The number of times use can try to login"}
     *   }
     * )
     *
     * @Post("/setLoginMaxAttempts", name="setLoginMaxAttempts", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function setLoginMaxAttemptsAction($attemptsCount)
    {

        // returns boolean
    }


    /**
     * This method gets current login attempts of the user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The number of times use can try to login"}
     *   }
     * )
     *
     * @Post("/getLoginAttempts", name="getLoginAttempts", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function getLoginAttemptsAction($userId)
    {
        // return number
    }


    /**
     * This method checks user login attempts count
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The number of times use can try to login"}
     *   }
     * )
     *
     * @Post("/checkMaxLoginAttempts", name="checkMaxLoginAttempts", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function isMaxLoginAttemptsAction($userId)
    {

    }

    /**
     * This method clears login attempts of a user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The number of times use can try to login"}
     *   }
     * )
     *
     * @Post("/clearUserLoginAttempts", name="clearUserLoginAttempts", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function clearLoginAttemptsAction($userId)
    {

    }


    /**
     * This method updates last login information of a user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The number of times use can try to login"}
     *   }
     * )
     *
     * @Post("/updateLastLogin", name="updateLastLogin", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function updateLastLoginAction($userId)
    {

    }

    /**
     * This method returns defined user groups
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   }
     * )
     *
     * @Post("/createRole", name="createRole", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function createRoleAction(Request $request)
    {
        $parameters = $request->request->all();
        $role = new Role();
        $role->setRoleName($parameters['name']);
        $role->setRole($parameters['role']);
        try {
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();
            $em->clear();

            return $this->buildResponse($role->toCanonical(), Response::HTTP_OK);
        }
        catch (\Exception $ex) {
            return $this->buildResponse($ex->getMessage(), Response::HTTP_NOT_ACCEPTABLE);
        }
    }

    /**
     * This method returns defined user groups
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   }
     * )
     *
     * @Post("/addRoleToGroup", name="addRoleToGroup", requirements={"attemptsCount" = "\d+"})
     *
     */

    public function addRoleToGroupAction(Request $request)
    {

        $groupId = $request->request->get('groupId');
        $roleId = $request->request->get('roleId');
        if (!empty($groupId)) {
            $em = $this->getDoctrine()->getManager();

            $role = $em->find('RestApiBundle:Role', $roleId);

            $group = $em->find('RestApiBundle:Group', $groupId);

            if ($role != null) {
                $groupRoles = $group->getRoles();
                if ($groupRoles != null) {
                    if (!in_array($role, $groupRoles)) {
                        array_push($groupRoles, $role);
                        $group->setRoles($groupRoles);
                        $em->persist($group);
                        $em->flush();

                    }
                } else {
                    array_push($groupRoles, $role);
                    $group->setRoles($groupRoles);
                    $em->persist($group);
                    $em->flush();
                }
                $em->clear();

                return $this->buildResponse($group->getRolesCanonical(), Response::HTTP_OK);
            }
        }

        return $this->buildResponse("Error unable to add role to a group", Response::HTTP_NOT_ACCEPTABLE);


    }

    /**
     * This method returns defined user groups
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   }
     * )
     *
     * @Post("/removeRoleFromGroup", name="removeRoleFromGroup", requirements={"attemptsCount" = "\d+"})
     *
     */

    public function removeRoleFromGroupAction(Request $request)
    {

        $groupId = $request->request->get('groupId');
        $roleId = $request->request->get('roleId');
        if (!empty($groupId)) {
            $em = $this->getDoctrine()->getManager();

            $group = $em->find('RestApiBundle:Group', $groupId);

            if ($group != null) {
                $role = $em->find('RestApiBundle:Role', $roleId);

                if (null != $role) {
                    if ($group->removeRole($role)) {
                        $em->persist($group);
                        $em->flush();
                        $em->clear();

                        return $this->buildResponse("User removed from group", Response::HTTP_OK);
                    }
                }

                return $this->buildResponse("Error unable to remove user from group", Response::HTTP_NOT_ACCEPTABLE);
            }
        }

        return $this->buildResponse("Error unable to retrieve user or group", Response::HTTP_NOT_ACCEPTABLE);


    }

    /**
     * This method returns defined user groups
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   }
     * )
     *
     * @Post("/listGroups", name="listGroups", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function getGroupsAction()
    {

        $em = $this->getDoctrine()->getManager();
        $groups = $em->getRepository('RestApiBundle:Group')->findAll();

        if (null != $groups) {
            return $this->buildResponse($groups, Response::HTTP_OK);
        }

        return $this->buildResponse('Unable to retrieve groups', Response::HTTP_NOT_ACCEPTABLE);

    }

    /**
     * This method returns defined user groups
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   }
     * )
     *
     * @Post("/createGroup", name="createGroup", requirements={"attemptsCount" = "\d+"})
     *
     */

    public function createGroupAction(Request $request)
    {

        $parameters = $request->request->all();

        if (!empty($parameters['name']) && !empty($parameters['group'])) {
            $group = new Group();
            $group->setName($parameters['name']);
            $group->setGroup($parameters['group']);

            $em = $this->getDoctrine()->getManager();
            $em->persist($group);
            $em->flush();

            return $this->buildResponse($group, Response::HTTP_OK);
        }

        return $this->buildResponse('Unable to create group', Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method return groups of a user
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The user id"}
     *   }
     * )
     *
     * @Post("/getUserGroups", name="getUserGroups", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function getUserGroupsAction(Request $request)
    {
        $userId = $request->request->get('userId');
        if (!empty($userId)) {
            $em = $this->getDoctrine()->getManager();

            $user = $em->find('RestApiBundle:User', $userId);

            if ($user != null) {
                return $this->buildResponse($user->getGroupsCanonical());
            }
        }

        return $this->buildResponse("Error unable to retrieve user groups", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method removes a user from a group
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The user id"},
     *      {"name"="$groupId", "dataType"="Integer", "required"=true, "description"="The group id"}
     *   }
     * )
     *
     * @Post("/removeFromUserGroup", name="removeFromUserGroup", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function removeFromUserGroupAction(Request $request)
    {
        $userId = $request->request->get('userId');
        $groupId = $request->request->get('groupId');
        if (!empty($userId)) {
            $em = $this->getDoctrine()->getManager();

            $user = $em->find('RestApiBundle:User', $userId);

            if ($user != null) {
                $group = $em->find('RestApiBundle:Group', $groupId);

                if (null != $group) {
                    if ($user->removeGroup($group)) {
                        $em->persist($user);
                        $em->flush();
                        $em->clear();

                        return $this->buildResponse("User removed from group", Response::HTTP_OK);
                    }
                }

                return $this->buildResponse("Error unable to remove user from group", Response::HTTP_NOT_ACCEPTABLE);
            }
        }

        return $this->buildResponse("Error unable to retrieve user or group", Response::HTTP_NOT_ACCEPTABLE);
    }

    public function updateUserLanguageAction($userId, $language)
    {

    }

    /**
     * This method adds a user to a group
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$groupId", "dataType"="Integer", "required"=true, "description"="The group id"}
     *   }
     * )
     *
     * @Get("/getGroup", name="getGroup", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function getGroupAction(Request $request)
    {
        $groupId = $request->query->get('groupId');
        if (!empty($groupId)) {
            $em = $this->getDoctrine()->getManager();
            $group = $em->find('RestApiBundle:Group', $groupId);
            if (null != $group) {
                return $this->buildResponse($group->getGroupCanonical(), Response::HTTP_OK);
            }
        }

        return $this->buildResponse('Unable to retrieve group', Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method adds a user to a group
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="$userId", "dataType"="Integer", "required"=true, "description"="The user id"},
     *      {"name"="$groupId", "dataType"="Integer", "required"=true, "description"="The group id"}
     *   }
     * )
     *
     * @Post("/addUserToGroup", name="addUserToGroup", requirements={"attemptsCount" = "\d+"})
     *
     */
    public function addUserToGroupAction(Request $request)
    {
        $userId = $request->request->get('userId');
        $groupId = $request->request->get('groupId');
        if (!empty($userId)) {
            $em = $this->getDoctrine()->getManager();

            $user = $em->find('RestApiBundle:User', $userId);

            $group = $em->find('RestApiBundle:Group', $groupId);

            //return new Response(count($user->getGroups()));
            if ($user != null) {
                $userGroups = $user->getGroups();
                //return $this->buildResponse($userGroups[0]);
                if ($userGroups != null) {
                    if (!in_array($group, $userGroups)) {
                        array_push($userGroups, $group);
                        $user->setGroups($userGroups);
                        $em->persist($user);
                        $em->flush();

                    }
                } else {
                    array_push($userGroups, $group);
                    $user->setGroups($userGroups);
                    $em->persist($user);
                    $em->flush();
                }
                $em->clear();

                return $this->buildResponse($user->getGroupsCanonical(), Response::HTTP_OK);
            }
        }

        return $this->buildResponse("Error unable to add user to a group", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * Get a boolean to determine if an account should be locked out due to
     * exceeded login attempts within a given period
     *
     * @return    boolean
     */
    private function is_time_locked_out($identity, $profile)
    {

        return $this->is_max_login_attempts_exceeded($identity, $profile) &&
            $this->get_last_attempt_time($identity) > time() - $this->container->getParameter('lockout_time');
    }

    /**
     * is_max_login_attempts_exceeded
     *
     * @param string $identity
     *
     * @return boolean
     */
    private function is_max_login_attempts_exceeded($identity, $profile)
    {
        if ($this->container->getParameter('track_login_attempts')) {
            $max_attempts = $this->container->getParameter('maximum_login_attempts');
            if ($max_attempts > 0) {
                $attempts = $this->get_attempts_num($identity, $profile);

                return $attempts >= $max_attempts;
            }
        }

        return false;
    }

    /**
     * Get number of attempts to login occured from given IP-address or identity
     *
     * @param    string $identity
     *
     * @return    int
     */
    private function get_attempts_num($identity, $profile)
    {
        if ($this->container->getParameter('track_login_attempts')) {
            $ip_address = isset($profile['ip_address']) ? $this->_prepare_ip(
                $profile['ip_address']
            ) : null; //$this->_prepare_ip($this->input->ip_address());

            $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

            $queryBuilder->select('u')->from('login_attempts', 'l');

            if ($this->container->getParameter('track_login_ip_address') && $ip_address !== null) {
                $queryBuilder->where('l.ip_address = :ipaddress')
                    ->setParameter('ipaddress', $ip_address);
            } else {
                if (strlen($identity) > 0) {
                    $queryBuilder->where('l.login = :login')
                        ->setParameter('login', $identity);
                }
            }

            return $queryBuilder->getQuery()->getSingleScalarResult();
        }

        return 0;
    }

    private function _prepare_ip($ip_address)
    {
        return $ip_address;
    }

    /**
     * update_last_login
     *
     * @return bool
     * @author Ben Edmunds
     **/
    private function update_last_login($id)
    {

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $q = $queryBuilder->update('User', 'u')
            ->set('u.last_login', time())
            ->where('u.id = :id')
            ->setParameter('id', $id)
            ->getQuery();

        $p = $q->execute();

        //$p->

        //return $this->db->affected_rows() == 1;

        return true;
    }

    /**
     * clear_login_attempts
     *
     * @param string $identity
     **/
    private function clear_login_attempts($identity, $profile, $expire_period = 86400)
    {

        if ($this->container->getParameter('track_login_attempts')) {
            $ip_address = isset($profile['ip_address']) ? $this->_prepare_ip(
                $profile['ip_address']
            ) : null; //$this->_prepare_ip($this->input->ip_address());

            $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

            $q = $queryBuilder->delete('login_attempts', 'l')
                ->set('u.last_login', time())
                ->where('l.ip_address = :ip_address')
                ->andWhere('l.login = :login')
                ->orWhere('time <', time() - $expire_period)
                ->setParameter('ip_address', $ip_address)
                ->setParameter('login', $identity)
                ->getQuery();

            $p = $q->execute();

            // return row count

            return true;
        }

        return false;
    }

    private function email_check($email = '')
    {
        if (empty($email)) {
            return false;
        }

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $count = $queryBuilder->select('count(u.id)')
            ->from('RestApiBundle:User', 'u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->getQuery()->getSingleScalarResult();

        return $count > 0;
    }

    private function username_check($username = '')
    {
        if (empty($username)) {
            return false;
        }

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $count = $queryBuilder->select('count(u.id)')
            ->from('RestApiBundle:User', 'u')
            ->where('u.username = :username')
            ->setParameter('username', $username)
            ->getQuery()->getSingleScalarResult();

        return $count > 0;
    }

    /**
     * Generates a random salt value.
     *
     * Salt generation code taken from https://github.com/ircmaxell/password_compat/blob/master/lib/password.php
     *
     * @return void
     *
     */
    private function salt()
    {

        $raw_salt_len = 16;

        $buffer = '';
        $buffer_valid = false;

        if (function_exists('mcrypt_create_iv') && !defined('PHALANGER')) {
            $buffer = mcrypt_create_iv($raw_salt_len, MCRYPT_DEV_URANDOM);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && function_exists('openssl_random_pseudo_bytes')) {
            $buffer = openssl_random_pseudo_bytes($raw_salt_len);
            if ($buffer) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid && @is_readable('/dev/urandom')) {
            $f = fopen('/dev/urandom', 'r');
            $read = strlen($buffer);
            while ($read < $raw_salt_len) {
                $buffer .= fread($f, $raw_salt_len - $read);
                $read = strlen($buffer);
            }
            fclose($f);
            if ($read >= $raw_salt_len) {
                $buffer_valid = true;
            }
        }

        if (!$buffer_valid || strlen($buffer) < $raw_salt_len) {
            $bl = strlen($buffer);
            for ($i = 0; $i < $raw_salt_len; $i++) {
                if ($i < $bl) {
                    $buffer[$i] = $buffer[$i] ^ chr(mt_rand(0, 255));
                } else {
                    $buffer .= chr(mt_rand(0, 255));
                }
            }
        }

        $salt = $buffer;

        // encode string with the Base64 variant used by crypt
        $base64_digits = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
        $bcrypt64_digits = './ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $base64_string = base64_encode($salt);
        $salt = strtr(rtrim($base64_string, '='), $base64_digits, $bcrypt64_digits);

        $salt = substr($salt, 0, $this->container->getParameter('salt_length'));


        return $salt;
    }


    /**
     * This method creates a user on the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="user", "dataType"="User", "required"=true, "description"="The user to be created."}
     *   }
     * )
     *
     * @View()
     * @param type Request
     * @Post("/createArtist", name="createArtist")
     *
     */
    public function createArtistAction(Request $request)
    {
        $parameters = $request->request->all();
        $name = $parameters['username'];
        $akaName = $parameters['additional_data']['stage_name'];
        $bio = $parameters['additional_data']['bio'];
        $imageLink = $parameters['additional_data']['photo'];
        $facebookLink = $parameters['additional_data']['facebook_link'];
        $twitterLink = $parameters['additional_data']['twitter_link'];

        //create artist account
        $artist = new Artist();
        $artist->setUsername($parameters['username']);
        $artist->setUsernameCanonical($parameters['username']);
        $artist->setPassword($parameters['password']);
        $artist->setEmail($parameters['email']);
        $artist->setEmailCanonical($parameters['email']);
        $artist->setFirstName($akaName);
        $artist->setMiddleName($akaName);
        $artist->setLastName($akaName);
        $artist->setMobileNumber($parameters['additional_data']['phone']);
        $artist->setGender($parameters['additional_data']['gender']);
        $artist->setUserType(UserType::NORMAL_USER);
        $artist->setDob(new \DateTime($parameters['additional_data']['dob']));
        $artist->setEnabled(true);
        $artist->setStatus(1);
        $artist->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $artist->setSalt(rand(1000, 9999));
        $artist->setLocked(false);
        $artist->setExpired(true);
        $artist->setCredentialsExpired(true);
        $secureRandom = new SecureRandom();
        $confirmationCode = $secureRandom->nextBytes(4);
        $artist->setConfirmationToken(md5($confirmationCode));
        //encode password
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($artist, $artist->getPassword());
        $artist->setPassword($encoded);

        $em = $this->getDoctrine()->getManager();
        //add to artist group
        $group = $em->getRepository('RestApiBundle:Group')->findOneBy(array('groupCode' => GroupType::ARTIST));
        $artist->addGroup($group);


        $artist->setName($name);
        $artist->setAkaName($akaName);
        $artist->setBio($bio);
        $artist->setImageLink($imageLink);
        $artist->setFacebookLink($facebookLink);
        $artist->setTwitterLink($twitterLink);

        $em->persist($artist);
        $em->flush();

        $event = new UserEvent($artist);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->addSubscriber(new CreateAccountListener($em));
        $dispatcher->dispatch(UserEvents::NEW_USER_CREATED, $event);

        return $this->buildResponse($artist->getId(), Response::HTTP_OK);
    }

    /**
     * This method gets the count of users of a specific type. To get all users, pass in UserType::ALL
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     * )
     *
     * @param type Request
     *
     * @Post("/getArtistsCount", name="getArtistsCount", requirements={"page" = "\d+", "pageCount" = "\d+"})
     *
     */
    public function getArtistsCountAction(Request $request)
    {

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();
        $count = 0;

        $count = $queryBuilder->select('count(a.id)')
            ->from('RestApiBundle:Artist', 'a')
            ->getQuery()->getSingleScalarResult();

        return $this->buildResponse($count, Response::HTTP_OK);

    }


    /**
     * This method gets artist managers.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     * )
     *
     * @param type Request
     *
     * @Post("/getArtistManagers", name="getArtistManagers", requirements={"page" = "\d+", "pageCount" = "\d+"})
     *
     */
    public function getArtistManagersAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();

        $managers = $queryBuilder->select('am')
            ->from('RestApiBundle:ArtistManager', 'am')
            ->getQuery()
            ->getArrayResult();

        return $this->buildResponse($managers, Response::HTTP_OK);

    }


    /**
     * This method creates a user on the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="fullname", "dataType"="string", "required"=true, "description"="Manager full name."},
     *      {"name"="email", "dataType"="string", "required"=true, "description"="His/her email."},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password."},
     *      {"name"="phone", "dataType"="string", "required"=true, "description"="His/her phone number."}
     *   }
     * )
     *
     * @View()
     * @param type Request
     * @Post("/createArtistManager", name="createArtistManager")
     *
     */
    public function createArtistManagerAction(Request $request)
    {
        $parameters = $request->request->all();
        $fullName = $parameters['fullname'];
        $email = $parameters['email'];
        $phone = $parameters['phone'];
        $password = $parameters['password'];
        //create user account
        $artistManager = new ArtistManager();
        $artistManager->setUsername($parameters['email']);
        $artistManager->setPassword($parameters['password']);
        $artistManager->setEmail($parameters['email']);
        $artistManager->setEmailCanonical($parameters['email']);
        $artistManager->setFirstName($fullName);
        $artistManager->setMiddleName($fullName);
        $artistManager->setLastName($fullName);
        $artistManager->setMobileNumber($parameters['phone']);
        $artistManager->setGender('N');
        $artistManager->setUserType(UserType::NORMAL_USER);
        $artistManager->setDob(new \DateTime(null));
        $artistManager->setEnabled(true);
        $artistManager->setSalt(rand(1000, 9999));
        $artistManager->setLocked(false);
        $artistManager->setExpired(true);
        $artistManager->setCredentialsExpired(true);
        $secureRandom = new SecureRandom();
        $confirmationCode = $secureRandom->nextBytes(4);
        $artistManager->setConfirmationToken(md5($confirmationCode));
        //encode password
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($artistManager, $artistManager->getPassword());
        $artistManager->setPassword($encoded);

        $em = $this->getDoctrine()->getManager();
        //add to artist group
        $group = $em->getRepository('RestApiBundle:Group')->findOneBy(array('groupCode' => GroupType::ARTIST_MANAGER));
        $artistManager->addGroup($group);

        $artistManager->setApprovalStatus(0);
        $artistManager->setStatus(1);
        $artistManager->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        $em->persist($artistManager);
        $em->flush();

        $event = new UserEvent($artistManager);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->addSubscriber(new CreateAccountListener($em));
        $dispatcher->dispatch(UserEvents::NEW_USER_CREATED, $event);

        return $this->buildResponse($artistManager->getId(), Response::HTTP_OK);
    }

    /**
     * This method creates a user on the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="fullname", "dataType"="string", "required"=true, "description"="Manager full name."},
     *      {"name"="email", "dataType"="string", "required"=true, "description"="His/her email."},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password."},
     *      {"name"="phone", "dataType"="string", "required"=true, "description"="His/her phone number."}
     *   }
     * )
     *
     * @View()
     * @param type Request
     * @Post("/createBlogger", name="createBlogger")
     *
     */
    public function createBloggerAction(Request $request)
    {
        $parameters = $request->request->all();
        $fullName = $parameters['fullname'];
        $email = $parameters['email'];
        $phone = $parameters['phone'];
        $password = $parameters['password'];
        //create user account
        $blogger = new Blogger();
        $blogger->setUsername($parameters['email']);
        $blogger->setPassword($parameters['password']);
        $blogger->setEmail($parameters['email']);
        $blogger->setEmailCanonical($parameters['email']);
        $blogger->setFirstName($fullName);
        $blogger->setMiddleName($fullName);
        $blogger->setLastName($fullName);
        $blogger->setMobileNumber($parameters['phone']);
        $blogger->setGender('N');
        $blogger->setUserType(UserType::NORMAL_USER);
        $blogger->setDob(new \DateTime(null));
        $blogger->setEnabled(true);
        $blogger->setSalt(rand(1000, 9999));
        $blogger->setLocked(false);
        $blogger->setExpired(true);
        $blogger->setCredentialsExpired(true);
        $secureRandom = new SecureRandom();
        $confirmationCode = $secureRandom->nextBytes(4);
        $blogger->setConfirmationToken(md5($confirmationCode));
        //encode password
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($blogger, $blogger->getPassword());
        $blogger->setPassword($encoded);

        $em = $this->getDoctrine()->getManager();
        //add to artist group
        $group = $em->getRepository('RestApiBundle:Group')->findOneBy(array('groupCode' => GroupType::BLOGGER));
        $blogger->addGroup($group);

        $blogger->setStatus(0);
        $blogger->setPercentagePaid(20);
        $blogger->setPercentageFree(20);
        $blogger->setStatus(1);
        $blogger->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));

        $em->persist($blogger);
        $em->flush();

        $event = new UserEvent($blogger);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->addSubscriber(new CreateAccountListener($em));
        $dispatcher->dispatch(UserEvents::NEW_USER_CREATED, $event);

        return $this->buildResponse($blogger->getId(), Response::HTTP_OK);

    }

    /**
     * This method gets bloggers.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     * )
     *
     * @param type Request
     *
     * @Post("/getBloggers", name="getBloggers", requirements={"page" = "\d+", "pageCount" = "\d+"})
     *
     */
    public function getBloggersAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $queryBuilder = $em->createQueryBuilder();

        $bloggers = $queryBuilder->select('b')
            ->from('RestApiBundle:Blogger', 'b')
            ->getQuery()
            ->getArrayResult();

        return $this->buildResponse($bloggers, Response::HTTP_OK);

    }

    /**
     * This method creates advertisers on the system.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="fullname", "dataType"="string", "required"=true, "description"="Manager full name."},
     *      {"name"="email", "dataType"="string", "required"=true, "description"="His/her email."},
     *      {"name"="password", "dataType"="string", "required"=true, "description"="Password."},
     *      {"name"="phone", "dataType"="string", "required"=true, "description"="His/her phone number."}
     *   }
     * )
     *
     * @View()
     * @param type Request
     * @Post("/createAdvertiser", name="createAdvertiser")
     *
     */
    public function createAdvertiserAction(Request $request)
    {
        $parameters = $request->request->all();
        $companyName = $parameters['company_name'];
        $email = $parameters['email'];
        $phone = $parameters['phone'];
        $password = $parameters['password'];
        //create user account
        $advertiser = new Advertiser();
        $advertiser->setUsername($parameters['email']);
        $advertiser->setPassword($parameters['password']);
        $advertiser->setEmail($parameters['email']);
        $advertiser->setEmailCanonical($parameters['email']);
        $advertiser->setFirstName($companyName);
        $advertiser->setMiddleName($companyName);
        $advertiser->setLastName($companyName);
        $advertiser->setMobileNumber($parameters['phone']);
        $advertiser->setGender('N');
        $advertiser->setUserType(UserType::NORMAL_USER);
        $advertiser->setDob(new \DateTime(null));
        $advertiser->setEnabled(true);
        $advertiser->setSalt(rand(1000, 9999));
        $advertiser->setLocked(false);
        $advertiser->setExpired(true);
        $advertiser->setCredentialsExpired(true);
        $secureRandom = new SecureRandom();
        $confirmationCode = $secureRandom->nextBytes(4);
        $advertiser->setConfirmationToken(md5($confirmationCode));
        //encode password
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($advertiser, $advertiser->getPassword());
        $advertiser->setPassword($encoded);

        $em = $this->getDoctrine()->getManager();
        //add to advertiser group
        $group = $em->getRepository('RestApiBundle:Group')->findOneBy(array('groupCode' => GroupType::ADVERTISER));
        $advertiser->addGroup($group);


        $advertiser->setApprovalStatus(0)
            ->setCompanyName($parameters['company_name'])
            ->setAdvertisementCost(50)
            ->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')))
            ->setStatus(1);

        $em->persist($advertiser);
        $em->flush();

        $event = new UserEvent($advertiser);
        $dispatcher = $this->get('event_dispatcher');
        $dispatcher->addSubscriber(new CreateAccountListener($em));
        $dispatcher->dispatch(UserEvents::NEW_USER_CREATED, $event);


        return $this->buildResponse($advertiser->getId(), Response::HTTP_OK);
    }


    /**
     * This method retrives bloggers.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *   }
     * )
     *
     *
     * @param type Request
     * @Post("/bloggersList", name="bloggersList")
     *
     */
    public function getBloggersListAction(Request $request)
    {

        $offset = $request->request->get('offset');
        $limit = $request->request->get('limit');
        $search = $request->request->get('search');

        $bloggers = array();
        $totalRecords = 0;
        $result = array(
            'data' => array(),
            'info' => array(
                'totalRecords' => $totalRecords,
            ),
        );

        $em = $this->getDoctrine()->getManager();

        if (empty($search)) {

            $qb = $em->createQueryBuilder();
            $query = $qb->select('b')
                ->from('RestApiBundle:Blogger', 'b')
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery();
            $qb = $em->createQueryBuilder();
            $qb->select('count(b.id)');
            $qb->from('RestApiBundle:Blogger', 'b');
            $totalRecords = $qb->getQuery()->getSingleScalarResult();
            $bloggers = $query->getResult();

        } else {

            $qb = $em->createQueryBuilder();
            $query = $qb->select('b')
                ->from('RestApiBundle:Blogger', 'b')
                ->where($qb->expr()->like('b.companyName', "'%$search%'"))
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getQuery();

            $qb = $em->createQueryBuilder();
            $qb->select('count(a.id)')
                ->from('RestApiBundle:Blogger', 'b')
                ->where($qb->expr()->like('b.companyName', "'%$search%'"));
            $totalRecords = $qb->getQuery()->getSingleScalarResult();
            $bloggers = $query->getResult();


        }

        $result['info'] = array(
            'totalRecords' => $totalRecords,
        );


        foreach ($bloggers as $blogger) {
            $result['data'][] = array(
                $blogger->getUsername(),
                $blogger->getPercentageFree(),
                $blogger->getPercentagePaid(),
                $blogger->getStatus(),
                $blogger->getCreatedAt()->format('Y-m-d H:i:s'),
            );
        }

        $result['info'] = array(
            'totalRecords' => count($bloggers),
        );

        return $this->buildResponse($result);
    }


    public function createAdminAction(Request $request)
    {
        $parameters = $request->request->all();
    }

}
