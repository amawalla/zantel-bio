<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use \aimgroup\RestApiBundle\Entity\SearchResult;
use \Symfony\Component\HttpFoundation\Request;
use \Symfony\Component\HttpFoundation\Response;
use \aimgroup\RestApiBundle\Dao\SearchDao;

// TODO remove this
use \aimgroup\RestApiBundle\Tests\TestDataGenerator;


/**
 * Description of SearchController
 *
 * @author Michael Tarimo
 *
 * @Route("/search")
 * @Prefix("/search")
 */
class SearchController extends AbstractController
{

    /**
     * This method used to search for a keyword.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="keyword", "dataType"="String", "required"=true, "description"="The keyword to search for"}
     *   }
     * )
     *
     *
     *
     * @param type String
     *
     * @Post("/search", name="search")
     * @RequestParam(name="keyword")
     *
     */
    public function searchAction(Request $request)
    {

        $theKeyword = $request->request->get("keyword");

        $searchResult = array();

        if (!empty($theKeyword)) {
            // Make calls to get the search results
            //$products = TestDataGenerator::generateDummyProducts($theKeyword);

            //$artists = TestDataGenerator::generateDummyArtists($theKeyword);

            $dao = new SearchDao($this->container);
            $searchResult = TestDataGenerator::generateDummyRegistrations($theKeyword); //$dao->searchRegistration($theKeyword);
        }

        return $this->buildResponse($searchResult);
    }

    /**
     * This method used to search for a keyword autocomplete.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="keyword", "dataType"="String", "required"=true, "description"="The keyword to search for"}
     *   }
     * )
     *
     *
     *
     * @param type String
     *
     * @Post("/autocompleteSearch", name="autocompleteSearch")
     * @RequestParam(name="keyword")
     *
     */
    public function searchAutoCompleteAction(Request $request)
    {
        $theKeyword = $request->request->get('keyword');

        if (!empty($theKeyword)) {
            $dao = new SearchDao($this->container);
            $searchResult = $dao->searchRegistrationAutoComplete($theKeyword);
        }

        return $this->buildResponse($searchResult);
    }
}
