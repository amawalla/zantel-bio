<?php

namespace aimgroup\RestApiBundle\Controller;


use aimgroup\RestApiBundle\Entity\ReservedNumbers;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use aimgroup\DashboardBundle\Entity\Admin;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use PDOException;
use Lsw\ApiCallerBundle\Call\HttpGetJson;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Class ConfigurationController
 * @package aimgroup\RestApiBundle\Controller
 * @Route("/dynamic")
 * @Prefix("dynamic")
 */
class DynamicNumberController extends AbstractController
{

    private $TAG = "DynamicNumberController";

    /**
     * Retrieve Dynamic number from Tigo via NNIM
     *
     * @Get("/get_numbers/{msisdn}", name="get_dynamic_numbers")
     * @param $msisdn
     *
     * @return JsonResponse
     */

    public function getTigoDynamicNumbersAction($msisdn = null)
    {

        $this->get('api.helper')->logInfo($this->TAG, "getTigoDynamicNumbersAction", ['msisdn' => $msisdn]);

        $reservedNumbers = $this->get('tigo.registration.gateway')->getAvailableDynamicNumbers($msisdn);

        if ($reservedNumbers !== false && !empty($reservedNumbers['numbers'])) {

            //Save reserved numbers
            $reservationCode = $reservedNumbers['reservation_code'];

            $entityManager = $this->getDoctrine()->getManager();

            $currentDateTime = new \DateTime("now");

            foreach ($reservedNumbers['numbers'] as $index => $msisdn) {

                $reservedNumberObj = new ReservedNumbers();
                $reservedNumberObj->setCreatedAt($currentDateTime);
                $reservedNumberObj->setUpdatedAt($currentDateTime);
                $reservedNumberObj->setMsisdn($msisdn);
                $reservedNumberObj->setReservationCode($reservationCode);
                $reservedNumberObj->setValidityWindow(10580);
                $reservedNumberObj->setStatus('pending');

                $entityManager->persist($reservedNumberObj);
            }

            $entityManager->flush();
            $entityManager->clear();

            $responseData = [
                'success'      => 1,
                'numberexists' => 0,
                'numbers'      => [
                    'alternativenumbers' => $reservedNumbers['numbers']
                ]
            ];

            return $this->buildResponse($responseData, Response::HTTP_OK);
        }

        $responseData = [
            'success'      => 0,
            'numberexists' => 0,
            'numbers'      => [
                'alternativenumbers' => []
            ]
        ];

        return $this->buildResponse($responseData, Response::HTTP_OK);

    }

}
