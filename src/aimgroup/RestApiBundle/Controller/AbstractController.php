<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use \FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\HttpFoundation\Response;

/**
 * This abstract controller encapsulates all functions which are common to all controllers.
 *
 * @author Michael Tarimo
 */
class AbstractController extends FOSRestController
{

    /**
     *
     * @param type $dataObject The data you wish to put in the response object
     * @param type $responseStatus The status of the response
     * @param type $format This can be 'json' or 'xml'. The daufault is 'json'
     *
     * @return JsonResponse
     */
    protected function buildResponse($dataObject, $responseStatus = Response::HTTP_OK, $format = 'json')
    {
        $response = new Response();
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new GetSetMethodNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        $serializedProduct = $serializer->serialize($dataObject, $format);

        return $response->create($serializedProduct, $responseStatus);
    }

}