<?php

namespace aimgroup\RestApiBundle\Controller;

use aimgroup\DashboardBundle\Entity\AppUpload;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use aimgroup\DashboardBundle\Dao\JsonObject;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Util\SecureRandom;

/**
 * Class ConfigurationController
 * @package aimgroup\RestApiBundle\Controller
 * @Route("/configuration")
 * @Prefix("configuration")
 */
class ConfigurationController extends AbstractController
{

    public function indexAction($name)
    {
        return $this->render('', array('name' => $name));
    }

    /**
     * @param Request $request
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *     )
     *   parameters={}
     *
     * @Post("/getEndpoints", name="getEndpoints",  requirements={"page" = "\d+", "pageCount" = "\d+"})
     * @return JsonResponse
     */
    public function getEndpointsAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = json_decode($request->getContent(), true);
            if (isset($attributes)) {
                $em = $this->get("doctrine")->getManager();
                $msisdn = $this->get("api.helper")->formatMsisdn($attributes["msisdn"]);
                /** @var  $device Device */
                $device = $em->getRepository("RestApiBundle:Device")->findByTokenAndMsisdn($attributes["accessToken"], $msisdn);
                if ($device) {

                    /** @var  $user User */
                    $user = $device->getUser();
                    $configResponse = array();
                    $configResponse["credentialConfig"]["publickey"] = $device->getPublicKey();
                    $configResponse["credentialConfig"]["secretKey"] = $device->getPrivateKey();
                    $configResponse["credentialConfig"]["username"] = $user->getUsername();
                    $configurations = $em->getRepository("DashboardBundle:ConfigMaster")->findAll();
                    foreach ($configurations as $configuration) {
                        $configResponse[$configuration->getConfigType()] = json_decode($configuration->getConfig(), true);
                    }
                    $status = true;
                    $resp->setItem($configResponse);
                    $resp->setMessage("SUCCESS");
                }
            }
        }
        catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *     )
     *   parameters={}
     *
     * @Post("/updateApk", name="updateApk",  requirements={"page" = "\d+", "pageCount" = "\d+"})
     * @return JsonResponse
     */
    public function updateApkAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = json_decode($request->getContent(), true);
            if (isset($attributes)) {
                $em = $this->get("doctrine")->getManager();
                $repo = $em->getRepository("DashboardBundle:AppUpload");
                /** @var  $appUpload  AppUpload */
                $appUpload = $repo->createQueryBuilder('r')
                    ->orderBy("r.id", 'DESC')
                    ->getQuery()->setMaxResults(1)->getOneOrNullResult();
                if ($appUpload !== null) {
                    $item = array();
                    $status = true;
                    if (version_compare($attributes["appVersion"], $appUpload->getVersionName()) < 0) {
                        $explode = pathinfo($appUpload->getFileLocation());
                        $httpPath = str_replace($this->container->getParameter("resource_path"), $this->container->getParameter("hostname") . DIRECTORY_SEPARATOR . "euploads", $explode["dirname"]);
                        $item["url"] = $httpPath . "/" . $explode["basename"];
                        $item['version'] = $appUpload->getVersionName();
                        $item['shouldUpdate'] = true;
                    } else {
                        $item['shouldUpdate'] = false;
                    }
                    $resp->setItem($item);
                    $resp->setMessage("SUCCESS");
                } else {
                    $item['shouldUpdate'] = false;
                }
            }
        }
        catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }


    /**
     * @param Request $request
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *     )
     *   parameters={}
     *
     * @Post("/getOperatorConfigs", name="getOperatorConfigs",  requirements={"page" = "\d+", "pageCount" = "\d+"})
     * @return JsonResponse
     */
    public function getOperatorsConfigAction(Request $request)
    {
        $resp = new JsonObject();
        $status = false;
        try {
            $attributes = json_decode($request->getContent(), true);
            if (isset($attributes)) {
                $em = $this->get("doctrine")->getManager();
                /** @var  $device Device */
                $operatorConfig = $em->getRepository("RestApiBundle:OperatorConfiguration")->findAll();
                $resp->setItem($operatorConfig);
                $resp->setMessage("SUCCESS");
                $status = true;
            }
        } catch (\Exception $e) {
            $resp->setMessage($e->getMessage());
        }
        $resp->setStatus($status);
        return $this->buildResponse($resp, Response::HTTP_OK);
    }

}
