<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of PartnerNetworkController
 *
 * @author Michael Tarimo
 *
 * @Route("/partner")
 * @Prefix("partner")
 */
class PartnerNetworkController extends AbstractController
{

    /**
     * This method activate an agent of a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="agentData", "dataType"="Agent", "required"=true, "description"="The Agent to be added"},
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/addAgent", name="addAgent")
     *
     */
    public function addAgentAction(Request $request)
    {
        $agentData = !empty($request->query->get('agentData')) ? $request->query->get('agentData') : null;
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($agentData && $partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->addAgent($agentData, $partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while adding partner agent", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method activate agents of a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="agentsData", "dataType"="Array", "required"=true, "description"="The list of Agents to be added"},
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/addAgents", name="addAgents")
     *
     */
    public function addAgentsAction(Request $request)
    {
        $agentsData = !empty($request->query->get('agentsData')) ? $request->query->get('agentsData') : null;
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($agentsData && $partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->addAgents($agentsData, $partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while adding partner agents", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method removes an agent of a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="agentData", "dataType"="Agent", "required"=true, "description"="The Agent to be removed"},
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/removeAgent", name="removeAgent")
     *
     */
    public function removeAgentAction(Request $request)
    {
        $agentData = !empty($request->query->get('agentData')) ? $request->query->get('agentData') : null;
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($agentData && $partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->removeAgent($agentData, $partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while removing partner agent", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method removes agents of a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="agentsData", "dataType"="Array", "required"=true, "description"="The Agents to be removed"},
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/removeAgents", name="removeAgents")
     *
     */
    public function removeAgentsAction(Request $request)
    {
        $agentsData = !empty($request->query->get('agentsData')) ? $request->query->get('agentsData') : null;
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($agentsData && $partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->removeAgents($agentsData, $partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while removing partner agents", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method updates an agent of a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="agentData", "dataType"="Agent", "required"=true, "description"="The Agent to be added"},
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/updateAgent", name="updateAgent")
     *
     */
    public function updateAgentAction(Request $request)
    {
        $agentData = !empty($request->query->get('agentData')) ? $request->query->get('agentData') : null;
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($agentData && $partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->updateAgent($agentData, $partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while updating partner agents", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method activate a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/activatePartner", name="activatePartner")
     *
     */
    public function activatePartnerAction(Request $request)
    {
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->activatePartner($partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while activating partner network", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method updates a partner network.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/updatePartner", name="updatePartner")
     *
     */
    public function updatePartnerAction(Request $request)
    {
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->udatePartner($partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while activating partner network", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * This method is used to inform a partner network that their agent has been terminated.
     *
     * @ApiDoc(
     *   resource = true,
     *   statusCodes = {
     *     200 = "Returned when successful",
     *     404 = "Error in calling the service",
     *     500 = "Service not available"
     *   },
     *   parameters={
     *      {"name"="agentData", "dataType"="Agent", "required"=true, "description"="The Agent to be added"},
     *      {"name"="partnerData", "dataType"="PartnerNetwork", "required"=true, "description"="The Partner Network"}
     *   }
     * )
     *
     * @param type boolean
     *
     * @Post("/informAgentTermination", name="informAgentTermination")
     *
     */
    public function informAgentTerminationAction(Request $request)
    {
        $agentData = !empty($request->query->get('agentData')) ? $request->query->get('agentData') : null;
        $partnerData = !empty($request->query->get('partnerData')) ? $request->query->get('partnerData') : null;

        if ($agentData && $partnerData) {
            $partnerNetService = $this->get('partner_nsetwork_service');
            $ret = $partnerNetService->terminateAgent($agentData, $partnerData);
            if ($ret) {
                return $this->buildResponse(true);
            }
        }

        return $this->buildResponse("Error while updating partner agents", Response::HTTP_NOT_ACCEPTABLE);
    }
}