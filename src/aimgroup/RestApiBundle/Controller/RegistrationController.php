<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Controller;

use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\DashboardBundle\Entity\ConfigMaster;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Dao\RegStatusEnum;
use aimgroup\RestApiBundle\Entity\AgentSignatures;
use aimgroup\RestApiBundle\Entity\Device;
use aimgroup\RestApiBundle\Entity\NidaRegistration;
use aimgroup\RestApiBundle\Entity\PortedRegistration;
use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\DashboardBundle\Entity\Verifications;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Event\ImagesEvent;
use aimgroup\RestApiBundle\Event\ImagesEvents;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use aimgroup\RestApiBundle\Event\PlainTextProcessEvents;
use aimgroup\RestApiBundle\EventListener\ImagesEventListener;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\Route;
use League\Flysystem\Adapter\AwsS3;
use League\Flysystem\Adapter\Dropbox;
use League\Flysystem\Filesystem;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use FOS\RestBundle\Controller\Annotations\Post;
use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\Prefix;
use aimgroup\RestApiBundle\Entity\User;
use FOS\RestBundle\Controller\Annotations\View;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use aimgroup\DashboardBundle\Entity\Admin;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\ORMException;
use PDOException;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Description of RegistrationController
 *
 * @author Michael Tarimo
 *
 * @Route("/registration")
 * @Prefix("registration")
 */
class RegistrationController extends AbstractController {

    private $TAG = "RegistrationController";

    protected $correctionCountry = array( "mtanzania", "mtz", "tz" );

    /**
     *
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function registerCustomerAction(Request $request) {
        $attributes = $request->request->all();

        if (array_key_exists('isPorted', $attributes)) {
            return $this->doPortedRegistration($attributes, $request->getHost(), 'data');
        }

        if (array_key_exists('fingercode', $attributes)) {
            // return $this->doBiometricRegistration($attributes);
        }

        return $this->doRegistration($attributes, $request->getHost(), "data");

    }

    private function doBiometricRegistration($attributes) {

        $status = false;
        $message = "SYSTEM ERROR";
        $resultCode = RegStatusEnum::FAIL;
        $apiHelper = $this->container->get("api.helper");


        try {

            $language = $apiHelper->getLanguage(isset($attributes[ "langCode" ]) ? $attributes[ "langCode" ] : "en");

            $entityManager = $this->getDoctrine()->getManager();

            $registeringDevice = $entityManager->getRepository('RestApiBundle:Device')->findOneBy(
                array( "msisdn" => $apiHelper->formatMsisdn($attributes[ "username" ]) )
            );


            if ($registeringDevice) {

                //check duplicates

                $registrationId = $attributes[ 'registrationId' ];

                //check if its a duplicate response
                $registration = $entityManager->getRepository('RestApiBundle:NidaRegistration')->findBy(
                    array(
                        'registrationId' => $registrationId,
                    )
                );

                if (!$registration) {

                    $registration = new NidaRegistration();
                    $registration->setMsisdn($attributes[ 'msisdn' ]);
                    $registration->setIccid($attributes[ 'simserial' ]);
                    $registration->setDeviceId($attributes[ 'deviceId' ]);
                    $registration->setNin($attributes[ 'nin' ]);
                    $registration->setFingerPrintCode($attributes[ 'fingercode' ]);
                    $registration->setOwner($registeringDevice->getUser());
                    $registration->setRegistrationId($attributes[ 'registrationId' ]);
                    $registration->setAppVersion($attributes[ 'appVersion' ]);
                    $registration->setDeviceSentTime(new \DateTime($attributes[ 'deviceSentTime' ]));
                    $registration->setRegisteredTime(new \DateTime($attributes[ 'registeredTime' ]));
                    $registration->setLanguageCode($attributes[ 'langCode' ]);

                    $entityManager->persist($registration);
                    $entityManager->flush();

                    $status = true;
                    $message = $this->get('translator')->trans(
                        'register.success',
                        array(),
                        'messages',
                        $language
                    );

                    $resultCode = RegStatusEnum::SUCCESS;
                } else {
                    $message = $this->get('translator')->trans('register.success', array(), 'messages', $language);
                    $resultCode = RegStatusEnum::SUCCESS;
                }

            } else {

                $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $language);
                $resultCode = RegStatusEnum::AGENT_NOT_EXIST;

            }

        } catch (\Exception $exception) {
            $message = $exception->getMessage();
        }

        $response = new JsonResponse();

        $response->setData(
            array(
                'status'     => $status,
                'message'    => $message,
                'resultCode' => $resultCode,
            )
        );

        return $response;

    }

    private function doRegistration($attributes, $host, $channel = "data") {

        $resp = new JsonObject();
        $status = false;
        $message = "SYSTEM ERROR";
        $resultCode = RegStatusEnum::FAIL;
        $utilityHelper = $this->container->get("utility.helper");
        $apiHelper = $this->container->get("api.helper");

        $apiHelper->logInfo(
            $this->TAG,
            "doRegistration",
            array( "result" => $attributes, "host" => $host, "channel" => $channel )
        );
        $lang = $apiHelper->getLanguage(isset($attributes[ "langCode" ]) ? $attributes[ "langCode" ] : "en");


        $resp->setItem(array( "registrationId" => $attributes[ "registrationId" ] ));
        try {
            $em = $this->getDoctrine()->getManager();
            $registration = $this->buildRegistrationObject($attributes, null, $channel);


            $apiHelper->logInfo(
                $this->TAG,
                "doRegistration",
                array(
                    "message" => " 111 ",
                )
            );


            if (!is_numeric($registration->getIdentificationType())) {
                $identificationType = $em->getRepository('DashboardBundle:Idtype')->findOneBy(
                    [ 'name' => $registration->getIdentificationType() ]
                );
                $registration->setIdentificationType($identificationType->getId());
            }


            $apiHelper->logInfo(
                $this->TAG,
                "doRegistration",
                array(
                    "message" => " 222 ",
                )
            );


            //validate the registration object
            $errors = $this->get('validator')->validate($registration);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;
                $apiHelper->logInfo(
                    $this->TAG,
                    "doRegistration",
                    array(
                        "message" => $errorsString,
                    )
                );
                $resp->setMessage($errorsString);


                $apiHelper->logInfo(
                    $this->TAG,
                    "doRegistration",
                    array(
                        "message" => " if333 ",
                    )
                );


            } else {


                $apiHelper->logInfo(
                    $this->TAG,
                    "doRegistration",
                    array(
                        "message" => " else 333 ",
                    )
                );


                /** @var Device $registeringAgent */
                $registeringAgent = $em->getRepository('RestApiBundle:Device')->findOneBy(
                    array( "msisdn" => $apiHelper->formatMsisdn($attributes[ "username" ]) )
                );
                if (!$registeringAgent) {
                    $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                    $resultCode = RegStatusEnum::AGENT_NOT_EXIST;

                    $apiHelper->logInfo(
                        $this->TAG,
                        "doRegistration",
                        array(
                            "message" => " if 4444 ",
                        )
                    );

                } else {

                    $apiHelper->logInfo($this->TAG, "doRegistration", array( "message" => " else444 " ));

                    $registration->setOwner($registeringAgent->getUser());
                    //evaluate if there is no registration with the same simserial/msisdn
                    //$previousRegistrations = $utilityHelper->findByMsisdnAndSerial(
                    //    $registration->getMsisdn(),
                    //    $registration->getSimSerial()
                    //);

                    $em = $this->getDoctrine()->getManager();
                    $previousRegistrations = $em->getRepository('RestApiBundle:Registration')->findBy(
                        [
                            'msisdn'    => $registration->getMsisdn(),
                            'simSerial' => $registration->getSimSerial()
                        ]
                    );


                    if ($previousRegistrations) {

                        $apiHelper->logInfo($this->TAG, "doRegistration", array( "message" => " if 5555 " ));

                        $isTheSame = false;
                        /** @var  $previousRegistration Registration */
                        foreach ($previousRegistrations as $previousRegistration) {
                            $apiHelper->logInfo(
                                $this->TAG,
                                "doRegistration",
                                array(
                                    "previous" => $previousRegistration->getRegistrationid(),
                                    "current"  => $registration->getRegistrationid(),
                                )
                            );

                            if (strcmp(
                                    $previousRegistration->getRegistrationid(),
                                    $registration->getRegistrationid()
                                ) == 0
                            ) {
                                $isTheSame = true;
                                //request was from sms so update
                                if ($previousRegistration->getIsSms() == 1) {
                                    $this->buildRegistrationObject($attributes, $previousRegistration, $channel);
                                    $em->flush();
                                }
                                break;
                            }
                        }

                        $apiHelper->logInfo(
                            $this->TAG,
                            "doRegistration",
                            array(
                                "same"           => $isTheSame,
                                "registrationid" => $registration->getRegistrationid(),
                                "simserial"      => $registration->getSimSerial(),
                                "msisdn"         => $registration->getMsisdn(),
                            )
                        );

                        if ($isTheSame) {
                            $status = true;
                            $message = $this->get('translator')->trans('register.success', array(), 'messages', $lang);
                            $resultCode = RegStatusEnum::SUCCESS;
                        } else {
                            $resultCode = RegStatusEnum::DUPLICATE;
                            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
                        }
                    } else {

                        $apiHelper->logInfo($this->TAG, "doRegistration", array( "message" => " else 5555 " ));

                        //$registration->setSimSerial("NA");

                        $em->persist($registration);
                        $em->flush();

                        $apiHelper->logInfo($this->TAG, "doRegistration", array( "message" => " 6666 persisted reg " ));


                        if ($registration->getAppVersion() != $registeringAgent->getAppVersion()) {
                            $registeringAgent->setAppVersion($registration->getAppVersion());
                            $em->flush();
                            $apiHelper->logInfo(
                                $this->TAG,
                                "registerCustomerAction",
                                array(
                                    'set appversion' => $registeringAgent->getAppVersion(),
                                    'agent'          => $registration->getAgentMsisdn(),
                                )
                            );
                        }

                        $apiHelper->logInfo(
                            $this->TAG,
                            "doRegistration",
                            array( "message" => " 67777 post persist reg " )
                        );

                        $registrationStatus = new RegistrationStatus();
                        $registrationStatus->setTemporaryRegStatus(0);
                        $registrationStatus->setFullRegStatus(0);
                        $registrationStatus->setFullRegDesc("");
                        $registrationStatus->setIcapState(0);
                        $registrationStatus->setRegistrationId($registration);

                        $registrationStatus->setRegistryType($this->isBiometric($attributes) ? 2 : 1);

                        $em->persist($registrationStatus);
                        $em->flush();

                        $apiHelper->logInfo(
                            $this->TAG,
                            "doRegistration",
                            array( "message" => " 8888 persisted regStatus " )
                        );

                        $status = true;
                        $message = "SUCCESS";
                        $resultCode = RegStatusEnum::SUCCESS;

                        $apiHelper->logInfo($this->TAG, "registerCustomerAction", array( 'start' => 'start dispatcher' ));

                        $dispatcher = $this->get('event_dispatcher');
                        $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
                        $event = new PlainTextEvent($registration);
                       // $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
			
                        $dispatcher->dispatch('plaintext.create', $event);

                        $apiHelper->logInfo(
                            $this->TAG,
                            "doRegistration",
                            array( "message" => " 9999 send plainTextEvntDis " )
                        );
                    }

                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, "registerCustomerAcction", $e);

            $apiHelper->logInfo(
                $this->TAG,
                "doRegistration",
                array(
                    "message" => $e->getMessage(),
                )
            );


            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
        }

        $resp->setResultCode($resultCode);
        $resp->setMessage($message);
        $resp->setStatus($status);


        return $this->buildResponse($resp, Response::HTTP_OK);

    }

    private function doPortedRegistration($attributes, $host = null, $channel) {

        $resp = new JsonObject();
        $status = false;
        $message = "SYSTEM ERROR";
        $resultCode = RegStatusEnum::FAIL;

        $utilityHelper = $this->container->get("utility.helper");
        $apiHelper = $this->container->get("api.helper");

        $apiHelper->logInfo(
            $this->TAG,
            "doPortedRegistration",
            array( "result" => $attributes, "host" => $host, "channel" => $channel )
        );

        $lang = $apiHelper->getLanguage(isset($attributes[ "langCode" ]) ? $attributes[ "langCode" ] : "en");

        $resp->setItem(array( "registrationId" => $attributes[ "registrationId" ] ));

        try {

            $entityManager = $this->getDoctrine()->getManager();

            /** @var PortedRegistration $registration */
            $registration = $this->buildRegistrationObject($attributes, null, $channel);

            //validate the registration object
            $errors = $this->get('validator')->validate($registration);
            if (count($errors) > 0) {
                $errorsString = (string)$errors;
                $resp->setMessage($errorsString);
            } else {
                /** @var Device $registeringAgent */
                $registeringAgent = $entityManager->getRepository('RestApiBundle:Device')->findOneBy(
                    array( "msisdn" => $apiHelper->formatMsisdn($attributes[ "username" ]) )
                );
                if (!$registeringAgent) {
                    $message = $this->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                    $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
                } else {
                    $registration->setOwner($registeringAgent->getUser());

                    //evaluate if there is no registration with the same simserial/msisdn
                    $previousRegistrations = $entityManager->getRepository(
                        'RestApiBundle:PortedRegistration'
                    )->findOneBy(
                        [
                            'msisdn'    => $registration->getMsisdn(),
                            'simSerial' => $registration->getSimSerial(),
                        ]
                    );

                    if ($previousRegistrations) {
                        $isTheSame = false;
                        /** @var  $previousRegistration Registration */
                        foreach ($previousRegistrations as $previousRegistration) {

                            $apiHelper->logInfo(
                                $this->TAG,
                                "doPortedRegistration",
                                [
                                    "previous" => $previousRegistration->getRegistrationid(),
                                    "current"  => $registration->getRegistrationid(),
                                ]
                            );

                            if (strcmp(
                                    $previousRegistration->getRegistrationid(),
                                    $registration->getRegistrationid()
                                ) == 0
                            ) {
                                $isTheSame = true;
                                //request was from sms so update
                                if ($previousRegistration->getIsSms() == 1) {
                                    $this->buildRegistrationObject($attributes, $previousRegistration, $channel);
                                    $entityManager->flush();
                                }
                                break;
                            }
                        }
                        $apiHelper->logInfo(
                            $this->TAG,
                            "doRegistration",
                            [
                                "same"           => $isTheSame,
                                "registrationid" => $registration->getRegistrationid(),
                                "simserial"      => $registration->getSimSerial(),
                                "msisdn"         => $registration->getMsisdn(),
                            ]
                        );

                        if ($isTheSame) {
                            $status = true;
                            $message = $this->get('translator')->trans('register.success', array(), 'messages', $lang);
                            $resultCode = RegStatusEnum::SUCCESS;
                        } else {
                            $resultCode = RegStatusEnum::DUPLICATE;
                            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
                        }
                    } else {
                        $entityManager->persist($registration);
                        $entityManager->flush();

                        if ($registration->getAppVersion() != $registeringAgent->getAppVersion()) {
                            $registeringAgent->setAppVersion($registration->getAppVersion());
                            $entityManager->flush();
                            $apiHelper->logInfo(
                                $this->TAG,
                                "registerCustomerAction",
                                array(
                                    'set appversion' => $registeringAgent->getAppVersion(),
                                    'agent'          => $registration->getAgentMsisdn(),
                                )
                            );
                        }

                        $status = true;
                        $message = "SUCCESS";
                        $resultCode = RegStatusEnum::SUCCESS;

                        /*$apiHelper->logInfo($this->TAG, "registerCustomerAction", array('start' => 'start dispatcher'));
                        $dispatcher = $this->get('event_dispatcher');
                        $dispatcher->addSubscriber(new PlainTextEventListener($entityManager, $this->container));
                        $event = new PlainTextEvent($registration);
                        $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);*/
                    }

                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, "registerCustomerAcction", $e);
            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
        }

        $resp->setResultCode($resultCode);
        $resp->setMessage($message);
        $resp->setStatus($status);


        return $this->buildResponse($resp, Response::HTTP_OK);

    }

    private function buildRegistrationObject($attributes, $registration = null, $channel) {
        if (!$registration) {

            $registration = new Registration();

            if (isset($attributes[ 'isPorted' ])) {

                $registration = new PortedRegistration();
            }
        }

        if (strcmp($channel, "sms") == 0) {

            $registration->setIsSms(1);
        }

        if (isset($attributes[ "isPorted" ])) {

            $registration->setIsPorted($attributes[ "isPorted" ]);
        }


        $registration->setMsisdn($this->container->get("api.helper")->formatMsisdn($attributes[ "msisdn" ]));


        if ($this->isBiometric($attributes)) {
            // Biometric registration
            $registration->setRegistrationType(2);
            $registration->setFingerPrintCode($attributes[ 'fingercode' ]);
            $registration->setIdentificationType(3); 
	   $registration->setIdentification($attributes[ "nin" ]);	

        } else {

            // Standard Registration

            $registration->setRegistrationType(1);

            $registration->setAddress($attributes[ "address" ]);
            $registration->setContact(
                $this->container->get("api.helper")->formatMsisdn($attributes[ "alternative-contact" ])
            );
	    $registration->setIdentification($attributes[ "id-number" ]);
            $registration->setFirstName($attributes[ "firstname" ]);
            $registration->setLastName($attributes[ "surname" ]);
            $registration->setMiddleName($attributes[ "middlename" ]);
            $registration->setGender($attributes[ "gender" ]);
            $registration->setWards($attributes[ "ward" ]);
            $registration->setTerritory($attributes[ "territory" ]);
            $registration->setRegion($attributes[ "region" ]);
            $registration->setDob($attributes[ "dateofbirth" ]);
            $registration->setEmail($attributes[ "email" ]);

            if (in_array($attributes[ "nationality" ], $this->correctionCountry)) {
                $attributes[ "nationality" ] = "Tanzania";
            }

           $identificationType = $attributes[ "id-type" ];

            if (is_numeric($identificationType)) {
                //Workaround for agent with wrong forms
                $identificationTypeCorrection = array( '15' => 1, '16' => 2, '17' => 5 );
                if (array_key_exists($identificationType, $identificationTypeCorrection)) {
                    $identificationType = $identificationTypeCorrection[ $identificationType ];
                }

            } else {
                $identity = $this->getDoctrine()->getManager()->getRepository('DashboardBundle:Idtype')->findOneBy([
                    'name' => $identificationType
                ]);

                if ($identity) {
                    $identificationType = $identity->getId();
                }
            }

            $registration->setIdentificationType($identificationType);

            $registration->setNationality($attributes[ "nationality" ]);

        }

        $registration->setSimSerial($attributes[ "simserial" ]);
        $registration->setLanguage(isset($attributes[ 'langCode' ]) ? $attributes[ 'langCode' ] : 'en');
        $registration->setRegistrationid($attributes[ "registrationId" ]);
     
        $registration->setAgentMsisdn($attributes[ "username" ]);
        $registration->setAppVersion($attributes[ "appVersion" ]);
        $registration->setDeviceId($attributes[ "deviceId" ]);
        $registration->setLocationLatLon(isset($attributes[ "loc" ]) ? $attributes[ "loc" ] : "");
        $registration->setDeviceModel(isset($attributes[ "deviceModel" ]) ? $attributes[ "deviceModel" ] : "");
        $registration->setAgentImei($attributes[ "imei" ]);
        $registration->setRegistrationTime(new \DateTime($attributes[ "registeredTime" ]));
        $registration->setDeviceSentTime(new \DateTime($attributes[ "deviceSentTime" ]));

       

        return $registration;
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function registerCustomerSmsAction(Request $request) {
        $json = $_POST;
        $this->container->get("api.helper")->logInfo(
            $this->TAG,
            "registerCustomerSmsAction",
            array( "request" => $json )
        );

        if ($json) {
            $attributes = array();
            $segments = explode(",", $json[ "message" ]);
            foreach ($segments as $segment) {
                $keyValue = explode(":", $segment);
                $this->container->get("api.helper")->logInfo(
                    $this->TAG,
                    "registerCustomerSmsAction",
                    array( "keyvalue" => $keyValue )
                );
                if ($keyValue && count($keyValue) == 2) {
                    $attributes[ $this->getSmsFields()[ $keyValue[ 0 ] ] ] = $keyValue[ 1 ];
                }
            }
            $this->container->get("api.helper")->logInfo(
                $this->TAG,
                "registerCustomerSmsAction",
                array( "attributes" => $attributes )
            );
            if (count($attributes) > 0) {
                return $this->doRegistration($attributes, $request->getHost(), "sms");
            }
        }

        return $this->buildResponse(false);
    }

    private function getSmsFields() {
        $sms_fields = array(
            1  => 'firstname',
            2  => 'middlename',
            3  => 'surname',
            4  => 'nationality',
            5  => 'dateofbirth',
            6  => 'id-type',
            7  => 'id-number',
            8  => 'msisdn',
            9  => 'simserial',
            10 => 'registrationId',
            11 => 'username',
            12 => 'territory',
            13 => 'region',
        );

        return $sms_fields;
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadSignatureAction(Request $request) {

        $apiHelper = $this->container->get("api.helper");
        $resp = new JsonObject();
        $status = false;

        $message = "";
        $username = "";
        try {

            /** @var  $file UploadedFile */
            $file = $request->files->get('file');
            $fileName = $file->getClientOriginalName();
            $imagesDir = $this->container->getParameter(
                    "resource_path"
                ) . DIRECTORY_SEPARATOR . "signatures" . DIRECTORY_SEPARATOR . date("Ymd");
            // Move the file to the directory where brochures are stored
            $file->move($imagesDir, $fileName);

            $attributes = $request->request->all();
            $username = $apiHelper->formatMsisdn($attributes[ 'username' ]);
            $imageType = $attributes[ 'image_type' ];

            $this->get("api.helper")->log(
                "uploadImagesAction",
                array( "image:" . $imageType . ",file:" . $fileName, $username ),
                true
            );


            // Generate a unique name for the file before saving it
            $em = $this->getDoctrine()->getManager();


            $imageObj = new AgentSignatures();
            $imageObj->setName($fileName);
            $imageObj->setFullPath($imagesDir . '/' . $fileName);
            $imageObj->setMsisdn($username);
            $explode = pathinfo($imageObj->getFullPath());
            $httpPath = str_replace(
                $this->container->getParameter("resource_path"),
                "https://" . $request->getHost() . DIRECTORY_SEPARATOR . "euploads",
                $explode[ "dirname" ]
            );
            $imageObj->setWebPath($httpPath . "/" . $explode[ "basename" ]);
            $sign = $em->getRepository("RestApiBundle:AgentSignatures")->findOneBy(array( "msisdn" => $username ));
            if (!$sign) {
                $em->persist($imageObj);
            } else {
                $sign->setFullPath($imageObj->getFullPath());
            }
            $em->flush();
            $status = true;
//            /** @var  $configuration ConfigMaster */
//            $configuration = $em->getRepository("DashboardBundle:ConfigMaster")->findOneBy(
//                array("configType" => "formConfig")
//            );
//            if ($configuration) {
//                $responseObj["formConfig"]["form"] = json_decode($configuration->getConfig(), true);
//                $responseObj["formConfig"]["version"] = $configuration->getVersion();
//                $resp->setItem($responseObj);
//            }

            //New form
            $services = $em->getRepository(
                'DashboardBundle:Service'
            ); //->findBy( array( 'status' => true, ), array('ordering' => 'asc'));

            if ($services) {
                $responseObj = array();
                foreach ($services as $index => $service) {
                    if ($service->getFormJson()) {
                        $savedForm = json_decode($service->getFormJson());
                        $responseObj[] = array(
                            'akuku'   => 'testing just to see',
                            'name'    => $service->getName(),
                            'order'   => $service->getOrdering(),
                            'form'    => $savedForm->form,
                            'version' => 20,
                        );
                    }
                }

                $resp->setItem($responseObj);
            }

            $message = "SUCCESS";
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        $this->get("api.helper")->log("uploadImagesAction", array( $resp->getMessage(), $username ), true);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadImagesAction(Request $request) {

        $attributes = $request->request->all();

        $resp = new JsonObject();
        $status = false;
        $message = "";
        $em = $this->getDoctrine()->getManager();
        try {

            /** @var  $file UploadedFile */
            $file = $request->files->get('file');

            $clientName = ($this->is_rtl($file->getClientOriginalName()) ? date('YmdHis') : $file->getClientOriginalName());

            $fileName = $attributes[ 'username' ] . "_" . $file->getClientOriginalName();

            if ($this->is_rtl($fileName)) {
                $fileName = $this->convert($fileName);
            }
            //$fileName = $file->getClientOriginalName() . date("h:i:sa")  ;

            $remoteFilePath = date('Ymd') . DIRECTORY_SEPARATOR . $fileName;

            $localImageFilePath = $file->getRealPath();

            $webPath = '';
            $this->get("api.helper")->logInfo(
                "uploadImagesAction",
                "uploadImagesAction0",
                array(
                    "local"  => $localImageFilePath,
                    "remote" => $remoteFilePath,
                )
            );
            //Upload the file to s3
            /** @var  $filesystem Filesystem */
            $filesystem = $this->container->get('s3_filesystem');
            $shouldProceed = true;
            if (!$filesystem->has($remoteFilePath)) {

                $uploaded = $filesystem->write(
                    $remoteFilePath,
                    file_get_contents($localImageFilePath),
                    array( 'ServerSideEncryption' => 'AES256' )
                );

                if ($uploaded) {
                    //Allow the file to be accessible publicly
                    $filesystem->setVisibility($remoteFilePath, 'public');
                    $webPath = "{$this->getParameter('images.upload.webpath')}/{$remoteFilePath}";
                } else {
                    $shouldProceed = false;
                }
            } else {
                $webPath = "{$this->getParameter('images.upload.webpath')}/{$remoteFilePath}";
            }

            if ($shouldProceed) {

                $imageType = $attributes[ 'image_type' ];

                $this->get("api.helper")->logInfo(
                    $this->TAG,
                    "uploadImagesAction",
                    array(
                        "type"           => $imageType,
                        "file:"          => $fileName,
                        "registrationId" => $attributes[ "registrationid" ],
                    )
                );

                // Generate a unique name for the file before saving it


                $em = $this->getDoctrine()->getManager();
                $imageObj = new RegImages();
                $imageObj->setFullPath($localImageFilePath);
                $imageObj->setRegistration($attributes[ "registrationid" ]);
                $imageObj->setName($fileName);
                $imageObj->setImageType($imageType);
                $imageObj->setWebPath($webPath);
                $em->persist($imageObj);


                try {
                    $em->flush();
                } catch (\Exception $e) {
                    #   $this->get('api.helper')->logE("uploadImagesAction", "uploadImagesAction", $e);
                    $this->get('api.helper')->logE("uploadImagesAction_on_flush", "uploadImagesAction-flush-ex", $e);
                }


              //  $squery = "INSERT INTO reg_images (registration, name, fullPath, webPath, createdOn, imageType) VALUES ('" . $attributes[ "registrationid" ] . "', '" . $fileName . "', '" . $localImageFilePath . "', '" . $webPath . "', '" . date('Y-m-d H:i:s') . "', '" . $imageType . "')";
              //  $em->getConnection()->exec($squery);

                $dispatcher = $this->get('event_dispatcher');
                $dispatcher->addSubscriber(new ImagesEventListener($em, $this->container));

                $event = new ImagesEvent($imageObj);
             //   $dispatcher->dispatch(ImagesEvents::PROCESS_IMAGES, $event);

		$dispatcher->dispatch('image.create', $event);


                $status = true;
                $queryBuilder = $em->createQueryBuilder();
                $count = $queryBuilder->select('count(r.id)')
                    ->from('RestApiBundle:RegImages', 'r')
                    ->where('r.registration=:registration')
                    ->setParameter('registration', $attributes[ "registrationid" ])
                    ->getQuery()->getSingleScalarResult();


                $resp->setItem($count);

                if ($count >= 4) {
                    $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(
                        array( "registrationid" => $attributes[ "registrationid" ] )
                    );
                    if ($registration) {
                        $query1 = "UPDATE RegistrationStatus SET allimageDate = '" . date(
                                'Y-m-d H:i:s'
                            ) . "' WHERE registrationid =" . $registration->getId();
                        //$em = $this->getDoctrine()->resetManager();
                        $em2 = $this->getDoctrine()->getManager();
                        $em2->getConnection()->exec($query1);
                        $this->get("api.helper")->logInfo(
                            "uploadImagesAction",
                            "uploadImagesActionQuery",
                            array( "query" => $query1 )
                        );
                    }
                }

                //   $squery = "INSERT INTO reg_images_two (registration, name, fullPath, webPath, createdOn, imageType) VALUES ('" . $attributes[ "registrationid" ] . "', '" . $fileName . "', '" . $localImageFilePath . "', '" . $webPath . "', '" . date('Y-m-d H:i:s') . "', '" . $imageType . "')";
                //   $em->getConnection()->exec($squery);

                $query1 = "UPDATE registration SET image_count =" . $count . " WHERE registrationid like '" . $attributes[ "registrationid" ] . "'";
                //$em = $this->getDoctrine()->resetManager();
                //$em3 = $this->getDoctrine()->getManager();
                $em->getConnection()->exec($query1);
                $this->get("api.helper")->logInfo(
                    "uploadImagesAction",
                    "uploadImagesActionQuery",
                    array( "query" => $query1 )
                );
                $message = "SUCCESS";
            }
        } catch (\Exception $e) {
            $message = $e->getMessage();
            $this->get('api.helper')->logE("uploadImagesAction", "uploadImagesAction-exception", $e);
        }
        $resp->setMessage($message);
        $resp->setStatus($status);
        $this->get("api.helper")->logInfo(
            "uploadImagesAction",
            "uploadImagesAction",
            array( "message" => $resp->getMessage() )
        );

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    public function retriveSummaryAction(Request $request) {
        // daily, weekly, monthly
    }

    function is_rtl($string) {
        $rtl_chars_pattern = '/[\x{0590}-\x{05ff}\x{0600}-\x{06ff}]/u';

        return preg_match($rtl_chars_pattern, $string);
    }

    function convert($string) {
        return strtr($string, array( '۰' => '0', '۱' => '1', '۲' => '2', '۳' => '3', '۴' => '4', '۵' => '5', '۶' => '6', '۷' => '7', '۸' => '8', '۹' => '9', '٠' => '0', '١' => '1', '٢' => '2', '٣' => '3', '٤' => '4', '٥' => '5', '٦' => '6', '٧' => '7', '٨' => '8', '٩' => '9' ));
    }

    /**
     * @View()
     * @Post()
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function checkStatusAction(Request $request) {
        $resp = new JsonObject();
        $status = false;
        $resObj = json_decode($request->getContent(), true);
        $lang = "sw";
        try {
            $temp = array();
            $result = $this->container->get("fos_elastica.manager")->getRepository(
                "RestApiBundle:RegistrationStatus"
            )->getRegStatusByRegNoAndMsisdn(@$resObj[ "registrationId" ], @$resObj[ "msisdn" ]);
            if ($result) {
                /** @var  $regStatus RegistrationStatus */
                $regStatus = $result[ 0 ];
                /** @var  $registration Registration */
                $registration = $regStatus->getRegistrationId();
                if ($regStatus->getFullRegStatus() == 7) {
                    $successMsg = $this->container->get('translator')->trans(
                        'register.status.successful',
                        array( '%msisdn%' => $resObj[ "msisdn" ] ),
                        'messages',
                        ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
                    );
                    $temp[ "message" ] = $successMsg;

                } else {
                    $failedMsg = $this->container->get('translator')->trans(
                        'register.status.failed',
                        array( '%msisdn%' => $resObj[ "msisdn" ], '%desc%' => $regStatus->getFullRegDesc() ),
                        'messages',
                        ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
                    );
                    $temp[ "message" ] = $failedMsg;
                }
                $temp[ "status" ] = $regStatus->getFullRegStatus() == 7 ? 1 : 3;
            }
            $status = true;
            $resp->setItem($temp);
            $message = "success";
        } catch (\Exception $e) {
            $message = $e->getMessage();
        }
        $resp->setMessage($message);
        $resp->setStatus($status);

        return $this->buildResponse($resp, Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/nida_api/{id}", name="/nida_api")
     */
    public function nidaApiAction($id) {

        $url = $this->container->getParameter('nida_api');
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);

        //list all id types to formaster key array now that we aint entity relating
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT t FROM DashboardBundle:Idtype t');

        $idtype = $query->getArrayResult();

        foreach ($idtype as $key => $val) {
            $idKeyArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        //get those configured to be verified
        $result = $em->createQuery(
            "SELECT p "
            . " from DashboardBundle:ConfigMaster p "
            . " WHERE p.name = 'id_verification_list' "
        )
            ->getOneOrNullResult();


        if ($result) {

            $idsToBeVerified = json_decode($result->getConfig());

            //echo $entity->getIdentificationType() . " | " .json_encode($idKeyArray) . " | " . json_encode($idsToBeVerified); exit;

            if (in_array($idKeyArray[ $entity->getIdentificationType() ], $idsToBeVerified)) {
                //clal API

                $data = array(
                    "idNumber" => $entity->getIdentification(),
                );

                $response = $this->curl_message($url, $data);


                $response = json_decode($response, true);

                if ($response[ 'json' ][ 'message' ] == "success") {
                    echo "OPT 1111";

                    $customer_compare_name = $firstname = $response[ 'json' ][ 'item' ][ 'firstName' ] . ' ' . $lastname = $response[ 'json' ][ 'item' ][ 'lastName' ];

                    //do 85% match
                    $likehood = $this->likehood_percent(
                        $entity->getFirstName() . ' ' . $entity->getLastName(),
                        $customer_compare_name
                    );
                    $likehood_reverse = $this->likehood_percent(
                        $entity->getLastName() . ' ' . $entity->getFirstName(),
                        $customer_compare_name
                    );

                    if ($likehood >= 85 || $likehood_reverse >= 85) {

                        $verification = new Verifications();
                        $verification->setIdType('NIDA');
                        $verification->setResultType('MATCH');
                        $em->persist($verification);
                        $em->flush();

                    } else {
                        echo "INNOPT 2ttt";
                        //decline on mismatch basis
                        $em->createQuery(
                            "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 3, u.temporaryRegDesc = 'Validation Box :: Name mismatch' "
                            . " WHERE u.registrationId = " . $entity->getRegistrationid()
                        )
                            ->getArrayResult();

                        $verification = new Verifications();
                        $verification->setIdType('NIDA');
                        $verification->setResultType('MISMATCH');
                        $em->persist($verification);
                        $em->flush();
                    }
                } else {
                    echo "OPT 222";

                    //Decline invalid ID type

                    $em->createQuery(
                        "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 13, u.temporaryRegDesc = 'Validation Box :: Invalid ID type' "
                        . " WHERE u.registrationId = " . $id
                    )
                        ->getArrayResult();


                    $verification = new Verifications();
                    $verification->setIdType('NIDA');
                    $verification->setResultType('INVALIDID');
                    $em->persist($verification);
                    $em->flush();
                }
            }
        }

        exit;
    }

//     * https://vodademo.registersim.com/api/registration/api_callback/222894/treg/retry/
//     */
//    /**
//     * Makes a Post to intergration box
//     * @Get("/api_callback/{record_id}/{reg_type}/{status}/", name="api_callback_retry")
//     * @Template("DashboardBundle:Admin:agent_details.html.twig")
//     * @View()
//     */
//    public function apiCallbackRetryAction($record_id, $reg_type, $status)
//    {
//        if (strcasecmp($reg_type, "treg") == 0 && strcasecmp($status, "retry") == 0) {
//            $em = $this->container->get('doctrine')->getManager();
//            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
//            if ($registration) {
//                $dispatcher = $this->get('event_dispatcher');
//                $dispatcher->addSubscriber(new PlainTextEventListener($em, $this->container));
//                $event = new PlainTextEvent($registration);
//                $dispatcher->dispatch(PlainTextProcessEvents::PROCESS_PLAINTEXT, $event);
//            }
//        }
//        exit;
//    }
    public function curl_message($url, $data) {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string),
            )
        );

        return curl_exec($ch);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_post_mpesa/{record_id}", name="api_post_mpesa")
     * @View()
     */
    public function resendMpesaAction($record_id) {
        $apiHelper = $this->container->get('api.helper');
        try {
            /**
             *
             * moves this to parameters
             */
            $url = "http://54.175.89.102:8080/VodaGateway/rest/services/vodaGateway/mpesaRegisterCustomerG2";
            $em = $this->container->get("doctrine")->getManager();

            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(array( "id" => $record_id ));
            if ($registration) {
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository("DashboardBundle:Idtype")->find($registration->getIdentificationType());
                $region = $em->getRepository("DashboardBundle:Region")->findOneBy(
                    array( "code" => $registration->getRegion() )
                );
                $territory = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory());
                $territoryN = $territory->getName();

                $callback = $this->container->getParameter("callback_url");
                $data = array(
                    "REG_TYPE"    => "TREG",
                    "RECORD_ID"   => $registration->getId(),
                    "MSISDN"      => "255" . $registration->getMsisdn(),
                    "FIRST_NAME"  => $registration->getFirstName(),
                    "LAST_NAME"   => $registration->getLastName(),
                    "MIDDLE_NAME" => $registration->getMiddleName(),
                    "DOB"         => date('d-m-Y', strtotime($registration->getDob())),
                    "ID_TYPE"     => $idType->getName() == "NSSF Pension Card" ? "Pension Fund ID" : $idType->getName(),
                    "ID_NUMBER"   => $registration->getIdentification(),
                    "AGENT_NAME"  => $user->getFirstName() . " " . $user->getLastName(),
                    "AGEN_CODE"   => "0" . $registration->getAgentMsisdn(),
                    "location"    => $territoryN,
                    "region"      => $region->getName(),
                );

                $response = $apiHelper->curl_message($url, $data, $callback);
                $apiHelper->logInfo($this->TAG, "resendMpesaAction", array( 'data' => $data, 'response' => $response ));
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, "resendMpesaAction", $e);
        }

        return $this->buildResponse(array(), Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_post_dbms/{record_id}", name="api_post_dbms_")
     * @View()
     */
    public function apiRetryDBMSAction($record_id) {
        $apiHelper = $this->container->get('api.helper');
        /** move this to parameters yml */
        $url = "http://54.175.89.102:8080/VodaGateway/rest/services/vodaGateway/dbmsRegisterCustomer";
        $apiHelper->logInfo($this->TAG, "apiRetryDBMSAction", array( 'id' => $record_id ));
        $em = $this->container->get("doctrine")->getManager();
        try {
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->findOneBy(array( "id" => $record_id ));
            if ($registration) {
                //get images
                $images = $em->getRepository("RestApiBundle:RegImages")->findBy(
                    array( "registration" => $registration->getRegistrationid() )
                );
                $signature = $em->getRepository("RestApiBundle:AgentSignatures")->findOneBy(
                    array( "msisdn" => $registration->getAgentMsisdn() )
                );
                /** @var  $user User */
                $user = $registration->getOwner();
                $idType = $em->getRepository("DashboardBundle:Idtype")->find($registration->getIdentificationType());

                $region = $em->getRepository("DashboardBundle:Region")->findOneBy(
                    array( "code" => $registration->getRegion() )
                );
                $territory = $em->getRepository("DashboardBundle:Territory")->find($registration->getTerritory());
                $territoryN = $territory->getName();

                $data = array(
                    "REG_TYPE"    => "FREG",
                    "RECORD_ID"   => $registration->getId(),
                    "MSISDN"      => "255" . $registration->getMsisdn(),
                    "FIRST_NAME"  => $registration->getFirstName(),
                    "LAST_NAME"   => $registration->getLastName(),
                    "MIDDLE_NAME" => $registration->getMiddleName(),
                    "DOB"         => date('d-m-Y', strtotime($registration->getDob())),
                    "ID_TYPE"     => $idType->getName() == "NSSF Pension Card" ? "Pension Fund ID" : $idType->getName(),
                    "ID_NUMBER"   => $registration->getIdentification(),
                    "AGENT_NAME"  => $user->getFirstName() . " " . $user->getLastName(),
                    "AGEN_CODE"   => "0" . $registration->getAgentMsisdn(),
                    "location"    => $territoryN,
                    "region"      => $region->getName(),
                );

                $image_key = array(
                    'signature' => 'customer_signature_image',
                    'rear-pic'  => 'id_back_image',
                    'camera'    => 'id_back_image',
                    'front-pic' => 'id_front_image',
                    'potrait'   => 'picture',
                );


                //$em->getRepository("RestApiBundle:AgentSignatures")
                /** @var  $image RegImages */
                foreach ($images as $image) {
                    $data[ $image_key[ $image->getImageType() ] ] = $image->getWebPath();
                }

                $data[ "agent_signature_image" ] = $signature->getWebPath();
                if (count($data) != 4) {
                    exit;
                }
                $callback = $this->container->getParameter("callback_url");
                $response = $apiHelper->curl_message($url, $data, $callback);
                $apiHelper->logInfo($this->TAG, "processImages", array( 'data' => $data, 'response' => $response ));

                $response_message = new \SimpleXMLElement($response);
                $response_message = json_decode(json_encode($response_message), true);

                if ($response_message[ "resultCode" ] == 200) {
                    $infos = $response_message[ "resultsParameters" ][ "entry" ];
                    $this->get('api.helper')->logInfo($this->TAG, "apiRetryDBMSAction", array( 'parameters' => $infos ));
                    $setString = "";
                    $shouldUpdate = false;
                    foreach ($infos as $info) {
                        if ($info[ "key" ] == "dbmsStatus" && $info[ "value" ] == "Success") {
                            $shouldUpdate = true;
                            $setString = $setString . "u.fregDate = '" . date('Y-m-d H:i:s') . "',";
                            $setString = $setString . "u.dbmsState = '7 '";
                            $setString = $setString . "u.fullRegStatus = '7',";
                        } else {
                            if ($info[ "key" ] == "dbmsMessage") {
                                $setString = $setString . " u.fullRegDesc = '" . $info[ "value" ] . "', ";
                            }
                        }
                    }


                    if ($shouldUpdate) {
                        $query = "UPDATE RestApiBundle:RegistrationStatus u SET " . $setString . "  WHERE u.registrationId = " . $record_id;
                        $em->createQuery($query)->getArrayResult();
                        $this->get('api.helper')->logInfo($this->TAG, "apiRetryDBMSAction", array( 'query' => $query ));
                    }
                }
            }
        } catch (\Exception $e) {
            $apiHelper->logE($this->TAG, "apiRetryDBMSAction", $e);
        }

        return $this->buildResponse(array(), Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_callback_dbms_retry/{record_id}/{reg_type}/{status}/{message}", name="api_callback_dbms_retry")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     * @View()
     */
    public function apiCallbackDBMSAction($record_id, $reg_type, $status, $message) {
        $this->get('api.helper')->logInfo(
            $this->TAG,
            "apiCallbackAction",
            array( 'id' => $record_id, 'type' => $reg_type, 'status' => $status, 'message' => $message )
        );

        return $this->buildResponse(array(), Response::HTTP_OK);
    }

    /**
     * Makes a Post to intergration box
     * @POST("/mpesaCallback/", name="mpesaCallback")
     * @View()
     */
    public function apiMpesaCallback(Request $request) {
        $attributes = $request->request->all();
        $this->get('api.helper')->logInfo($this->TAG, "apiMpesaCallback", $attributes);

    }

    /**
     * Makes a Post to intergration box
     * @POST("/api_post_callback/", name="api_post_callback_one")
     * @POST("/api_post_callback", name="api_post_callback_two")
     * @POST("/api_callback/", name="api_callback_one")
     * @POST("/api_callback", name="api_callback_two")
     * @View()
     */
    public function apiPostCallbackAction(Request $request) {
        $attributes = $request->request->all();
        $this->get('api.helper')->logInfo($this->TAG, "apiPostCallbackAction", $attributes);

        if ($attributes[ 'timetogorandom' ] && $attributes[ 'timetogorandom' ] == "400523L59fgjarkfjdkdjf30391dffmvcRbvmbgghl"
            && $attributes[ 'evenmorerandom' ] && $attributes[ 'evenmorerandom' ] == "3fflddHGwewPerenl5590azzmbkerebphhkam34lgm"
        ) {
            if (isset($attributes[ "task" ])) {
                $task = strtoupper($attributes[ "task" ]);
                if ($task == "FREG") {
                    $this->processFregCallback(
                        $attributes[ 'recordId' ],
                        $attributes[ 'task' ],
                        $attributes[ 'subtask' ],
                        $attributes[ 'status' ],
                        $attributes[ 'message' ]
                    );
                } else {
                    if (strtolower($task) == "treg") {
                        $this->processTregCallback(
                            $attributes[ 'recordId' ],
                            $attributes[ 'task' ],
                            $attributes[ 'subtask' ],
                            $attributes[ 'status' ],
                            $attributes[ 'createMPESAStatus' ],
                            $attributes[ 'mpesaStatus' ],
                            $attributes[ 'newRegistration' ],
                            $attributes[ "tregMessage" ],
                            $attributes[ "message" ]
                        );
                    }
                }
            }
        }
        exit;
    }

    private function processTregCallback(
        $record_id,
        $reg_type,
        $reg_sub_type,
        $status,
        $create_mpesa_status,
        $mpesa_status,
        $isRereg,
        $tregMessage,
        $message
    ) {
        $this->get('api.helper')->logInfo(
            $this->TAG,
            "processTregCallback",
            array(
                'id'           => $record_id,
                'type'         => $reg_type,
                'sub_type'     => $reg_sub_type,
                'status'       => $status,
                'message'      => $message,
                'treg_message' => $tregMessage,
                "re_reg"       => $isRereg,
            )
        );


        $where = null;
        $description = "";
        if ($create_mpesa_status || strtolower($reg_sub_type) == "mpesacreatecustomer") {
            $mpesaStatus = $this->getMpesaState()[ $create_mpesa_status ];
            $setString = "u.mpesaState = '" . $mpesaStatus . "',";
        } else {
            $description = strlen($tregMessage) > 0 ? $tregMessage : $message;
            $setString = "u.temporaryRegDesc = '" . $description . "' ,";
            if ($reg_type) {
                $status = $this->getStatusKeys()[ $status ];
                $regType = ($isRereg == true || $isRereg == null) ? 1 : 0;
                $setString = $setString . "u.temporaryRegStatus = '" . $status . "',";
                if (strtolower($reg_type) == "treg") { // treg
                    $setString = $setString . "u.tregDate = '" . date('Y-m-d H:i:s') . "', ";
                    $setString = $setString . "u.regType = '" . $regType . "' ";
                    $setString = $setString . "u.icapState = '" . $status . "',";
                }
                $where = "u.icapState !=6";
            }
        }
        $this->updateRegistration($setString, $record_id, $status, $message, $where);

        if ($record_id) {
            $em = $this->getDoctrine()->getManager();
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
            if ($registration) {
                $this->get('api.helper')->logInfo(
                    $this->TAG,
                    "tregNotification",
                    array( 'status' => $status, 'msisdn' => $registration->getMsisdn() )
                );
                if (in_array($status, array( 6 ))) {
                    $msg = $this->container
                        ->get("api.helper")
                        ->getTranslatedLanguage(
                            "register.status.tregsuccess",
                            array( '%msisdn%' => $registration->getMsisdn() ),
                            $registration->getLanguage()
                        );
                    $message = new Message();
                    $message->setDeviceId($registration->getDeviceId());
                    $message->setMessage($msg);
                    $message->setTopic($this->container->getParameter("mqtt_notify_msg"));
                    $this->container->get('api.helper')->sendMessage($message);
                } else {
                    if ($status == 4 && $description == "Fully Registered iCAP") {
                        $this->container->get('api.helper')->notifyRegStatus($registration, $status, $message);
                    }
                }
            }
        }
    }

    private function processFregCallback($record_id, $reg_type, $reg_sub_type, $status, $message) {

        $apiHelper = $this->container->get('api.helper');
        $em = $this->getDoctrine()->getManager();
        $this->get('api.helper')->logInfo(
            $this->TAG,
            "apiCallbackAction",
            array(
                'id'       => $record_id,
                'type'     => $reg_type,
                'sub_type' => $reg_sub_type,
                'status'   => $status,
                'message'  => $message,
            )
        );


        //"status":"success_dbms"
        $message = $this->clean($message);
        $setString = "u.fullRegDesc = '" . $message . "', ";

        if ($reg_sub_type) {
            $status = $this->getStatusKeys()[ $status ];
            $setString = $setString . "u.fullRegStatus = '" . $status . "',";

            if ($reg_sub_type == "freg_dbms") { // dbms
                $setString = $setString . "u.fregDate = '" . date('Y-m-d H:i:s') . "',";
                $setString = $setString . "u.dbmsState = '" . $status . "' ";
            } else {
                if ($reg_sub_type == "freg_icap") { //freg icap
                    $setString = $setString . "u.dbmsDate = '" . date('Y-m-d H:i:s') . "',";
                    $setString = $setString . "u.icapState = '" . $status . "',";

                }
            }
        }

        $this->updateRegistration($setString, $record_id, $status, $message);
        if ($record_id) {
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
            if ($registration) {
                $this->get('api.helper')->logInfo(
                    $this->TAG,
                    "apiCallbackAction",
                    array( 'status' => $status, 'msisdn' => $registration->getMsisdn() )
                );
                if (in_array($status, array( 6, 7 ))) {
                    $apiHelper->notifyRegStatus($registration, $status, $message);
                }
            }
        }
    }

    private function updateRegistration($setString, $record_id, $status, $message, $where = null) {
        $setString = substr($setString, 0, -1);
        $em = $this->getDoctrine()->getManager();
        $apiHelper = $this->container->get('api.helper');
        $query = "UPDATE RestApiBundle:RegistrationStatus u SET " . $setString . "  WHERE u.registrationId = " . $record_id;
        if (!is_null($where)) {
            $query = $query . " and " . $where;
        }
        $em->createQuery($query)->getArrayResult();
        $this->get('api.helper')->logInfo($this->TAG, "updateRegistration", array( 'query' => $query ));
    }

    private function getStatusKeys() {
        $status_keys = array(
            'success'      => 2,
            'declined'     => 3,
            'fail'         => 4,
            'retry'        => 5,
            'success_cap'  => 6,
            'success_dbms' => 7,
        );

        return $status_keys;
    }

    private function getMpesaState() {
        $mpesa_states = array(
            'success'    => 1,
            'failed'     => 2,
            'fail'       => 2,
            'registered' => 3,
        );

        return $mpesa_states;
    }

    /**
     * Makes a Post to intergration box
     * @Get("/api_callback/{record_id}/{reg_type}/{status}/{message}", name="api_callback")
     * @Template("DashboardBundle:Admin:agent_details.html.twig")
     * @View()
     */

    public function apiCallbackAction($record_id, $reg_type, $status, $message) {
        /*
          /api_callback/{record_id}/{reg_type}/{status}/{message}

          record_id —>
          reg_type —> treg | freg
          status —> success | fail | retry | success_cap | success_dbms
          message —> url_encode_descriptive_message
         */

        //
        $this->get('api.helper')->logInfo(
            $this->TAG,
            "apiCallbackAction",
            array( 'id' => $record_id, 'type' => $reg_type, 'status' => $status, 'message' => $message )
        );

        $reg_type_key = array(
            'treg' => 'temporaryRegStatus',
            'freg' => 'fullRegStatus',
        );
        $registration_type = $reg_type_key[ $reg_type ];
        $status_keys = array(
            'success'      => 2,
            'declined'     => 3,
            'fail'         => 4,
            'retry'        => 5,
            'success_cap'  => 6,
            'success_dbms' => 7,
        );

        $message_key = array(
            'treg_desc' => 'temporaryRegDesc',
            'freg_desc' => 'fullRegDesc',
        );

        $description = $message_key[ $reg_type . '_desc' ];

        $status = $status_keys[ $status ];

        $em = $this->getDoctrine()->getManager();

        /**
         * This is a temporary solution to mark the ones with name mismatch as declined.
         * Later will evalute on the gateway to mark as declined.
         */
        if ($status == '4' && strtolower($message) == 'mismatched') {
            $status = 3;
        }

        $call_back_datefield = "fregDate";
        if ($reg_type == "treg") {
            $call_back_datefield = "tregDate";
        }

        $em->createQuery(
            "UPDATE RestApiBundle:RegistrationStatus u SET u." . $registration_type . " = '" . $status . "', u." . $call_back_datefield . " = '" . date(
                'Y-m-d H:i:s'
            ) . "', u." . $description . " = '" . $message . "' WHERE u.registrationId = " . $record_id
        )
            ->getArrayResult();


        if ($record_id) {
            /** @var  $registration Registration */
            $registration = $em->getRepository("RestApiBundle:Registration")->find($record_id);
            $this->get('api.helper')->logInfo(
                $this->TAG,
                "apiCallbackAction",
                array( 'status' => $status, 'msisdn' => $registration->getMsisdn() )
            );
        }

        if (in_array($status, array( 7, 6 ))) {
            $apiHelper = $this->container->get('api.helper');
            $apiHelper->notifyRegStatus($registration, $status, $message);
        }


        exit;
    }

    /**
     * Makes a Post to intergration box
     * @Get("/tigo_api/{id}", name="/tigo_api")
     */
    public function tigoApiAction($id) {

        $url = "http://196.41.61.247:8080/Nida_API/api/nida/getfulldemographicdata";


        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('RestApiBundle:Registration')->find($id);

        //list all id types to formaster key array now that we aint entity relating
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT t FROM DashboardBundle:Idtype t');

        $idtype = $query->getArrayResult();

        foreach ($idtype as $key => $val) {
            $idKeyArray[ $val[ 'id' ] ] = $val[ 'name' ];
        }

        //get those configured to be verified
        $result = $em->createQuery(
            "SELECT p "
            . " from DashboardBundle:ConfigMaster p "
            . " WHERE p.name = 'id_verification_list' "
        )
            ->getOneOrNullResult();


        if ($result) {

            $idsToBeVerified = json_decode($result->getConfig());

            //echo $entity->getIdentificationType() . " | " .json_encode($idKeyArray) . " | " . json_encode($idsToBeVerified); exit;

            if (in_array($idKeyArray[ $entity->getIdentificationType() ], $idsToBeVerified)) {
                //clal API

                $data = array(
                    "idNumber" => $entity->getIdentification(),
                );

                echo $response = $this->curl_message($url, $data);
            }
        }

        $response = json_decode($response, true);

        $em = $this->getDoctrine()->getManager();
        if ($response[ 'json' ][ 'message' ] == "success") {

            $customer_compare_name = $firstname = $response[ 'json' ][ 'item' ][ 'firstName' ] . ' ' . $lastname = $response[ 'json' ][ 'item' ][ 'lastName' ];

            //do 85% match
            $likehood = $this->likehood_percent(
                $entity->getFirstName() . ' ' . $entity->getLastName(),
                $customer_compare_name
            );
            $likehood_reverse = $this->likehood_percent(
                $entity->getLastName() . ' ' . $entity->getFirstName(),
                $customer_compare_name
            );

            if ($likehood >= 85 || $likehood_reverse >= 85) {
                //let it be
            } else {
                //decline on mismatch basis
                $em->createQuery(
                    "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 3, u.temporaryRegDesc = 'Validation Box :: Name mismatch' "
                    . " WHERE u.registrationId = " . $entity->getRegistrationid()
                )
                    ->getArrayResult();
            }
        } elseif ($response[ 'json' ][ 'message' ] == "Invalid id number") {

            //Decline invalid ID type
            $em->createQuery(
                "UPDATE RestApiBundle:RegistrationStatus u SET u.temporaryRegStatus = 3, u.temporaryRegDesc = 'Invalid ID type' "
                . " WHERE u.registrationId = " . $entity->getRegistrationid()
            )
                ->getArrayResult();
        }

        exit;
    }

    function likehood_percent($str1, $str2) {
        similar_text($str1, $str2, $percentMatch);

        return round($percentMatch);
    }

    /**
     * https://vodademo.registersim.com/api/registration/api_callback/125/treg/fail/Successful%20Temporary%20Registered%20iCAP
     */
    private function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }


    /**
     * Is Reg Type biometric
     *
     * @param $data
     * @return bool
     */
    protected function isBiometric($data) {

        return isset($data['fingercode']) && isset($data['nin']);
    }

}
