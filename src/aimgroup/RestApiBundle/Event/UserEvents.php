<?php 
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Event;

class UserEvents{
	const NEW_USER_CREATED = "aimgroup_rest.new_user_created";
	const USER_PASSWORD_CHANGED = "aimgroup_rest.user_password_changed";
}