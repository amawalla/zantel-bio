<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 2:50 PM
 */

namespace aimgroup\RestApiBundle\Event;


use aimgroup\RestApiBundle\Entity\Registration;
use Symfony\Component\EventDispatcher\Event;

class PlainTextEvent extends Event
{
    protected $registration;

    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return Registration
     */
    public function getRegistration()
    {
        return $this->registration;
    }


}