<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 3:09 PM
 */

namespace aimgroup\RestApiBundle\Event;


final class ImagesEvents
{
    const PROCESS_IMAGES = "process.images";

}