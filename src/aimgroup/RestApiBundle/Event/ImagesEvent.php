<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 3:08 PM
 */

namespace aimgroup\RestApiBundle\Event;

use aimgroup\RestApiBundle\Entity\RegImages;
use Symfony\Component\EventDispatcher\Event;


class ImagesEvent extends Event {

    protected $regImage;

    public function __construct(RegImages $regImage) {
        $this->regImage = $regImage;
    }

    public function getRegImages() {
        return $this->regImage;
    }


    public function image() {

        return $this->regImage;
    }
}
