<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 8/6/15
 * Time: 11:26
 */

namespace aimgroup\RestApiBundle\Provider;



use aimgroup\RestApiBundle\Entity\Device;

interface DeviceInterface
{
    public function save(Device $device);

    public function findByTokenAndMsisdn($accessToken, $msisdn);

    public function findByMsisdnAndImei($msisdn, $imei);


}