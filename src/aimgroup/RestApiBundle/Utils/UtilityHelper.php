<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 12:04 PM
 */

namespace aimgroup\RestApiBundle\Utils;


use FOS\ElasticaBundle\Manager\RepositoryManager;
use FOS\ElasticaBundle\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UtilityHelper
{

    const MINIMUM_NAME_LENGTH = 4;
    const NAME_REGEX = '/(.)\1{3}/';
    const MINIMUM_CUSTOMER_AGE = 18;
    const MINIMUM_CUSTOMER_AGEDAYS = 6570;
    const SAME_ID_TYPE_SAME_NUMBER_THRESHOLD = 5;
    const SAME_NUMBER_THRESHOLD = 5;
    protected $container;
    protected $manager;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->manager = $this->container->get("fos_elastica.manager");
    }


    public function isValidPersonName($firstName, $lastName)
    {
        if (is_null($firstName) || is_null($lastName)) {
            return false;
        }
        $name = $firstName . " " . $lastName;
        return (strlen($name) > UtilityHelper::MINIMUM_NAME_LENGTH && preg_match(UtilityHelper::NAME_REGEX, $name) == 0);
    }

    public function isValidDateOfBirth($dob)
    {
        if (is_null($dob)) {
            return false;
        }
        try {
            //$difference = (new \DateTime())->diff(new \DateTime($dob));
            $difference = (strtotime(date('Y-m-d')) - strtotime($dob))/86400;
            return ($difference >= UtilityHelper::MINIMUM_CUSTOMER_AGEDAYS);
        } catch (\Exception $e) {
            return false;
        }
    }


    public function checkIdNumberUsageLimit($idnumber)
    {
        return true;
        //$count = $this->manager->getRepository("RestApiBundle:RegistrationStatus")->findCountOfIdNumber($idnumber);
        //return ($count < UtilityHelper::SAME_NUMBER_THRESHOLD);
    }

    public function checkIdUsageLimit($idType, $idNo)
    {
        return true; 
        //$count = $this->manager->getRepository("RestApiBundle:RegistrationStatus")->findCountOfIdTypeAndIdNumber($idType, $idNo);
        //return ($count < UtilityHelper::SAME_ID_TYPE_SAME_NUMBER_THRESHOLD);
    }

    public function findByMsisdnAndSerial($msisdn, $serial)
    {
        return $this->manager->getRepository("RestApiBundle:Registration")->findByMsisdnAndSerial($msisdn, $serial);
    }

    public function findByMsisdnAndSerialOrRegistrationId($msisdn, $serial, $registrationId)
    {
        return $this->manager->getRepository("RestApiBundle:Registration")->findByMsisdnAndSerialOrRegistrationId($msisdn, $serial, $registrationId);
    }
}
