<?php

/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/8/15
 * Time: 12:34 PM
 */

namespace aimgroup\RestApiBundle\Utils;

use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Entity\Registration;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiHelper
{

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function log($method, $array, $status = false)
    {
        if ($status) {
            $array = json_encode(
                array(
                    "msg"  => $array[0],
                    "user" => $array[1],
                )
            );
        }
        $this->container->get("monolog.logger.api")->info(sprintf("action:".$method." |data:%s", $array));
    }

    public function publish($topic, $command, $message)
    {
        $url = $this->container->getParameter("mqtt_gateway_url");
        $topic = str_replace("{1}", preg_replace('/\s+/', '', $topic), $this->container->getParameter("mqtt_topic"));
        $topic = str_replace("{2}", preg_replace('/\s+/', '', $command), $topic);
        $this->container->get("monolog.logger.api")->info(
            "publish|data:command:".$command.",topic:".$topic.",message:".$message
        );
        $qos = 2;
        $decode = array();
        $decode["message"] = json_decode($message, true);
        $decode["topic"] = json_encode($topic);

        $this->curl_message_mqtt($url, $decode, null, "message");

        return 1;
    }

    public function curl_message_mqtt($url, $data, $callback = null, $requestKey = "customer")
    {
        $this->LogI("ApiHelper", "curl_message", array("messag" => $data));
        $data_string = json_encode($data);
        $username = "ekyc2";
        $password = "ekycRestfullUser123@321#";
        $ch = curl_init($url);

        $requestArray = array();
        $requestArray[$requestKey] = json_encode($data["message"]);//$data_string;
        $requestArray['topic'] = json_decode($data["topic"]);
        if ($callback) {
            $requestArray["callbackURL"] = $callback;
        }

        $this->container->get("api.helper")->logInfo("ApiHelper", "curl_message", $requestArray);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $requestArray);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        #    curl_setopt($ch,CURLOPT_HTTPHEADER,array("Content-type: multipart/form-data"));
        curl_setopt($ch, CURLOPT_USERPWD, $username.":".$password);

        return curl_exec($ch);
    }

    public function post($url = null, $options = array())
    {
        if (is_null($url)) {
            return false;
        }
        $curl_options = array(
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HEADER         => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_AUTOREFERER    => true,
        );

        if (!empty($options)) {
            foreach ($options as $key => $option) {
                $curl_options[$key] = $option;
            }
        }

        $ch = curl_init($url);
        curl_setopt_array($ch, $curl_options);
        $content = curl_exec($ch);
        $err_code = curl_errno($ch);
        $errmsg = curl_error($ch);
        $header = curl_getinfo($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);


        return array(
            'errno'    => $err_code,
            'httpCode' => $httpCode,
            'errmsg'   => $errmsg,
            'response' => $content,
        );
    }

    public function formatMsisdn($msisdn)
    {
        if (isset($msisdn)) {
            return substr($msisdn, -9);
        }

        return $msisdn;
    }

    public function getTopic($type)
    {
        switch ($type) {
            case "login":
                $topic = $this->container->getParameter("mqtt_loginconfig_msg");
                break;
            case "image_matrix_update":
                $topic = $this->container->getParameter("mqtt_imageconfig_msg");
                break;
            case "formupdate":
                $topic = $this->container->getParameter("mqtt_formupdate_msg");
                break;
            case "apk_update":
                $topic = $this->container->getParameter("mqtt_apkupdate_msg");
                break;
            default:
                $topic = $this->container->getParameter("mqtt_endpointupdate_msg");
        }

        return $topic;
    }

    public function getLanguage($string)
    {
        if ($string) {
            return $string;
        }

        return explode("_", $string)[0];
    }

    public function logInfo($TAG, $method, $content)
    {
        $this->container->get("monolog.logger.api")->info($TAG.":".$method, $content);
    }

    public function logE($TAG, $method, $e)
    {
        $msg = $e->getMessage();
        $exception = array(
            'class'     => $TAG,
            'method'    => $method,
            'exception' => get_class($e),
            'line'      => $e->getLine(),
            'file'      => $e->getFile(),
        );
        $this->container->get('monolog.logger.api')->err($msg, $exception);
    }

    public function LogI($TAG, $method, $content)
    {
        $this->container->get('monolog.logger.api')->info($method, $content);
    }

    public function curl_message($url, $data)
    {
        $data_string = json_encode($data);
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt(
            $ch,
            CURLOPT_HTTPHEADER,
            array(
                'Content-Type: application/json',
                'Content-Length: '.strlen($data_string),
            )
        );

        return curl_exec($ch);
    }

    /**
     * @param Registration $registration
     * @param $status
     * @param string $trans
     * @param array|string $options
     */
    public function notifyDevice(Registration $registration, $status, $trans = '', $options = array()) {


        $lang = $registration->getLanguage() ? $registration->getLanguage() : "sw";

        if ($trans != '') {

            $failedMsg = $successMsg = $this->getTranslatedLanguage($trans, $options, $lang);

        } else {

            $failedMsg = $this->container->get('translator')->trans(
                'register.status.fail',
                array( '%msisdn%' => $registration->getMsisdn() ),
                'messages',
                ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
            );

            $successMsg = $this->container->get('translator')->trans(
                'register.status.successful',
                array( '%msisdn%' => $registration->getMsisdn() ),
                'messages',
                ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
            );
        }

        $status = in_array($status, array( 6, 7 )) ? 1 : 3;

        $notification = array( "1" => $successMsg, "3" => $failedMsg );

        $this->logInfo("ApiHelper", "notifyRegStatus ", [
            "status"  => "current registration status " . $status,
            'message' => $trans
        ]);


        $deviceId = $registration->getDeviceId();
        $topic = $this->container->getParameter("mqtt_registration_status");

        $requestJson = array(
            "id"             => $this->container->getParameter("operator_id"),
            "operatorName"   => $this->container->getParameter("operator_name"),
            "status"         => $status,
            "registrationId" => $registration->getRegistrationid(),
            "message"        => @$notification[ $status ],
        );

        $this->publish($deviceId, $topic, json_encode($requestJson));


    }


    public function notifyRegStatus(Registration $registration, $status)
    {
        $lang = "sw";
        $failedMsg = $this->container->get('translator')->trans(
            'register.status.fail',
            array('%msisdn%' => $registration->getMsisdn()),
            'messages',
            ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
        );
        $successMsg = $this->container->get('translator')->trans(
            'register.status.successful',
            array('%msisdn%' => $registration->getMsisdn()),
            'messages',
            ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
        );
        $linkageFailure = $this->container->get('translator')->trans(
            'register.failed.linkage',
            array('%msisdn%' => $registration->getMsisdn()),
            'messages',
            ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
        );

        $notification = array("1" => $successMsg, "3" => $failedMsg, "10903001" => $linkageFailure);

        $this->logInfo("ApiHelper", "notifyRegStatus", array("" => "started"));

        $status = (in_array($status, array(6,2)) ? 1 : 3);

        $this->logInfo(
            "ApiHelper",
            "notifyRegStatus",
            array("status" => "current registration status ".$status, "msisdn" => $registration->getMsisdn())
        );
        
        echo " \n 3.  APiHelper: -- status: " . $status;

        $deviceId = $registration->getDeviceId();
        $topic = $this->container->getParameter("mqtt_registration_status");
        $requestJson = array(
            "id"             => $this->container->getParameter("operator_id"),
            "operatorName"   => $this->container->getParameter("operator_name"),
            "status"         => $status,
            "registrationId" => $registration->getRegistrationid(),
            "message"        => @$notification[$status],
        );
        $respond = $this->publish($deviceId, $topic, json_encode($requestJson));
        $this->logInfo("ApiHelper", "notifyRegStatus", array("mqtt_status" => $status));

        $this->logInfo("ApiHelper", "notifyRegStatus", array("" => "end"));
    }

    public function notifyRegStatusDetail(Registration $registration, $responseMsg, $errorArray = null)
    {
        $lang = "sw";
        $failedMsg = $this->container->get('translator')->trans(
            'register.status.fail.verification',
            array('%verify_reasons%' => $responseMsg),
            'messages',
            ($registration->getLanguage() == null) ? $lang : $registration->getLanguage()
        );

        $this->logInfo("ApiHelper", "notifyRegStatusDetail", array("" => "started"));

        $this->logInfo(
            "ApiHelper",
            "notifyRegStatusDetail",
            array(
                "status" => "current registration status Detail ".$responseMsg,
                "msisdn" => $registration->getMsisdn(),
            )
        );

        $deviceId = $registration->getDeviceId();
        $topic = $this->container->getParameter("mqtt_registration_status");

        $requestJson = array(
            "id"             => $this->container->getParameter("operator_id"),
            "operatorName"   => $this->container->getParameter("operator_name"),
            "status"         => 3,
            "registrationId" => $registration->getRegistrationid(),
            "message"        => $failedMsg,
        );
        if (!is_null($errorArray)) {
            $requestJson["editFields"] = implode(',', $this->$errorArray);
        }

        $respond = $this->publish($deviceId, $topic, json_encode($requestJson));
        $this->logInfo("ApiHelper", "notifyRegStatus", array("mqtt_status" => $respond));

        $this->logInfo("ApiHelper", "notifyRegStatus", array("" => "end"));
    }

    /**
     * Parse a csv file
     *
     * @return array
     */
    public function parseCSV($csv)
    {
        $header = null;
        $rows = array();
        if (($handle = fopen($csv, "r")) !== false) {
            while (($data = fgetcsv($handle, null, ",")) !== false) {
                if (!$header) {
                    $header = $data;
                } else {
                    $rows[] = array_combine($header, $data);
                }
            }
            fclose($handle);
        }

        return $rows;
    }

    public function sendMessage(Message $message)
    {
        $message->setOperatorName($this->container->getParameter("operator_name"));
        $message->setOperatorId($this->container->getParameter("operator_id"));
        $respond = $this->publish(
            $message->getDeviceId(),
            $message->getTopic(),
            json_encode($message->getMessageBody())
        );

        return $respond;
    }


    public function iccdCheckValidate($idWithoutCheckdigit)
    {
        $sum = 0;
        $length = strlen($idWithoutCheckdigit);
        if ($length == 18) {
            for ($i = 0; $i < $length; $i++) {
                $char = substr($idWithoutCheckdigit, $length - $i - 1, 1);
                if ($i % 2 == 0) {
                    $weight = (2 * $char) - (int)($char / 5) * 9;
                } else {
                    $weight = $char;
                }
                $sum += $weight;
            }
            $sum = abs($sum) + 10;
            $lastDigit = (10 - ($sum % 10)) % 10;
            $idWithoutCheckdigit = $idWithoutCheckdigit.$lastDigit;
        }

        return $idWithoutCheckdigit;
    }

    public function getTranslatedLanguage($key, $params, $lang)
    {
        return $this->container->get('translator')->trans($key, $params, 'messages', ($lang == null) ? "sw" : $lang);
    }

    public function sendSMS($msisdn, $accessToken)
    {

        $operator = strtolower($this->container->getParameter("operator_name"));

        $message = urlencode($accessToken);

        $url = $this->container->getParameter("sms.api")."?mobile=255{$msisdn}&msg={$message}&operator={$operator}";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_exec($ch);
        curl_close($ch);
    }

}
