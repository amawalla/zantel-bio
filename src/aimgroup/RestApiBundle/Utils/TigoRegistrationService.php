<?php
namespace aimgroup\RestApiBundle\Utils;

use \Symfony\Component\DependencyInjection\ContainerInterface;

class TigoRegistrationService {

	protected $container = null;
	protected $customApiHelper = null;
	protected $tigoBaseApiEndpoint = null;
	protected $tag = "TigoDynamicNumberService";

	public function __construct( ContainerInterface $container ) {
		$this->container = $container;
		$this->customApiHelper = $this->container->get('api.helper');
		$this->tigoBaseApiEndpoint = $this->container->getParameter('tigo.nnim.api_endpoint');
	}


	/**
	 * @param string $msisdn
	 * @param boolean $getAnyAvailableMsisdn
	 * @param int $quantity
	 * @param bool|false $sequential
	 * @param int| $validityWindow
	 * @return array|bool
	 */
	public function onReserveNetworkNumbersTemporarily( $msisdn = null, $validityWindow = null, $getAnyAvailableMsisdn = false, $quantity = 3, $sequential = false ) {

		$this->customApiHelper->logInfo( $this->tag, "onReserveNetworkNumbersTemporarily", array('start'=>'entered'));

		if( is_null( $msisdn ) || strlen($msisdn) < 9 ) {
			return false;
		}

		if( is_null( $validityWindow ) ) {
			$validityWindow = $this->container->getParameter('tigo.nnim.validity_window');
		}

		$msisdn = '255%'.substr( $msisdn, -6 );

		// Just get any available MSISDN
		if( $getAnyAvailableMsisdn ) {
			$msisdn = '';
		}

		$apiUriEndpoint = 'reservenetworknumbers';

		//formulate request body
		$xml_request_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/ReserveNetworkNumbersTemporarilyRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
                       <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                       <cor:debugFlag>true</cor:debugFlag>
                          <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                             <wsse:UsernameToken>
                                <wsse:Username>'.$this->container->getParameter('tigo.nnim.username').'</wsse:Username>
                                <wsse:Password>'.$this->container->getParameter('tigo.nnim.password').'</wsse:Password>
                             </wsse:UsernameToken>
                          </wsse:Security>
                       </soapenv:Header>
                       <soapenv:Body>
                          <v1:ReserveNetworkNumbersTemporarilyRequest>
                             <v3:RequestHeader>
                                <v3:GeneralConsumerInformation>
                                   <v3:consumerID>'.$this->container->getParameter('tigo.nnim.consumer_id').'</v3:consumerID>
                                   <!--Optional:-->
                                   <v3:transactionID>TRX_'.substr($msisdn, -6).'</v3:transactionID>
                                   <v3:country>TZA</v3:country>
                                   <v3:correlationID>COR_'.substr($msisdn, -6).'</v3:correlationID>
                                </v3:GeneralConsumerInformation>
                             </v3:RequestHeader>
                             <v1:RequestBody>
                                <v1:networkNumberSubTypeId>'.$this->container->getParameter('tigo.nnim.sub_type_id').'</v1:networkNumberSubTypeId>
                                <v1:quantitytobereserved>'.$quantity.'</v1:quantitytobereserved>
                                <v1:validityWindowMinutes>'.$validityWindow.'</v1:validityWindowMinutes>
                                <v1:sequential>'.($sequential ? 'true' : 'false').'</v1:sequential>
                                <!--Optional:-->
                                <v1:networkNumberPskFilter>'.$msisdn.'</v1:networkNumberPskFilter>
                                <!--Optional:-->
                                <v1:additionalParameters>
                                   <!--1 or more repetitions:-->
                                   <v2:ParameterType>
                                      <v2:parameterName>?</v2:parameterName>
                                      <v2:parameterValue>?</v2:parameterValue>
                                   </v2:ParameterType>
                                </v1:additionalParameters>
                             </v1:RequestBody>
                          </v1:ReserveNetworkNumbersTemporarilyRequest>
                       </soapenv:Body>
                    </soapenv:Envelope>';

//		var_dump($xml_request_string);

		$this->customApiHelper->logInfo( $this->tag, 'onReserveNetworkNumbersTemporarily', array( 'request' => $xml_request_string) );

		$tigoCurlResponse = $this->sendDataRemotely( $apiUriEndpoint, $xml_request_string );

		$this->customApiHelper->logInfo( $this->tag, 'onReserveNetworkNumbersTemporarily', array('response' => $tigoCurlResponse['response']) );


		if( !isset( $tigoCurlResponse['response'] ) || $tigoCurlResponse === false || $tigoCurlResponse['httpCode'] != 200 ) {
			return false;
		}

		//dump($response['response']);

		//file_put_contents(".logs/{$msisdn}_reservenetworknumbers_response.xml", $response['response'], 8|2);

		//parse response
		$xml = simplexml_load_string($tigoCurlResponse['response']);

		$xml->registerXPathNamespace('soapenv', 'http://schemas.xmlsoap.org/soap/envelope/');
		$xml->registerXPathNamespace('v31', 'http://xmlns.tigo.com/ResponseHeader/V3');
		$xml->registerXPathNamespace('wsse', 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd');

		$reservation_code = (string)$xml->children('soapenv', true )->Body->children('v11', true)->ReserveNetworkNumbersTemporarilyResponse->responseBody->reservationCode;

		$mobile_numbers = $log_data = array();

		foreach( $xml->xpath('//v31:ResponseHeader') as $header ) {
			$counter = 0;
			foreach($header->xpath('//v11:psk') as $val) {
				$counter = ($counter + 1);
				$mobile = (string)$val[0];
				$mobile_numbers[] = $mobile;
				$log_data[] = array(
					'msisdn' => $mobile,
					'reservation_code' => $reservation_code,
					'validity_window' => $validityWindow
				);
			}
		}


		return array(
			'reservation_code' => $reservation_code,
			'numbers' => $mobile_numbers
		);

	}

	/**
	 * @param string $iccid
	 * @param string $msisdn
	 * @return bool|string
	 */
	public function findNetworkDevice( $msisdn = null, $iccid = null ) {

		$this->customApiHelper->logInfo($this->tag, "findNetworkDevice", array('start' => 'called'));

		if( is_null($iccid) || is_null($msisdn) ) {
			return false;
		}

		$requestXMLRequest = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/FindNetworkDeviceRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
						  <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
						   <cor:debugFlag>true</cor:debugFlag>
							  <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								 <wsse:UsernameToken>
									<wsse:Username>'.$this->container->getParameter('tigo.nnim.username').'</wsse:Username>
									<wsse:Password>'.$this->container->getParameter('tigo.nnim.password').'</wsse:Password>
								 </wsse:UsernameToken>
							  </wsse:Security>
						   </soapenv:Header>
						   <soapenv:Body>
							  <v1:FindNetworkDeviceRequest>
								 <v3:RequestHeader>
									<v3:GeneralConsumerInformation>
									   <v3:consumerID>'.$this->container->getParameter('tigo.nnim.consumer_id').'</v3:consumerID>
									   <!--Optional:-->
									   <v3:transactionID>TRX_'.$iccid.'</v3:transactionID>
									   <v3:country>TZA</v3:country>
									   <v3:correlationID>COR_'.$iccid.'</v3:correlationID>
									</v3:GeneralConsumerInformation>
								 </v3:RequestHeader>
								 <v1:RequestBody>
									<v1:iccid>'.$iccid.'</v1:iccid>
									<!--Optional:-->
									<v1:searchCriteria>usk</v1:searchCriteria>
									<!--Optional:-->
									<v1:additionalParameters>
									   <!--1 or more repetitions:-->
									   <v2:ParameterType>
										  <v2:parameterName>?</v2:parameterName>
										  <v2:parameterValue>?</v2:parameterValue>
									   </v2:ParameterType>
									</v1:additionalParameters>
								 </v1:RequestBody>
							  </v1:FindNetworkDeviceRequest>
						   </soapenv:Body>
						</soapenv:Envelope>';

		$this->customApiHelper->logInfo( $this->tag, 'findNetworkDevice', array('request' => $requestXMLRequest));

		$tigoCurlResponse = $this->sendDataRemotely( 'findnetworkdevice', $requestXMLRequest );

		$this->customApiHelper->logInfo( $this->tag, 'findNetworkDevice', array( 'curl response' => $tigoCurlResponse));

		$imsi = $this->findStringBetween( $tigoCurlResponse['response'], '<v1:psk>', '</v1:psk>' );

		$this->customApiHelper->logInfo($this->tag, "findNetworkDevice", array('message' => "IMSI number for ICCID {$msisdn} found --> [{$imsi}]"));

		return ( strlen($imsi) > 0 ? $imsi : false );

	}

	/**
	 * @param string $selectedMsisdn
	 * @param string $numberReservationCode
	 * @param string $registrationID
	 * @return bool|string
	 */
	public function confirmNetworkNumberReservation($selectedMsisdn = null, $numberReservationCode = null, $registrationID = null ) {

		$this->customApiHelper->logInfo($this->tag, 'ConfirmNetworkNumberReservation', array('start' => 'called' ));

		if( is_null($selectedMsisdn) || is_null($numberReservationCode) || is_null($registrationID) ) {
			return false;
		}

		$uri_end_point = 'confirmnetworknumberreserve';

		$request_body_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/ConfirmNetworkNumberReservationRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
								   <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								   <cor:debugFlag>true</cor:debugFlag>
									  <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
										 <wsse:UsernameToken>
											<wsse:Username>'.$this->container->getParameter('tigo.nnim.username').'</wsse:Username>
											<wsse:Password>'.$this->container->getParameter('tigo.nnim.password').'</wsse:Password>
										 </wsse:UsernameToken>
									  </wsse:Security>
								   </soapenv:Header>
								   <soapenv:Body>
									  <v1:ConfirmNetworkNumberReservationRequest>
										 <v3:RequestHeader>
											<v3:GeneralConsumerInformation>
											   <v3:consumerID>'.$this->container->getParameter('tigo.nnim.consumer_id').'</v3:consumerID>
											   <!--Optional:-->
											   <v3:transactionID>TRX_'.substr($selectedMsisdn, -9).'</v3:transactionID>
											   <v3:country>'.$this->container->getParameter('tigo.nnim.country_code').'</v3:country>
											   <v3:correlationID>CRX_'.substr($selectedMsisdn, -9).'</v3:correlationID>
											</v3:GeneralConsumerInformation>
										 </v3:RequestHeader>
										 <v1:RequestBody>
											<v1:selectedMsisdn>'.$selectedMsisdn.'</v1:selectedMsisdn>
											<v1:reservationCode>'.$numberReservationCode.'</v1:reservationCode>
											<!--Optional:-->
											<v1:additionalParameters>
											   <!--1 or more repetitions:-->
											   <v2:ParameterType>
												  <v2:parameterName>?</v2:parameterName>
												  <v2:parameterValue>?</v2:parameterValue>
											   </v2:ParameterType>
											</v1:additionalParameters>
										 </v1:RequestBody>
									  </v1:ConfirmNetworkNumberReservationRequest>
								   </soapenv:Body>
								</soapenv:Envelope>';

		$this->customApiHelper->logInfo( $this->tag, 'ConfirmNetworkNumberReservation', array( 'request' => $request_body_string) );

		$tigoCurlResponse = $this->sendDataRemotely( $uri_end_point, $request_body_string );

		$this->customApiHelper->logInfo( $this->tag, 'ConfirmNetworkNumberReservation', array('response' => $tigoCurlResponse['response']) );

		if( $tigoCurlResponse['httpCode'] == '200' ) {
			$status = ($this->findStringBetween( $tigoCurlResponse['response'], '<v3:status>', '</v3:status>') == 'OK');
			$error_code = $this->findStringBetween( $tigoCurlResponse['response'], '<v3:code>', '</v3:code>');
			$description = $this->findStringBetween( $tigoCurlResponse['response'], '<v3:description>', '</v3:description>');

		} else {
			//confirmnetworknumberreservation-1089-3001-E
			//$status = $this->find_between( $response['response'], '<cmn:status>', '</cmn:status>');
			$error_code = $this->findStringBetween( $tigoCurlResponse['response'], '<cmn:code>', '</cmn:code>');
			$description = $this->findStringBetween( $tigoCurlResponse['response'], '<cmn:description>', '</cmn:description>');

			// Todo -> further checks needed
			//fix double posting issue bug
			$status = false; //($error_code != 'confirmnetworknumberreservation-1089-3001-E');
		}

		$this->customApiHelper->logInfo( $this->tag, 'ConfirmNetworkNumberReservation', array('message' => "Response recieved with status {$status}; Errorcode {$error_code} -> {$description}"));

		return $status;

	}

	/**
	 * @param string $msisdn
	 * @param string $imsi
	 * @param array $customerPersonalDetails
	 * @return array|bool
	 */
	public function onCreateSubscriber($msisdn = null, $imsi = null, $customerPersonalDetails = array() ) {

		$this->customApiHelper->logInfo( $this->tag, 'onCreateSubscriber', func_get_args());

		if( is_null($msisdn) || is_null($imsi) || empty($customerPersonalDetails) ) {
			return false;
		}

		$theXMLRequestBody = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/CreateSubscriberRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v11="http://xmlns.tigo.com/CustomerType/V1" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2" xmlns:v12="http://xmlns.tigo.com/SubscriberType/V1" xmlns:v13="http://xmlns.tigo.com/AccountType/V1">
							   <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								  <cor:debugFlag>true</cor:debugFlag>
								  <wsse:Security>
									 <wsse:UsernameToken>
										<wsse:Username>'.$this->container->getParameter('tigo.nnim.username').'</wsse:Username>
										<wsse:Password>'.$this->container->getParameter('tigo.nnim.password').'</wsse:Password>
									 </wsse:UsernameToken>
								  </wsse:Security>
							   </soapenv:Header>
							   <soapenv:Body>
   									<v1:CreateSubscriberRequest>
										 <v3:RequestHeader>
											<v3:GeneralConsumerInformation>
											   <v3:consumerID>'.$this->container->getParameter('tigo.nnim.consumer_id').'</v3:consumerID>
											   <v3:country>'.$this->container->getParameter('tigo.nnim.country_code').'</v3:country>
											   <v3:correlationID>COR_'.substr($msisdn, -9).'</v3:correlationID>
											</v3:GeneralConsumerInformation>
										 </v3:RequestHeader>
										 <v1:requestBody>
											<v1:msisdn>'.$msisdn.'</v1:msisdn>
											<v1:imsi>'.$imsi.'</v1:imsi>
											<v1:KI>12343678</v1:KI>
											<v1:customer>
											   <v11:name>'.$customerPersonalDetails['name'].'</v11:name>
											   <v11:birthDate>'.$customerPersonalDetails['dob'].'</v11:birthDate>
											   <v11:gender>'.$customerPersonalDetails['gender'].'</v11:gender>
											   <v11:address>'.$customerPersonalDetails['address'].'</v11:address>
											   <v11:clientGrade>Default</v11:clientGrade>
											</v1:customer>
											<v1:subscriber>
											   <v12:language>Swahili</v12:language>
											   <v12:customerType>Prepaid</v12:customerType>
											   <v12:mainProductID>3000048</v12:mainProductID>
											</v1:subscriber>
											<v1:account>
											   <v13:customerName>'.$customerPersonalDetails['name'].'</v13:customerName>
											   <v13:address>'.$customerPersonalDetails['address'].'</v13:address>
											   <v13:title>Mr</v13:title>
											   <v13:billcycleType>1</v13:billcycleType>
											   <v13:additionalProperties>
												  <v2:ParameterType>
													 <v2:parameterName>EmailBillAddr</v2:parameterName>
													 <v2:parameterValue>ekyc@aimgroup.co.tz</v2:parameterValue>
												  </v2:ParameterType>
												  <v2:ParameterType>
													 <v2:parameterName>SMSBillAddr</v2:parameterName>
													 <v2:parameterValue>255713123454</v2:parameterValue>
												  </v2:ParameterType>
												  <v2:ParameterType>
													 <v2:parameterName>BillAddr1</v2:parameterName>
													 <v2:parameterValue>NA</v2:parameterValue>
												  </v2:ParameterType>
												  <v2:ParameterType>
													 <v2:parameterName>FirstName</v2:parameterName>
													 <v2:parameterValue>NA</v2:parameterValue>
												  </v2:ParameterType>
											   </v13:additionalProperties>
											</v1:account>
											<v1:orderBMList>
											   <v1:orderBM>
												  <v1:billMediumId>2</v1:billMediumId>
												  <v1:effDate>20140408000000</v1:effDate>
											   </v1:orderBM>
											</v1:orderBMList>
											<v1:products>
											</v1:products>
										 </v1:requestBody>
									  </v1:CreateSubscriberRequest>
								   </soapenv:Body>
								</soapenv:Envelope>';

		$this->customApiHelper->logInfo( $this->tag, 'onCreateSubscriber', array('Request' => $theXMLRequestBody) );

		$tigoCurlResponse = $this->sendDataRemotely( 'createsubscriber', $theXMLRequestBody );

		$this->customApiHelper->logInfo( $this->tag, 'onCreateSubscriber', array('curl_response' => $tigoCurlResponse) );


		if( $tigoCurlResponse['httpCode'] == 200 ) {
			//$status = $this->find_between( $response['response'], '<v31:status>', '</v31:status>' );
			$error_code = $this->findStringBetween( $tigoCurlResponse['response'], '<v31:code>', '</v31:code>' );
			$description = $this->findStringBetween( $tigoCurlResponse['response'], '<v31:description>', '</v31:description>');

			$final_status = ( $this->findStringBetween( $tigoCurlResponse['response'], '<v31:status>', '</v31:status>' ) == 'OK' );
		} else {
			//$status = $this->find_between( $response['response'], '<cmd:status>', '</cmd:status>' );
			$error_code = $this->findStringBetween( $tigoCurlResponse['response'], '<cmn:code>', '</cmn:code>' );
			$description = $this->findStringBetween( $tigoCurlResponse['response'], '<cmn:description>', '</cmn:description>');

			$final_status = false;
			$acceptable_errors = array('imsi already defined', 'msisdn already defined');

			if( $error_code == 'createsubscriber-1054-3001-E' AND ( in_array(strtolower($description), $acceptable_errors)) ) {
				$final_status = true;
			}

		}

		$this->customApiHelper->logInfo( $this->tag, 'onCreateSubscriber', array('message'=> "CreateSubscriber response received Status : {$final_status} ErrorCode {$error_code} -> {$description}") );

		return $final_status;
	}

	/**
	 * @param string $customer_msisdn
	 * @param string $imsi
	 * @return string
	 */
	public function linkNumberToNetworkDevice( $customer_msisdn = null, $imsi = null ) {

		$this->customApiHelper->logInfo( $this->tag, 'LinkNumberToNetworkDevice', func_get_args());

		$theXMLRequestString = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/LinkNumbertoNetworkDeviceRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
							   <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								  <cor:debugFlag>true</cor:debugFlag>
								  <wsse:Security>
									 <wsse:UsernameToken>
										<wsse:Username>'.$this->container->getParameter('tigo.nnim.username').'</wsse:Username>
										<wsse:Password>'.$this->container->getParameter('tigo.nnim.password').'</wsse:Password>
									 </wsse:UsernameToken>
								  </wsse:Security>
							   </soapenv:Header>
							   <soapenv:Body>
								  <v1:LinkNumbertoNetworkDeviceRequest>
									 <v3:RequestHeader>
										<v3:GeneralConsumerInformation>
										   <v3:consumerID>'.$this->container->getParameter('tigo.nnim.consumer_id').'</v3:consumerID>
										   <!--Optional:-->
										   <v3:transactionID>TRX_'.substr($customer_msisdn, -9).'</v3:transactionID>
										   <v3:country>'.$this->container->getParameter('tigo.nnim.country_code').'</v3:country>
										   <v3:correlationID>COR_'.substr($customer_msisdn, -9).'</v3:correlationID>
										</v3:GeneralConsumerInformation>
									 </v3:RequestHeader>
									 <v1:RequestBody>
										<v1:msisdn>'.$customer_msisdn.'</v1:msisdn>
										<v1:IMSI>'.$imsi.'</v1:IMSI>
										<!--Optional:-->
										<v1:externalReference>REGISTRATION</v1:externalReference>
										<v1:authCode/>
										<!--Optional:-->
										<v1:additionalParameters>
										   <!--1 or more repetitions:-->
										   <v2:ParameterType>
											  <v2:parameterName/>
											  <v2:parameterValue/>
										   </v2:ParameterType>
										</v1:additionalParameters>
									 </v1:RequestBody>
								  </v1:LinkNumbertoNetworkDeviceRequest>
							   </soapenv:Body>
							</soapenv:Envelope>';

		$this->customApiHelper->logInfo($this->tag, 'linkNumberToNetworkDevice', array( 'Request' => $theXMLRequestString));

		$tigoCurlResponse = $this->sendDataRemotely( 'linknumbers', $theXMLRequestString );

		$this->customApiHelper->logInfo($this->tag, 'linkNumberToNetworkDevice', array( 'curl_response' => $tigoCurlResponse['response']));

		if( $tigoCurlResponse['httpCode'] == 200 ) {
			$status = $this->findStringBetween( $tigoCurlResponse['response'], '<v31:status>', '</v31:status>' );
			$description = $this->findStringBetween( $tigoCurlResponse['response'], '<v31:description>', '</v31:description>');

			$final_status = ( $status == 'OK' );
		} else {
			$error_code = $this->findStringBetween( $tigoCurlResponse['response'], '<cmn:code>', '</cmn:code>' );
			$description = $this->findStringBetween( $tigoCurlResponse['response'], '<cmn:description>', '</cmn:description>');

			$final_status = ( trim($error_code) == 'linknumbertonetworkdevice-1090-3001-E' );
			//$final_status = false;
		}

		$this->customApiHelper->logInfo( $this->tag, 'LinkNumberToNetworkDevice', array('message' => "LinkNumberToNetworkDevice response received Status : {$final_status} ErrorCode {$error_code} -> {$description}") );

		return $final_status;

	}

	/**
	 *
	 */
	public function removeNetworkNumberDeviceLink() {

	}

	/**
	 * @param null $msisdn
	 * @return bool
	 */
	public function onCancelNetworkNumberReservation($msisdn = null ) {

		$this->customApiHelper->logInfo($this->tag, 'onCancelNetworkNumberReservation', array('start' => 'called'));

		if( is_null($msisdn) ) {
			return false;
		}

		$msisdn = '255'.substr($msisdn, -9);

		$uri_end_point = 'cancelnetworknumberreserve';

		$xml_request_string = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v1="http://xmlns.tigo.com/FindNetworkDeviceRequest/V1" xmlns:v3="http://xmlns.tigo.com/RequestHeader/V3" xmlns:v2="http://xmlns.tigo.com/ParameterType/V2">
								  <soapenv:Header xmlns:cor="http://soa.mic.co.af/coredata_1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
								   <cor:debugFlag>true</cor:debugFlag>
									  <wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
										 <wsse:UsernameToken>
											<wsse:Username>'.$this->container->getParameter('tigo.nnim.username').'</wsse:Username>
											<wsse:Password>'.$this->container->getParameter('tigo.nnim.password').'</wsse:Password>
										 </wsse:UsernameToken>
									  </wsse:Security>
								   </soapenv:Header>
								   <soapenv:Body>
								   		<v1:CancelNetworkNumberReservationRequest>
											 <v3:RequestHeader>
												<v3:GeneralConsumerInformation>
												   <v3:consumerID>'.$this->container->getParameter('tigo.nnim.consumer_id').'</v3:consumerID>
												   <v3:transactionID>TRX_'.substr($msisdn, -9).'</v3:transactionID>
												   <v3:country>'.$this->container->getParameter('tigo.nnim.country_code').'</v3:country>
												   <v3:correlationID>COR_'.substr($msisdn, -9).'</v3:correlationID>
												</v3:GeneralConsumerInformation>
											 </v3:RequestHeader>
											 <v1:RequestBody>
												<v1:msisdn>'.$msisdn.'</v1:msisdn>
												<v1:additionalParameters>
												   <v2:ParameterType>
													  <v2:parameterName></v2:parameterName>
													  <v2:parameterValue></v2:parameterValue>
												   </v2:ParameterType>
												</v1:additionalParameters>
											 </v1:RequestBody>
										 </v1:CancelNetworkNumberReservationRequest>
									   </soapenv:Body>
									</soapenv:Envelope>';

		$this->customApiHelper->logInfo( $this->tag, 'onCancelNetworkNumberReservation');

		$response = $this->sendDataRemotely( $uri_end_point, $xml_request_string );

		return ( $response['httpCode'] == 200 ? true : false );

	}

	/**
	 * @param string $msisdn
	 * @return mixed
	 */
	public function getReservationCode($msisdn = null ) {
		return rand(1010101, 999910019);
	}

	/**
	 * @param string $string
	 * @param string $start
	 * @param string $end
	 * @return string
	 */
	private function findStringBetween($string, $start, $end ) {

		$string = " ".$string;
		$ini = strpos($string,$start);

		if ($ini == 0) {
			return "";
		}

		$ini += strlen($start);

		$len = strpos( $string, $end, $ini ) - $ini;
		return substr( $string, $ini, $len );

	}

	/**
	 * @param string $uri
	 * @param null $xml_string
	 * @return array|bool
	 */
	private function sendDataRemotely( $uri = null, $xml_string = null  ) {

		if( is_null($uri) || is_null($xml_string) ) {
			return false;
		}

		$tigoFullApiEndPointUrl = $this->tigoBaseApiEndpoint.'/'.$uri;

		$this->customApiHelper->logInfo( $this->tag, 'sendDataRemotely', array('URL' => $tigoFullApiEndPointUrl));

		return $this->customApiHelper->post( $tigoFullApiEndPointUrl, array(
			CURLOPT_SSL_VERIFYHOST => 0,
			CURLOPT_SSL_VERIFYPEER => 0,
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $xml_string,
			CURLOPT_RETURNTRANSFER => 1,
			CURLOPT_HEADER => 0,
			CURLOPT_HTTPHEADER => array(
				"Content-type: text/xml;charset=\"utf-8\"",
				"Accept: text/xml",
				"Cache-Control: no-cache",
				"Pragma: no-cache",
				"SOAPAction: \"\",",
				"Content-length: ".strlen($xml_string)
			)
		));
	}

}
