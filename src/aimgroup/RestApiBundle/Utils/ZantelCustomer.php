<?php 
/**
 * ZantelCustomer.php
 * This class tracks registration details for a zantel customer
 *
 * @category   
 * @author     Salim Said | June 15 2017 17:38
 * @copyright  2017 Aim Group (T) Limited
 * @license    internal
 * @version    CVS: 1.0.0
 **/

namespace aimgroup\RestApiBundle\Utils;

class ZantelCustomer implements CustomerInterface
{
	
	private $firstName;
	
	private $lastName;
	
	private $msisdn;
	
	private $registrationDate;
	
	private $registrationResponseCode;
	
	private $hasImages;
	
	private $registrationResponseMessage;
	

	public function __construct($gatewayResponse)
	{   if(gettype($gatewayResponse) != 'array' OR (count($gatewayResponse) == 0) )
		{
		$this->firstName                   = "not set";
		$this->lastName                    = "not set";
		$this->msisdn                      = "not set";
		$this->registrationDate            = "not set";
		$this->registrationResponseCode    = "not set";
		$this->hasImages 				   = "not set";
		$this->registrationResponseMessage = "not set";
		
		}elseif($gatewayResponse['code'] == '0')
		{
		$this->firstName = $gatewayResponse['accountInformation']['firstName'];
		$this->lastName  = $gatewayResponse['accountInformation']['lastName'];
		$this->msisdn    = $gatewayResponse['accountInformation']['mobileNumber'];
		$this->registrationDate = $gatewayResponse['accountInformation']['registrationDate'];
		$this->registrationResponseCode = $gatewayResponse['code'];
		$this->hasImages = $gatewayResponse['hasImages'];
		$this->registrationResponseMessage = $gatewayResponse['message'];
		
		}elseif($gatewayResponse['code'] == '1')
		{
			$this->firstName = "not set";
			$this->lastName  = "not set";
			$this->msisdn    = "not set";
			$this->registrationDate = "not set";
			$this->registrationResponseCode = $gatewayResponse['code'];
			$this->hasImages = "not set";
			$this->registrationResponseMessage = $gatewayResponse['message'];
		}
	}
	
	public function isRegistered()
	{
		if($this->registrationResponseCode == '0')
			return true;
		return false;
	}
	
	public function hasImages()
	{
		return $this->hasImages;
	}
	
	//getters
	
	public function getFirstName()
	{
		return $this->firstName;
	}
	
	public function getLastName()
	{               
		return $this->lastName;	
	}               
	                
	public function getMsisdn()
	{
		return $this->msisdn;
	}
	
	public function getRegistrationDate()
	{
		return $this->registrationDate;
	}
	
	public function getRegistrationResponseCode()
	{
		return $this->registrationResponseCode;
	}
	
	
	public function getRegistrationResponseMessage()
	{
		return $this->registrationResponseMessage;
	}
	
	//setters
	
	public function setFirstName($firstName)
	{
		$this->firstName = $firstName;
	}
	
	public function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}
	 
	public function setMsisdn($msisdn)
	{
		$this->msisdn = $msisdn;
	}
	
	public function setRegistrationDate($registrationDate)
	{
		$this->registrationDate = $registrationDate;
	}
	
	public function setRegistrationResponseCode($registrationResponseCode)
	{
		$this->registrationResponseCode = $registrationResponseCode;
	}
	
	public function setHasImages($hasImages)
	{
		$this->hasImages = $hasImages;
	}
	
	public function setRegistrationResponseMessage($registrationResponseMessage)
	{
		$this->registrationResponseMessage = $registrationResponseMessage;
	}
	 
}



?>
