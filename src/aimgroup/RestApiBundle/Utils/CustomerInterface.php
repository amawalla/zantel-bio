<?php 

/**
 * CustomerInterface.php
 * Implement this interface to build a customized customer object
 *  
 * @author     Salim Said | June 15 2017 22:08
 * @copyright  2017 Aim Group (T) Limited
 * @license    internal
 * @version    CVS: 1.0.0
 **/

namespace aimgroup\RestApiBundle\Utils;

interface CustomerInterface
{
	public function isRegistered();
}


