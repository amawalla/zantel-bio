<?php 
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use aimgroup\RestApiBundle\Event\UserEvent;
use aimgroup\RestApiBundle\Event\UserEvents;

class SendRandomPasswordEmailListener implements EventSubscriberInterface {

  private $mailer;
  private $controller;

  public function __construct(\SwiftMailer $mailer,AbstractController $controller){
    $this->mailer = $mailer;
    $this->controller = $controller;
  }

  public static function getSubscribedEvents(){
  	
  	return array(
         UserEvents::USER_PASSWORD_CHANGED => "onPasswordChange",
  		);
  }

  public function onPasswordChange(UserEvent $event){

    $this->sendPasswordChangeMail($event->getUser());

  }

  private function sendPasswordChangeMail(User $user){
     // send random password to the email
            $message = \Swift_Message::newInstance()
                ->setSubject('Hello Email')
                ->setFrom($controller->container->getParameter('system_email_address'))
                ->setTo($user->getEmail())
                ->setBody(
                    $controller->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                        $user->getLanguage() == $controller->container->getParameter('default_language') ? 'emails/change_password.html.twig' : 'emails/change_password_other_lang.html.twig',
                        array('user' => $user)
                    ),
                    'text/html'
                );
            
            $this->mailer->send($message);
  }

}