<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use aimgroup\RestApiBundle\Event\UserEvent;
use aimgroup\RestApiBundle\Event\UserEvents;
use aimgroup\RestApiBundle\Entity\User;
use aimgroup\RestApiBundle\Entity\Currency;
use aimgroup\AccountingBundle\Entity\Account;
use aimgroup\AccountingBundle\Model\AccountType;
use Doctrine\ORM\EntityManager;

/**
 * Description of CreateAccountListener
 *
 * @author robert
 */
class CreateAccountListener implements EventSubscriberInterface{
    
    private $entityManager;
    
    public function __construct(EntityManager $entityManager){
        $this->entityManager = $entityManager;
    }

    public static function getSubscribedEvents(){
        return array(
            UserEvents::NEW_USER_CREATED => "onNewUserCreated"
        );
    }
    
    public function onNewUserCreated(UserEvent $event){
        $this->createAccounts($event->getUser());
    } 
    
    private function createAccounts(User $user){
        $currencies = $this->getCurrencies();
        $metadata = new \Doctrine\ORM\Mapping\ClassMetadata((string)$user);
        if($metadata->discriminatorValue == 'user'){
            $this->addAccount($user);
        }else{
            foreach($currencies as $currency){
              $this->addAccount($user,$currency->getId());
            }
        }
    }
    
    private function generateAccountNumber(User $user,$currency = 1){
        $initial = '1011';
        return $initial.'$'.$user->getId().'$'.$currency;
    }
    
    private function getCurrencies(){
        return $this->entityManager->getRepository('RestApiBundle:Currency')->findAll();
    }
    
    private function addAccount(User $user, $currency = 1){
        
        $newAccount = new Account();
        $newAccount->setAccountHolder($user);
        $newAccount->setAccountNumber($this->generateAccountNumber($user,$currency));
        $newAccount->setBalance(0);
        $newAccount->setCurrency($currency);
        $newAccount->setParentAccount(NULL);
        $newAccount->setAccountType(AccountType::USER_ACCOUNT);
        $newAccount->setCreatedAt(new \DateTime(date('Y-m-d H:i:s')));
        $newAccount->setUpdatedAt(new \DateTime(date('Y-m-d H:i:s')));

        $this->entityManager->persist($newAccount);
        $this->entityManager->flush();
    }
    
}
