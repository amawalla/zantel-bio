<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 3:10 PM
 */

namespace aimgroup\RestApiBundle\EventListener;


use aimgroup\RestApiBundle\Dao\GkycRequest;
use aimgroup\RestApiBundle\Dao\ImagesOperatorStatus;
use aimgroup\RestApiBundle\Dao\PlainTextOperatorStatus;
use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Event\ImagesEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class ImagesEventListener implements EventSubscriberInterface {

    private $entityManager;
    private $container;

    public function __construct(EntityManager $entityManager, ContainerInterface $container) {
        $this->entityManager = $entityManager;
        $this->container = $container;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents() {
        return array(
            'process.images' => array( 'onReceiveImages', 0 ),
            'image.create'  => ['onCreate', 0]
        );
    }

    public function onReceiveImages(ImagesEvent $imagesEvent) {

        try {

            $registrationImage = $imagesEvent->getRegImages();

            //$registrationStatus = $this->entityManager->getRepository('RestApiBundle:RegistrationStatus')->findOneBy([
            //    'registrationId' => $registrationImage->getRegistration()
            //]);

            $registration = $this->entityManager->getRepository('RestApiBundle:Registration')->findOneBy([
                'registrationid' => $registrationImage->getRegistration()
            ]);

            $receivedImages = $this->entityManager->getRepository("RestApiBundle:RegImages")->findBy([
                "registration" => $registrationImage->getRegistration()
            ]);

            //$this->container->get("registration.processor.endpoint")->processImages($registration, $receivedImages);

        } catch (\Exception $exception) {
            throw $exception;
        }

    }

    /**
     * @param ImagesEvent $event
     */
    public function onCreate(ImagesEvent $event) {

        $images = $this->entityManager->getRepository(RegImages::class)->findBy([
            'registration' => $event->image()->getRegistration(),
        ], [ ], 4, 0);


        if (count($images) == 4) {

            $registration = $this->entityManager->getRepository(Registration::class)->findOneBy([
                'registrationid' => $event->image()->getRegistration(),
            ]);

            $lastImage = end($images);

            if ($lastImage) {

                if ($registration) {


                    $registrationStatus = $this->entityManager->getRepository(RegistrationStatus::class)
                        ->findOneBy([
                            'registrationId' => $registration,
                        ]);

                    if ($registrationStatus) {

                        $registration->setImageCount(4);

                        if($registrationStatus->getAllimageDate() == null) {
											
			 //$this->container->get('logger')->info(__CLASS__.'::'.__FUNCTION__. '::: Setting All Image Date to '.$lastImage->getCreatedOn() . '  registrationId (' .$registration->getId() . ')');	
                            $registrationStatus->setAllimageDate($lastImage->getCreatedOn());

                        }

                        $this->entityManager->persist($registration);
                        $this->entityManager->persist($registrationStatus);

                        $this->entityManager->flush();

                    }
                }
            }

        }
    }

}

