<?php

/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 2:58 PM
 */

namespace aimgroup\RestApiBundle\EventListener;

use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Event\PlainTextEvent;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\KernelEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PlainTextEventListener implements EventSubscriberInterface {

    private $tag = "PlainTextEventListener";
    private $entityManager;
    private $container;
    private $logger;

    public function __construct(EntityManager $entityManager, ContainerInterface $container) {
        $this->entityManager = $entityManager;
        $this->container = $container;
        $this->logger = $this->container->get('monolog.logger.api');
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents() {
        return [
            'process.plaintext'   => [ 'onReceivePlainText', 0 ],
            'plaintext.create' => [ 'onCreate', 0 ]
        ];

    }


    /**
     * @param PlainTextEvent $plainTextEvent
     */
    public function onReceivePlainText(PlainTextEvent $plainTextEvent) {
        try {

            $this->logger->info($this->tag, [ 'onReceivePlainText' => 'entered' ]);

            $registration = $plainTextEvent->getRegistration();

            if (null !== $registration) {

                $identificationType = $this->entityManager->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

                if (null !== $identificationType) {

                    return $this->container->get('registration.filter')->filter($plainTextEvent->getRegistration(), $identificationType);
                    //$this->logger->info($this->tag, ['onReceivePlainText' => 'completed']);
                }

                $this->logger->warning('Identification number not found, with ID: ' . $registration->getIdentificationType());
            }

            $this->logger->warning('Got null registration object');

        } catch (\Exception $exception) {
            $this->logger->info($this->tag, [
                'onReceivePlainText' => 'exception',
                'message'            => $exception->getMessage()
            ]);
        }

        $this->logger->info('onReceivePlainText completed..');
    }


    /**
     * @param PlainTextEvent $event
     */
    public function onCreate(PlainTextEvent $event) {
        
        $this->tag = $this->tag . ' '. __FUNCTION__;


        $this->logger->info($this->tag .' ::: Checking reg images for '.$event->getRegistration()->getId());


        $this->correctImageCount($event->getRegistration());
        
        // You can add up here 

    }


    /**
     * @param Registration $registration
     */
    protected function correctImageCount(Registration $registration) {
        
        try {

            $images = $this->entityManager->getRepository(RegImages::class)->findBy([
                'registration' => $registration->getRegistrationid(),
            ], [ ], 4, 0);

            if (count($images) == 4) {
                
                $this->logger->info($this->tag .' ::: Found ( '.count($images). ' ) Reg images for '.$registration->getId());

                $lastImage = end($images);

                if ($lastImage) {

                    $registrationStatus = $this->entityManager->getRepository(RegistrationStatus::class)
                        ->findOneBy([
                            'registrationId' => $registration,
                        ]);
                    
                    if($registrationStatus) {

                        $registration->setImageCount(4);

                        if($registrationStatus->getAllimageDate() == null) {

                            $registrationStatus->setAllimageDate($lastImage->getCreatedOn());

                        }

                        $this->entityManager->persist($registration);
                        $this->entityManager->persist($registrationStatus);

                        $this->entityManager->flush();

                    }
                }
            }

	 $this->logger->info(count($images). ' images found, ignoring the allImageDate for [ImageEventListener] process to handle ('.$registration->getId().')');

        }catch (\ErrorException $e) {
            
            $this->container->get('logger')->error('There was an error :::: '. $e->getMessage());
        }
    }
}

