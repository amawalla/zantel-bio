<?php

namespace aimgroup\RestApiBundle\Command;


use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RegistrationProcessorCommand extends ContainerAwareCommand {

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ekyc:registration:process')
            ->addArgument("type")
            ->addArgument("registrationId")
            ->setDescription('Registration processor..');
    }


    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');
        $registrationId = $input->getArgument('registrationId');

        $output->writeln('type: ' . $type);
        $output->writeln('registrationId: ' . $registrationId);

        if ($type == 'treg') {
            $output->writeln('Processing plaintext information.. ');
            $this->processPlainText($registrationId);
            $output->writeln('done');
        }

        if ($type == 'freg') {
            $output->writeln('Processing images information..');
            $this->processImages($registrationId);
        }

        if ($type === 'verify') {
            $this->verify($registrationId);
        }

    }


    private function processPlainText($ids)
    {
        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        $registrationIds = explode(",", $ids);

        foreach ($registrationIds as $registrationId) {

            $registration = $entityManager->getRepository("RestApiBundle:Registration")->find($registrationId);
            echo "  \n/n CalledFind Reg " . $registrationId; 
            if (!is_null($registration)) {
                echo " \n/n Found Registrations ";
                $this->getContainer()->get('registration.processor.endpoint')->processText($registration);
            }else{
                echo " \n/n NOT found Registrations ";
            }
        }

    }

    private function processImages($ids)
    {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        $registrationIds = explode(",", $ids);

        foreach ($registrationIds as $registrationId) {

            $registration = $entityManager->getRepository("RestApiBundle:Registration")->find($registrationId);

            if (!is_null($registration)) {

                $images = $entityManager->getRepository('RestApiBundle:RegImages')->findBy(['registration' => $registration->getRegistrationid()]);

                $this->getContainer()->get('registration.processor.endpoint')->processImages($registration, $images);
            }
        }
    }

    private function verify($id) {
        /** @var EntityManager $entityManager */
        $entityManager = $this->getContainer()->get('doctrine')->getManager();

        $registration = $entityManager->getRepository("RestApiBundle:Registration")->find($id);


        if(null !== $registration) {

            $identification = $entityManager->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

            if(null !== $identification) {

                echo $this->getContainer()->get('registration.filter')->filter($registration, $identification);
            }
        }
    }

}
