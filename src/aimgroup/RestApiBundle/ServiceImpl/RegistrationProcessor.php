<?php

/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:13 PM
 */

namespace aimgroup\RestApiBundle\ServiceImpl;

use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Entity\Verifications;
use aimgroup\RestApiBundle\Dao\GatewayRequest;
use aimgroup\RestApiBundle\Dao\ImagesOperatorStatus;
use aimgroup\RestApiBundle\Dao\OperatorResponse;
use aimgroup\RestApiBundle\Dao\PlainTextOperatorStatus;
use aimgroup\RestApiBundle\Entity\AgentSignatures;
use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\Services\EndpointInterface;
use aimgroup\RestApiBundle\Services\RequestProcessor\ImagesRequestProcessor;
use aimgroup\RestApiBundle\Services\RequestProcessor\PlainTextRequestProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use aimgroup\RestApiBundle\Utils\ZantelCustomer;

class RegistrationProcessor implements EndpointInterface
{

    private $container;

    private $logger;

    private $tag;

    /** @var \Doctrine\ORM\EntityManager|object */
    private $entityManger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger = $this->container->get('monolog.logger.api');
        $this->entityManger = $this->container->get('doctrine.orm.entity_manager');
    }

    /**
     * @param Registration $registration
     *
     * @return bool
     * @throws \Exception
     */
    public function processText(Registration $registration)
    {

        try {

            /*
            $idtype = $this->entityManger->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

            if (null === $idtype) {
                $this->logger->error("IdType with ID number {$registration->getIdentificationType()} not found");

                return false;
            }

            $filtered = $this->container->get('registration.filter')->filter($registration, $idtype);
            */

            if ($this->isValidRegistration($registration)) {

		/*
                $entityManager = $this->container->get('doctrine')->getManager();
                $region = $entityManager->getRepository('DashboardBundle:Region')->findOneBy(['code' => $registration->getRegion()]);
                $regionName = $this->getRegionNamefromCode($registration->getRegion());
                $registration->setRegion($regionName);

                if (is_numeric($registration->getTerritory())) {
                    $territory = $entityManager->getRepository('DashboardBundle:Territory')->findOneBy(['id' => $registration->getTerritory()]);
                } else {
                    $territory = $entityManager->getRepository('DashboardBundle:Territory')->findOneBy(['name' => $registration->getTerritory()]);
                }

                $registration->setRegion($territory->getRegion()->getName());
                $territoryName = $territory->getName();
                $registration->setTerritory($territoryName);

		*/

                $requestProcessor = new PlainTextRequestProcessor($registration, $this->container);

                $reqProcess = $requestProcessor->process();
                if ($reqProcess instanceof OperatorResponse) {
                    echo 'CALLED OPERATORresponse ';

                    return $this->processPlainTextOperatorResponse($reqProcess);
                } else {
                    echo 'FAILED FOR REGISTRATION: '.$registration->getId().' MSISDN: '.$registration->getMsisdn();
                }
            }

        } catch (\Exception $exception) {
            throw $exception;
        }
    }


    public function getRegionNamefromCode($regCode = '')
    {
        $em = $this->container->get('doctrine')->getManager();

        $query1 = "SELECT * FROM Region WHERE code = '".$regCode."' OR name = '".$regCode."' LIMIT 1";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        //deviceAppVersion | deviceIMEI | deviceModel | deviceOSVersion | deviceRegistrationCreatedDate locationLatLon
        echo '::: count:'.count($result1).' :::<pre>';
        print_r($result1);
        echo '</pre>';

        if (count($result1) == 1) {
            return $result1[0]['name'];
        } else {
            return 'Dar North';
        }

    }

    public function nidaApiAction(Registration $registration)
    {
        return true;

        $nationality = strtolower($registration->getNationality());

        if (!in_array(strtolower($nationality), array('tanzania', 'mtanzania', 'mtz'))) {
            return true;
        }

        $identificationType = strtolower($registration->getIdentificationType());

        if (!in_array($identificationType, ['national id', 'nation id', 3, '3'])) {
            return true;
        }

        $this->container->get('api.helper')->logInfo(
            $this->tag,
            'nidaApiAction',
            array('start' => 'entered for:'.$registration->getMsisdn())
        );
        $url = $this->container->getParameter('nida_api');
        $entityManager = $this->container->get('doctrine')->getManager();

        //list all id types to formaster key array now that we aint entity relating
        $idtypes = $entityManager->getRepository('DashboardBundle:Idtype')->findAll();
        /** @var  $idtype Idtype */
        foreach ($idtypes as $idtype) {
            $idKeyArray[$idtype->getId()] = $idtype->getName();
        }

        $result = $entityManager->getRepository('DashboardBundle:ConfigMaster')->findOneBy(
            array('name' => 'id_verification_list')
        );
        if ($result) {
            $idsToBeVerified = json_decode($result->getConfig());
            if (in_array($idKeyArray[$registration->getIdentificationType()], $idsToBeVerified)) {
                $this->container->get('api.helper')->logInfo(
                    $this->tag,
                    'nidaApiAction',
                    array('msg' => 'id valid for verification:'.$registration->getMsisdn())
                );
                //clal API
                $data = array(
                    'idNumber' => $registration->getIdentification(),
                );

                $time_start = strtotime(date('Y-m-d H:i:s'));
                $response = $this->curl_message($url, $data);
                $time_end = strtotime(date('Y-m-d H:i:s'));

                $response = json_decode($response, true);
                $this->container->get('api.helper')->logInfo(
                    $this->tag,
                    'nidaApiAction',
                    array(
                        'msg'           => $registration->getMsisdn(),
                        'nida response' => $response['json']['message'],
                    )
                );
                if ($response['json']['message'] == 'success') {

                    $customer_compare_name = strtolower(
                        $response['json']['item']['firstName'].' '.$response['json']['item']['lastName']
                    );
                    $database_customer_name = strtolower(
                        $registration->getFirstName().' '.$registration->getLastName()
                    );
                    $database_customer_name_reversed = strtolower(
                        $registration->getLastName().' '.$registration->getFirstName()
                    );

                    //do 85% match
                    $likehood = $this->likehood_percent($database_customer_name, $customer_compare_name);
                    $likehood_reverse = $this->likehood_percent(
                        $database_customer_name_reversed,
                        $customer_compare_name
                    );

                    unset($response['json']['item']['photo']);

                    $verification = new Verifications();
                    $verification->setIdType('NIDA');
                    $verification->setNidaResponseString(json_encode($response));
                    $verification->setCustomerMsisdn($registration->getMsisdn());
                    $verification->setCustomerRecordNames(
                        $registration->getLastName().' '.$registration->getFirstName()
                    );
                    $verification->setTimeTaken($time_end - $time_start);
                    $verification->setCardDistrict($response['json']['item']['district']);
                    $verification->setRegion($registration->getRegion());

                    $this->container->get('api.helper')->logInfo(
                        $this->tag,
                        'nidaApiAction',
                        array(
                            'msg' => 'likehood:'.$likehood.' reverse-likehood:'.$likehood_reverse.' for : '.$registration->getMsisdn(),
                        )
                    );
                    if ($likehood >= 85 || $likehood_reverse >= 85) {
                        $verification->setResultType('MATCH');
                        $entityManager->persist($verification);
                        $entityManager->flush();

                        return true;

                    } else {
                        //decline on mismatch basis
                        $entityManager->getRepository('RestApiBundle:RegistrationStatus')->updateTemporaryRegStatus(
                            3,
                            'Validation Box :: Name mismatch',
                            $registration
                        );
                        $verification->setResultType('MISMATCH');
                        $entityManager->persist($verification);
                        $entityManager->flush();


                        $message = new Message();
                        $message->setDeviceId($registration->getDeviceId());
                        $message->setTopic($this->container->getParameter('mqtt_registration_status'));
                        $message->setMessage(
                            $this->container->get('api.helper')->getTranslatedLanguage(
                                'register.status.failed_nida',
                                array(),
                                $registration->getLanguage()
                            )
                        );
                        $message->setData(
                            array(
                                'status'         => 3,
                                'registrationId' => $registration->getRegistrationid(),
                            )
                        );
                        $this->container->get('api.helper')->sendMessage($message);

                        //send a message
                        return false;
                    }
                } else {
                    $this->container->get('api.helper')->logInfo(
                        $this->tag,
                        'nidaApiAction',
                        array('msg' => 'Failed to get valid response from nida:'.$registration->getMsisdn())
                    );

                    return false;
                }
            } else {
                $this->container->get('api.helper')->logInfo(
                    $this->tag,
                    'nidaApiAction',
                    array('msg' => 'id in-valid for verification:'.$registration->getMsisdn())
                );
            }
        }

        return true;
    }

    /**
     * @param Registration $registration
     * @param array $images
     *
     * @return bool|mixed
     * @throws \Exception
     */
    public function processImages(Registration $registration, $images = [])
    {

        echo " \n 1.  processImages: ";
        try {

            //update image count
            $receivedImagesCount = count($images);

            //update image count
            $registration->setImageCount($receivedImagesCount);
            $this->entityManger->persist($registration);
            $this->entityManger->flush();

            if ($receivedImagesCount < $this->container->getParameter('images.required.total')) {
                echo " \n 1.  processImages: -- 1.1 receivedImagesCount:".$receivedImagesCount.' :: req:'.$this->container->getParameter('images.required.total');

                $this->logger->info(
                    "Required total images is {$this->container->getParameter('images.required.total')}, Total images received so far {$receivedImagesCount}"
                );

                return false;
            }

            echo " \n 1.  processImages: -- 1";

            /** @var RegistrationStatus $registrationStatus */
            $registrationStatus = $this->entityManger->getRepository('RestApiBundle:RegistrationStatus')->findOneBy(
                [
                    'registrationId' => $registration->getId(),
                ]
            );

            echo " \n 1.  processImages: -- 2 fullregStatus: ".$registrationStatus->getFullRegStatus().' :: PlainText::SUCCESS'.PlainTextOperatorStatus::SUCCESS;
            //Just avoid resending successfully registrations
            if ($registrationStatus->getFullRegStatus() != ImagesOperatorStatus::SUCCESS) {

                echo " \n 1.  processImages: -- 3 if";

                if ($registrationStatus->getTemporaryRegStatus() == PlainTextOperatorStatus::SUCCESS) {

                    echo " \n 1.  processImages: -- 4 if";

                    $entityManager = $this->container->get('doctrine')->getManager();
#                    $region = $entityManager->getRepository("DashboardBundle:Region")->findOneBy(['code'=>$registration->getRegion()]);
#                    $regionName = $region->getName();
#                    $registration->setRegion($regionName);

                    if (is_numeric($registration->getTerritory())) {
                        $territory = $entityManager->getRepository('DashboardBundle:Territory')->findOneBy(['id' => $registration->getTerritory()]);
                    } else {
                        $territory = $entityManager->getRepository('DashboardBundle:Territory')->findOneBy(['name' => $registration->getTerritory()]);
                    }

                    $registration->setRegion($territory->getRegion()->getName());
                    $territoryName = $territory->getName();
                    $registration->setTerritory($territoryName);

                    #                   //$territory = $entityManager->getRepository("DashboardBundle:Territory")->findOneBy(['id'=>$registration->getTerritory()]);
                    #                   //$territoryName = $territory->getName();
                    #                   //$registration->setTerritory($territoryName);

                    $gatewayRequest = new GatewayRequest($registration);

                    /** @var AgentSignatures $agentSignature */
                    $agentSignature = $this->entityManger->getRepository('RestApiBundle:AgentSignatures')->findOneBy(
                        ['msisdn' => $registration->getAgentMsisdn()]
                    );

                    if ($agentSignature) {
                        $gatewayRequest->setAgentSignatureImage($agentSignature->getWebPath());
                    }

                    echo " \n 1.  processImages: -- 4.1";

                    /**   @var RegImages $image */
                    foreach ($images as $index => $image) {

                        $imageType = $image->getImageType();
                        $imageWebPath = $image->getWebPath();

                        if ($imageType == 'front-pic') {
                            $gatewayRequest->setIdentificationFrontImage($imageWebPath);
                        }

                        if ($imageType == 'rear-pic') {
                            $gatewayRequest->setIdentificationBackImage($imageWebPath);
                        }

                        if ($imageType == 'potrait') {
                            $gatewayRequest->setCustomerPortrait($imageWebPath);
                        }

                        if ($imageType == 'signature') {
                            $gatewayRequest->setCustomerSignatureImage($imageWebPath);
                        }
                    }

                    echo " \n 1.  processImages: -- 4.2";

                    $requestProcessor = new ImagesRequestProcessor($gatewayRequest, $this->container);

                    echo " \n 1.  processImages: -- 4.3";

                    $operatorResponse = $requestProcessor->process();

                    return $this->processImagesOperatorResponse($operatorResponse);
                } else {
                    echo " \n 1.  processImages: -- 4 else";
                }

            } else {
                echo " \n 1.  processImages: -- 3 else ";
            }

            $this->logger->info('Registration has already been sent to the operator');

            return true;

        } catch (\Exception $exception) {
            echo " \n 1.  processImages: -- CATCH:: ".$exception->getMessage();
            throw $exception;
        }
    }

    /**
     * @param OperatorResponse $operatorResponse
     *
     * @return bool
     * @throws \Exception
     */
    public function processPlainTextOperatorResponse(OperatorResponse $operatorResponse)
    {
        try {

            $this->logger->info('processPlainTextOperatorResponse entered', ['OperatorResponse' => $operatorResponse]);


            if (!$operatorResponse) {
                throw new \InvalidArgumentException('OperatorResponse can not be null');
            }


            /** @var RegistrationStatus $registrationStatus */
            $registrationStatus = $this->entityManger->getRepository('RestApiBundle:RegistrationStatus')->findOneBy(
                [
                    'registrationId' => $operatorResponse->getRegistration()->getId(),
                ]
            );

            //Check if the this is a retrial request, avoid overriding of status
            if ($registrationStatus->getTemporaryRegStatus() == PlainTextOperatorStatus::SUCCESS) {
//                return true;
            }

            echo 'processPlainTextOperatorResponse  00 ';

            /*
             * Starting processing for error code 400
             *
             */
            $registrationErrorCode = $operatorResponse->getErrorCode();
            $registration = $operatorResponse->getRegistration();
            $customerMsisdn = $operatorResponse->getRegistration()->getMsisdn();

            if ($registrationErrorCode == 406) {//exit("exiting with 503");

                echo "\n /n processPlainTextOperatorResponse  01 if regErrorCode == 406";

                $zantelCustomer = $this->queryCustomerDetails($registration->getMsisdn());

                if ($zantelCustomer->isRegistered()) {
                    echo " \n /n processPlainTextOperatorResponse  02 IF is registered zantel cust ";

                    //if($this->isValidRegistrationPeriod($registration, $zantelCustomer))
                    if ($zantelCustomer->isRegistered()) {
                        echo " \n /n processPlainTextOperatorResponse  03 IF valid reg ";

                        $this->logger->info($customerMsisdn.' is already registered');

                        $tregStatus = PlainTextOperatorStatus::SUCCESS;
                        $fregStatus = ImagesOperatorStatus::SUCCESS;

                        if ($zantelCustomer->hasImages() == 'true') {
                            echo " \n /n processPlainTextOperatorResponse 04 IF has imaged ";

                            $this->logger->info($customerMsisdn.' is already registered and has all images');

                            $this->doTregUpdate($registrationStatus, $tregStatus, $operatorResponse);
                            $this->doFregUpdate($registrationStatus, $fregStatus, $operatorResponse);

                        } else {
                            echo " \n /n processPlainTextOperatorResponse  04 ELSE no images ";

                            $this->logger->info($customerMsisdn.' is already registere with NO images');

                            $this->doTregUpdate($registrationStatus, $tregStatus, $operatorResponse);
                            $images = $this->getImages($registration->getId());
                            $this->processImages($registration, $images);
                        }
                    } else {

                        echo " \n /n processPlainTextOperatorResponse  03 NOT valid reg ";

                        $this->logger->info($customerMsisdn.' Declining registration as found with invalid registration period');
                        //registration not registered with aimEkyc ,, decline
                        $this->declineInvalidRegistration($registrationStatus, 4, $operatorResponse);
                    }

                } else {
                    echo " \n /n processPlainTextOperatorResponse  02 ELSE NOT reg zantel cust  ";

                    $this->logger->info($customerMsisdn.' customer not registered, attempting to register the customer');

                    $this->processText($registration);
                }

                exit;
            }

            echo " \n /n processPlainTextOperatorResponse  01 NOT 406 so proceed ";


            //Successfully response
            $operatorErrorMessage = "{$operatorResponse->getErrorMessage()} [{$operatorResponse->getErrorCode()}]";
            $statusCode = PlainTextOperatorStatus::PENDING;


            echo " \n /n processPlainTextOperatorResponse  20 ErrorCode: ".$operatorResponse->getErrorCode().' AcceptableCodes: '.json_encode($operatorResponse->getAcceptableErrorCodes()).' RejectedCodes: '.json_encode($operatorResponse->getRejectedErrorCodes());

            if (in_array($operatorResponse->getErrorCode(), $operatorResponse->getAcceptableErrorCodes())) {

                echo " \n /n processPlainTextOperatorResponse  21 IF inArray Errorcode in acceptable  ";

                $statusCode = PlainTextOperatorStatus::SUCCESS;

            } elseif (in_array($operatorResponse->getErrorCode(), $operatorResponse->getRejectedErrorCodes())) {

                echo " \n /n processPlainTextOperatorResponse  21 ELSEIF inArray Errorcode in reject  ";
                $statusCode = PlainTextOperatorStatus::DECLINED;
            } else {
                $operatorErrorMessage = '';
                echo " \n /n processPlainTextOperatorResponse  21 ELSE  ";
            }

            $this->logger->debug(
                "Got Error Code --> {$operatorResponse->getErrorCode()}, hence success code.. {$statusCode}"
            );

            $registrationStatus->setTemporaryRegStatus($statusCode);
            $registrationStatus->setTemporaryRegDesc($operatorErrorMessage);
            $registrationStatus->setTregDate(new \DateTime());
            $registrationStatus->setFregDate(new \DateTime());
            $this->entityManger->persist($registrationStatus);
            $this->entityManger->flush();

            if (PlainTextOperatorStatus::SUCCESS == $statusCode) {

                echo " \n /n processPlainTextOperatorResponse  22 IF statuscode SUCCESS  ";

                $confirmationMessage = $this->container->get('translator')->trans('registration.message.successful');
                $customerMsisdn = $operatorResponse->getRegistration()->getMsisdn();

                $this->container->get('aimgroup.rest.messaging.sms')->send($customerMsisdn, $confirmationMessage);

            }

            $status = PlainTextOperatorStatus::SUCCESS;


            //Notify the agent on the status of the registration..
            $this->container->get('api.helper')->notifyRegStatus(
                $operatorResponse->getRegistration(),
                $status
            );

            $this->logger->info('processPlainTextOperatorResponse completed..');

            return (PlainTextOperatorStatus::SUCCESS == $statusCode);

        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param OperatorResponse $operatorResponse
     *
     * @return bool
     */
    public function processImagesOperatorResponse(OperatorResponse $operatorResponse)
    {
        $this->logger->info('processImagesOperatorResponse entered');

        echo " \n 2.  processImagesOperatorResponse: -- 4.3";
        echo " \n 2.  processImagesOperatorResponse: -- 4.3 resp:".$operatorResponse->getErrorMessage().' :: code:'.$operatorResponse->getErrorCode();


        if (is_null($operatorResponse)) {
            throw new \InvalidArgumentException('OperatorResponse can not be null');
        }

        /** @var RegistrationStatus $registrationStatus */
        $registrationStatus = $this->entityManger->getRepository('RestApiBundle:RegistrationStatus')->findOneBy(
            [
                'registrationId' => $operatorResponse->getRegistration()->getId(),
            ]
        );

        if (!$registrationStatus) {

            $this->logger->warning('RegistrationStatus missing -->Oops, No record found meeting specified criteria');

            return true;
        }

        $status = ImagesOperatorStatus::PENDING;
        $description = "{$operatorResponse->getErrorMessage()} [{$operatorResponse->getErrorCode()}]";

        echo $description;

        if ($registrationStatus->getFullRegStatus() != ImagesOperatorStatus::SUCCESS) {

            if (in_array($operatorResponse->getErrorCode(), $operatorResponse->getAcceptableErrorCodes())) {
                $status = ImagesOperatorStatus::SUCCESS;
            } elseif (in_array($operatorResponse->getErrorCode(), $operatorResponse->getRejectedErrorCodes())) {
                $status = ImagesOperatorStatus::DECLINED;
            } else {
                $description = '';
            }

            //Update registration status
            $registrationStatus->setFullRegStatus($status);
            $registrationStatus->setFullRegDesc($description);
            $registrationStatus->setFregDate(new \DateTime());

            $this->entityManger->persist($registrationStatus);
            $this->entityManger->flush();

        } else {
            $status = ImagesOperatorStatus::SUCCESS;
        }

        //send notification to the device when success or declined
        if ($status != ImagesOperatorStatus::PENDING) {
            $this->container->get('api.helper')->notifyRegStatus($operatorResponse->getRegistration(), $status);
        }

        $this->logger->info('processImagesOperatorResponse completed..');

    }

    /**
     * Checks if registration is qualified to be sent to operators
     *
     * @param Registration $registration
     *
     * @return bool
     */
    private function isValidRegistration(Registration $registration)
    {
      
        $idtype = $this->entityManger->getRepository('DashboardBundle:Idtype')->find($registration->getIdentificationType());

            if (null === $idtype) {
                $this->logger->error("IdType with ID number {$registration->getIdentificationType()} not found");

                return false;
            }


        $filtered = $this->container->get('registration.filter')->filter($registration, $idtype);

        if ($filtered && $this->nidaApiAction($registration)) {
            return true;
        }

        return false;

    }

    public function queryCustomerDetails($msisdn)
    {
        $login = 'ekyc2';
        $password = 'ekycRestfullUser123@321#';
        $url = 'http://middleware3.registersim.com:8080/ZantelGateway/rest/services/zantelGateway/checkRegistrationStatus?msisdn='.$msisdn;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json'));
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        $result = curl_exec($ch);
        curl_close($ch);
        $customerDetails = json_decode($result, true);

        if ($customerDetails) {
            return new ZantelCustomer($customerDetails);
        };

        return [];
    }

    private function getImages($id)
    {
        $registrationId = $id;

        $registration = $this->entityManger->getRepository('RestApiBundle:Registration')->find($registrationId);

        if (!is_null($registration)) {

            $images = $this->entityManger->getRepository('RestApiBundle:RegImages')->findBy(['registration' => $registration->getRegistrationid()]);

            return $images;
        }

        //log no images found
        return [];
    }

    private function doTregUpdate($registrationStatus, $status, $operatorResponse)
    {

        $customerMsisdn = $operatorResponse->getRegistration()->getMsisdn();
        $operatorErrorMessage = 'Number successfully activated treg..';

        $this->logger->info('doFregUpdate entered for '.$customerMsisdn);

        $registrationStatus->setTemporaryRegStatus($status);
        $registrationStatus->setTemporaryRegDesc($operatorErrorMessage);
        $registrationStatus->setTregDate(new \DateTime());
        $registrationStatus->setFregDate(new \DateTime());
        $this->entityManger->persist($registrationStatus);
        $this->entityManger->flush();

        if (PlainTextOperatorStatus::SUCCESS == $status) {

            $confirmationMessage = $this->container->get('translator')->trans('registration.message.successful');
            $customerMsisdn = $operatorResponse->getRegistration()->getMsisdn();
            $this->container->get('aimgroup.rest.messaging.sms')->send($customerMsisdn, $confirmationMessage);

        }

        //Notify the agent on the status of the registration..
        $this->container->get('api.helper')->notifyRegStatus(
            $operatorResponse->getRegistration(),
            $status
        );

        $this->logger->info('processPlainTextOperatorResponse completed..');

        return (PlainTextOperatorStatus::SUCCESS == $status);
    }

    private function doFregUpdate($registrationStatus, $status, $operatorResponse)
    {

        $customerMsisdn = $operatorResponse->getRegistration()->getMsisdn();

        $this->logger->info('doFregUpdate entered for '.$customerMsisdn);
        $description = 'Number successfully activated freg..';

        //Update registration status
        $registrationStatus->setFullRegStatus($status);
        $registrationStatus->setFullRegDesc($description);
        $registrationStatus->setFregDate(new \DateTime());

        $this->entityManger->persist($registrationStatus);
        $this->entityManger->flush();

        //send notification to the device when success or declined
        $this->container->get('api.helper')->notifyRegStatus($operatorResponse->getRegistration(), $status);

        $this->logger->info('doFregUpdate completed..');

    }


    public function isValidRegistrationPeriod(Registration $registration, ZantelCustomer $zantelCustomer)
    {
        $aimRegisteredDate = $registration->getCreatedDate();
        $aimRegisteredDate = $aimRegisteredDate->format('Y-m-d H:i:s');

        $zantelRegisteredDate1 = $zantelCustomer->getRegistrationDate();
        $zantelRegisteredDate2 = date_create($zantelRegisteredDate1);
        $zantelRegisteredDate = $zantelRegisteredDate2->format('Y-m-d H:i:s');

        $this->logger->info('isValidRegistration entered for '.$zantelCustomer->getMsisdn(), [
            'aim registration time'  => strtotime($aimRegisteredDate),
            'zantel registered time' => strtotime($zantelRegisteredDate)
        ]);

        if (strtotime($zantelRegisteredDate) >= strtotime($aimRegisteredDate)) {
            return true;
        }

        return false;

    }

    public function declineInvalidRegistration($registrationStatus, $status, $operatorResponse)
    {
        $customerMsisdn = $operatorResponse->getRegistration()->getMsisdn();

        $this->logger->info('decline invalid registration entered for '.$customerMsisdn);
        $description = 'Number declined as not registered via EKYC..';

        //Update registration status
        $registrationStatus->setFullRegStatus($status);
        $registrationStatus->setFullRegDesc($description);
        $registrationStatus->setFregDate(null);

        $this->entityManger->persist($registrationStatus);
        $this->entityManger->flush();

        //send notification to the device when success or declined
        $this->container->get('api.helper')->notifyRegStatus($operatorResponse->getRegistration(), $status);

        $this->logger->info('decline invalid registration completed..');

    }


}
