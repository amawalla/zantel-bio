<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:45 AM
 */
namespace aimgroup\RestApiBundle\ServiceImpl\Filter;


use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Dao\Message;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Services\AbstractFilter;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;


class RegistrationFilter extends AbstractFilter {


    /**
     * @param Registration $registration
     *
     * @param Idtype $idtype
     *
     * @return bool
     * @throws InvalidArgumentException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function filter(Registration $registration, Idtype $idtype)
    {

	
        $status = parent::filter($registration, $idtype);


        if (!$status) {

            $reasons = $descriptions = array();

            foreach ($this->errors as $index => $item) {

                $reasons[] = $this->container->get('api.helper')->getTranslatedLanguage( $item, array(), $registration->getLanguage());
                $descriptions[] = $this->container->get('api.helper')->getTranslatedLanguage( $item, array(), 'en');
            }


            $this->container->get('doctrine')->getRepository('RestApiBundle:RegistrationStatus')->updateDueToFilter($registration, json_encode($descriptions));


            $message = new Message();
            $message->setDeviceId($registration->getDeviceId());
            $message->setMessage(implode(',', $reasons));

            $message->setData([
                'status'         => 3,
                'registrationId' => $registration->getRegistrationid()
            ]);

            $message->setTopic($this->container->getParameter('mqtt_registration_status'));
            $this->container->get('api.helper')->sendMessage($message);
        }

        return $status;

    }

}
