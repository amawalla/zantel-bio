<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:28 AM
 */

namespace aimgroup\RestApiBundle\Factory;



use aimgroup\RestApiBundle\ServiceImpl\Filter\RegistrationFilter;
use aimgroup\RestApiBundle\ServiceImpl\Filter\TigoRegistrationFilter;
use Symfony\Component\DependencyInjection\ContainerInterface;

class RegistrationFilterFactory
{

    /**
     * Static factory method for creating endpoints
     *
     * @param ContainerInterface $container
     *
     * @return RegistrationFilter
     * @internal param $operator
     */
    public static function createFilter(ContainerInterface $container)
    {
        return new RegistrationFilter($container);
    }

}