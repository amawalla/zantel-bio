<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:18 PM
 */

namespace aimgroup\RestApiBundle\Factory;


use aimgroup\RestApiBundle\ServiceImpl\AirtelProcessor;
use aimgroup\RestApiBundle\ServiceImpl\RegistrationProcessor;
use aimgroup\RestApiBundle\ServiceImpl\ZantelProcessor;
use aimgroup\RestApiBundle\ServiceImpl\VodacomProcessor;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EndpointFactory {


    /**
     * Static factory method for creating endpoints
     *
     * @param ContainerInterface $container
     *
     * @return RegistrationProcessor
     * @internal param $operator
     */
    public static function createEndpoint(ContainerInterface $container)
    {
        return new RegistrationProcessor($container);
    }

}