<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:17 PM
 */

namespace aimgroup\RestApiBundle\Dao;


use aimgroup\RestApiBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractEndpoint
{
    protected $container;

    protected $logger;

    protected $entityManger;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->logger = $this->container->get('logger');
        $this->entityManger = $this->container->get('doctrine.orm.entity_manager');
    }

}