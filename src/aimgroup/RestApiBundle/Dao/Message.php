<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 23/06/2016
 * Time: 1:34 PM
 */

namespace aimgroup\RestApiBundle\Dao;


class Message
{

    protected $operatorId;
    protected $operatorName;
    protected $message;
    protected $topic;
    protected $deviceId;
    protected $data;
    
    /**
     * Sets the data. For Android, this is any custom data to use
     *
     * @param array $data The custom data to send
     */
    public function setData($data)
    {
        $this->data = (is_array($data) ? $data : array($data));
    }

    /**
     * Returns any custom data
     *
     * @return array
     */
    public function getData()
    {
        return array_merge(array('message' => $this->getMessage()), $this->data);
    }

    /**
     * @return mixed
     */
    public function getOperatorId()
    {
        return $this->operatorId;
    }

    /**
     * @param mixed $operatorId
     */
    public function setOperatorId($operatorId)
    {
        $this->operatorId = $operatorId;
    }

    /**
     * @return mixed
     */
    public function getOperatorName()
    {
        return $this->operatorName;
    }

    /**
     * @param mixed $operatorName
     */
    public function setOperatorName($operatorName)
    {
        $this->operatorName = $operatorName;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getTopic()
    {
        return $this->topic;
    }

    /**
     * @param mixed $topic
     */
    public function setTopic($topic)
    {
        $this->topic = $topic;
    }

    /**
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param mixed $deviceId
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return array
     *
     *  $requestJson = array(
     * "id" => $this->container->getParameter("operator_id"),
     * "operatorName" => $this->container->getParameter("operator_name"),
     * "status" => $status,
     * "registrationId" => $registration->getRegistrationid(),
     * "message" => $notification[$status],
     * );
     */

    public function getMessageBody()
    {
        $data = array(
            "operatorName" => $this->operatorName,
            "id" => $this->operatorId,
            "operator" => $this->operatorId,
            "message" => $this->message
        );
        if (!empty($this->data)) {
            $data = array_merge($data, $this->data);
        }
        return $data;
    }


}