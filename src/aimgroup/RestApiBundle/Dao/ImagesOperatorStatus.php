<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 15/08/2016
 * Time: 13:24
 */

namespace aimgroup\RestApiBundle\Dao;

abstract class ImagesOperatorStatus {

    const PENDING   = 0;
    const SUBMITTED = 1;
    const SUCCESS   = 6;
    const DECLINED  = 4;

    /**
     * Error description
     *
     * @var array
     */
    static protected $errorDescriptions = [
        self::PENDING   => 'Pending',
        self::SUBMITTED => 'Submitted',
        self::SUCCESS   => 'Success',
        self::DECLINED  => 'Declined'
    ];

    /**
     * @param integer $errorCode
     *
     * @return array
     */
    public static function getErrorDescription( $errorCode = null )
    {
        return (array_key_exists($errorCode, self::$errorDescriptions) ? self::$errorDescriptions[$errorCode] : null);
    }

}