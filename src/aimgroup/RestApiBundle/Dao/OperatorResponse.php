<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 13/08/2016
 * Time: 18:00
 */

namespace aimgroup\RestApiBundle\Dao;

use aimgroup\RestApiBundle\Entity\Registration;

class OperatorResponse
{

    /**
     * @var Registration
     */
    private $registration;

    /**
     * Http Status Code
     *
     * @var int
     */
    private $statusCode = 200;

    /**
     * Operator Error Code
     *
     * @var integer
     */
    private $errorCode = null;

    private $referenceId = null;

    /**
     * Operator Error Message
     *
     * @var string
     */
    private $errorMessage = null;

    /**
     * @var array
     */
    private $acceptableErrorCodes = [0, 200];

    private $rejectedErrorCodes = [1,400,406];

    /**
     * OperatorResponse constructor.
     *
     * @param Registration $registration
     * @param int $statusCode
     * @param int $errorCode
     * @param string $errorMessage
     *
     * @internal param string $task
     */
    public function __construct(
        Registration $registration = null,
        $statusCode = 200,
        $errorCode = null,
        $errorMessage = null
    ) {
        $this->registration = $registration;
        $this->statusCode = $statusCode;
        $this->errorCode = $errorCode;
        $this->errorMessage = $errorMessage;
    }


    /**
     * @return Registration
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param Registration $registration
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return string
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param string $task
     */
    public function setTask($task)
    {
        $this->task = $task;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return null
     */
    public function getReferenceId()
    {
        return $this->referenceId;
    }

    /**
     * @param null $referenceId
     */
    public function setReferenceId($referenceId)
    {
        $this->referenceId = $referenceId;
    }

    /**
     * @return integer
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param integer $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param null $errorMessage
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
    }

    /**
     * @return array
     */
    public function getAvailableTasks()
    {
        return $this->availableTasks;
    }

    /**
     * @return array
     */
    public function getAcceptableErrorCodes()
    {
        return $this->acceptableErrorCodes;
    }

    /**
     * @return array
     */
    public function getRejectedErrorCodes()
    {
        return $this->rejectedErrorCodes;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return "Registration [{$this->getRegistration()->getId()} {$this->getRegistration()->getRegistrationid()} {$this->getRegistration()->getMsisdn()}] Response [ ErrorCode:{$this->getErrorCode()} ErrorMessage:{$this->getErrorMessage()} ] AcceptableErrorCodes [".implode(
                ',',
                $this->getAcceptableErrorCodes()
            )."] RejectedErrorCodes [".implode(',', $this->getRejectedErrorCodes())."]";
    }

}
