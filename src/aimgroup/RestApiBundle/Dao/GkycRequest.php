<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 3:53 PM
 */

namespace aimgroup\RestApiBundle\Dao;


use Symfony\Component\DependencyInjection\ContainerInterface;

class GkycRequest
{


    protected $container;
    protected $GKYC_ENCRYPTION_KEY;
    protected $GKYC_USERNAME;
    protected $GKYC_PASSWORD;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->GKYC_ENCRYPTION_KEY = $this->container->getParameter("gkyc_encryption_key");
        $this->GGKYC_USERNAME = $this->container->getParameter("gkyc_username");
        $this->GKYC_PASSWORD = $this->container->getParameter("gkyc_password");
    }


    protected $username;
    protected $password;
    protected $msisdn;
    protected $firstName;
    protected $lastName;
    protected $gender;
    protected $nationality;
    protected $placeOfBirth;
    protected $dateOfBirth;
    protected $address;
    protected $region;
    protected $typeOfId;
    protected $idNumber;
    protected $iccid;
    protected $imageData;
    protected $retailerUsername;
    protected $retailerMsisdn;

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getPlaceOfBirth()
    {
        return $this->placeOfBirth;
    }

    /**
     * @param mixed $placeOfBirth
     */
    public function setPlaceOfBirth($placeOfBirth)
    {
        $this->placeOfBirth = $placeOfBirth;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }

    /**
     * @param mixed $dateOfBirth
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getTypeOfId()
    {
        return $this->typeOfId;
    }

    /**
     * @param mixed $typeOfId
     */
    public function setTypeOfId($typeOfId)
    {
        $this->typeOfId = $typeOfId;
    }

    /**
     * @return mixed
     */
    public function getIdNumber()
    {
        return $this->idNumber;
    }

    /**
     * @param mixed $idNumber
     */
    public function setIdNumber($idNumber)
    {
        $this->idNumber = $idNumber;
    }

    /**
     * @return mixed
     */
    public function getIccid()
    {
        return $this->iccid;
    }

    /**
     * @param mixed $iccid
     */
    public function setIccid($iccid)
    {
        $this->iccid = $iccid;
    }

    /**
     * @return mixed
     */
    public function getImageData()
    {
        return $this->imageData;
    }

    /**
     * @param mixed $imageData
     */
    public function setImageData($imageData)
    {
        $this->imageData = $imageData;
    }

    /**
     * @return mixed
     */
    public function getRetailerUsername()
    {
        return $this->retailerUsername;
    }

    /**
     * @param mixed $retailerUsername
     */
    public function setRetailerUsername($retailerUsername)
    {
        $this->retailerUsername = $retailerUsername;
    }

    /**
     * @return mixed
     */
    public function getRetailerMsisdn()
    {
        return $this->retailerMsisdn;
    }

    /**
     * @param mixed $retailerMsisdn
     */
    public function setRetailerMsisdn($retailerMsisdn)
    {
        $this->retailerMsisdn = $retailerMsisdn;
    }

    public function createXML()
    {
        return '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:in1="http://www.in10stech.com/">
							   <soapenv:Header/>
							   <soapenv:Body>
							      <in1:SaveMetaData>
							         <in1:cUsername>' . $this->aesEncryptString($this->GKYC_ENCRYPTION_KEY, $this->GKYC_USERNAME) . '</in1:cUsername>
							         <in1:cPassword>' . $this->aesEncryptString($this->GKYC_ENCRYPTION_KEY, $this->GKYC_PASSWORD) . '</in1:cPassword>
							         <in1:cMSISDN>' . substr($this->getMsisdn(), -9) . '</in1:cMSISDN>
							         <in1:cFirstName>' . ucwords($this->getFirstName()) . '</in1:cFirstName>
							         <in1:cLastName>' . ucwords($this->getLastName()) . '</in1:cLastName>
							         <in1:cGender>' . strtoupper($this->getGender()) . '</in1:cGender>
							         <in1:cNationality>' . $this->getNationality() . '</in1:cNationality>
							         <in1:cPlaceofBirth>' . $this->getPlaceOfBirth() . '</in1:cPlaceofBirth>
							         <in1:cDateofBirth>' . date('dmY', strtotime($this->getDateOfBirth())) . '</in1:cDateofBirth>
							         <in1:cAddress>' . $this->getAddress() . '</in1:cAddress>
							         <in1:cRegion>' . $this->getRegion() . '</in1:cRegion>
							         <in1:cTypeofID>' . $this->getTypeOfId() . '</in1:cTypeofID>
							         <in1:cIDNo>' . $this->getIdNumber() . '</in1:cIDNo>
							         <in1:cICCID>' . substr($this->getIccid(), 0, 5) . '</in1:cICCID>
							         <in1:cImageData>' . $this->createMultipageTiffImage($this->getMsisdn(), $this->getImageData()) . '</in1:cImageData>
							         <in1:cRetailerUsername>' . $this->getRetailerUsername() . '</in1:cRetailerUsername>
							         <in1:cRetailerMSISDN>' . substr($this->getRetailerMsisdn(), -9) . '</in1:cRetailerMSISDN>
							      </in1:SaveMetaData>
							   </soapenv:Body>
							</soapenv:Envelope>';
    }


    /**
     * create_multipage_tiff_image()
     * @param string $image Image URL
     * @param string $mobile
     * @return string aes128 encrypted image
     */
    private function createMultipageTiffImage($mobile, $imagesArray = array())
    {
        #dump_exit($images_array);
        $multiTIFF = new \Imagick;
        foreach ($imagesArray as $file) {
            //echo "<br /><i>" . $file . "</i><br />";
            if (file_exists($file) AND is_file($file)) {
                $auxIMG = new \Imagick();
                $auxIMG->readImage($file);
                $multiTIFF->addImage($auxIMG);
            }
        }

        $combined_filename = $this->container->getParameter("image_path") . "/multi_{$mobile}.TIFF";
        //dump($combined_filename);
        //file multi.TIF
        $multiTIFF->writeImages($combined_filename, true);
        $contents = file_get_contents($combined_filename);
        return $this->aesEncryptString($this->GKYC_ENCRYPTION_KEY, $contents);

    }

    function aesEncryptString($key, $input)
    {


        $alg = MCRYPT_RIJNDAEL_128; // AES
        $mode = MCRYPT_MODE_ECB; // not recommended unless used with OTP
        //generate IV
        $iv_size = mcrypt_get_iv_size($alg, $mode);

        $block_size = mcrypt_get_block_size($alg, $mode);

        $iv = mcrypt_create_iv($iv_size); // pull from /dev/urandom
        //PKCS5 padding
        $input = $this->pkcs5Pad($input, $block_size);
        return base64_encode(mcrypt_encrypt($alg, $key, $input, $mode, $iv));
    }

    function pkcs5Pad($text, $blocksize)
    {
        $pad = $blocksize - (strlen($text) % $blocksize);
        return $text . str_repeat(chr($pad), $pad);
    }
}