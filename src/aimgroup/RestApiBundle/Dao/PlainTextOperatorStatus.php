<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 15/08/2016
 * Time: 13:24
 */

namespace aimgroup\RestApiBundle\Dao;


abstract class PlainTextOperatorStatus {

    const PENDING   = 0;
    const SUBMITTED = 1;
    const FAILED    = 3;
    const DECLINED  = 4;
    const SUCCESS   = 6;

    /**
     * Error description
     *
     * @var array
     */
    static protected $errorDescriptions = [
        self::PENDING   => 'Pending',
        self::SUBMITTED => 'Request successfully submitted',
        self::SUCCESS   => 'Success',
        self::FAILED    => 'Failed',
        self::DECLINED  => 'Declined'
    ];

    /**
     * @param integer $errorCode
     *
     * @return array
     */
    public static function getErrorDescription($errorCode = null)
    {
        return (array_key_exists($errorCode, self::$errorDescriptions) ? self::$errorDescriptions[$errorCode] : null);
    }

}