<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 15/08/2016
 * Time: 13:24
 */

namespace aimgroup\RestApiBundle\Dao;

abstract class EkycStatus
{
    const PENDING   = 'pending';
    const SUBMITTED = 'submitted';
    const RECEIVED  = 'received';
    const DECLINED  = 'declined';
    const COMPLETED = 'completed';

    /**
     * Status Messages
     *
     * @var array
     */
    static protected $statusMessages = [
        self::PENDING   => 'Pending information',
        self::RECEIVED  => 'All Information Received',
        self::SUBMITTED => 'Information Successfully Submitted to Operator',
        self::DECLINED  => 'Registration Declined',
        self::COMPLETED => 'Registration Completed'
    ];

    /**
     * @param integer $status
     * @return mixed
     */
    public static function getStatusMessages($status = null)
    {
        $status = strtolower($status);
        return (array_key_exists($status, self::$statusMessages) ? self::$statusMessages[$status] : null);
    }
}