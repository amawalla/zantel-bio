<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 01/08/2016
 * Time: 17:47
 */

namespace aimgroup\RestApiBundle\Dao;


use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\DashboardBundle\Entity\Region;
use aimgroup\DashboardBundle\Entity\Territory;
use aimgroup\RestApiBundle\Entity\Registration;

class GatewayRequest {

    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var Idtype
     */
    private $identificationType;

    /**
     * @var Region
     */
    private $region;

    /**
     * @var Territory
     */
    private $territory;

    /**
     * @var string
     */
    private $customerPortrait;

    /**
     * @var string
     */
    private $identificationFrontImage;

    /**
     * @var string
     */
    private $identificationBackImage;

    /**
     * @var string
     */
    private $customerSignatureImage;

    /**
     * @var string
     */
    private $agentSignatureImage;

    /**
     * AirtelGatewayRequest constructor.
     *
     * @param Registration $registration
     * @param Territory $territory
     */
    public function __construct(Registration $registration, Territory $territory)
    {
        $this->setRegistration($registration);
	$this->setTerritory($territory);
    }

    /**
     * @return Registration
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param Registration $registration
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return Idtype
     */
    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    /**
     * @param string $identificationType
     */
    public function setIdentificationType($identificationType)
    {
        $this->identificationType = $identificationType;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return Territory
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param Territory $territory
     */
    public function setTerritory($territory)
    {
        $this->territory = $territory;
    }


    /**
     * @return string
     */
    public function getCustomerPortrait()
    {
        return $this->customerPortrait;
    }

    /**
     * @param string $customerPortrait
     */
    public function setCustomerPortrait($customerPortrait)
    {
        $this->customerPortrait = $customerPortrait;
    }

    /**
     * @return string
     */
    public function getIdentificationFrontImage()
    {
        return $this->identificationFrontImage;
    }

    /**
     * @param string $identificationFrontImage
     */
    public function setIdentificationFrontImage($identificationFrontImage)
    {
        $this->identificationFrontImage = $identificationFrontImage;
    }

    /**
     * @return string
     */
    public function getIdentificationBackImage()
    {
        return $this->identificationBackImage;
    }

    /**
     * @param string $identificationBackImage
     */
    public function setIdentificationBackImage($identificationBackImage)
    {
        $this->identificationBackImage = $identificationBackImage;
    }

    /**
     * @return string
     */
    public function getCustomerSignatureImage()
    {
        return $this->customerSignatureImage;
    }

    /**
     * @param string $customerSignatureImage
     */
    public function setCustomerSignatureImage($customerSignatureImage)
    {
        $this->customerSignatureImage = $customerSignatureImage;
    }

    /**
     * @return string
     */
    public function getAgentSignatureImage()
    {
        return $this->agentSignatureImage;
    }

    /**
     * @param string $agentSignatureImage
     */
    public function setAgentSignatureImage($agentSignatureImage)
    {
        $this->agentSignatureImage = $agentSignatureImage;
    }

    /**
     * @return array
     */
    public function getTemporaryRequest()
    {
        return [
            'record_id'          => $this->getRegistration()->getId(),
            'msisdn'             => $this->getRegistration()->getMsisdn(),
            'first_name'         => $this->getRegistration()->getFirstName(),
            'middle_name'        => ( strlen($this->getRegistration()->getMiddleName()) > 2 ) ? $this->getRegistration()->getMiddleName(): "N/A",
            'last_name'          => $this->getRegistration()->getLastName(),
            'gender'             => $this->getRegistration()->getGender(),
            'dob'                => date('Y-m-d', strtotime($this->getRegistration()->getDob())),
            'address'            => $this->getRegistration()->getWards(),
            'location'           => $this->getRegistration()->getAddress(),
            'id_type'            => $this->getIDName($this->getRegistration()->getIdentificationType()),
            'id_number'          => $this->getRegistration()->getIdentification(),
            'nationality'        => $this->getRegistration()->getNationality(),
            'region'             => $this->getTerritory()->getRegion()->getName(),
            'district'           => $this->getTerritory()->getName(),
            'iccid'              => $this->getRegistration()->getSimSerial(),
            'retailer_username'  => "{$this->getRegistration()->getOwner()->getFirstName()} {$this->getRegistration()->getOwner()->getLastName()}",
            'retailer_msisdn'    => "0{$this->getRegistration()->getAgentMsisdn()}",
            //'alternative_number' => $this->getRegistration()->getContact(),
            //'occupation'         => '',
            'email'              => 'test@test.com'
            //'password'           => '4321'
        ];
    }

    function getIDName($id){
        $idname = array(
            '1' => 'Voters ID',            
            '2' => 'Driving License',      
            '3' => 'National ID',          
            '4' => 'Passport',             
            '5' => 'Zanzibar Residence ID',
	    '6' => 'Zanzibar Voters ID',
            '7' => 'Zanzibar Driving License',
            'Voters ID' => 'Voters ID',
            'Driving License' => 'Driving License',
            'Drivers License' => 'Driving License',
            'National ID' => 'National ID',
            'Passport' => 'Passport',
            'Zanzibar Residence ID' => 'Zanzibar Residence ID',
	    'Voters ID (ZNZ)' => 'Zanzibar Voters ID',
            'Drivers License (ZNZ)' => 'Zanzibar Driving License',
        );

        return @$idname[$id];
    }

    /**
     * @return array
     */
    public function getFullRegistrationRequest()
    {
        $images = [
            'id_front_image'           => $this->getIdentificationFrontImage(),
            'id_back_image'            => $this->getIdentificationBackImage(),
            'picture'                  => $this->getCustomerPortrait(),
            'customer_signature_image' => $this->getCustomerSignatureImage(),
            'agent_signature_image'    => $this->getAgentSignatureImage()
        ];

        return ($this->getTemporaryRequest() + $images);
    }

}
