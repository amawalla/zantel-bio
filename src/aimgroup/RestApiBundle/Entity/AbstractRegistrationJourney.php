<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 04/04/2016
 * Time: 2:54 PM
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 *
 * @ORM\HasLifecycleCallbacks
 * Class AbstractRegistrationJourney
 * @package aimgroup\RestApiBundle\Entity
 */
abstract class  AbstractRegistrationJourney
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @var
     */
    protected $registrationId;

    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    protected $capturedTime;
    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    protected $receivedTime;
    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    protected $fullySubmittedTime;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRegistrationId()
    {
        return $this->registrationId;
    }

    /**
     * @param mixed $registrationId
     */
    public function setRegistrationId($registrationId)
    {
        $this->registrationId = $registrationId;
    }

    /**
     * @return mixed
     */
    public function getCapturedTime()
    {
        return $this->capturedTime;
    }

    /**
     * @param mixed $capturedTime
     */
    public function setCapturedTime($capturedTime)
    {
        $this->capturedTime = $capturedTime;
    }

    /**
     * @return mixed
     */
    public function getReceivedTime()
    {
        return $this->receivedTime;
    }

    /**
     * @param mixed $receivedTime
     */
    public function setReceivedTime($receivedTime)
    {
        $this->receivedTime = $receivedTime;
    }

    /**
     * @return mixed
     */
    public function getFullySubmittedTime()
    {
        return $this->fullySubmittedTime;
    }

    /**
     * @param mixed $fullySubmittedTime
     */
    public function setFullySubmittedTime($fullySubmittedTime)
    {
        $this->fullySubmittedTime = $fullySubmittedTime;
    }


    /**
     * @orm:prePersist
     */
    public function prePersist()
    {
        $this->setReceivedTime(new \DateTime());

    }


}