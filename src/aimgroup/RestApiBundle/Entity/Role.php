<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Doctrine\Common\Collections\ArrayCollection;
use aimgroup\RestApiBundle\Entity\Group;

/**
 * Description of Role
 * 
 * @ORM\Entity
 * @ORM\Table(name="role")
 *
 * @author Robert Makoye
 */
class Role implements RoleInterface {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=50)
     */
    private $name;

    /**
     * @ORM\Column(name="role", type="string", length=50, unique=false)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="dept_permissions", type="text")
     */
    private $dept_perms;

    /**
     * @ORM\ManyToMany(targetEntity="Group", mappedBy="groups")
     */
    private $groups;

    public function __construct() {
        $this->groups = new ArrayCollection();
    }

    public function getRole() {
        return $this->role;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set roleName
     *
     * @param string $roleName
     * @return Role
     */
    public function setRoleName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get roleName
     *
     * @return string 
     */
    public function getRoleName() {
        return $this->name;
    }

    /**
     * Set $dept_perms
     *
     * @param string $dept_perms
     * @return Role
     */
    public function setDeptPerms($dept_perms) {
        $this->dept_perms = $dept_perms;

        return $this;
    }

    /**
     * Get dept_perms
     *
     * @return string 
     */
    public function getDeptPerms() {
        return $this->dept_perms;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Role
     */
    public function setName($name) {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName() {
        return $this->name;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Role
     */
    public function setRole($role) {
        $this->role = $role;

        return $this;
    }

    /**
     * Add users
     *
     * @param \aimgroup\RestApiBundle\Entity\User $users
     * @return Role
     */
    public function addGroup(Group $group) {
        $this->groups->add($group);

        return $this;
    }

    /**
     * Remove users
     *
     * @param \aimgroup\RestApiBundle\Entity\User $users
     */
    public function removeGroup(Group $group) {
        $this->groups->removeElement($group);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGroups() {
        return $this->groups;
    }

    public function toCanonical() {
        return array(
            'id' => $this->getId(),
            'name' => $this->getName(),
            'role' => $this->getRole()
        );
    }

}
