<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description of UserActivities
 *
 * @author Michael Tarimo
 * 
 * @ORM\Table(name="user_activity")
 * @ORM\Entity
 * 
 */
class UserActivity {
    /**
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @ORM\Column(name="action", type="string", nullable=false)
     */
    private $action;
    /**
     *  @ORM\Column(type="array") 
     */
    private $geographicData = array();
    /**
     *  @ORM\Column(type="array") 
     */
    private $deviceProfile = array();
    /**
     * @ORM\Column(name="action_time", type="datetime", nullable=false)
     */
    private $actionTime = array();
    
    function getId() {
        return $this->id;
    }

    function getAction() {
        return $this->action;
    }

    function getGeographicData() {
        return $this->geographicData;
    }

    function getDeviceProfile() {
        return $this->deviceProfile;
    }

    function getActionTime() {
        return $this->actionTime;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setAction($action) {
        $this->action = $action;
    }

    function setGeographicData($geographicData) {
        $this->geographicData = $geographicData;
    }

    function setDeviceProfile($deviceProfile) {
        $this->deviceProfile = $deviceProfile;
    }

    function setActionTime($actionTime) {
        $this->actionTime = $actionTime;
    }
}
