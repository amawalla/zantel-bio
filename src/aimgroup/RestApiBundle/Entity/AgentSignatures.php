<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 8/26/15
 * Time: 15:20
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Class AgentSignatures
 * @package aimgroup\RegistrationBundle\Entity
 * @ORM\Entity(repositoryClass="aimgroup\RestApiBundle\Repository\AgentSignatureRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class AgentSignatures
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    protected $id;
    /**
     *
     * @ORM\Column(type="string",unique=true)
     */
    protected $msisdn;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;


    /**
     * @ORM\Column(type="string")
     */
    protected $fullPath;


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $webPath;
    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdOn;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getFullPath()
    {
        return $this->fullPath;
    }

    /**
     * @param mixed $fullPath
     */
    public function setFullPath($fullPath)
    {
        $this->fullPath = $fullPath;
    }

    /**
     * @return mixed
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * @param mixed $createdOn
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersistDevice()
    {
        $this->setCreatedOn(new \DateTime(date('Y-m-d H:i:s')));
    }

    /**
     * @return mixed
     */
    public function getWebPath()
    {
        return $this->webPath;
    }

    /**
     * @param mixed $webPath
     */
    public function setWebPath($webPath)
    {
        $this->webPath = $webPath;
    }


}