<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 04/04/2016
 * Time: 3:01 PM
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 *@ORM\Entity
 *
 * Class TigoRegistrationJourney
 * @package aimgroup\RestApiBundle\Entity
 */
class TigoRegistrationJourney extends AbstractRegistrationJourney
{


    /**
     * @ORM\Column(type="integer")
     * @var
     */
    private $isNNNIM=0;
    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    private $nnimRequestTime;
    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    private $nnimSubmitedTime;
    /**
     * @ORM\Column(type="datetime")
     * @var
     */
    private $fullyCompletedTime;

    /**
     * @return mixed
     */
    public function getIsNNNIM()
    {
        return $this->isNNNIM;
    }

    /**
     * @param mixed $isNNNIM
     */
    public function setIsNNNIM($isNNNIM)
    {
        $this->isNNNIM = $isNNNIM;
    }

    /**
     * @return mixed
     */
    public function getNnimRequestTime()
    {
        return $this->nnimRequestTime;
    }

    /**
     * @param mixed $nnimRequestTime
     */
    public function setNnimRequestTime($nnimRequestTime)
    {
        $this->nnimRequestTime = $nnimRequestTime;
    }

    /**
     * @return mixed
     */
    public function getNnimSubmitedTime()
    {
        return $this->nnimSubmitedTime;
    }

    /**
     * @param mixed $nnimSubmitedTime
     */
    public function setNnimSubmitedTime($nnimSubmitedTime)
    {
        $this->nnimSubmitedTime = $nnimSubmitedTime;
    }

    /**
     * @return mixed
     */
    public function getFullyCompletedTime()
    {
        return $this->fullyCompletedTime;
    }

    /**
     * @param mixed $fullyCompletedTime
     */
    public function setFullyCompletedTime($fullyCompletedTime)
    {
        $this->fullyCompletedTime = $fullyCompletedTime;
    }


}