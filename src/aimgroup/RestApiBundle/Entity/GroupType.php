<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

/**
 * Description of GroupType
 *
 * @author Robert Gunze
 */
final class GroupType extends EnumType  {

    const SUPER_ADMIN = 'SUPERADMIN';
    const ADMIN = 'ADMIN';
    const SUPER_MANAGER = 'SUPERAGENT';
    const AGENT = 'AGENT';
    const QUALITY_ASSURANCE = 'QUALITYASSURANCE';
}


