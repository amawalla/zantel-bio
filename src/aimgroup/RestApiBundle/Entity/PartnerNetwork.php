<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;


use Doctrine\ORM\Mapping as ORM;


/**
 * Description of PartnerNetwork
 *
 * @ORM\Entity
 * @ORM\Table(name="partner_network")
 *
 * @author Michael Tarimo
 */
class PartnerNetwork {

    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

}