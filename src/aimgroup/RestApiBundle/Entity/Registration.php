<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 8/13/15
 * Time: 23:34
 */

namespace aimgroup\RestApiBundle\Entity;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use aimgroup\RestApiBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="aimgroup\RestApiBundle\Repository\RegistrationRepository")
 * @ORM\Table(name="registration", uniqueConstraints={
 *     @ORM\UniqueConstraint(columns={"msisdn","sim_serial"})
 *     })
 * @ORM\HasLifecycleCallbacks()
 *
 */
class Registration implements \Serializable
{
    /**
     * @Assert\NotBlank(
     *     message= "Msisdn cannot be empty"
     * )
     * @ORM\Column(type="string")
     */
    protected $msisdn;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     *
     * @Assert\NotBlank(
     *     *     message= "RegistrationId cannot be empty"
     * )
     * @ORM\Column(type="string")
     */
    private $registrationid;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $contact;
    /**
     * @ORM\Column(type="integer")
     */
    protected $state = 0;
    /**
     * @ORM\Column(type="integer", options={"comment":"number of images received"})
     */
    protected $image_count = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $createdDate;
    /**
     * @ORM\Column(type="string")
     */
    protected $lastName;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $firstName;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $middleName;
    /**
     * @ORM\Column(type="string")
     */
    protected $territory;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $gender;

    /**
     * @ORM\Column(type="string",name="sim_serial",nullable=true)
     */
    protected $simSerial;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $address;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $wards;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $email;
    /**
     * @ORM\Column(type="string")
     */
    protected $identification;
    /**
     * @ORM\Column(type="string")
     */
    protected $nationality;
    /**
     * @ORM\Column(type="string")
     */
    protected $region;
    /**
     * @ORM\Column(type="string")
     */
    protected $dob;
    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $esn;
    /**
     * @ORM\Column(type="string")
     */
    protected $identificationType;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $appVersion;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $deviceId;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $locationLatLon;

    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $deviceModel;


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $agentImei;


    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    protected $isSms = 0;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $imsi;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $language = "sw";

    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $registrationTime;


    /**
     * @ORM\Column(type="datetime",nullable=true)
     */
    protected $deviceSentTime;


    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $isPorted;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $mnpNetwork;

    /**
     * @ORM\Column(type="smallint", name="registration_type")
     */
    protected $registrationType;

    /**
     * @ORM\Column(type="string", name="finger_print_code", nullable=true)
     */
    protected $fingerPrintCode;


    /**
     * @return mixed
     */
    public function getAgentImei()
    {
        return $this->agentImei;
    }

    /**
     * @param mixed $agentImei
     */
    public function setAgentImei($agentImei)
    {
        $this->agentImei = $agentImei;
    }

    /**
     * @return mixed
     */
    public function getOsVersion()
    {
        return $this->osVersion;
    }

    /**
     * @param mixed $osVersion
     */
    public function setOsVersion($osVersion)
    {
        $this->osVersion = $osVersion;
    }


    /**
     * @ORM\Column(type="string",nullable=true)
     */
    protected $osVersion;
    /**
     *
     * @ORM\Column(type="string")
     */
    private $agentMsisdn;

    /**
     *
     * @ORM\ManyToOne(targetEntity="aimgroup\RestApiBundle\Entity\User")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;


    /**
     * @return mixed
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param mixed $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     *
     * @return User
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     *
     * @param User $owner
     */
    public function setOwner(User $owner)
    {
        $this->owner = $owner;
    }


    /**
     * @return mixed
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @ORM\PrePersist
     * @param \Datetime $createdDate
     * @return Task
     */
    public function setCreatedDate()
    {
        if (!isset($this->createdDate)) {
            $this->createdDate = new \DateTime;
        }

    }


    /**
     * @return mixed
     */
    public function getIdentificationType()
    {
        return $this->identificationType;
    }

    /**
     * @param mixed $identificationType
     */
    public function setIdentificationType($identificationType)
    {
        $this->identificationType = $identificationType;
    }

    /**
     * @return mixed
     */
    public function getMsisdn()
    {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     */
    public function setMsisdn($msisdn)
    {
        $this->msisdn = $msisdn;
    }


    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getImageCount()
    {
        return $this->image_count;
    }

    /**
     * @param mixed $image_count
     */
    public function setImageCount($image_count)
    {
        $this->image_count = $image_count;
    }


    /**
     * @return mixed
     */
    public function getImage_count()
    {
        return $this->image_count;
    }

    /**
     * @param mixed $image_count
     */
    public function setImage_count($image_count)
    {
        $this->image_count = $image_count;
    }

    /**
     * @return mixed
     */
    public function getTreg()
    {
        return $this->treg;
    }

    /**
     * @param mixed $treg
     */
    public function setTreg($treg)
    {
        $this->treg = $treg;
    }

    /**
     * @return mixed
     */
    public function getFreg()
    {
        return $this->freg;
    }

    /**
     * @param mixed $freg
     */
    public function setFreg($freg)
    {
        $this->freg = $freg;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * @param mixed $middleName
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * @return mixed
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param mixed $territory
     */
    public function setTerritory($territory)
    {
        $this->territory = $territory;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getSimSerial()
    {
        return $this->simSerial;
    }

    /**
     * @param mixed $simSerial
     */
    public function setSimSerial($simSerial)
    {
        $this->simSerial = $simSerial;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getWards()
    {
        return $this->wards;
    }

    /**
     * @param mixed $wards
     */
    public function setWards($wards)
    {
        $this->wards = $wards;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getIdentification()
    {
        return $this->identification;
    }

    /**
     * @param mixed $identification
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
    }

    /**
     * @return mixed
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * @param mixed $nationality
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;
    }

    /**
     * @return mixed
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param mixed $region
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * @return mixed
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param mixed $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return mixed
     */
    public function getEsn()
    {
        return $this->esn;
    }

    /**
     * @param mixed $esn
     */
    public function setEsn($esn)
    {
        $this->esn = $esn;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getRegistrationid()
    {
        return $this->registrationid;
    }

    /**
     * @param mixed $registrationid
     */
    public function setRegistrationid($registrationid)
    {
        $this->registrationid = $registrationid;
    }

    /**
     * @return mixed
     */
    public function getAgentMsisdn()
    {
        return $this->agentMsisdn;
    }

    /**
     * @param mixed $agentMsisdn
     */
    public function setAgentMsisdn($agentMsisdn)
    {
        $this->agentMsisdn = $agentMsisdn;
    }

    /**
     * @return mixed
     */
    public function getAppVersion()
    {
        return $this->appVersion;
    }

    /**
     * @param mixed $appVersion
     */
    public function setAppVersion($appVersion)
    {
        $this->appVersion = $appVersion;
    }

    /**
     * @return mixed
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param mixed $deviceId
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return mixed
     */
    public function getLocationLatLon()
    {
        return $this->locationLatLon;
    }

    /**
     * @param mixed $locationLatLon
     */
    public function setLocationLatLon($locationLatLon)
    {
        $this->locationLatLon = $locationLatLon;
    }

    /**
     * @return mixed
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    /**
     * @param mixed $deviceModel
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;
    }


    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     */
    public function serialize()
    {
        return serialize(
            array(
                $this->id,
            )
        );
    }

    /**
     * (PHP 5 &gt;= 5.1.0)<br/>
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            ) = unserialize($serialized);
    }

    /**
     * @return mixed
     */
    public function getImsi()
    {
        return $this->imsi;
    }

    /**
     * @param mixed $imsi
     */
    public function setImsi($imsi)
    {
        $this->imsi = $imsi;
    }

    /**
     * @return mixed
     */
    public function getRegistrationTime()
    {
        return $this->registrationTime;
    }

    /**
     * @param mixed $registrationTime
     */
    public function setRegistrationTime($registrationTime)
    {
        $this->registrationTime = $registrationTime;
    }

    /**
     * @return mixed
     */
    public function getDeviceSentTime()
    {
        return $this->deviceSentTime;
    }

    /**
     * @param mixed $deviceSentTime
     */
    public function setDeviceSentTime($deviceSentTime)
    {
        $this->deviceSentTime = $deviceSentTime;
    }


    public function __toString()
    {
        return "";
    }

    /**
     * @return mixed
     */
    public function getIsSms()
    {
        return $this->isSms;
    }

    /**
     * @param mixed $isSms
     */
    public function setIsSms($isSms)
    {
        $this->isSms = $isSms;
    }

    /**
     * @return mixed
     */
    public function getIsPorted()
    {
        return $this->isPorted;
    }

    /**
     * @param mixed $isPorted
     */
    public function setIsPorted($isPorted)
    {
        $this->isPorted = $isPorted;
    }

    /**
     * @return mixed
     */
    public function getMnpNetwork()
    {
        return $this->mnpNetwork;
    }

    /**
     * @param mixed $mnpNetwork
     */
    public function setMnpNetwork($mnpNetwork)
    {
        $this->mnpNetwork = $mnpNetwork;
    }


    /**
     * Set the Reg Type
     *
     * @param $type
     */
    public function setRegistrationType($type) {

        $this->registrationType = $type;
    }

    /**
     * Get the reg type
     *
     * @return mixed
     */
    public function getRegistrationType() {

        return $this->registrationType;
    }

    /**
     * @return bool
     */
    public function isNidaRegistration() {

        return $this->registrationType == 2;
    }


    public function getFingerPrintCode() {

        return $this->fingerPrintCode;
    }


    public function setFingerPrintCode($code) {

        $this->fingerPrintCode = $code;
    }




}
