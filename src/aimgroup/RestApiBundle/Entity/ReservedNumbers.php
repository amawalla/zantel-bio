<?php
/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * Description of ReservedNumbers
 *
 * @author Sam Samson
 */

/**
 * @ORM\Table(name="reserved_numbers")
 * @ORM\Entity
 */

class ReservedNumbers {

    const PENDING    = 'pending';
    const CONFIRMED  = 'confirmed';
    const CREATED    = 'created';
    const LINKED     = 'linked'; /* completed -> send to registration system */
    const CANCELED   = 'canceled';

    
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    private $createdAt;


    /**
     * @ORM\Column(name="msisdn", type="string", nullable=false)
     */
    private $msisdn;

    /**
     * @ORM\Column(name="imsi_number", type="string", nullable=true)
     */
    private $imsiNumber;

    /**
     * @return mixed
     */
    public function getImsiNumber() {
        return $this->imsiNumber;
    }

    /**
     * @param mixed $imsiNumber
     */
    public function setImsiNumber($imsiNumber) {
        $this->imsiNumber = $imsiNumber;
    }


    /**
     * @ORM\Column(name="reservation_code", type="string", nullable=false)
     */
    private $reservationCode;


    /**
     * @ORM\Column(name="validity_window", type="integer", nullable=true)
     */
    private $validityWindow;

    /**
     * @ORM\Column(name="status", type="string", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;


    public function __construct() {}

    /**
     * @return mixed
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt() {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getMsisdn() {
        return $this->msisdn;
    }

    /**
     * @param mixed $msisdn
     */
    public function setMsisdn($msisdn) {
        $this->msisdn = $msisdn;
    }

    /**
     * @return mixed
     */
    public function getReservationCode() {
        return $this->reservationCode;
    }

    /**
     * @param mixed $reservationCode
     */
    public function setReservationCode($reservationCode) {
        $this->reservationCode = $reservationCode;
    }

    /**
     * @return mixed
     */
    public function getValidityWindow() {
        return $this->validityWindow;
    }

    /**
     * @param mixed $validityWindow
     */
    public function setValidityWindow($validityWindow) {
        $this->validityWindow = $validityWindow;
    }

    /**
     * @return mixed
     */
    public function getStatus() {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status) {

        if(!in_array( $status, array(self::PENDING, self::CONFIRMED, self::CREATED, self::LINKED, self::CANCELED))) {
            throw new \InvalidArgumentException('Invalid status');
        }

        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt() {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt) {
        $this->updatedAt = $updatedAt;
    }

}
