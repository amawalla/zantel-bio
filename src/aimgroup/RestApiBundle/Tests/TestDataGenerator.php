<?php

/*
 * Copyright (C) AIM Group (T) Limited - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace aimgroup\RestApiBundle\Tests;

use aimgroup\RegistrationBundle\Entity\Registration;
use \aimgroup\RestApiBundle\Entity\Product;
use \aimgroup\RestApiBundle\Entity\Artist;


/**
 * Description of TestDataGenerator
 * This is a class to generate random data. you can use it, improve on it. But dont break it!
 *
 * @author Michael Tarimo
 */
class TestDataGenerator {

    
    public static function generateDummyRegistrations($theKeyword) {
        $registrations = array();

        for($x = 0; $x < 100; $x++) {
            $registration = new Registration();

            $registration->setFirstName(TestDataGenerator::generateRandomSentence($theKeyword));
            $registration->setLastName(TestDataGenerator::generateRandomString(5));
            $registration->setContact(TestDataGenerator::generateRandomSentence($theKeyword));
            $registration->setAddress(TestDataGenerator::generateRandomString(5));
            $registration->setMsisdn(rand(20000, 259890));
            
            array_push($registrations, $registration);
        }
        
        return $registrations;
    }
    
    
    public static function generateDummyArtists($theKeyword) {
        $artists = array();

        for($x = 0; $x < 10; $x++) {
            $artist = new Artist();
        
            $artist->setName(TestDataGenerator::generateRandomString(5).' '.TestDataGenerator::generateRandomString(7));
            $artist->setAkaName(TestDataGenerator::generateRandomString(6));
            $artist->setBio(TestDataGenerator::generateRandomSentence($theKeyword, false));
            
            array_push($artists, $artist);
        }
        
        return $artists;
    }
    
    
    public static function generateRandomString($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function generateRandomSentence($keyword, $short = true) {
        //
        // A list of sentences:
        // 
        // %something ==> is a variable 
        //
        if($short) {
            $r_sentences = '
            %noun by %adjective
            %nouns from album %adjective
            Single ya %adjective ya 2014
            Hot song %nouns feat. %adjective
            %noun vs %adjective in bongo star search
            %adjective ni mtindo wa kisasa
            %noun ni noma from %adjective
            ';
        } else {
            $r_sentences = '
            %adjective (amezaliwa tar. 13 Agosti 1982, katika hospitali ya %noun iliyopo jijini Mwanza) ni msanii wa hip hop na bongo flava kutoka nchini Tanzania. Anafahamika zaidi kwa jina lake la kisanii kama %adjective. %adjective alianza shughuli za muziki mnamo miaka ya 1990
            %adjective , known by her stage name %adjective (stylized as %adjective and at times %adjective) (born June 15, 1979), is a Tanzanian singer. She specializes in the R&B/Zouk/Kwaito genres. %adjective was voted Best Tanzanian Female R&B Artist in 2002, performed at the Kora All Africa Designers Competition, and was awarded "Best R&B Album" at the Tanzania Music Awards on August 6, 2004. In July 2005, she won an award for "best female video for South Africa". She was among the first females to sing R&B in Swahili. She has also recorded with a number of artists including Samba Mapangala (Congo), Chameleone (Uganda), Mad Ice (Uganda / Tanzania), Matonya (Tanzania) and Mina Nawe (South Africa) amongst others.
            %adjective started rapping at young age in Tanzania. In high school, he became popular with his performances during talent shows. He never took it seriously because of pressure from family to pursue other things. It was until he moved to Chicago in pursuit of the American Dream, where his love for Hip Hop was rejuvenated. In a short period, the self proclaimed, “Bilingual Beast” has gone from Freestyling, recording demos, to performing in festivals, opening up for big acts like Ice Prince and 2face Idibia, and finally becoming a seasoned performer. He has evolved from a young child in the streets of Dar es Salaam, to a lyrical giant who sees nothing else than Work Ethic and Dedication as being the keys to success. After years of commitment and dedication to his craft, %adjective is poised to become Tanzania’s leading Ambassador in Africa and in the Music World His signature Blend of English and Swahili rhymes caused him to be dubbed the “Bilingual Beast”, but his ability to tell a story and clever use of wordplay and metaphors, seem to be his real Strength. His ability to create context for the listeners is unparalleled, and he credits that to his personal life experiences as well as his imaginative part of the brain, which is sometimes more vivid than life itself. %adjective believes he has something to prove to first Tanzania, and then the rest of Africa, that he deserves a spot among the Best. With the release of Three Mixtapes (MYU: The Trilogy) in the past year, “Abacus EP” and his debut Album “Kisimani LP” slated to drop soon, %adjective has set the platform with which his dreams could be fulfilled. The sky is the limit for this TZee Emcee. Accomplishments • Performed alongside Davido, Chidinma, Tiwa Savage, Mario Winans, & Praiz in Lagos, Nigeria at the MTN Big Rave Show in February 2014. • The Only Tanzanian Artist to perform at the Industry Nite in Lagos Nigeria. • Performed with Temi Dollface in the Beat99FM Showcase at the Social Media Week Event in Lagos. • Released a highly anticipated EP titled ABACUS in January 2013. • Released his fourth Mixtape titled MYU: The Trinity in October 2013. • Winner of "Best Music Video of The Year" in the AFRO ENTERTAINMENT AWARDS in Chicago for Weekend Video w/ Reezon (Ghana) & Chibbz (Nigeria). • TOUCH was Number 9 on the Top 100 songs of 2013 by TIMES FM. • Nominated for "The Best Video of the Year" at the AFRO ENTERTAINMENT AWARDS in Chicago • Represented Tanzania as the 2013 Artist on the Big Brother Africa. Performed on The Chase Eviction show on July 7th and was watched by over 60 Million viewers. • Dropped 3 Mixtapes in the last 2 years, namely Mixtape Ya Ukweli, MYU2: The Return of The Bilingual Beast, MYU3: Welcome To Heartbreak. • Featured in the song “Weekend” alongside Reezon (Ghana) and Chibbz (Nigeria), a smash hit that is getting airplay across Africa. • Opened Up for the biggest African Acts like Iyanya, J Martins, 2face Idibia, Samklef, Jesse Jagz, Profesa Jay, Mercy Myra, Fuse ODG, Jose Chameleone, Lady Jaydee and Fid Q. • Performed with Ice Prince at the House of Blues during the “United Sounds of Africa” Tour in the United States. • Has been featured in various Tanzanian, Nigerian and Ghanaian Media outlets (Bongo5, TayoTV, GhanaMixtape, Naijamayor) as one of the “Best New Emcees” from Africa. • Wrote and performed in the song “Proud To Be” by AACUSA, a song that promotes African Pride and Unity among Diaspora Artists. • A Part of AACUSA (African Artist Coalition USA), a Diaspora group which features artists from Tanzania, Sierra Leone, Nigeria, Kenya, South Sudan, Zambia & Ghana. • Performed at the REPRESENT AFRICA showcase at the Shrine in Chicago. • Performed at the 2011 Afro Entertainment Awards held in Chicago • Performed at the 2011 Afro Caribbean Festival in Chicago. • Performed at the 2010 African World Festival in Detroit. 
            %adjective musical story began when she was six years old, spending time with a group of friends dancing and singing their way into vocal and dancing competitions and performances in Mombasa Makupa , Kenya. RUKENYA gradually morphed into a professional dancer,one of the most successful female dance choreographer in Kenya. Her ambitions became a joint and worked with Boomba girls , Sarakasi dancers, Galang girls , Sisqo dance Crew , F.B.I Dance crew ,Alqaeeda Dance Crew and Toxic Dancers. In 2009 Toxic Dancers became the first African Dancers to Work With MultiPlatinum Award Winner WYCLEF JEAN and AKON At the 2009 MTV AFRICA MUSIC AWARDS ( MAMA ) Held at the Kasarani Stadium in Nairobi. Following her success with Toxic Dancers , RUKENYA Became a household name in the East Africa Music Industry where she Danced for both local and international artist . It Was a transition into a beautiful artist with the female duo ARIO ARIO and Their eye popping perfomances all over East Africa including THE 2012 BAYIMBA INTERNATIONAL FESTIVAL in Kampala ,UGANDA with their hit songs DEEP IT LOW Fet FILTHY RICH , USIKU HUU and SLOWDOWN. RUKENYA is one of the defining artists of our generation. As a singer, songwriter, performer, actor, Filmmaker, RUKENYA is a creative artist working on her solo album to be celebrated by the world. " Nomatter how many times a song plays you get bored eventually but you never get bored of Listening. Listen" RUKENYA.	
            %adjective, also referred to as Vee Money, is a Tanzanian recording artist, songwriter, youth activist, TV and radio host. Vee Money is popularly known for being the first ever Tanzanian MTV VJ. She was born in Arusha, Tanzania, and first garnered recognition for being the first MTV VJ from Tanzania. She later rose to prominence as a Radio and TV host, hosting Epic Bongo Star Search and DUME CHALLENGE both on ITV Tanzania before signing to BHits Music Group in late 2012. After joining B’Hits Music Group, Vee collaborated with Tanzanian rapper AY on a record ‘’Money’’ and Ommy Dimples, a Bongo Flava artist on a record ‘Me and You’ that was later voted as Tanzania’s song of the year ( 2012- 2013 ) during the Kilimanjaro Music Awards 2013. She received an even bigger buzz with the release of her first solo single Closer, which in its first week was downloaded over 30,000 times. A feat achieved by no other Tanzanian artist. She also managed to remain on the charts with her single closer for over 13 weeks, becoming a breath of fresh air in a male dominated industry. In 2014, %adjective was nominated for 3 Kilimanjaro Tanzania Music Awards for Female Artist of the year, RNB song of the year and Female performer of the year.
            ';
        }
         

        //
        // This is another list of variables:
        // (This list can also contain variables (like %something))
        //
        // Formatting:
        // (first-line) = Variablename
        // (second-line) = Variables (seperated by semicolon)
        //

        $r_variables = '
        adjective
        %adjective_list;ya %adjective_list;by %adjective_list

        adjective_list
        '.$keyword.';'.$keyword.'

        noun
        %noun_list;%adjective %noun_list

        noun_list
        music;makeke ya mjini;diamond;djaro;p square;prezo;bongo flava;malle

        nouns
        beers;monkeys;donkeys;examples;cars;trees;birds;dogs

        love_or_hate
        love;hate;like

        family
        %adjective %family_members;%family_members

        family_members
        grandpa;brother;sister;mom;dad;grandma

        sentence_ending
        .;!;!!;!?;*lol*
        ';

        // strip spaces:
        $r_sentences = trim($r_sentences);
        $r_variables = trim($r_variables);

        // fix new lines and split sentences up:
        $r_sentences = str_replace("\r\n", "\n", $r_sentences);
        $r_sentences = str_replace("\r", "\n", $r_sentences);
        $r_sentences = explode("\n", $r_sentences);

        $r_variables = str_replace("\r\n", "\n", $r_variables);
        $r_variables = str_replace("\r", "\n", $r_variables);
        $r_variables = explode("\n\n", $r_variables);

        // this array contains all variables:
        $r_vars = array();

        // go trough all variables:
        for ($x = 0; $x < count($r_variables); $x++) {

            $var = explode("\n", trim($r_variables[$x]));

            // lowecase all:
            $key = strtolower(trim($var[0]));

            // split words:
            $words = explode(";", trim($var[1]));

            // add variables to the $r_vars Array
            $r_vars[$key] = $words;
        }

        // calc. max.
        $max_s = count($r_sentences) - 1;
        $rand_s = rand(0, $max_s);

        // get a random sentence:
        $sentence = $r_sentences[$rand_s];

        // format the resulting sentence, so that I looks nice:
        // (delete whitespace infront of punctuation marks)
        $sentence = str_replace(' ,', ',', ucfirst(TestDataGenerator::replace_words($sentence, $r_vars)));
        $sentence = str_replace(' .', '.', $sentence);
        $sentence = str_replace(' !', '!', $sentence);
        $sentence = str_replace(' ?', '?', $sentence);
        $sentence = trim($sentence);

        // finally print the new sentence! :-D
        return $sentence;
    }

    /**
     * returns a word from the variables array:
     * 
     * @param type $key
     * @param type $r_vars
     * @return type 
     */
    private static function get_word($key, $r_vars) {
        //global $r_vars;

        if (isset($r_vars[$key])) {

            $words = $r_vars[$key];

            // calc max.
            $w_max = count($words) - 1;
            $w_rand = rand(0, $w_max);

            // return the word, and check if the word contains
            // another variable:
            return TestDataGenerator::replace_words(trim($words[$w_rand]), $r_vars);
        } else {
            // the word was not found :-(
            return "(Error: Word '$key' was not found!)";
        }
    }

    
    /**
     * 
     * This function replaces a variable like %something with the proper variable-value
     * 
     * @param type $sentence
     * @param type $r_vars
     * @return type
     */
    private static function replace_words($sentence, $r_vars) {

        // if there are no variables in the sentence,
        // return it without doing anything
        if (str_replace('%', '', $sentence) == $sentence)
            return $sentence;

        // split the words up:
        $words = explode(" ", $sentence);

        $new_sentence = array();

        // go trough all words:
        for ($w = 0; $w < count($words); $w++) {

            $word = trim($words[$w]);

            if ($word != '') {

                // is this word a variable?
                if (preg_match('/^%(.*)$/', $word, $m)) {

                    // --> yes
                    $varkey = trim($m[1]);

                    // get the proper word from the variable list:
                    $new_sentence[] = TestDataGenerator::get_word($varkey, $r_vars);
                } else {
                    // --> no it is a default word
                    $new_sentence[] = $word;
                }
            }
        }

        // join the array to a new sentence:
        return implode(" ", $new_sentence);
    }
}
