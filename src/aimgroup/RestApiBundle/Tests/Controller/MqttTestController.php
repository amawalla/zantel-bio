<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 18/04/2016
 * Time: 12:17 PM
 */

namespace aimgroup\RestApiBundle\Tests\Controller;


use aimgroup\DashboardBundle\Dao\JsonObject;
use aimgroup\RestApiBundle\Dao\RegStatusEnum;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use aimgroup\RestApiBundle\EventListener\PlainTextEventListener;
use Doctrine\DBAL\Exception\NotNullConstraintViolationException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use League\Flysystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Stopwatch\Stopwatch;

class MqttTestController extends KernelTestCase
{

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;
    private $TAG = "test";

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
    }


    /**
     * phpunit -c app/ --filter="testRegistration" src/aimgroup/RestApiBundle/Tests/Controller/MqttTestController.php
     */
    public function testRegistration()
    {
        $request = '{"deviceId":"1462266227769","loc":"-6.74468053,39.28011448","region":"CS","simserial":"892550204127785192","langCode":"sw","deviceSentTime":"2016-05-03 12:40:09","gender":"M","id-type":"1","customer-confirmation":"","appVersion":"2.0.8","imei":"357226062205836","dateofbirth":"29-08-1988","territory":"106","surname":"MAKALA","osVersion":"SDK 23","deviceModel":"HTCHTC_M9u","msisdn":"713522107","registrationId":"1462268147420713565370","address":"","confirm":"","email":"","firstname":"OSCAR","reRegistrationFlag":"0","id-number":"T100108350851","nationality":"Tanzania","esn":"","alternative-contact":"0715190268","username":"713565370","registeredTime":"2016-05-03 12:40:09","ward":"MBEZI JUU","middlename":"JOHN"}';
        $stopwatch = new Stopwatch();
        $stopwatch->start('registerCustomerAction');
        $apiHelper = $this->container->get("api.helper");
        $resp = new JsonObject();
        $resultCode = RegStatusEnum::FAIL;

        $status = false;
        $message = "SYSTEM ERROR";

        $apiHelper->logInfo($this->TAG, "registerCustomerAction", array("result" => $request));
        $attributes = json_decode($request, true);

        $lang = $apiHelper->getLanguage(isset($attributes["langCode"]) ? $attributes["langCode"] : "en");
        $apiHelper->logInfo($this->TAG, "registerCustomerAction-start", array('req' => $attributes));
        $em = $this->container->get("doctrine")->getManager();
        try {


            $registration = new Registration();

            $registration->setAddress($attributes["address"]);
            $registration->setContact($apiHelper->formatMsisdn($attributes["alternative-contact"]));
            $registration->setDob($attributes["dateofbirth"]);
            $registration->setEmail($attributes["email"]);
            $registration->setEsn($attributes["esn"]);
            $registration->setSimSerial($attributes["simserial"]);
            $registration->setFirstName($attributes["firstname"]);
            $registration->setLastName($attributes["surname"]);
            $registration->setMiddleName($attributes["middlename"]);
            $registration->setGender($attributes["gender"]);
            $registration->setNationality($attributes["nationality"]);
            $registration->setWards($attributes["ward"]);
            $registration->setTerritory($attributes["territory"]);
            $registration->setRegion($attributes["region"]);
            $registration->setMsisdn($apiHelper->formatMsisdn($attributes["msisdn"]));
            $registration->setRegistrationid($attributes["registrationId"]);
            $registration->setIdentificationType($attributes["id-type"]);
            $registration->setIdentification($attributes["id-number"]);
            $registration->setAgentMsisdn($attributes["username"]);
            $registration->setAppVersion($attributes["appVersion"]);
            $registration->setDeviceId($attributes["deviceId"]);
            $registration->setLocationLatLon($attributes["loc"]);
            $registration->setDeviceModel(isset($attributes["deviceModel"]) ? $attributes["deviceModel"] : "");
            $registration->setAgentImei($attributes["imei"]);
            $registration->setRegistrationTime(new \DateTime($attributes["registeredTime"]));
            $registration->setDeviceSentTime(new \DateTime($attributes["deviceSentTime"]));

            $count = $this->container->get("fos_elastica.manager")
                ->getRepository('RestApiBundle:Registration')
                ->findCountByMsisdnAndSerial($registration->getMsisdn(), $registration->getSimSerial());

            if ($count > 0) {
                $resultCode = RegStatusEnum::DUPLICATE;
                $message = $this->container->get('translator')->trans('register.failed.duplicate', array(), 'messages', $lang);
            } else {

                /** @var Device $registeringAgent */
                $registeringAgent = $em->getRepository('RestApiBundle:Device')->findOneBy(array("msisdn" => $apiHelper->formatMsisdn($attributes["username"])));
                if (!$registeringAgent) {
                    $message = $this->container->get('translator')->trans('register.agent.not.exist', array(), 'messages', $lang);
                    $resultCode = RegStatusEnum::AGENT_NOT_EXIST;
                } else {
                    $registration->setOwner($registeringAgent->getUser());
                    if ($attributes["appVersion"] > $registeringAgent->getAppVersion()) {
                        $registeringAgent->setAppVersion($attributes["appVersion"]);
                        $em->flush();
                    }
                    $occuranceCount = $this->container->get("fos_elastica.manager")->getRepository('RestApiBundle:Registration')->findCountByMsisdn($registration->getMsisdn());
                    $registration->setMsisdnOccuranceCount($occuranceCount);
                    dump($registration);
                    exit;
                    $em->persist($registration);
                    $em->flush();
                    $registrationStatus = new RegistrationStatus();
                    $registrationStatus->setTemporaryRegStatus(0);
                    $registrationStatus->setFullRegStatus(0);
                    $registrationStatus->setFullRegDesc("");
                    $registrationStatus->setIcapState(8);
                    $registrationStatus->setRegistrationId($registration);

                    $em->persist($registrationStatus);
                    $em->flush();

                    $status = true;
                    $message = "SUCCESS";
                    $resultCode = RegStatusEnum::SUCCESS;
                }
            }

        } catch (UniqueConstraintViolationException $e) {
            $resultCode = RegStatusEnum::DUPLICATE;
            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
        } catch (NotNullConstraintViolationException $e) {
            $apiHelper->logE($this->TAG, "registerCustomerAcction.NotNullConstraintViolationException", $e);
            $resultCode = RegStatusEnum::FAIL;
            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
        } catch (\Exception $e) {
            dump($e->getMessage());
            $apiHelper->logE($this->TAG, "registerCustomerAcction", $e);
            $message = $this->get('translator')->trans('register.failed', array(), 'messages', $lang);
            $resultCode = RegStatusEnum::FAIL;
        }
        $resp->setResultCode($resultCode);
        $resp->setMessage($message);
        $resp->setStatus($status);


        dump($resp);
        $event = $stopwatch->stop('registerCustomerAction');
        $apiHelper->logInfo($this->TAG, "registerCustomerAction-end", array('time' => $event->getDuration(), 'resp' => (array)$resp));

    }

    public function testS3()
    {

        $fileName = "oscarmakala_potrait_.jpeg";
        $remoteFilePath = "20160911" . DIRECTORY_SEPARATOR . $fileName;

        $localImageFilePath = "/Users/oscarmakala/Desktop/oo.png";

        $webPath = '';

        //Upload the file to s3
        /** @var  $filesystem Filesystem */
        $filesystem = $this->container->get('s3_filesystem');
        $result = $filesystem->has($remoteFilePath);
        dump($filesystem->getSize($remoteFilePath));

        $uploaded = $filesystem->write($remoteFilePath, file_get_contents($localImageFilePath));
        if ($uploaded) {
            //Allow the file to be accessible publicly
            $filesystem->setVisibility($remoteFilePath, 'public');
            $webPath = "https://s3.amazonaws.com/ereg2/tigo/images/{$remoteFilePath}";
            dump($webPath);
        }


    }

    public function testNotifyRegistrationStatus()
    {


    }

    public function testICCIDAddition()
    {
        $idWithoutCheckdigit = "892550204134834804";
        $sum = 0;
        $length = strlen($idWithoutCheckdigit);
        for ($i = 0; $i < $length; $i++) {
            $char = substr($idWithoutCheckdigit, $length - $i - 1, 1);
            if ($i % 2 == 0) {
                $weight = (2 * $char) - (int)($char / 5) * 9;
            } else {
                $weight = $char;
            }
            $sum += $weight;
        }
        $sum = abs($sum) + 10;
        $lastDigit = (10 - ($sum % 10)) % 10;
        dump($lastDigit);
    }

    public function testNNIMQueries()
    {
        $number = '{"1460647861980":"715787878","1460816913490":"656684834"}';
        $jsons = json_decode($number, true);
        $idArray = array();
        foreach ($jsons as $key => $value) {
            array_push($idArray, $key);
        }
        $registrations = $this->em->createQuery(
            "SELECT r.registrationid as registrationId,s.fullRegStatus as status,r.msisdn,r.language FROM RestApiBundle:Registration r INNER JOIN RestApiBundle:RegistrationStatus s
             where r.id = s.registrationId and r.registrationid in (:id)"
        )
            ->setParameter("id", $idArray)
            ->getArrayResult();

        if ($registrations) {
            $lang = "sw";
            $output = array();
            foreach ($registrations as $registration) {
                $failedMsg = $this->container->get('translator')->trans(
                    'register.status.successful',
                    array('%msisdn%' => $registration["msisdn"]),
                    'messages',
                    ($registration["language"] == null) ? $lang : $registration->getLanguage()
                );
                $successMsg = $this->container->get('translator')->trans(
                    'register.status.successful',
                    array('%msisdn%' => $registration["msisdn"]),
                    'messages',
                    ($registration["language"] == null) ? $lang : $registration->getLanguage()
                );

                if ($registration["status"] == 1) {
                    $registration["message"] = $successMsg;
                } else {
                    $registration["message"] = $failedMsg;
                }
                array_push($output, $registration);
            }
        }

        dump($output);
        dump(json_encode($output));
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }

}