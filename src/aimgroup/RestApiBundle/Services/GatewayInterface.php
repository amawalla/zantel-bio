<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 22/09/2016
 * Time: 03:12
 */

namespace aimgroup\RestApiBundle\Services;

use aimgroup\RestApiBundle\Dao\GatewayRequest;
use aimgroup\RestApiBundle\Entity\Registration;

interface GatewayInterface {

    /**
     * queryCustomerInformation() - Get Customer Information
     *
     * @param Registration $registration
     *
     * @return mixed
     */
    public function queryCustomerInformation(Registration $registration);

    /**
     * processPlainText() - Process customer plain text information
     *
     * @param Registration $registration
     *
     * @return mixed
     */
    public function processPlainText(GatewayRequest $tigoGatewayRequest, $isNnimRegistration = false, $reservationCode = null);

    /**
     * processImages() - Process customer image information
     *
     * @param Registration $registration
     * @param array $images
     *
     * @return mixed
     */
    public function processImages(GatewayRequest $tigoGatewayRequest);

    /**
     * checkRegistrationStatus() - Check the current status of the registration
     *
     * @param Registration $registration
     *
     * @return mixed
     */
    public function checkRegistrationStatus(Registration $registration);

}