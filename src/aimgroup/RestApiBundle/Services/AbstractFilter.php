<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 11:40 AM
 */

namespace aimgroup\RestApiBundle\Services;


use aimgroup\DashboardBundle\Entity\Idtype;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Services\Filter\DateOfBirthFilter;
use aimgroup\RestApiBundle\Services\Filter\FilterProcessor;
use aimgroup\RestApiBundle\Services\Filter\IdentityCardFilter;
use aimgroup\RestApiBundle\Services\Filter\IDUsageFilter;
use aimgroup\RestApiBundle\Services\Filter\MobileNumberFilter;
use aimgroup\RestApiBundle\Services\Filter\PersonNameFilter;
use aimgroup\RestApiBundle\Utils\UtilityHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractFilter implements RegistrationFilterInterface
{

    protected $container;
    /** @var  UtilityHelper */
    protected $utilityHelper;
    protected $errors = array();
    protected $reasonCode = array();

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->utilityHelper = $this->container->get('utility.helper');

    }

    public function filter(Registration $registration, Idtype $idtype)
    {

        $em = $this->container->get('doctrine')->getManager();

        $processor = new FilterProcessor();
       
	 // Check if the field is null ( for biometric )  
	$dateOfBirth = !is_null($registration->getDob()) ? \DateTime::createFromFormat('d-m-Y', $registration->getDob()) : null;

        $client = $this->container->get('guzzle.client.zantel_gateway');

        $this->errors = $processor->process([
            !is_null($dateOfBirth) ? new DateOfBirthFilter($dateOfBirth) : null,
            new IdentityCardFilter($idtype, $registration->getIdentification()),
            new IDUsageFilter($em, $registration),
           !$registration->isNidaRegistration() ? new PersonNameFilter($registration) : null,
            new MobileNumberFilter($client, $registration->getMsisdn())
        ]);

        return count($this->errors) === 0;

    }
}
