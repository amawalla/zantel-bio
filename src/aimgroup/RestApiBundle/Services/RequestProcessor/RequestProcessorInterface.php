<?php
/**
 * Created by PhpStorm.
 * User: ssamson
 * Date: 21/11/2016
 * Time: 10:04
 */

namespace aimgroup\RestApiBundle\Services\RequestProcessor;

interface RequestProcessorInterface
{
    public function process();
}