<?php


namespace aimgroup\RestApiBundle\Services\RequestProcessor;


use aimgroup\RestApiBundle\Entity\Registration;

class Customer {

    private $registration;

    private $images = array();

    /**
     * Customer constructor.
     *
     * @param Registration $registration
     */
    public function __construct(Registration $registration)
    {
        $this->registration = $registration;
    }

    public function toJson() {

        $territory = $this->registration->getTerritory();

        $parameters = array (
            'record_id' => $this->registration->getId(),
            'msisdn' => "255{$this->registration->getMsisdn()}",
            'email' => $this->registration->getEmail(),
            'first_name' => $this->registration->getFirstName(),
            'middle_name' => $this->registration->getMiddleName(),
            'last_name' => $this->registration->getLastName(),
            'dob' => $this->registration->getDob(),
            'id_type' => $this->registration->getIdentificationType()->getName(),
            'id_number' => $this->registration->getIdentification(),
            'nationality' => $this->registration->getNationality(),
            'region' => $territory->getRegion()->getName(),
            'district' => $territory->getName(),
            'address' => $this->registration->getAddress(),
            'iccid' => $this->registration->getSimSerial(),
            'gender' => (strtoupper($this->registration->getGender()) == 'M' ? 'Male' : 'Female'),
            'password' => $this->registration->getOwner()->getAgentCode(),
            'retailer_msisdn' => $this->registration->getAgentMsisdn(),
            'alternative_number' => $this->registration->getContact()
        );


        if( $this->getImages() != null) {
            $parameters += $this->getImages();
        }


        return \GuzzleHttp\json_encode($parameters);
    }

    /**
     * @return Registration
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param Registration $registration
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param array $images
     */
    public function setImages($images)
    {
        $this->images = $images;
    }

}