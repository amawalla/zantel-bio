<?php


namespace aimgroup\RestApiBundle\Services\RequestProcessor;


use aimgroup\RestApiBundle\Dao\GatewayRequest;
use aimgroup\RestApiBundle\Dao\OperatorResponse;
use aimgroup\RestApiBundle\Entity\RegImages;
use aimgroup\RestApiBundle\Entity\Registration;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ImagesRequestProcessor implements RequestProcessorInterface
{

    /** @var  Registration */
    private $registration;

    private $registrationImages;
    /**
     * @var ContainerInterface
     */
    private $container;
    /**
     * @var GatewayRequest
     */
    private $gatewayRequest;

    /**
     * ImagesRequestProcessor constructor.
     *
     * @param GatewayRequest $gatewayRequest
     * @param ContainerInterface $container
     *
     */
    public function __construct(GatewayRequest $gatewayRequest, ContainerInterface $container)
    {
        $this->container = $container;
        $this->gatewayRequest = $gatewayRequest;
    }

    /**
     * @return OperatorResponse|bool
     * @throws \Exception
     */
    public function process()
    {
        try {

            $client = $this->container->get('guzzle.client.zantel_gateway');
            
            $requestJson = \GuzzleHttp\json_encode($this->gatewayRequest->getFullRegistrationRequest());

            $response = $client->request(
                'POST',
                $this->container->getParameter('zantel.gateway.endpoint.submit_images'),
                [
                    'multipart' => [
                        [
                            'name'     => 'customer',
                            'contents' => $requestJson,
                        ],
                    ],
                ]
            );
        
        //var_dump($requestJson);
	    $respContent = $response->getBody()->getContents();	
	    echo " \n /n processing images picha..vardumping below..";
	    $responseObject = \GuzzleHttp\json_decode($respContent);
        var_dump($requestJson);exit;
            ###$responseObject = \GuzzleHttp\json_decode($response->getBody()->getContents());

            if ($responseObject) {

                $operatorResponse = new OperatorResponse($this->gatewayRequest->getRegistration());
                $operatorResponse->setStatusCode($response->getStatusCode());
                $operatorResponse->setErrorCode($responseObject->code);
                $operatorResponse->setErrorMessage($responseObject->message);
                $operatorResponse->setReferenceId($responseObject->referenceId);

                return $operatorResponse;
            }

            return false;


        } catch (\Exception $exception) {
            throw $exception;
        }
    }

}
