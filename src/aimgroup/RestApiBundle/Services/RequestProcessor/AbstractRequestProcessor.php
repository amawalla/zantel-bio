<?php


namespace aimgroup\RestApiBundle\Services\RequestProcessor;


use aimgroup\RestApiBundle\Entity\Registration;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AbstractRequestProcessor implements RequestProcessorInterface {

    protected $urlEndpoint;

    private $username;

    private $password;

    public function process() {
        return $this->processRequest();
    }

    protected abstract function processRequest();

    /**
     * @param string $endpointUrl
     * @param array $parameters
     * @param array $headers
     *
     * @return array
     */
    protected function processPostHttpRequest($endpointUrl = null, $parameters = array(), $headers = array('Accept' => 'application/json'))
    {

        if (is_null($endpointUrl) || empty($parameters)) {
            throw new \InvalidArgumentException('AbstractRequestProcessor::sendPostHttpRequest() expects $url and array $parameters ');
        }

        try {

            $fieldsParameters = array();

            foreach ($parameters as $name => $value) {
                $fieldsParameters[] = array(
                    'name'     => $name,
                    'contents' => $value
                );
            }

            $requestParameters = array(
                'multipart' => $fieldsParameters,
                'headers'   => $headers
            );

            //Set basic authentication when needed
            if(isset($this->username) && isset($this->password)) {
                $requestParameters['auth'] = array($this->username, $this->password);
            }

            //Send the request
            $response = (new Client())->request('POST', $endpointUrl, $requestParameters);

            return array(
                'statusCode' => $response->getStatusCode(),
                'responseContent'    => $response->getBody()->getContents()
            );

        }
        catch (RequestException $requestException) {
            throw $requestException;
        }

    }

    /**
     * @param $url
     *
     * @return void
     */
    public function setEndpointUrl($url) {
        $this->urlEndpoint = $url;
    }

    /**
     * @return string
     */
    public function getEndpointUrl() {
        return $this->urlEndpoint;
    }

    public function setBasicAuthUsername($username) {
        $this->username = $username;
    }

    public function setBasicAuthPassword($username) {
        $this->password = $username;
    }


}