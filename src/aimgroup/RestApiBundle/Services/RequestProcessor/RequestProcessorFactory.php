<?php


namespace aimgroup\RestApiBundle\Services\RequestProcessor;


use aimgroup\RestApiBundle\Dao\OperatorResponse;

class RequestProcessorFactory {

    /**
     * @param RequestProcessorInterface $requestProcessor
     *
     * @return OperatorResponse
     */
    public static function process(RequestProcessorInterface $requestProcessor) {
        return $requestProcessor->process();
    }

}