<?php


namespace aimgroup\RestApiBundle\Services\RequestProcessor;


use aimgroup\RestApiBundle\Dao\GatewayRequest;
use aimgroup\RestApiBundle\Dao\OperatorResponse;
use aimgroup\RestApiBundle\Entity\Registration;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PlainTextRequestProcessor implements RequestProcessorInterface
{

    /**
     * @var Registration
     */
    private $registration;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * PlainTextRequestProcessor constructor.
     *
     * @param Registration $registration
     * @param ContainerInterface $container
     *
     */
    public function __construct(Registration $registration, ContainerInterface $container)
    {
        $this->registration = $registration;
        $this->container = $container;
    }


    /**
     * @return OperatorResponse|bool
     * @throws \Exception
     */
    public function process()
    {
        try {

            $client = $this->container->get('guzzle.client.zantel_gateway');

	
            // Get Territory
            $territory = $this->container->get('doctrine')->getRepository('RestApiBundle:Registration')->getTheTerritory($this->registration);

            $gatewayRequest = new GatewayRequest($this->registration, $territory);


            $response = $client->post('/ZantelGateway/rest/services/zantelGateway/registerCustomer',
                [
                    'multipart' => [
                        [
                            'name'     => 'customer',
                            'contents' => \GuzzleHttp\json_encode($gatewayRequest->getTemporaryRequest()),
                        ],
                    ],
                ]
            );
            
            \GuzzleHttp\json_encode($gatewayRequest->getTemporaryRequest());

            echo " /n /n :::: " . json_encode($gatewayRequest->getTemporaryRequest()) . "  ::::: /n /n ";
            echo " process sent reading resp... ";
            
            
            
            $repoJson = $response->getBody()->getContents();
            var_dump($repoJson);
            
            
            $responseObject = \GuzzleHttp\json_decode($repoJson);
            
            echo " \n /n vardump responseObject: ";
            var_dump($responseObject);
            
            //echo " \n respo:: " . $responseObject;

	       // $postedJson = \GuzzleHttp\json_encode($gatewayRequest->getTemporaryRequest());
            
            $apiHelper = $this->container->get('api.helper');
            //$apiHelper->logInfo("plaintextRequestProcessor", "gateway post request", array('posted' => $postedJson));
	    $apiHelper->logInfo("plaintextRequestProcessor_gateway_reponse", "gateway response Object", array('GatewayResponse' => $responseObject));

            if ($responseObject) {
                echo " Gotten responseObject ";
                $operatorResponse = new OperatorResponse($this->registration);
                $operatorResponse->setStatusCode($response->getStatusCode());
                $operatorResponse->setErrorCode($responseObject->code);
                $operatorResponse->setErrorMessage($responseObject->message);

                if (isset($responseObject->referenceId)) {
                    $operatorResponse->setReferenceId($responseObject->referenceId);
                }

                return $operatorResponse;
            }else{
                echo " NOT Gotten responseObject ";
            }

            return false;


        } catch (\Exception $exception) {
            throw $exception;
        }
    }


}
