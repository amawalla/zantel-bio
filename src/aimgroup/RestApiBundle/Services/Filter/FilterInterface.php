<?php

namespace aimgroup\RestApiBundle\Services\Filter;


interface FilterInterface
{

    /**
     * @return bool
     */
    public function validate();

    /**
     * @return array
     */
    public function getErrors();

    /**
     * @param string $message
     *
     * @return void
     */
    public function setError($message);
}