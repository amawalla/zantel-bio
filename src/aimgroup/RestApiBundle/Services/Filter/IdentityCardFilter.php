<?php


namespace aimgroup\RestApiBundle\Services\Filter;


use aimgroup\DashboardBundle\Entity\Idtype;

class IdentityCardFilter extends AbstractFilter
{

    const MESSAGE_INACTIVE        = 'Inactive ID Type';
    const MESSAGE_WRONG_ID_NUMBER = 'Inactive ID Type';

    /**
     * @var Idtype
     */
    private $idtype;

    /**
     * @var
     */
    private $identification;

    /**
     * IdentityCardFilter constructor.
     *
     * @param Idtype $idtype
     * @param $identification
     */
    public function __construct(Idtype $idtype, $identification)
    {
        $this->idtype = $idtype;
        $this->identification = $identification;
    }

    public function validate()
    {
        if (!$this->idtype->getIsActive()) {
            $this->setError(self::MESSAGE_INACTIVE);

            return false;
        }

        /*if ('' !== $this->idtype->getRegexString() && 0 === preg_match($this->idtype->getRegexString(), $this->identification)) {
            $this->setError(self::MESSAGE_WRONG_ID_NUMBER);

            return false;
        }*/

        return true;
    }
}