<?php

namespace aimgroup\RestApiBundle\Services\Filter;

abstract class AbstractFilter implements FilterInterface
{

    /**
     * @var array
     */
    private $errors;

    /**
     * {@inheritdoc}
     */
    abstract public function validate();

    /**
     * {@inheritdoc}
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * {@inheritdoc}
     */
    public function setError($message)
    {
        if (null === $message) {
            return;
        }

        $this->errors[] = $message;
    }

}