<?php


namespace aimgroup\RestApiBundle\Services\Filter;


use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;

class IDUsageFilter extends AbstractFilter
{

    const MAXIMUM_ALLOWED_USAGE = 4;

    const MESSAGE_ID_USAGE_LIMIT = 'validation.invalid.maximum_usage_same_id_number';

    /**
     * @var EntityManager
     */
    private $manager;
    /**
     * @var Registration
     */
    private $registration;

    /**
     * IdUsageFilter constructor.
     *
     * @param ObjectManager|EntityManager $manager
     * @param Registration $registration
     */
    public function __construct(EntityManager $manager, Registration $registration)
    {
        $this->manager = $manager;
        $this->registration = $registration;
    }

    /**
     * {@inheritdoc}
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function validate()
    {
        $queryBuilder = $this->manager->createQueryBuilder()
            ->select('count(status.id)')
            ->from(RegistrationStatus::class, 'status')
            ->leftJoin('status.registrationId', 'registration')
            ->where('status.temporaryRegStatus IN(:temporary_Status)')
            ->andWhere('registration.identification = :identification')
            ->setParameters([
                'temporary_Status' => array(1,6),
                'identification'   => $this->registration->getIdentification()
            ]);

        $count = $queryBuilder->getQuery()->getSingleScalarResult();


        if ($count >= self::MAXIMUM_ALLOWED_USAGE) {

            $this->setError(self::MESSAGE_ID_USAGE_LIMIT);

            return false;
        }

        return true;
    }
}
