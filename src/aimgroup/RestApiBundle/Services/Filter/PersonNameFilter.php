<?php


namespace aimgroup\RestApiBundle\Services\Filter;


use aimgroup\RestApiBundle\Entity\Registration;

class PersonNameFilter extends AbstractFilter
{

    const REGEX = '/(.)\1{2}/';

    const ERROR_MESSAGE = 'validation.invalid.name';

    /**
     * @var Registration
     */
    private $registration;

    /**
     * NameFilter constructor.
     *
     * @param Registration $registration
     */
    public function __construct(Registration $registration)
    {

        $this->registration = $registration;
    }


    public function validate()
    {
        if (null === $this->registration->getFirstName() || null === $this->registration->getLastName()) {
            $this->setError(self::ERROR_MESSAGE);

            return false;
        }

        if (0 !== preg_match(self::REGEX, $this->registration->getFirstName()) || 0 !== preg_match(self::REGEX, $this->registration->getLastName())) {
            $this->setError(self::ERROR_MESSAGE);

            return false;
        }

        return true;
    }

}