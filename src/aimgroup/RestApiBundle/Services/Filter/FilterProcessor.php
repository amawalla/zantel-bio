<?php


namespace aimgroup\RestApiBundle\Services\Filter;

class FilterProcessor
{

    /**
     * @var array
     */
    private $errors = array();

    /**
     * @param FilterInterface[] $filters
     *
     * @return array
     */
    public function process(array $filters)
    {
        foreach ($filters as $index => $filter) {
          if (!is_null($filter)) {
            $valid = $filter instanceof FilterInterface && $filter->validate();

            if (!$valid) {
                foreach ($filter->getErrors() as $key => $error) {
                    $this->errors[] = $error;

                }
            }
        }
    }

        return $this->errors;
    }

}
