<?php
/**
 * Created by PhpStorm.
 * User: oscar
 * Date: 12/14/15
 * Time: 1:11 PM
 */

namespace aimgroup\RestApiBundle\Services;


use aimgroup\RestApiBundle\Dao\OperatorResponse;
use aimgroup\RestApiBundle\Entity\Registration;

interface EndpointInterface
{

    /**
     * @param Registration $registration
     *
     * @return mixed
     */
    public function processText(Registration $registration);

    /**
     * @param Registration $registration
     * @param array $images
     *
     * @return mixed
     */
    public function processImages(Registration $registration, $images);

    /**
     * @param OperatorResponse $operatorResponse
     * @return bool
     */
    public function processPlainTextOperatorResponse(OperatorResponse $operatorResponse);

    /**
     * @param OperatorResponse $operatorResponse
     * @return bool
     */
    public function processImagesOperatorResponse(OperatorResponse $operatorResponse);

}