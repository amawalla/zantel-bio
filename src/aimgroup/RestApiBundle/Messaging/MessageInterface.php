<?php


namespace aimgroup\RestApiBundle\Messaging;


interface MessageInterface
{

    /**
     * @param string $recipient
     * @param string $message
     *
     * @return mixed
     */
    public function send($recipient, $message);
}