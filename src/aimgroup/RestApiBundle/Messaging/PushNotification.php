<?php


namespace aimgroup\RestApiBundle\Messaging;


use GuzzleHttp\Client;

class PushNotification implements MessageInterface
{

    /**
     * @var Client
     */
    private $client;

    /**
     * PushNotification constructor.
     *
     * @param Client $client
     */
    public function __construct(Client $client)
    {

        $this->client = $client;
    }


    /**
     * @inheritdoc
     *
     * @return mixed
     */
    public function send($topic, $message)
    {
        $response = $this->client->post('/MQTTService/rest/services/mqttservice/publishMessage', [
            'multipart' => [
                [
                    'name'     => 'topic',
                    'contents' => $topic,
                ],
                [
                    'name'     => 'message',
                    'contents' => $message,
                ],
            ],
        ]);

        return $response->getStatusCode() == 200;
    }
}