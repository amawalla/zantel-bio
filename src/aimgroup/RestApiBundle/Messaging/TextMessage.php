<?php


namespace aimgroup\RestApiBundle\Messaging;


use GuzzleHttp\Client;

class TextMessage implements MessageInterface
{

    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $operator;


    /**
     * TextMessages constructor.
     *
     * @param Client $client
     * @param $operator
     */
    public function __construct(Client $client, $operator)
    {
        $this->client = $client;
        $this->operator = $operator;
    }

    /**
     * @inheritdoc
     *
     * @return bool
     */
    public function send($recipient, $message)
    {

        $response = $this->client->get( 'ereg.php', [
            'query' => [
                'mobile' => $recipient,
                'msg' => $message,
                'operator' => $this->operator
            ]
        ]);

        if ($response->getStatusCode() == 200) {

            return strpos($response->getBody()->getContents(), 'Accepted for delivery') !== false;

        }

        return false;
    }
}