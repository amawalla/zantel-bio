<?php

namespace aimgroup\VerificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use aimgroup\DashboardBundle\Controller\AbstractController;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStates;
use aimgroup\RestApiBundle\Dao\Message;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Verify controller.
 *
 * @Route("/verify")
 */
class VerifyController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }
    
    
    
    /**
     * View Reports
     *
     * @Route("/verifyer", name="verifyer")
     * @Method("GET")
     * @Template("VerificationBundle:Verify:index_verifyer.html.twig")
     */
    public function verifyerAction() {

        //var_dump(json_decode($this->session->get('user_role_perms')));
        if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $where = "";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND a.parent_id = " . $user->getId();
        }

        $data = array(
            'title' => "Index",
            'title_descr' => "View Frequently Asked Questions",
            'ddate' => date('Y-m-d'),
            'akuku' => json_encode($user->getRoles()) . " | " . $this->session->get('user_role_perms'),
            'date_today' => date('Y-m-d'),
            'date_start' => date_format(date_sub(date_create(date('Y-m-d')), date_interval_create_from_date_string("10 days")), "Y-m-d")
        );

        return $this->prepareResponse($data);
    }
    
    /**
     * View Reports
     *
     * @Route("/indexverify", name="indexverify")
     * @Method("GET")
     * @Template("VerificationBundle:Verify:index.html.twig")
     */
    public function indexverifyAction() {

        if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'newverifierFieldList' ")
                ->getQuery();

        /** @var  $configMaster ConfigMaster */
        $configMaster = $query->getOneOrNullResult();

        $allconfig = null;

        if ($configMaster) {
            $allconfig = json_decode($configMaster->getConfig(), true);

            $numRows = 1;
            $configArray = explode(":::", $allconfig['queryString']);
            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }
        //--

        $data = array(
            'title' => "New Verification Page",
            'title_descr' => "Verify registration",
            'idtype' => $idtype,
            'numRows' => $numRows,
            'verifyArray' => $allconfig ? json_encode($allconfig['validations']) : json_encode(array()),
            'secsCount' => $secsCount,
            'user_id' => $user->getId(),
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }
    
    
    
    /**
     * View Reports
     *
     * @Route("/editnewverification", name="editnewverification")
     * @Method("GET")
     * @Template("VerificationBundle:Verify:newverification_edit.html.twig")
     */
    public function newverificationeditAction() {

        if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $em = $this->getDoctrine()->getManager();
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();

        $user = $this->get('security.token_storage')->getToken()->getUser();

        //--
        $configArray = array();
        $numRows = 1;
        $secsCount = 40;
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'newverifierFieldList' ")
                ->getQuery();

        /** @var  $configMaster ConfigMaster */
        $configMaster = $query->getOneOrNullResult();

        $allconfig = null;

        if ($configMaster) {
            $allconfig = json_decode($configMaster->getConfig(), true);

            //        echo "<p>**** * dbstring * ****</p><pre><small><small><small>";
            //        print_r($allconfig['validations']);
            //        echo "</small></small></small></pre><p>**** * /dbstring * ****</p>";


            $numRows = 1;
            $configArray = explode(":::", $allconfig['queryString']);
            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }
        //--

        $data = array(
            'title' => "Edit Verification Page",
            'title_descr' => "Edit Verified registration",
            'idtype' => $idtype,
            'numRows' => $numRows,
            'verifyArray' => $allconfig ? json_encode($allconfig['validations']) : json_encode(array()),
            'secsCount' => $secsCount,
            'user_id' => $user->getId(),
            'id' => $user->getId(),
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }


    function getRecordForVerification($verifyState) {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->beginTransaction();

        $where3 = "";
        if ($verifyState == 2) {
            $vstate = 2;
        }

        $where3 .= " AND registry_type = 1 AND createdDate >= '2016-" . date("m-d", strtotime("-1 week")) . "'";

        $sql1 = "select registrationid, verifyDescr, verifyState from RegistrationStatus where registrationid = "
                . "(select registrationid from RegistrationStatus where verifyState = " . $verifyState .
                " and allimageDate is not null " . $where3 . " order by id desc limit 1) and verifyState = " . $verifyState . " FOR UPDATE";

        if ($verifyState === 0) {
            $sql1 = "select registrationid, verifyDescr, verifyState from RegistrationStatus where registrationid = "
                    . "(select registrationid from RegistrationStatus where verifyState in (" . $verifyState .
                    ") and allimageDate is not null " . $where3 . " order by id desc limit 1) and verifyState in (" . $verifyState . ") FOR UPDATE";
        }
        

        $stmt1 = $em->getConnection()->prepare($sql1);
        $stmt1->execute();

        $resultRegStatus = $stmt1->fetch();

        if ($resultRegStatus) {
            $user = $this->get('security.token_storage')->getToken()->getUser();

            $locker = ($verifyState == '0' || $verifyState == 0) ? '-1' : '-' . @$resultRegStatus['verifyState'];
            $locked = $this->lockRegistrationForVerification($resultRegStatus['registrationid'], $locker, $user);

            if ($locked) {
                return $resultRegStatus;
            }
        } else {
            return "no_record";
        }

        return FALSE;
    }

    /**
     * @param $limit
     * @param $timestamp
     * @param int $verifyState
     * @param Request $request
     *
     * @return array
     * @throws \Exception
     * @Route("/listverifyregistrations/{limit}/{timestamp}/{verifyState}",name="listverifyregistrations")
     * @Method({"POST","GET"})
     *
     */
    public function listverifyregistrationsAction($limit, $timestamp, $verifyState = 0, Request $request) {
        $em = $this->getDoctrine()->getManager();
        $response['success'] = 0;

        try {

            // exit('Hellow');
            //Get List Fields
            $query = $em->createQueryBuilder()
                    ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'newverifierFieldList' ")
                    ->getQuery();
            $configMaster = $query->getOneOrNullResult();

            //exit('Hapa..');
            if ($configMaster) {
                $allconfig = json_decode($configMaster->getConfig(), true);

                $selectFields = str_replace(":::", ", ", $allconfig['queryString']);

                //-----------------------------------

                $try = true;
                $nn = 1;
                while ($try) {
                    $resultRegStatus = $this->getRecordForVerification($verifyState);
                    if ($resultRegStatus) {
                        if ($resultRegStatus == 'no_record') {
                            $resultRegStatus = FALSE;
                        }
                        $try = FALSE;
                    }

                    $nn++;

                    if ($nn == 20) {
                        $try = FALSE;
                    }
                }

                //-----------------------------------

                if ($resultRegStatus) {

                    //Pull Records..
                    $sql = "select u.registrationid, u.id as reg_id, " . $selectFields . " FROM registration u "
                            . " left join Idtype i on i.id = u.identificationType "
                            . " where u.id = "
                            . $resultRegStatus['registrationid'];

                    $stmt = $em->getConnection()->prepare($sql);
                    $stmt->execute();
                    $result = $stmt->fetchAll();



                    if ($result) {
                        $response['success'] = 1;
                        foreach ($result as $index => $result) {
                            $result['reg_images'] = $this->getImagesFromRegid($result['registrationid']);
                            $response['verification_description'] = $resultRegStatus['verifyDescr'];
                            $response['records'][] = $result;

                            //$locker = ($verifyState == '0' || $verifyState == 0) ? '-1' : '-' . $verifyState;
                            //$this->lockRegistrationForVerification($result['reg_id'], $locker, $user);
                            //unset($result['reg_id']);
                        }
                    }

                    $em->getConnection()->commit();
                } else {
                    $response['success'] = 0;
                    $response['resp_message'] = "There are currently no registrations to verify.. please refresh screen in a few minutes to continue the verification process. ";
                }
            }
            echo json_encode($response);
        } catch (OptimisticLockException $e) {
            //echo "OptimisticLockException: " . $e->getMessage();
            $em->getConnection()->rollback();
            throw $e;
        } catch (\Exception $ex) {
            $em->getConnection()->rollback();
            throw $ex;
        }
        exit;
        return $this->prepareResponse($data);
    }

    function getImagesFromRegid($registrationid) {
        $images['potrait'] = "No Image Yet";
        $images['front-pic'] = "No Image Yet";
        $images['rear-pic'] = "No Image Yet";
        $images['signature'] = "No Image Yet";

        $sql = "select * FROM reg_images where registration = '" . $registrationid . "';";

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        if ($result) {
            foreach ($result as $kkey => $vval) {
                $images[$vval['imageType']] = $vval['webPath'];
            }
        }

        return $images;
    }

    public function lockRegistrationForVerification($registration_id, $verifyState, $user) {
        $em = $this->getDoctrine()->getManager();

        $sql = "select verifyState from RegistrationStatus where registrationId = " . $registration_id;

        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        

        if (in_array(@$result[0]['verifyState'], array(0,2) )) {

            $apiHelper = $this->container->get('api.helper');
            $apiHelper->logInfo('VERIFY', 'lockRegistrationForVerification', array(
                'RegID:: ' => $registration_id,
                'ResultFROMdb: ' => json_encode(@$result),
                'USERID: ' => $user->getId(),
                'dateTIME' => date('Y-m-d H:i:s')
            ));


            $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . date('Y-m-d H:i:s') . "', verifyBy = '" . $user->getId() . "',  auditDescr = IF(auditDescr IS NULL, concat('  :: " . date('Y-m-d H:i:s') . "', ' - " . $user->getId() . "'), concat(auditDescr, '  :: " . date('Y-m-d H:i:s') . "', ' - " . $user->getId() . "')), auditState = '55'  WHERE registrationid = " . $registration_id;
            
            $em->getConnection()->exec($query1);
            return TRUE;
        }

        return FALSE;
    }

    /**
     * View Reports
     *
     * @Route("/verify_registration_post/{timestamp}/{recordID}/{action}", name="verify_registration_post")
     * @Method("POST")
     */
    public function verify_registration_postAction($timestamp, $recordID, $action = 1, Request $request) {
        $data['recid'] = $recordID;
        $descr = "";
        $rate = 0;

        $em = $this->getDoctrine()->getManager();
        $registration = $em->getRepository('RestApiBundle:Registration')->find($recordID);
        if (!$registration) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $query = $em->createQueryBuilder()
                ->select("u.id as sid, DATE_FORMAT(u.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate,  DATE_FORMAT(u.verifyLock, '%Y-%m-%d %H:%i%:%s') as verifyLock"
                        . " from RestApiBundle:RegistrationStatus u "
                        . " WHERE u.registrationId = " . $registration->getId())
                ->getQuery();

        $result1 = $query->getResult();

        $timetaken = 0;
        if ($result1) {
            $timetaken = strtotime(date('Y-m-d H:i:s')) - strtotime($result1[0]['verifyLock']);
        }
        
        

        $configArray = array();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'newverifierFieldList' ")
                ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        $allconfig = json_decode($configMaster->getConfig(), true);

        

        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(":::", $allconfig['queryString']);
            $ScoreArray = $allconfig['ratingScore'];

            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }

        $attributes = json_decode($request->getContent(), true);

        
        
        $errorDescription = "";
        foreach ($attributes as $kkey => $vval) {
            if ($kkey == "allDescr") {
                $descr = json_encode($vval);
            } else {
                $rate = $rate + $vval['value'];

                if ($vval['value'] > 0) {
                    //$vval['fieldName'] . " " . $vval['fieldDescr'];
                    $errorDescription = $errorDescription . ", " . $vval['descr'];
                }
            }
        }
        


        $rateposted = $rate;
        $rate = 100 - $rate;

        $score = 0;
        if ($rateposted > 100) {
            $score = 3;
        } else {
            if ($rate > $ScoreArray['fair']) { //Good
                $score = 1;
            } elseif ($rate >= $ScoreArray['bad'] && $rate <= $ScoreArray['fair']) {
                $score = 2;
            } else {
                $score = 3;
            }
        }
        
        
        
//        echo " scrore: " . $score;
//        echo " rate: " . $rate;
//        echo " descr: " . $descr;
        //updateVerificationStatus( $registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken) {
        
        
        
        $this->updateVerificationStatus($registration, 0, 0, "", "Live Verification :: Approved ", $score, $descr, $user, $timetaken, $action);
        

        
        
        // submit freg
        $activeLiveVerification = $this->container->getParameter("live_verification_active");
        $fregVerificationThreshold = $this->container->getParameter("freg_score_threshold");
        if ($activeLiveVerification) {
            if ($score == 3) {
                $this->triggerNotfication($registration, "register.status.fail.verification", $errorDescription, 3);
            } else if ($score > 0 && $score <= $fregVerificationThreshold) {
                $receivedImages = $em->getRepository("RestApiBundle:RegImages")->findBy(
                        array("registration" => $registration->getRegistrationid())
                );
                //         $this->container->get("vodacom.endpoint")->processImages($receivedImages);
            }
        }
        
        
        
        $data['message'] = 'ScoreArray: ' . json_encode($ScoreArray) . ' :: recordID: ' . $recordID . " score: " . $score . " rate: " . $rate . " rateposted: " . $rateposted . "  descr: " . $descr;
        $data['response'] = "Registration verified Successfully";
        echo json_encode($data);

        exit;

        return array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }

    public function updateVerificationStatus(Registration $registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken) {


        $em = $this->getDoctrine()->getManager();
	$apiHelper = $this->container->get("api.helper");


        $query1 = "UPDATE RegistrationStatus SET verifyState =  '" . $verifyState . "', verifyDate = '" . date('Y-m-d H:i:s') . "', verifyLock = '" . $timetaken . "', verifyBy = '" . $user->getId() . "', verifyDescr = '" . $verifyDesc . "' WHERE registrationid = " . $registration->getId();
        $em->getConnection()->exec($query1);

        if($verifyState == 3){
        	$msisdn = $registration->getMsisdn();
        	$sim_serial = $registration->getSimSerial();
        	$current_timestamp = date("Y-m-d h:i:sa");
 $overwriteSimserialQuery = "update registration set sim_serial = concat('$sim_serial','$current_timestamp') where id = ". $registration->getId();
        	$updateTregStatusQuery = " update RegistrationStatus set fullRegStatus = 4, fullRegDesc = 'Failed on verification',  temporaryRegStatus = 4, temporaryRegDesc = 'Failed on verification' where registrationId = " . $registration->getId();
        	$em->getConnection()->exec($overwriteSimserialQuery);
        	$em->getConnection()->exec($updateTregStatusQuery);
        	$apiHelper->logInfo("updateVerificationStatus()", "update verification status", array( 'Updaing sim_serial', 'verifyState = [ '.$verifyState .' ]]', 'registration' => $registration, 'msisdn' => $registration->getMsisdn(), 'sim_serial' => $registration->getSimSerial() ));
        	$this->triggerNotfication($registration, "register.status.fail.verification", "usajili umekataliwa", 3);
        	$apiHelper->logInfo("updateVerificationStatus()","logging after notification sent", array("notification"=>"sent"));
        }

    }
    
    private function triggerNotfication($registration, $messageKey, $errorDescription, $level) {
        $msg = $this->container->get("api.helper")->getTranslatedLanguage($messageKey, array('%msisdn%' => $registration->getMsisdn(), "%errordescription%" => $errorDescription), $registration->getLanguage());
           
       // echo ":::  .  " . $msg; exit;
        
        $msg = str_replace('verify_reasons', $errorDescription, $msg);    
        
        $message = new Message();
        $message->setDeviceId($registration->getDeviceId());
        $message->setMessage($msg);
        $message->setData(array("status" => $level, "registrationId" => $registration->getRegistrationid()));
        $message->setTopic($this->container->getParameter("mqtt_registration_status"));
        $this->container->get('api.helper')->sendMessage($message);
    }

    
    
    /**
     * active_agents Summary
     *
     * @Route("/verify_daily_report/{start_date}/{end_date}/{timestamp}/{pull_all_records}", name="verify_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_daily_reportAction($start_date, $end_date, $timestamp, $pull_all_records) {
        $em = $this->getDoctrine()->getManager();

        if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }
        
        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }

        $where .= " AND s.verifyState NOT IN ('5', '-1', '-2') ";

        //if (in_array("ROLE_VERIFYER_SUPERVISOR", $roles)) {
        if ($pull_all_records == 1) {
            // To do nothing :)) smile
        } else {
            $where .= " AND s.verifyBy != 3260 "; // OR s.verifyBy IS NULL ";
        }


        //$where.=" AND r.image_count >= 3 ";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
                ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
                        . " from RestApiBundle:RegistrationStatus s"
                        . " JOIN s.registrationId r "
                        . " JOIN r.owner u " . $where
                        . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
                ->getQuery();

        //echo $result1 = $query->getSql(); exit; //Result(); 
        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['created_on']] = (@$data[$value['created_on']] ? $data[$value['created_on']] : $value);

            $data[$value['created_on']]['sum_total'] = @$data[$value['created_on']]['sum_total'] + $value['sum_regs'];
            $data[$value['created_on']]['sum_good'] = (@$data[$value['created_on']]['sum_good'] ? $data[$value['created_on']]['sum_good'] : 0);
            $data[$value['created_on']]['sum_fair'] = (@$data[$value['created_on']]['sum_fair'] ? $data[$value['created_on']]['sum_fair'] : 0);
            $data[$value['created_on']]['sum_bad'] = (@$data[$value['created_on']]['sum_bad'] ? $data[$value['created_on']]['sum_bad'] : 0);
            $data[$value['created_on']]['sum_unrated'] = (@$data[$value['created_on']]['sum_unrated'] ? $data[$value['created_on']]['sum_unrated'] : 0);

            if ($value['verifyState'] == 1) {
                $data[$value['created_on']]['sum_good'] = $data[$value['created_on']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 2) {
                $data[$value['created_on']]['sum_fair'] = $data[$value['created_on']]['sum_fair'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == 2) {
                $data[$value['created_on']]['sum_bad'] = $data[$value['created_on']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data[$value['created_on']]['sum_unrated'] = $data[$value['created_on']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }
    
    
    
    /**
     * hourly_summary Summary
     *
     * @Route("/newccount_verifysummary/{start_date}/{end_date}/{timestamp}", name="newccount_verifysummary")
     * @Method("GET")
     * @Template()
     */
    public function newccount_verifysummaryAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND r.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }
        //$where .= " AND date(r.verifyDate) >= '2016-07-01' ";

        if (in_array("ROLE_VERIFYER_SUPERVISOR", $roles)) {
            $where .= " AND r.verifyBy != 3260 ";
        }

        $where .= " AND x.image_count >= 3 ";
        $where .= " AND r.allimageDate IS NOT NULL ";
        $where .= " AND r.temporaryRegStatus IN (0, 2, 6) ";
        $where .= " AND r.verifyState in (0, 1, 2, 3, '-1', '-2', '-3') ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');


        $treg = $freg = $fail = $pending = 0;

        $count_tfreg = $em->createQuery("SELECT count(r) as reg_count, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, r.verifyState as verifyState FROM RestApiBundle:RegistrationStatus r"
                        . " JOIN r.registrationId x "
                        . $where
                        . " GROUP BY r.verifyState"
                )->getArrayResult();


        $data['total'] = $data['good'] = $data['fair'] = $data['pending'] = $data['bad'] = 0;
        foreach ($count_tfreg as $key => $val) {
            $data['total'] = $data['total'] + $val['reg_count'];

            if ($val['verifyState'] == 3 || $val['verifyState'] == '-3') {
                $data['bad'] = $data['bad'] + $val['reg_count'];
            } elseif ($val['verifyState'] == 2 || $val['verifyState'] == '-2') {
                $data['fair'] = $data['fair'] + $val['reg_count'];
            } elseif ($val['verifyState'] == 1) {
                $data['good'] = $data['good'] + $val['reg_count'];
            } else {
                $data['pending'] = $data['pending'] + $val['reg_count'];
            }
        }


        echo json_encode($data);
        exit;
    }
    
    
    
       /**
     * View Reports
     *
     * @Route("/newverifyaction_registrations/{timestamp}/{recordID}/{action}", name="newverifyaction_registrations")
     * @Method("POST")
     */
    public function newverifyaction_registrationsAction($timestamp, $recordID, $action = 1, Request $request) {
        $data['recid'] = $recordID;
        $descr = "";
        $rate = 0;
        
        
if (!in_array(49, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }


        $em = $this->getDoctrine()->getManager();
        $registration = $em->getRepository('RestApiBundle:Registration')->find($recordID);
        if (!$registration) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $query = $em->createQueryBuilder()
                ->select("u.id as sid, DATE_FORMAT(u.verifyDate, '%Y-%m-%d %H:%i%:%s') as verifyDate,  DATE_FORMAT(u.verifyLock, '%Y-%m-%d %H:%i%:%s') as verifyLock"
                        . " from RestApiBundle:RegistrationStatus u "
                        . " WHERE u.registrationId = " . $registration->getId())
                ->getQuery();

        $result1 = $query->getResult();

        $timetaken = 0;
        if ($result1) {
            $timetaken = strtotime(date('Y-m-d H:i:s')) - strtotime($result1[0]['verifyLock']);
        }

        $configArray = array();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'newverifierFieldList' ")
                ->getQuery();

        $configMaster = $query->getOneOrNullResult();

        $allconfig = json_decode($configMaster->getConfig(), true);

        

        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(":::", $allconfig['queryString']);
            $ScoreArray = $allconfig['ratingScore'];

            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }

        $attributes = json_decode($request->getContent(), true);

        $data['message'] = 'Invalid request sent :: ' . json_encode($attributes);
        $data['response'] = "Registration NOT verified";






        $apiHelper = $this->container->get("api.helper");
        $apiHelper->logInfo("newADMINcontroller", "verifyRegistration", array('ccount:: ' => count($attributes), 'ATTRIBUTESposted: ' => $attributes, 'RECORDid: ' => $recordID, 'userID: ' => $user->getId()));


        if (count($attributes) > 5) {

            $errorDescription = "";
            foreach ($attributes as $kkey => $vval) {
                if ($kkey == "allDescr") {
                    $descr = json_encode($vval);
                } else {
                    $rate = $rate + $vval['value'];

                    if ($vval['value'] > 0) {
                        //$vval['fieldName'] . " " . $vval['fieldDescr'];
                        $errorDescription = $errorDescription . ", " . $vval['descr'];
                    }
                }
            }

            $rate = 100 - $rate;

            $score = 0;
            if ($rate > $ScoreArray['fair']) { //Good
                $score = 1;
            } elseif ($rate >= $ScoreArray['bad'] && $rate <= $ScoreArray['fair']) {
                $score = 2;
            } else {
                $score = 3;
            }



            //        echo " scrore: " . $score;
            //        echo " rate: " . $rate;
            //        echo " descr: " . $descr;
            //updateVerificationStatus( $registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken) {
            $this->updateVerificationStatus($registration, 0, 0, "", "Live Verification :: Approved ", $score, $descr, $user, $timetaken, $action);
            //---$registration, $tempRegStatus, $fullRegStatus, $tempRegDesc, $fullRegDesc, $verifyState, $verifyDesc, $user, $timetaken, $action) {
            // submit freg
            $activeLiveVerification = $this->container->getParameter("live_verification_active");
            $fregVerificationThreshold = $this->container->getParameter("freg_score_threshold");
            if ($activeLiveVerification) {
                if ($score == 3) {
                    $this->triggerNotfication($registration, "register.verify.status.fregdeclined", $errorDescription, 3);
                } else if ($score > 0 && $score <= $fregVerificationThreshold) {
                    $receivedImages = $em->getRepository("RestApiBundle:RegImages")->findBy(
                            array("registration" => $registration->getRegistrationid())
                    );
                    //         $this->container->get("vodacom.endpoint")->processImages($receivedImages);
                }
            }

            $data['message'] = 'ScoreArray: ' . json_encode($ScoreArray) . ' :: recordID: ' . $recordID . " score: " . $score . " rate: " . $rate . " descr: " . $descr;
            $data['response'] = "Registration verified Successfully";
        }
        echo json_encode($data);

        exit;

        return array(
            'title' => "Active & Inactive",
            'title_descr' => "View Reports for Active Inactive",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );
    }
    

       /**
     * Creates a new Faq entity.
     *
     * @Route("/editUserFormPost/{id}", name="editUserFormPost")
     * @Method("POST")
     * @Template("DashboardBundle:Admin:reports.html.twig")
     */
    public function editUserFormPostAction($id, Request $request) {

        $keyId = $request->request->get('id');

        $em = $this->getDoctrine()->getManager();

        $registration = $em->getRepository('RestApiBundle:Registration')->find($id);
        if (!$registration) {
            throw $this->createNotFoundException('Unable to find Registration entity.');
        }

        $registrationId = $registration->getRegistrationid();
        
        
        $idtype = $em->getRepository('DashboardBundle:Idtype')->findAll();
        
        $idtypeArray = array();
        foreach($idtype as $key => $val){
            $idtypeArray[$val->getName()] = $val->getId();
        }
        
        

        $strlenth = strlen($request->request->get('value'));
        
        if ($keyId == "FirstName" && $strlenth >= 3) {
            $registration->setFirstName($request->request->get('value'));
        } elseif ($keyId == "MiddleName" && $strlenth >= 3) {
            $registration->setMiddleName($request->request->get('value'));
        } elseif ($keyId == "LastName" && $strlenth >= 3) {
            $registration->setLastName($request->request->get('value'));
        } elseif ($keyId == "Gender" && $strlenth >= 1) {
            $registration->setGender($request->request->get('value'));
        } elseif ($keyId == "DoB" && $strlenth >= 3) {
            $registration->setDob(date('d-m-Y', strtotime($request->request->get('value'))));
        } elseif ($keyId == "SimSerial" && $strlenth >= 17) {
            $registration->setSimSerial($request->request->get('value'));
        } elseif ($keyId == "IDNumber" && $strlenth >= 5) {
            $registration->setIdentification(trim($request->request->get('value')));
        } elseif ($keyId == "IdentificationType") {
            $registration->setIdentificationType(@$idtypeArray[$request->request->get('value')]);
        } elseif ($keyId == "Email") {
            $registration->setEmail($request->request->get('value'));
        }

        $em->flush();


        /*
          $registrationStatus = $em->getRepository('RestApiBundle:RegistrationStatus')->findBy(array("registrationId" => $registration->getId()));
          if (!$registrationStatus) {
          throw $this->createNotFoundException('Unable to find Registration entity.');
          }

          //echo "<pre>"; print_r($registrationStatus); echo "</pre>"; exit;

          $registrationStatus->setTemporaryRegStatus('0');
          $registrationStatus->setFullRegStatus('0');
          $registrationStatus->setFullRegDesc('Reset By EditRecord');
          $em->flush();
         */

        $query1 = "UPDATE RegistrationStatus SET temporaryRegStatus = '0', fullRegStatus = '0', temporaryRegDesc = 'Reset By EditRecord2', fullRegDesc = '' WHERE registrationId = " . $id;
        $em->getConnection()->exec($query1);

        echo $request->request->get('value');

        exit;

        return array(
            'title' => "View Registrations Report:"
        );
    }
    
}
