<?php

namespace aimgroup\VerificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use aimgroup\DashboardBundle\Controller\AbstractController;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStates;
use aimgroup\RestApiBundle\Dao\Message;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Verify Reports controller.
 *
 * @Route("/verify/reports")
 */
class ReportsController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * View Reports
     *
     * @Route("/index", name="mutisya")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:reports_index.html.twig")
     */
    public function indexAction() {

        if (!in_array(55, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'isAdmin' => true,
            'title' => "Verification Report",
            'title_descr' => "View Reports for Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    
    /**
     * View Reports
     *
     * @Route("/verification_reports", name="verification_reports")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verification_reports.html.twig")
     */
    public function verification_reportsAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Verification Report",
            'title_descr' => "View Reports for Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }
    
    /**
     * View Reports
     *
     * @Route("/verify_agent_reports", name="verify_agent_reports")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_agent_reports.html.twig")
     */
    public function verify_agent_reportsAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Verification Report",
            'title_descr' => "View Reports for Agents Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * verify_agents_report Summary
     *
     * @Route("/verify_agents_report/{start_date}/{end_date}/{timestamp}", name="verify_agents_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_agents_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND registration.parent_id = " . $user->getId();
            $where2 .= " AND registration.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(RegistrationStatus.createdDate) >= '" . $start_date . "' AND date(RegistrationStatus.createdDate) <= '" . $end_date . "' ";
        }
        //$where .= " AND s.verifyState in (1,2,3) ";




        $start = 0;
        $check_continue = true;


        $data = $active = array();
        $data['active'] = array();

        /*
          select user.first_name, user.last_name, msisdn, Region.name as RegionName, Territory.name as TerritoryName, count(*) as Total, sum(IF(RegistrationStatus.verifyState not in (0, -1), 1, 0)) AS Rated, sum(IF(RegistrationStatus.verifyState in (0, -1), 1, 0)) AS Unrated, sum(IF(RegistrationStatus.verifyState = 1, 1, 0)) AS RegGood, sum(IF(RegistrationStatus.verifyState in (2, -2), 1, 0)) AS RegFair, sum(IF(RegistrationStatus.verifyState in (3, -3), 1, 0)) AS RegBad from registration left join Region on Region.code = registration.region left join Territory on Territory.id = registration.territory left join user on user.id = registration.owner_id left join RegistrationStatus on registration.id = RegistrationStatus.registrationId group by owner_id
         */

        $em = $this->getDoctrine()->getManager();

        $result1 = array();

        $query1 = "select user.first_name as FirstName, user.last_name AS LastName, msisdn AS MSISDN, Region.name as RegionName,"
                . " Territory.name as TerritoryName, count(*) as Total, "
                . "sum(IF(RegistrationStatus.verifyState not in (0, -1, 10), 1, 0)) AS Verified, "
                . "sum(IF(RegistrationStatus.verifyState in (0, -1), 1, 0)) AS Unverified, sum(IF(RegistrationStatus.verifyState = 1, 1, 0)) AS RegGood, sum(IF(RegistrationStatus.verifyState in (2, -2), 1, 0)) AS RegFair, sum(IF(RegistrationStatus.verifyState in (3, -3), 1, 0)) AS RegBad from registration left join Region on Region.code = registration.region left join Territory on Territory.id = registration.territory "
                . "left join user on user.id = registration.owner_id "
                . "left join RegistrationStatus on registration.id = RegistrationStatus.registrationId "
                . $where
                . " group by owner_id";

        $connection = $em->getConnection();
        $statement = $connection->prepare($query1);
        $statement->execute();
        $result1 = $statement->fetchAll();

        echo json_encode($result1);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verify_region_reports", name="verify_region_reports")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_region_reports.html.twig")
     */
    public function verify_region_reportsAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Regions Report",
            'title_descr' => "View Reports for Regions Registration Verification",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_region_report/{start_date}/{end_date}/{timestamp}", name="verify_region_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_region_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND date(s.createdDate) >= '" . $start_date . "' AND date(s.createdDate) <= '" . $end_date . "' ";
        }
        $where2 .= " AND s.verifyState not in (10) ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
                ->select("s.id as sid, COUNT(r.id) as sum_regs, t.name as territory_name, d.name as region_name, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.verifyState as verifyState "
                        . " from RestApiBundle:RegistrationStatus s"
                        . " JOIN s.registrationId r "
                        . " JOIN r.owner u "
                        . " JOIN u.region d "
                        . " JOIN u.territory t " . $where2
                        . " GROUP BY region_name, territory_name, verifyState ORDER BY sum_regs DESC")
                ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['region_name']][$value['territory_name']] = (@$data[$value['region_name']][$value['territory_name']] ? $data[$value['region_name']][$value['territory_name']] : $value);

            //first append total, good faor bad to array as zero
            $data[$value['region_name']][$value['territory_name']]['sum_total'] = @$data[$value['region_name']][$value['territory_name']]['sum_total'] + $value['sum_regs'];
            $data[$value['region_name']][$value['territory_name']]['sum_total_verified'] = (@$data[$value['region_name']][$value['territory_name']]['sum_total_verified'] ? $data[$value['region_name']][$value['territory_name']]['sum_total_verified'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_good'] = (@$data[$value['region_name']][$value['territory_name']]['sum_good'] ? $data[$value['region_name']][$value['territory_name']]['sum_good'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_fair'] = (@$data[$value['region_name']][$value['territory_name']]['sum_fair'] ? $data[$value['region_name']][$value['territory_name']]['sum_fair'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_bad'] = (@$data[$value['region_name']][$value['territory_name']]['sum_bad'] ? $data[$value['region_name']][$value['territory_name']]['sum_bad'] : 0);
            $data[$value['region_name']][$value['territory_name']]['sum_unrated'] = (@$data[$value['region_name']][$value['territory_name']]['sum_unrated'] ? $data[$value['region_name']][$value['territory_name']]['sum_unrated'] : 0);



            if (in_array($value['verifyState'], array(1, 2, 3, '-2', '-3'))) {
                $data[$value['region_name']][$value['territory_name']]['sum_total_verified'] = $data[$value['region_name']][$value['territory_name']]['sum_total_verified'] + $value['sum_regs'];
            }

            //now fill in the data
            if ($value['verifyState'] == 1) {
                $data[$value['region_name']][$value['territory_name']]['sum_good'] = $data[$value['region_name']][$value['territory_name']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 2 || $value['verifyState'] == -2) {
                $data[$value['region_name']][$value['territory_name']]['sum_fair'] = $data[$value['region_name']][$value['territory_name']]['sum_fair'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == -3) {
                $data[$value['region_name']][$value['territory_name']]['sum_bad'] = $data[$value['region_name']][$value['territory_name']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data[$value['region_name']][$value['territory_name']]['sum_unrated'] = $data[$value['region_name']][$value['territory_name']]['sum_unrated'] + $value['sum_regs'];
            }
        }
//        echo "<pre>";
//        print_r($data);
//        echo "<pre>";
        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verify_auditor_reports", name="verify_auditor_reports")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_auditor_reports.html.twig")
     */
    public function verify_auditor_reportsAction() {

       
        $data = array(
            'title' => "Verifiers Report",
            'title_descr' => "View Reports for Auditor | Verifier Registration Performance",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/getverify_auditor_reports/{start_date}/{end_date}/{timestamp}", name="getverify_auditor_reports")
     * @Method("GET")
     * @Template()
     */
    public function getverify_auditor_reportsAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where2 .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }
        $where2 .= " AND s.verifyState in (1, 3, 2, '-2', '-3')";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
                ->select("u.id as sid, COUNT(s.id) as sum_regs, CONCAT(u.firstName , ' ', u.lastName) as agent_names, u.username, DATE_FORMAT(s.verifyDate, '%Y-%m-%d') as created_on, s.verifyState "
                        . " from RestApiBundle:RegistrationStatus s"
                        . " JOIN s.verifyBy u " . $where2
                        . " GROUP BY s.verifyBy, s.verifyState")
                ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();
        $data['active'] = array();
        foreach ($result1 as $key => $value) {
            $data['active'][$value['sid']] = (@$data['active'][$value['sid']] ? $data['active'][$value['sid']] : $value);

            $data['active'][$value['sid']]['sum_total'] = @$data['active'][$value['sid']]['sum_total'] + $value['sum_regs'];
            $data['active'][$value['sid']]['sum_good'] = (@$data['active'][$value['sid']]['sum_good'] ? $data['active'][$value['sid']]['sum_good'] : 0);
            $data['active'][$value['sid']]['sum_fair'] = (@$data['active'][$value['sid']]['sum_fair'] ? $data['active'][$value['sid']]['sum_fair'] : 0);
            $data['active'][$value['sid']]['sum_bad'] = (@$data['active'][$value['sid']]['sum_bad'] ? $data['active'][$value['sid']]['sum_bad'] : 0);
            $data['active'][$value['sid']]['sum_unrated'] = (@$data['active'][$value['sid']]['sum_unrated'] ? $data['active'][$value['sid']]['sum_unrated'] : 0);

            if ($value['verifyState'] == 1) {
                $data['active'][$value['sid']]['sum_good'] = $data['active'][$value['sid']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 2 || $value['verifyState'] == -2) {
                $data['active'][$value['sid']]['sum_fair'] = $data['active'][$value['sid']]['sum_fair'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == -3) {
                $data['active'][$value['sid']]['sum_bad'] = $data['active'][$value['sid']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data['active'][$value['sid']]['sum_unrated'] = $data['active'][$value['sid']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verify_daily_reports", name="verify_daily_reports")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_daily_reports.html.twig")
     */
    public function verify_daily_reportsAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms'), true))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Daily Count Report",
            'title_descr' => "View Agents Daily Verify Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_daily_reports_system", name="verify_daily_reports_system")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_daily_reports_system.html.twig")
     */
    public function verify_daily_reports_systemAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Agents Daily Verify Breakdown Report :: Includes System Verified",
            'title_descr' => "View Agents Daily Verify Breakdown Report :: Includes System Verified",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_daily_report/{start_date}/{end_date}/{timestamp}/{pull_all_records}", name="verify_daily_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_daily_reportAction($start_date, $end_date, $timestamp, $pull_all_records) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.createdDate) >= '" . $start_date . "' AND date(s.createdDate) <= '" . $end_date . "' ";
        }

        //if (in_array("ROLE_VERIFYER_SUPERVISOR", $roles)) {
        if ($pull_all_records == 1) {
            // To do nothing :)) smile
        } else {
            $where .= " AND s.verifyBy != 3260 OR s.verifyBy IS NULL ";
        }

        //$where.=" AND r.image_count >= 3 ";


        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
                ->select("s.id as sid, COUNT(s.id) as sum_regs, DATE_FORMAT(s.createdDate, '%Y-%m-%d') as created_on, s.verifyState "
                        . " from RestApiBundle:RegistrationStatus s"
                        . " JOIN s.registrationId r "
                        . " JOIN r.owner u " . $where
                        . " GROUP BY created_on, s.verifyState ORDER BY created_on DESC")
                ->getQuery();

        //echo $result1 = $query->getSql(); exit; //Result();
        $result1 = $query->getResult();

        $data = $active = array();
        foreach ($result1 as $key => $value) {
            $data[$value['created_on']] = (@$data[$value['created_on']] ? $data[$value['created_on']] : $value);

            $data[$value['created_on']]['sum_total'] = @$data[$value['created_on']]['sum_total'] + $value['sum_regs'];
            $data[$value['created_on']]['sum_good'] = (@$data[$value['created_on']]['sum_good'] ? $data[$value['created_on']]['sum_good'] : 0);
            $data[$value['created_on']]['sum_fair'] = (@$data[$value['created_on']]['sum_fair'] ? $data[$value['created_on']]['sum_fair'] : 0);
            $data[$value['created_on']]['sum_bad'] = (@$data[$value['created_on']]['sum_bad'] ? $data[$value['created_on']]['sum_bad'] : 0);
            $data[$value['created_on']]['sum_unrated'] = (@$data[$value['created_on']]['sum_unrated'] ? $data[$value['created_on']]['sum_unrated'] : 0);

            if ($value['verifyState'] == 1) {
                $data[$value['created_on']]['sum_good'] = $data[$value['created_on']]['sum_good'] + $value['sum_regs'];
            } elseif ($value['verifyState'] == 3 || $value['verifyState'] == 2) {
                $data[$value['created_on']]['sum_bad'] = $data[$value['created_on']]['sum_bad'] + $value['sum_regs'];
            } else {
                $data[$value['created_on']]['sum_unrated'] = $data[$value['created_on']]['sum_unrated'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verify_total_reports", name="verify_total_reports")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_total_reports.html.twig")
     */
    public function verify_total_reportsAction() {


        $data = array(
            'title' => "Total Registrations Report",
            'title_descr' => "Total Registrations Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_total_report/{start_date}/{end_date}/{timestamp}", name="verify_total_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_total_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(r.createdDate) >= '" . $start_date . "' AND date(r.createdDate) <= '" . $end_date . "' ";
        }
        //$where .= " AND s.verifyState != 10 ";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');

//$result = $em->createQuery("SELECT u.username, CONCAT(u.first_name , ' ', u.last_name) as agent_names, COUNT(r.id) as registrations_count "

        $query = $em->createQueryBuilder()
                ->select("s.id as sid, r.image_count, COUNT(r.id) as sum_regs, DATE_FORMAT(r.createdDate, '%Y-%m-%d') as created_on, s.temporaryRegStatus as treg, s.fullRegStatus as freg, s.verifyState "
                        . " from RestApiBundle:RegistrationStatus s"
                        . " JOIN s.registrationId r "
                        . " JOIN r.owner u " . $where
                        . " GROUP BY created_on, r.image_count, s.temporaryRegStatus, s.verifyState ORDER BY sum_regs DESC")
                ->getQuery();

        $result1 = $query->getResult();

        $data = $active = array();


        /*
          $result = $em->createQuery("SELECT e.image_count, p.temporaryRegStatus as treg, p.fullRegStatus as freg, p.verifyState as vstate, DATE(e.createdDate) as ddate, COUNT(p.id) as ccount "
          . " from RestApiBundle:RegistrationStatus p "
          //. "WHERE date(created_date) >= '2015-09-28' AND date(created_date) <= '2015-10-08' "
          . " JOIN p.registrationId e"
          . " JOIN e.owner a" . $where
          . " GROUP BY ddate, e.image_count, treg, freg, vstate "
          . " ORDER BY ddate desc")
          ->getArrayResult(); //getSingleResult();

          if( in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))){
          if(in_array($value['vstate'], array(1,2,3,'-2','-3'))) {
          $data[$value['ddate']][30]['count'] = @$data[$value['ddate']][30]['count'] + $value['ccount'];

          if ($value['vstate'] == 1) {
          //vgood
          $data[$value['ddate']][32]['count'] = @$data[$value['ddate']][32]['count'] + $value['ccount'];
          } elseif ($value['vstate'] == 2 || $value['vstate'] == '-2') {
          //vfair
          $data[$value['ddate']][33]['count'] = @$data[$value['ddate']][33]['count'] + $value['ccount'];
          } elseif ($value['vstate'] == 3 || $value['vstate'] == '-3') {
          //vbad
          $data[$value['ddate']][34]['count'] = @$data[$value['ddate']][34]['count'] + $value['ccount'];
          }
          } else {
          //notverified
          $data[$value['ddate']][31]['count'] = @$data[$value['ddate']][31]['count'] + $value['ccount'];
          }
          }
         */
        foreach ($result1 as $key => $value) {
            $data[$value['created_on']] = (@$data[$value['created_on']] ? $data[$value['created_on']] : $value);

            $data[$value['created_on']]['sum_total_reg'] = @$data[$value['created_on']]['sum_total_reg'] + $value['sum_regs'];
            $data[$value['created_on']]['sum_total_approved'] = (@$data[$value['created_on']]['sum_total_approved'] ? $data[$value['created_on']]['sum_total_approved'] : 0);
            $data[$value['created_on']]['sum_total_pending'] = (@$data[$value['created_on']]['sum_total_pending'] ? $data[$value['created_on']]['sum_total_pending'] : 0);
            $data[$value['created_on']]['sum_total_declined'] = (@$data[$value['created_on']]['sum_total_declined'] ? $data[$value['created_on']]['sum_total_declined'] : 0);
            $data[$value['created_on']]['sum_total_audited'] = (@$data[$value['created_on']]['sum_total_audited'] ? $data[$value['created_on']]['sum_total_audited'] : 0);
            $data[$value['created_on']]['sum_good'] = (@$data[$value['created_on']]['sum_good'] ? $data[$value['created_on']]['sum_good'] : 0);
            $data[$value['created_on']]['sum_fair'] = (@$data[$value['created_on']]['sum_fair'] ? $data[$value['created_on']]['sum_fair'] : 0);
            $data[$value['created_on']]['sum_bad'] = (@$data[$value['created_on']]['sum_bad'] ? $data[$value['created_on']]['sum_bad'] : 0);
            $data[$value['created_on']]['sum_unrated'] = (@$data[$value['created_on']]['sum_unrated'] ? $data[$value['created_on']]['sum_unrated'] : 0);

            /*
              if( in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4 && in_array($value['vstate'], array(0, 1, 2, 3, '-1', '-2', '-3'))){
              if(in_array($value['vstate'], array(1,2,3,'-2','-3'))) {
             */
            if (in_array($value['verifyState'], array(0, 1, 2, 3, '-1', '-2', '-3')) && in_array($value['treg'], array(0, 2, 6)) && $value['image_count'] >= 4) {
                if (in_array($value['verifyState'], array(1, 2, 3, '-2', '-3'))) {
                    //
                } else {
                    $data[$value['created_on']]['sum_unrated'] = $data[$value['created_on']]['sum_unrated'] + $value['sum_regs'];
                }
            }

            if (in_array($value['verifyState'], array(1, 2, 3, -3, -2))) {
                $data[$value['created_on']]['sum_total_audited'] = $data[$value['created_on']]['sum_total_audited'] + $value['sum_regs'];

                if ($value['verifyState'] == 1) {
                    $data[$value['created_on']]['sum_good'] = $data[$value['created_on']]['sum_good'] + $value['sum_regs'];
                } elseif ($value['verifyState'] == 2 || $value['verifyState'] == -2) {
                    $data[$value['created_on']]['sum_fair'] = $data[$value['created_on']]['sum_fair'] + $value['sum_regs'];
                } elseif ($value['verifyState'] == 3 || $value['verifyState'] == -3) {
                    $data[$value['created_on']]['sum_bad'] = $data[$value['created_on']]['sum_bad'] + $value['sum_regs'];
                }
            }

            if ($value['image_count'] >= 3) {
                $data[$value['created_on']]['sum_total_approved'] = $data[$value['created_on']]['sum_total_approved'] + $value['sum_regs'];
            } else {
                $data[$value['created_on']]['sum_total_pending'] = $data[$value['created_on']]['sum_total_pending'] + $value['sum_regs'];
            }
            if (in_array($value['treg'], array(4, 5, 3))) {
                $data[$value['created_on']]['sum_total_declined'] = $data[$value['created_on']]['sum_total_declined'] + $value['sum_regs'];
            }
        }

        echo json_encode($data);
        exit;
    }

    /**
     * View Reports
     *
     * @Route("/verify_listview", name="verify_listview")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_listview.html.twig")
     */
    public function verify_listviewAction() {

        $data = array(
            'title' => "Verification List View Report & Result",
            'title_descr' => "Verification List View Report & Result",
            'users' => $this->loadUsers('ROLE_VERIFYER'),
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * View Reports
     *
     * @Route("/verify_timebreakdown", name="verify_timebreakdown")
     * @Method("GET")
     * @Template("VerificationBundle:Reports:verify_timebreakdown.html.twig")
     */
    public function verify_timebreakdownAction() {

        if (!in_array(56, json_decode($this->session->get('user_role_perms')))) {
            return $this->redirect($this->generateUrl('admin'));
        }

        $data = array(
            'title' => "Verification Report TimeTaken Breakdown Report",
            'title_descr' => "Verification Report TimeTaken Breakdown Report",
            'message' => "" //"Report Request made successfully.. Report will be sent to email once generated",
        );

        return $this->prepareResponse($data);
    }

    /**
     * active_agents Summary
     *
     * @Route("/verify_timebreakdown_report/{start_date}/{end_date}/{timestamp}", name="verify_timebreakdown_report")
     * @Method("GET")
     * @Template()
     */
    public function verify_timebreakdown_reportAction($start_date, $end_date, $timestamp) {
        $em = $this->getDoctrine()->getManager();

        $where = $where2 = " WHERE 1=1 ";
        /** @var  $user User */
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $roles = $user->getRoles(); //array of roles

        if (in_array("ROLE_SUPERAGENT", $roles)) {
            $where .= " AND u.parent_id = " . $user->getId();
            $where2 .= " AND u.parent_id = " . $user->getId();
        }
        if ($start_date != 1) {
            $where .= " AND date(s.verifyDate) >= '" . $start_date . "' AND date(s.verifyDate) <= '" . $end_date . "' ";
        }

        $where .= " AND s.verifyBy NOT IN (3260) ";


        $where .= " AND s.verifyState IN (1,2)";

        $emConfig = $em->getConfiguration();
        $emConfig->addCustomDatetimeFunction('YEAR', 'DoctrineExtensions\Query\Mysql\Year');
        $emConfig->addCustomDatetimeFunction('MONTH', 'DoctrineExtensions\Query\Mysql\Month');
        $emConfig->addCustomDatetimeFunction('DAY', 'DoctrineExtensions\Query\Mysql\Day');
        $emConfig->addCustomDatetimeFunction('DATE', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('DateDiff', 'DoctrineExtensions\Query\Mysql\Date');
        $emConfig->addCustomDatetimeFunction('Now', 'DoctrineExtensions\Query\Mysql\Date');

//$emConfig->addCustomStringFunction($name, 'aimgroup\DashboardBundle\DQL');
        $emConfig->addCustomNumericFunction('FLOOR', 'aimgroup\DashboardBundle\DQL\MysqlFloor');
//$emConfig->addCustomDatetimeFunction($name, 'aimgroup\DashboardBundle\DQL');

        $query = $em->createQuery("SELECT COUNT(s.id) as RegistrationsCount, (CASE WHEN s.verifyLock <=20 THEN '0-20' WHEN s.verifyLock >=20 AND s.verifyLock <=40 THEN '21-40' WHEN s.verifyLock >=41 AND s.verifyLock <=60 THEN '41-60' WHEN s.verifyLock >=61 AND s.verifyLock <=80 THEN '61-80' WHEN s.verifyLock >=81 AND s.verifyLock <=100 THEN '81-100' WHEN s.verifyLock >=101 AND s.verifyLock <=120 THEN '101-120' WHEN s.verifyLock >=121 AND s.verifyLock <=140 THEN '121-140' WHEN s.verifyLock >=141 AND s.verifyLock <=160 THEN '141-160' WHEN s.verifyLock >=161 THEN '161+' ELSE '1+' END) as verifyTimeRange "
                        . " from RestApiBundle:RegistrationStatus s " . $where
                        . " GROUP BY verifyTimeRange")
                ->getArrayResult();

        $data = array('0-20' => 0, '21-40' => 0, '41-60' => 0, '61-80' => 0, '81-100' => 0, '101-120' => 0, '121-140' => 0, '141-160' => 0, '161+' => 0);
        foreach ($query as $key => $value) {
            $data[$value['verifyTimeRange']] = $value['RegistrationsCount'];
        }

        //ksort($data);
        echo json_encode($data);
        exit;
    }

    private function loadUsers($userType) {
        $em = $this->getDoctrine()->getManager();

        $queryBuilder = $this->getDoctrine()->getManager()->createQueryBuilder();

        $users = $queryBuilder->select('u')
                ->from('RestApiBundle:User', 'u')
                ->where($queryBuilder->expr()->like('u.roles', ':userType'))//('u.type = :userType ')
                ->setParameter('userType', '%' . $userType . '%')
                ->getQuery()
                ->getArrayResult();

        return $users;
    }

}
