<?php

namespace aimgroup\VerificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use aimgroup\DashboardBundle\Controller\AbstractController;
use aimgroup\RestApiBundle\Entity\Registration;
use aimgroup\RestApiBundle\Entity\RegistrationStates;
use aimgroup\RestApiBundle\Dao\Message;
use FOS\RestBundle\Controller\Annotations\Get;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Verify controller.
 *
 * @Route("/verify/config")
 */
class ConfigController extends AbstractController {

    var $session;

    public function __construct() {
        $this->session = new Session();
    }

    /**
     * Load ID Configured and allowed  to be used in the mobile App.
     *
     * @Route("/index", name="index")
     * @Method("GET")
     * @Template("VerificationBundle:Config:index.html.twig")
     */
    public function indexAction(Request $request) {
       
        $configArray = array();
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQueryBuilder()
                ->select(" u from DashboardBundle:ConfigMaster u WHERE u.name = 'newverifierFieldList' ")
                ->getQuery();

        $configMaster = $query->getOneOrNullResult();
        
        $allconfig = json_decode($configMaster->getConfig(), true);

         
        $numRows = 1;
        if ($configMaster) {
            $configArray = explode(":::", $allconfig['queryString']);
            $ScoreArray = $allconfig['ratingScore'];
            

            $settingMerged = explode(":::", $configMaster->getConfigType());

            $numRows = @$settingMerged[0];
            $secsCount = @$settingMerged[1];
        }

        $data = array(
            'configArray' => json_encode($configArray),
            'verifyArray' => json_encode($allconfig['validations']),
            'bad' => $ScoreArray['bad'],
            'fair' => $ScoreArray['fair'],
            'numRows' => $numRows,
            'secsCount' => $secsCount,
            'title' => "Verifier View Fields",
            'title_descr' => "List, create, delete, activate System Admins"
        );
        
        return $this->prepareResponse($data);
    }

}
