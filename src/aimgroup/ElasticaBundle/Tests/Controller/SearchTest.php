<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 4:41 PM
 */

namespace aimgroup\ElasticaBundle\Tests\Controller;


use Elastica\Query\Bool;
use Elastica\Query\Term;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;


class SearchTest extends KernelTestCase
{

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    private $container;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->container = static::$kernel->getContainer();
        $this->em = $this->container->get('doctrine')->getManager();
    }


    public function testPrefixQuery()
    {
        $searchManager = $this->container->get('fos_elastica.finder.tigo.registration');

        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $mustArray = array();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('msisdn', "654279914");
        array_push($mustArray, $fieldQuery);



        $fieldQuery = new \Elastica\Query\Prefix();
        $fieldQuery->setPrefix('simSerial', '8925502041365426854');
        array_push($mustArray, $fieldQuery);
        $bool->addMust($mustArray);

        $bool->addMust($mustArray);
        $query->setQuery($bool);

        dump(json_encode(array('query' => $query->getQuery()->toArray())));
        $data = $searchManager->find($query);

//        return count($this->find($query));
    }

    public function testCountRegistration()
    {
        $idnumber = "T100257781091";
        $result = $this->container->get("fos_elastica.manager")->getRepository("RestApiBundle:RegistrationStatus")->findCountOfIdTypeAndIdNumber(1, $idnumber);
        dump($result);
    }

    public function testSearchRegistration()
    {

        $searchManager = $this->container->get('fos_elastica.finder.ereg.registration_status');

        $attributes = array();

        //$attributes["msisdn"] = "754311023";


        $filterParams = array();
        if (strlen(@$attributes["msisdn"]) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.msisdn', $attributes["msisdn"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["first_name"]) >= 2) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.firstName', $attributes["first_name"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["last_name"]) >= 2) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.lastName', $attributes["last_name"]);
            array_push($filterParams, $fieldQuery);
        }
        if (@$attributes["identificationType"] >= 1) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.identificationType', $attributes["identificationType"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["id_number"]) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.identification', $attributes["id_number"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["agent_firstname"]) >= 3) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.firstName', $attributes["agent_firstname"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["agent_lastname"]) >= 3) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.lastName', $attributes["agent_lastname"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["agent_phone"]) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.username', $attributes["agent_phone"]);
            array_push($filterParams, $fieldQuery);
        }
        if (@$attributes["territory"] > 0) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.territory', $attributes["territory"]);
            array_push($filterParams, $fieldQuery);
        }
        if (strlen(@$attributes["from_date"]) > 4 && strlen(@$attributes["to_date"]) > 4) {
            $rangeQuery = new \Elastica\Query\Range();
            $rangeQuery->addField('createdDate', array(
                'gte' => \Elastica\Util::convertDate((new \DateTime($attributes["from_date"]))->getTimestamp()),
                'lte' => \Elastica\Util::convertDate((new \DateTime($attributes["to_date"]))->getTimestamp())
            ));
            array_push($filterParams, $rangeQuery);
        } else if (strlen(@$attributes["from_date"]) > 4) {
            $rangeQuery = new \Elastica\Query\Range();
            $rangeQuery->addField('createdDate', array(
                'gte' => \Elastica\Util::convertDate((new \DateTime($attributes["from_date"]))->getTimestamp())
            ));
            array_push($filterParams, $rangeQuery);
        } else if (strlen(@$attributes["to_date"]) > 4) {
            $rangeQuery = new \Elastica\Query\Range();
            $rangeQuery->addField('createdDate', array(
                'lte' => \Elastica\Util::convertDate((new \DateTime($attributes["to_date"]))->getTimestamp())
            ));
            array_push($filterParams, $rangeQuery);
        }

        if (count($filterParams) > 0) {
            $query = new \Elastica\Query();
            $boolean = new \Elastica\Query\Bool();
            $boolean->addMust($filterParams);
            $query->setQuery($boolean);
        } else {
            $query = new \Elastica\Query\MatchAll();
        }


//        $query->setSort(array("createdDate" => array('order' => 'desc')));


        //   dump(json_encode(array('query' => $query->getQuery()->toArray())));
        $data = $searchManager->find($query);
        // $data->setMaxPerPage(10);
//        $data->setCurrentPage(1);
//        $records = $data->getCurrentPageResults();


        dump($data);

    }


    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
    }
}