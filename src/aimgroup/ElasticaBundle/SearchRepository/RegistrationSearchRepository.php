<?php

/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 10/06/2016
 * Time: 4:17 PM
 */
namespace aimgroup\ElasticaBundle\SearchRepository;

use FOS\ElasticaBundle\Repository;

class RegistrationSearchRepository extends Repository
{
    public function findCountOfIdTypeAndIdNumber($idType, $idNumber)
    {
        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('identification', $idNumber);
        $bool->addMust(
            array(
                new \Elastica\Query\Term(array("identificationType" => $idType)),
                $fieldQuery
            ));
        $query->setQuery($bool);
        return count($this->find($query));
    }

    public function findCountOfIdNumber($idnumber)
    {
        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('identification', $idnumber);
        $bool->addMust(
            array(
                $fieldQuery
            ));
        $query->setQuery($bool);
        return count($this->find($query));
    }


    public function findCountByMsisdnAndSerial($msisdn, $simserial)
    {
        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $mustArray = array();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);


        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('simSerial', $simserial);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        return count($this->find($query));
    }

    public function findByMsisdnAndSerialOrRegistrationId($msisdn, $simserial, $registrationId)
    {
        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $mustArray = array();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);;
        $fieldQuery = new \Elastica\Query\Prefix();
        $fieldQuery->setPrefix('simSerial', $simserial);
        array_push($mustArray, $fieldQuery);
        $bool->addMust($mustArray);

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('registrationId.', $registrationId);
        $bool->addShould($fieldQuery);


        $query->setQuery($bool);
        return count($this->find($query));
    }

    public function findCountByMsisdn($msisdn)
    {
        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $mustArray = array();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        return count($this->find($query));
    }

    public function findByMsisdnAndSerial($msisdn, $simserial)
    {
        $query = new \Elastica\Query();
        $bool = new \Elastica\Query\Bool();
        $mustArray = array();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

//        $fieldQuery = new \Elastica\Query\Match();
//        $fieldQuery->setField('simSerial', $simserial);
        $fieldQuery = new \Elastica\Query\Prefix();
        $fieldQuery->setPrefix('simSerial', $simserial);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        return $this->find($query);
    }


}