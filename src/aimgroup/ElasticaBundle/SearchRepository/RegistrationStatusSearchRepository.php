<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 15/06/2016
 * Time: 3:25 PM
 */

namespace aimgroup\ElasticaBundle\SearchRepository;


use aimgroup\RestApiBundle\Dao\ImagesOperatorStatus;
use aimgroup\RestApiBundle\Dao\PlainTextOperatorStatus;
use Elastica\Query\Image;
use FOS\ElasticaBundle\Repository;

class RegistrationStatusSearchRepository extends Repository {

    public function getRegStatusByRegNoAndMsisdn($idnumber, $msisdn)
    {
        $query = new \Elastica\Query();

        $bool = new \Elastica\Query\Bool();
        $mustArray = array();

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('registrationId.registrationid', $idnumber);
        array_push($mustArray, $fieldQuery);

        $fieldQuery = new \Elastica\Query\Match();
        $fieldQuery->setField('registrationId.msisdn', $msisdn);
        array_push($mustArray, $fieldQuery);

        $bool->addMust($mustArray);
        $query->setQuery($bool);
        $data = $this->find($query);

        return $data;

    }

    /**
     * @param $attributes
     * @param $jtPageSize
     * @param $jtStartIndex
     *
     * @return array
     */
    public function listRegistrationStatus($attributes, $jtPageSize, $jtStartIndex)
    {
        $query = new \Elastica\Query();
        $query->addSort(array("registrationId.createdDate" => array('order' => 'desc')));

        $filterParams = array();
        $dateRangeParams = array();

        $boolean = new \Elastica\Query\Bool();

        if (isset($attributes["msisdn"]) && strlen($attributes["msisdn"]) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.msisdn', $attributes["msisdn"]);
            array_push($filterParams, $fieldQuery);
        }

        if (isset($attributes["first_name"]) && strlen($attributes["first_name"]) >= 2) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.firstName', $attributes["first_name"]);
            array_push($filterParams, $fieldQuery);
        }
        if (isset($attributes["last_name"]) && strlen($attributes["last_name"]) >= 2) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.lastName', $attributes["last_name"]);
            array_push($filterParams, $fieldQuery);
        }
        if (isset($attributes["identificationType"]) && $attributes["identificationType"] >= 1) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.identificationType.id', $attributes["identificationType"]);
            array_push($filterParams, $fieldQuery);
        }
        if (isset($attributes["id_number"]) && strlen(@$attributes["id_number"]) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.identification', $attributes["id_number"]);
            array_push($filterParams, $fieldQuery);

        }
        if (isset($attributes["agent_firstname"]) && strlen(@$attributes["agent_firstname"]) >= 3) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.firstName', $attributes["agent_firstname"]);
            array_push($filterParams, $fieldQuery);
        }
        if (isset($attributes["agent_lastname"]) && strlen(@$attributes["agent_lastname"]) >= 3) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.owner.lastName', $attributes["agent_lastname"]);
            array_push($filterParams, $fieldQuery);
        }
        if (isset($attributes["agent_phone"]) && strlen(@$attributes["agent_phone"]) >= 4) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.agentMsisdn', $attributes["agent_phone"]);
            array_push($filterParams, $fieldQuery);
        }

        if(isset($attributes['ekyc_status'])) {

            $ekyc_status = $attributes['ekyc_status'];

            if($ekyc_status == 'pending')
            {
                $rangeQuery = new \Elastica\Query\Range();
                $rangeQuery->addField('registrationId.image_count', array("lt" => 3));
                array_push($filterParams, $rangeQuery);

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::PENDING);
                array_push($filterParams, $fieldQuery);
            }
            elseif($ekyc_status == 'received')
            {
                $rangeQuery = new \Elastica\Query\Range();
                $rangeQuery->addField('registrationId.image_count', array("gte" => 3));
                array_push($filterParams, $rangeQuery);

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::PENDING);
                array_push($filterParams, $fieldQuery);
            }
            elseif( $ekyc_status == 'submitted')
            {
                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::SUCCESS);
                array_push($filterParams, $fieldQuery);
            }
            elseif ($ekyc_status == 'declined')
            {
                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::DECLINED);
                array_push($filterParams, $fieldQuery);
            }
        }


        if (isset($attributes["path"]) && strlen(@$attributes["path"]) >= 2) {
            $attributes["operator_status"] = $attributes["path"];
        }

        if (isset($attributes["operator_status"])) {
            $status = $attributes["operator_status"];
            if ($status == 'notsent') {
                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::PENDING);
                array_push($filterParams, $fieldQuery);
            } else if ($status == 'declined') {
                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::DECLINED);
                array_push($filterParams, $fieldQuery);

            } else if ($status == 'freg') {
                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::SUCCESS);
                array_push($filterParams, $fieldQuery);

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('fullRegStatus', ImagesOperatorStatus::SUCCESS);
                array_push($filterParams, $fieldQuery);

            } else if ($status == 'treg') {

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::SUCCESS);
                array_push($filterParams, $fieldQuery);

                $fieldQuery = new \Elastica\Query\Terms();
                $fieldQuery->setTerms('fullRegStatus', array(ImagesOperatorStatus::PENDING, ImagesOperatorStatus::DECLINED));

                array_push($filterParams, $fieldQuery);

            } else if ($status == 'pending') {
                $rangeQuery = new \Elastica\Query\Range();
                $rangeQuery->addField('registrationId.image_count', array("lte" => 3));
                array_push($filterParams, $rangeQuery);

                $fieldQuery = new \Elastica\Query\Term();
                $fieldQuery->setTerm('temporaryRegStatus', PlainTextOperatorStatus::PENDING);
                array_push($filterParams, $fieldQuery);

            } else if ($status == 'tobesent') {
                $fieldQuery = new \Elastica\Query\Terms();
                $fieldQuery->setTerms('temporaryRegStatus', array(0, 1));
                array_push($filterParams, $fieldQuery);
            }
        }

        if (@$attributes["region"]) {
            $fieldQuery = new \Elastica\Query\Match();

            $fieldQuery->setField('registrationId.region', $attributes["region"]);
            array_push($filterParams, $fieldQuery);
        }

        if (@$attributes["territory"] > 0) {
            $fieldQuery = new \Elastica\Query\Match();
            $fieldQuery->setField('registrationId.territory.id', $attributes["territory"]);
            array_push($filterParams, $fieldQuery);

        }

        if (strlen(@$attributes["from_date"]) > 4) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($attributes["from_date"]))->getTimestamp());
        }
        if (strlen(@$attributes["to_date"]) > 4) {
            $dateRangeParams['lte'] = \Elastica\Util::convertDate((new \DateTime($attributes["to_date"]))->getTimestamp());
        }


        if (strlen(@$attributes["date"]) > 4) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($attributes["date"]))->getTimestamp());
            $tomorrow = new \DateTime($attributes["date"]);
            $tomorrow->modify("+1 day");
            $dateRangeParams['lt'] = \Elastica\Util::convertDate($tomorrow->getTimestamp());
        }


        if (count($dateRangeParams) > 0) {
            $rangeQuery = new \Elastica\Query\Range();
            $rangeQuery->addField('registrationId.createdDate', $dateRangeParams);
            $boolean->addMust($rangeQuery);

        }

        if (count($filterParams) > 0) {
            $boolean->addMust($filterParams);
        }
        $query->setQuery($boolean);
        $data = $this->findPaginated($query);

        if (isset($jtPageSize) && isset($jtStartIndex)) {
            $data->setMaxPerPage($jtPageSize);
            $data->setCurrentPage($jtStartIndex);
        }

        //return array("count" => $data->getNbResults(), "results" => $data->getCurrentPageResults());

        $theResult = $data->getCurrentPageResults();
        $theCount = count($data) * $jtPageSize;

        //echo $theCount; exit;
        return array("count" => $theCount, "results" => $theResult);
    }


    public function getRegistrationJourney()
    {
        $query = new \Elastica\Query(new \Elastica\Query\MatchAll());

        $createdDate = new \Elastica\Script("abs(doc['registrationId.createdDate'].value-doc['registrationId.deviceSentTime'].value)/1000");

        $avgRegTime = new \Elastica\Aggregation\Avg("avg_reg_time");
        $avgRegTime->setScript($createdDate);


        $allimageDate = new \Elastica\Script("abs(doc['allimageDate'].value-doc['registrationId.deviceSentTime'].value)/1000");
        $avgImageTime = new \Elastica\Aggregation\Avg("avg_image_time");
        $avgImageTime->setScript($allimageDate);


        $tregDate = new \Elastica\Script("abs(doc['tregDate'].value-doc['registrationId.deviceSentTime'].value)/1000");
        $avgTRegTime = new \Elastica\Aggregation\Avg("avg_treg_time");
        $avgTRegTime->setScript($tregDate);

        $fregDate = new \Elastica\Script("abs(doc['fregDate'].value-doc['registrationId.deviceSentTime'].value)/1000");
        $avgFRegTime = new \Elastica\Aggregation\Avg("avg_freg_time");
        $avgFRegTime->setScript($fregDate);


        $dateAggregation = new \Elastica\Aggregation\DateHistogram('dateHistogram', 'registrationId.createdDate', 'day');
        $dateAggregation->setFormat("yyyy-MM-dd");

        $dateAggregation->addAggregation($avgRegTime);
        $dateAggregation->addAggregation($avgImageTime);
        $dateAggregation->addAggregation($avgTRegTime);
        $dateAggregation->addAggregation($avgFRegTime);

        $query->addAggregation($dateAggregation);
        $query->setSize(0);

        return $query;
    }

    public function getRegistrationStatsSummary($start_date, $end_date)
    {
        $query = new \Elastica\Query(new \Elastica\Query\MatchAll());
        $dateRangeParams = array();
        if (strlen($start_date)) {
            $dateRangeParams['gte'] = \Elastica\Util::convertDate((new \DateTime($start_date))->getTimestamp());
            $dateRangeParams['lte'] = \Elastica\Util::convertDate((new \DateTime($end_date))->getTimestamp());
        }


        $dateAggregation = new \Elastica\Aggregation\DateHistogram('dateHistogram', 'registrationId.createdDate', 'day');
        $dateAggregation->setFormat("yyyy-MM-dd");

        $terms = new \Elastica\Aggregation\Terms("image_count");
        $terms->setField("registrationId.image_count");
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);

        $terms = new \Elastica\Aggregation\Terms("icapState");
        $terms->setField("icapState");
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);

        $terms = new \Elastica\Aggregation\Terms("dbmsState");
        $terms->setField("dbmsState");
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);


        $terms = new \Elastica\Aggregation\Terms("mpesaState");
        $terms->setField("mpesaState");
        $terms->setSize(0);
        $dateAggregation->addAggregation($terms);

        $boolean = new \Elastica\Query\Bool();
        if (count($dateRangeParams) > 0) {
            $rangeQuery = new \Elastica\Query\Range();
            $rangeQuery->addField('registrationId.createdDate', $dateRangeParams);
            $boolean->addMust($rangeQuery);
            $query->setQuery($boolean);
        }
        $query->setSort(array("createdDate" => array('order' => 'desc')));
        $query->addAggregation($dateAggregation);
        $query->setSize(0);

        return $query;
    }
}