<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 13/06/2016
 * Time: 4:10 PM
 */

namespace aimgroup\ElasticaBundle\Transformer;


use aimgroup\RestApiBundle\Entity\Registration;
use Elastica\Document;
use FOS\ElasticaBundle\Transformer\ModelToElasticaTransformerInterface;

class UserToElasticaTransformation implements ModelToElasticaTransformerInterface
{

    /**
     * Transforms an object into an elastica object having the required keys.
     *
     * @param object $object the object to convert
     * @param array $fields the keys we want to have in the returned array
     *
     * @return \Elastica\Document
     **/
    public function transform($object, array $fields)
    {

        $identifier = $object->getId();

        $nestedAuthor = array(
            'username' => $object->getOwner()->getUsername()
        );

        $values = array(
            'id' => $object->getId(),
            "agentImei" => $object->getAgentImei(),
            "registrationid" => $object->getRegistrationid(),
            "agentMsisdn" => $object->getAgentMsisdn(),
            'sowner' => $object->getOwner(),

        );
        $document = new Document($identifier, $values);
        $document->setParent($object->getOwner()->getId());
        return $document;
    }
}