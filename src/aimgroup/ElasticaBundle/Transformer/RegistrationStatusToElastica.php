<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 01/07/2016
 * Time: 2:24 PM
 */

namespace aimgroup\ElasticaBundle\Transformer;


use aimgroup\RestApiBundle\Entity\RegistrationStatus;
use Elastica\Document;
use FOS\ElasticaBundle\Transformer\ModelToElasticaTransformerInterface;

class RegistrationStatusToElastica implements ModelToElasticaTransformerInterface
{

    /**
     * Transforms an object into an elastica object having the required keys.
     *
     * @param object $object the object to convert
     * @param array $fields the keys we want to have in the returned array
     *
     * @return \Elastica\Document
     **/
    public function transform($object, array $fields)
    {
        $identifier = $object->getId();

        $values = array(
            'id' => $object->getId(),
            'registrationId' => $object->getRegistrationId(),
            'temporaryRegDesc' => $object->getTemporaryRegDesc(),
            'temporaryRegStatus' => $object->getTemporaryRegStatus(),
            'fullRegStatus' => $object->getFullRegStatus(),
            'fullRegDes' => $object->getFullRegDesc()
        );

        //Create a document to index
        $document = new Document($identifier, $values);
        $document->setParent($object->getId());

        return $document;
    }
}