<?php
/**
 * Created by PhpStorm.
 * User: oscarmakala
 * Date: 16/06/2016
 * Time: 5:35 PM
 */

namespace aimgroup\ElasticaBundle\Transformer;


use FOS\ElasticaBundle\Doctrine\AbstractElasticaToModelTransformer;
use Doctrine\ORM\Query;

class ElasticToRegistrationStatus extends AbstractElasticaToModelTransformer
{

    /**
     * Fetches objects by theses identifier values.
     *
     * @param array $identifierValues ids values
     * @param Boolean $hydrate whether or not to hydrate the objects, false returns arrays
     *
     * @return array of objects or arrays
     */
    protected function findByIdentifiers(array $identifierValues, $hydrate)
    {
        if (empty($identifierValues)) {
            return array();
        }

        $hydrationMode = $hydrate ? Query::HYDRATE_OBJECT : Query::HYDRATE_ARRAY;
        $qb = $this->registry
            ->getManagerForClass('RestApiBundle:RegistrationStatus')
            ->getRepository('RestApiBundle:RegistrationStatus')
            ->createQueryBuilder('rs')
            ->select('rs,r')
            ->join('rs.registrationId', 'r');

        /* @var $qb \Doctrine\ORM\QueryBuilder */

        $qb->where($qb->expr()->in('rs.' . $this->options['identifier'], ':values'))
            ->setParameter('values', $identifierValues);

        return $qb->getQuery()->setHydrationMode($hydrationMode)->execute();
    }
}