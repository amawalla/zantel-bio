#!/bin/bash

#####################################################
#                                                   #
# @author Sam Samson <samdev06@gmail.com>           #
# @date Tue February 2017                           #
#                                                   #
# Process pending temporary registrations           #
#                                                   #
#####################################################


SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo ${SCRIPT_DIRECTORY}
. ${SCRIPT_DIRECTORY}"/parse_yaml.sh"

PROCESS_LABEL="plaintext"

pidfile="${SCRIPT_DIRECTORY}/bashlogs/${PROCESS_LABEL}.pid"
LOGDIR="${SCRIPT_DIRECTORY}/bashlogs/${PROCESS_LABEL}.log"
EKYC_DIRECTORY=${SCRIPT_DIRECTORY}/..
CONFIG="${EKYC_DIRECTORY}/app/config/parameters.yml"
TEMP="${SCRIPT_DIRECTORY}/bashlogs/${PROCESS_LABEL}.out"

SQL_QUERY="SELECT registrationId FROM RegistrationStatus WHERE temporaryRegStatus = 0 AND date(createdDate) >= '2017-05-01';"
EKYC_CONSOLE="${EKYC_DIRECTORY}/app/console"
EKYC_COMMAND='ekyc:registration:process treg'

echo "`date`  starting script at "; >> ${LOGDIR};

eval $(parse_yaml ${CONFIG} "m_")

#Check if there is another process running
if test -e "${pidfile}"
then
    #get the pid value from the file
    pid_id=`cat ${pidfile}`
    pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
    if [ ${pid_count} -ge 1 ]; then
        echo "`date` | Another process is still running ($pid_id)" >> ${LOGDIR}
        exit 1
    fi
fi

#Check if there is queue still being processed
processCount=$(ps -ef|grep ${EKYC_COMMAND}|wc -l)
if [ ${processCount} -gt 1 ];
then
    echo "`date` | Too many processes are currently running. Exit to avoid retry" >> ${LOGDIR}
    exit 1
fi

:> ${TEMP} #Empty temporary file to avoid multiple requests

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > ${pidfile};

$mysql_conn "${SQL_QUERY}" ${m_parameters__database_name} > ${TEMP}

if [ -s ${TEMP} ];
then
    while read id;
    do
      php ${EKYC_CONSOLE} ${EKYC_COMMAND} "${id}" &
    done < ${TEMP}
fi

:> ${TEMP}

rm -f ${pidfile}

exit 1
