#!/bin/bash
ROOT_DIR="/var/www/html/tigoEKYC/";
MYSQL_CONN="mysql -uroot tigo_ekyc"
#MYSQL_CONN="mysql -uroot -ppassword -h10.0.1.221 tigo_ekyc"

#php $ROOT_DIR."app"

function createAdmin(){
php $ROOT_DIR"app/console" fos:user:create tigomanager tigo@registersim.com admin
php $ROOT_DIR"app/console" fos:user:promote tigomanager ROLE_ADMIN
}

function initDabase(){
$MYSQL_CONN << EOF
INSERT IGNORE INTO Idtype (id,name,use_count) VALUES (1,'Voters ID',0),(2,'Drivers License',0),(3,'National ID',0),(4,'Passport',0),(5,'Business Licence',0),(6,'TIN Certificate',0),(7,'VAT Certificate',0),(8,'Certificate of Incorporation',0),(9,'Health Insurance ID',0),(10,'Employer ID',0),(11,'Pension Fund ID',0),(12,'NSSF Pension Card',0),(13,'Student ID',0),(14,'WEO/VEO/REF',0);
INSERT IGNORE INTO FieldNames (id,name) VALUES (13,'address'),(10,'alternative-contact'),(16,'back-pic'),(18,'confirm'),(20,'customer-confirmation'),(5,'dateofbirth'),(8,'email'),(9,'esn'),(1,'firstname'),(15,'front-pic'),(4,'gender'),(21,'id-number'),(7,'id-type'),(2,'middlename'),(6,'nationality'),(14,'potrait'),(17,'rear-pic'),(11,'region'),(19,'signature'),(3,'surname'),(12,'territory'),(22,'ward');
INSERT IGNORE INTO FieldTypes (id,name) VALUES (4,'camera'),(9,'choice'),(10,'confirm'),(7,'country'),(8,'date'),(2,'integer'),(6,'signature'),(3,'spinner'),(1,'string'),(11,'string-barcode'),(5,'text');
INSERT IGNORE INTO Region (id, name, code) VALUES
(1, 'Coast', 'LK'),
(2, 'South', 'CS'),
(3, 'North', 'NT'),
(4, 'Lake', 'LK'),
(5, 'Northsdf', 'NT'),
(6, 'Southsdf', 'CN');
INSERT IGNORE INTO Territory (id, region_id, name) VALUES
(89, 4, 'mwanza'),
(90, 4, 'kagera'),
(91, 1, 'pwani'),
(92, 1, 'morogoro'),
(93, 2, 'njombe'),
(94, 1, 'zanzibar'),
(95, 4, 'kigoma'),
(96, 1, 'mtwara'),
(97, 2, 'ruvuma'),
(99, 3, 'singida'),
(100, 4, 'shinyanga'),
(101, 3, 'arusha'),
(102, 3, 'manyara'),
(103, 4, 'mara'),
(104, 2, 'mbeya'),
(105, 2, 'rukwa'),
(106, 1, 'dar-es-salaam'),
(107, 3, 'dodoma'),
(108, 4, 'tabora'),
(109, 1, 'lindi'),
(110, 4, 'Geita'),
(111, 3, 'kilimanjaro'),
(112, 1, 'tanga'),
(113, 2, 'Katavi'),
(114, 2, 'iringa'),
(115, 2, 'simiyu');
INSERT IGNORE INTO UserType (id,name,description,createdOn) VALUES (1,'freelancer','Freelancer','2015-11-10 13:46:18'),(2,'retailer','Retailer','2015-11-10 13:46:28'),(3,'atm_kiosk','ATM Kiosks','2015-11-10 13:46:32'),(4,'shops','Shops','2015-11-10 13:46:35');
INSERT IGNORE INTO role (id,name,role,dept_permissions) VALUES (1,'ROLE ADMIN','ROLE_ADMIN','[2,3,37,36,39,40,27,26,42,43,44,45,23,29,30,31,32,33,34,21,20,19,18,17,16,15,13,14,9,8,11,10,6,5]'),(4,'ROLE USER','ROLE_USER','[0]'),(6,'ROLE QA','ROLE_QA','0'),(7,'ROLE TEST','ROLE_TEST','[2,3,23,21,20,19,18,17]'),(8,'ROLE_TEST','ROLE_NEW','0');
EOF
}

createAdmin
initDabase