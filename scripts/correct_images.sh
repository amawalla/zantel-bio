#!/bin/bash
#####
# @Author Mutisya Kimuyu
# @Date Tue May 2016
# 1. scripts to call and rectify registrations with wrong image count
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/recon.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_recon1.out"
yesterday=$(date -d "yesterday" '+%Y-%m-%d')

echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

url=$(echo "${m_parameters__hostname/\'/}")
url=$(echo "${url/\'/}")


$mysql_conn "update registration left join (select reg_images.registration, count(reg_images.id) as total_images from reg_images where createdOn >= '${yesterday} 00:00:00' group by reg_images.registration) as images on images.registration = registration.registrationid set registration.image_count = images.total_images where registration.image_count != images.total_images;" ${m_parameters__database_name}

$mysql_conn "UPDATE RegistrationStatus SET temporaryRegStatus = 0 WHERE temporaryRegStatus = 1 AND TIMESTAMPDIFF(MINUTE, tregDate, NOW()) >= 5;" ${m_parameters__database_name}

#empty the file
:> ${TEMP}


$mysql_conn "select CONCAT(\"UPDATE RegistrationStatus SET allimagedate = '\",max(reg_images.createdOn),\"' WHERE registrationId = \",registration.id,\";\") as ccode from registration left join RegistrationStatus on RegistrationStatus.registrationId = registration.id left join reg_images on reg_images.registration = registration.registrationid where registration.createdDate >= '${yesterday} 00:00:00' and allimageDate IS NULL group by registration.id having count(reg_images.id) > 3;" ${m_parameters__database_name} > ${TEMP}


if [ -s ${TEMP} ];
then
while read id;
do
echo "${id}"
$mysql_conn "${id}" ${m_parameters__database_name}
done < ${TEMP}
fi

:> ${TEMP}

rm -f ${pidfile}
exit 1

