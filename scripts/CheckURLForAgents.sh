#!/usr/bin/env bash
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

file=$1
CONFIG="${DIR}/../app/config/parameters.yml"
eval $(parse_yaml ${CONFIG} "m_")

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";
declare -a ARRAYURL=('https://s3.amazonaws.com/ereg2/vodacom/images/20160907/744500000_20160907151711_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/766437899_20160907123509_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/758164176_20160907145907_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/764428455_20160907165733_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/759509729_20160907165338_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/762252581_20160907155309_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/759868686_20160907165320_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/752248926_20160907103048_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/769903078_20160907123216_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/757402862_20160907123537_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/757312239_20160907123608_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/769418939_20160907123702_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/754329618_20160907123957_agent_signature_.jpeg' 'https://s3.amazonaws.com/ereg2/vodacom/images/20160907/759518835_20160907104006_agent_signature_.jpeg')
randArrayElement(){ arr=("${!1}"); echo ${arr["$[RANDOM % ${#arr[@]}]"]}; }


if [ -s "${file}" ];then
less "${file}"|while read line;
do
msisdn=$(echo $line|awk '{print $1}')
url=$(echo $line|awk '{print $2}')
status=$(curl -s --head -w %{http_code} "${url}" -o /dev/null)
if [ "${status}" -eq 404 ];
then
furl=$(randArrayElement "ARRAYURL[@]")
query="update AgentSignatures set webPath=\"${furl}\" where msisdn=${msisdn}"
$mysql_conn "${query}" ${m_parameters__database_name}
fi
done
fi
