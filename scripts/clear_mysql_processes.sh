#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
#echo ${DIR}
. $DIR/parse_yaml.sh

#eval $(parse_yaml ../app/config/parameters.yml "m_")
eval $(parse_yaml "${DIR}/../app/config/parameters.yml" "m_")

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} ";

mysqladmin_command="mysqladmin -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} ";

TIME=50
for ID in $( ${mysql_conn} information_schema -e "SELECT ID FROM processlist WHERE INFO REGEXP '^[Ss][Ee][Ll][Ee][Cc][Tt]' AND TIME >= ${TIME};" )
do
  echo -n "Killing thread ${ID}..."
  ${mysqladmin_command} kill ${ID}
  echo "Done"
done

exit 1

