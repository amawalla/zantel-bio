#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 2016
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/freg.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_mfreg.out"
num_files=20
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
        #get the pid value from the file
        pid_id=`cat $pidfile`
        pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
        if [ $pid_count -ge 1 ];then
                echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
                exit 1
        fi
fi

countR=$(ps -ef|grep 'rest_api:vodacom_processor_command freg'|wc -l)
if [ $countR -gt 1 ];
then
                echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
                exit 1
fi



mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;
:> ${TEMP}
#checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
$mysql_conn "select registration.registrationid from RegistrationStatus inner join registration on RegistrationStatus.registrationId = registration.id where temporaryRegStatus in (2,6) and fullRegStatus in (0) and image_count >= 4 and RegistrationStatus.verifyState = 1 and left(RegistrationStatus.createdDate,10) >= '2016-11-01' limit 1000 " ${m_parameters__database_name} > ${TEMP}

count=0
if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
((lines_per_file = (total_lines + num_files - 1) / num_files))
split --lines=${lines_per_file} ${TEMP} fregxyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in fregxyzzy.*;
do
id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/vodacomEKYCLive/app/console -e=prod rest_api:vodacom_processor_command "freg" "${id}" &
done
fi

rm ${TEMP}
echo "ending script at `date`";

if [ "${hour}" -eq "00" ];then
if [ "${minute}" -lt "10" ];then
`${DIR}/mqtt_clear_app_data.sh`
fi

fi
rm -f $pidfile
exit 1
