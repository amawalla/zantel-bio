#!/bin/sh
#name            :reset-verify-lock.sh
#description     :This script resets locked registrations so they can be verified.
#author          :Salim Said
#date            :June 12 2017 14:16 
#version         :CVS 1.0.0    
#usage           :sh reset-verify-lock.sh
#==============================================================================

php /var/www/html/zantel-ekyc/live/scripts/reset-verify-lock.php
