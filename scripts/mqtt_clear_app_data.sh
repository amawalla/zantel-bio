#!/bin/bash
### desc ###
#@author Oscar Makala
#scripts gets all devices from the devices. and fires the command which will clear
#past records both notifications and requests.
####

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"


LOGDIR="${DIR}/notify.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/messaging_notify.out"


eval $(parse_yaml ${CONFIG} "m_")


event="${m_parameters__mqtt_clear_prev_trans}"
topic_main=$(echo "${m_parameters__mqtt_topic}" |sed -e "s/{2}/${event}/")

operator=$(echo "${m_parameters__operator_name}" | tr '[:upper:]' '[:lower:]')
topic=$(echo "${topic_main}"| sed -e "s/{1}/${operator}/"|sed "s/'//g")
`mosquitto_pub -h "${m_parameters__mqtt}" -t "$topic" -m '{"deleteInterval":7,"deleteNotificationInterval":1}' -q '2'`