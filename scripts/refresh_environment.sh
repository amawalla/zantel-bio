#!/bin/bash
now=$(date +"%Y-%m-%d:%H:%M:%S")
log_file=/var/log/refresh_service.log
echo "Starting at: $now" >> $log_file
echo "Clearing memory...." >> $log_file
sudo sync; echo 1 > /proc/sys/vm/drop_caches
echo ".....done" >> $log_file
echo "Reloading NGINX..." >> $log_file
sudo service nginx reload
echo ".....done" >> $log_file
echo "Restarting PHP5-FPM" >> $log_file
sudo service php5-fpm restart >> $log_file
echo "....done" >> $log_file
now=$(date +"%Y-%m-%d:%H:%M:%S")
echo "Finished at: $now" >> $log_file
echo "========================================" >> $log_file
