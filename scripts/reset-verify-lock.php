<?php 
/**
 * reset-verify-lock.php
 * This script resets locked registrations so they can be verified
 * Resets verifyState field from -1 to 0
 * Collect records that have been locked starting from 3 minutes ago
 *
 * @category   Database scripts
 * @author     Salim Said | June 12 2017 14:16 
 * @copyright  2017 Aim Group (T) Limited
 * @license    internal
 * @version    CVS: 1.0.0
 */

//database params
$user = 'zantelroot';
$password = 'Op3n4m3bY200%';
$host = '192.168.101.2';
$database = 'zantel_ekyc';
$options = '';
$logFile = '/var/log/scriptLogs/reset-verify-lock.log';
$conn;

run();

function connect(){
	global $conn,$host,$user,$password,$database;
	$conn = new mysqli($host, $user, $password, $database);
	// Check connection
	if ($conn->connect_error) {
		logInfo("Failed to connect " . $conn->connect_error);
		die("Connection failed: " . $conn->connect_error);
	}
	logInfo("Connection to database successful ");
	return true;
}

function resetVerifyLock(){
	global $conn;
	$updateQuery = "update RegistrationStatus set verifyState = 0 where verifyState = -1 and verifyLock <= date_sub(now(), interval 3 minute )";
	if ($conn->query($updateQuery) === TRUE) {
		logInfo("Records reset Successfully");
	} else {
		 logInfo("Error reseting records " . $conn->error);
	}
	$conn->close();
}

function run(){
	connect();
	resetVerifyLock();
}

function logInfo($message,$messageHeader=null){
	global $logFile;
	$fileHandle = fopen($logFile, "a+");
	$timeStamp = date("d-n-Y G:ia");
	$message = $timeStamp . $messageHeader . "\n" . $message;
	fwrite($fileHandle, $message);
	fclose($fileHandle);

}

?>
