#!/bin/bash

#####################################################
#                                                   #
# @author Sam Samson <samdev06@gmail.com>           #
# @date Mon August 2017                             #
#                                                   #
#####################################################


SCRIPT_DIRECTORY="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo ${SCRIPT_DIRECTORY}
. ${SCRIPT_DIRECTORY}"/parse_yaml.sh"


EKYC_DIRECTORY=${SCRIPT_DIRECTORY}/..
CONFIG="${EKYC_DIRECTORY}/app/config/parameters.yml"

eval $(parse_yaml ${CONFIG} "m_")

lastXminutes=`date --date='5 minutes ago' +%Y-%m-%d:%H:%M`

mysql_conn="mysql -u${m_parameters__database_user} -p${m_parameters__database_password} -h${m_parameters__database_host} -ss -e";

#$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE date(verifyDate) <= '${lastXminutes}' AND verifyState IN(-1) AND createdDate >= '2017-08-10';" ${m_parameters__database_name} 

#$mysql_conn "UPDATE RegistrationStatus set verifyState = 2 WHERE date(verifyDate) <= '${lastXminutes}' AND verifyState IN(-2) AND createdDate >= '2017-08-10';" ${m_parameters__database_name}

$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE verifyState IN(-1) AND TIMESTAMPDIFF(MINUTE, verifyLock , NOW()) > 5;" ${m_parameters__database_name}

$mysql_conn "UPDATE RegistrationStatus set verifyState = 0 WHERE verifyState IN(-2) AND TIMESTAMPDIFF(MINUTE, verifyLock , NOW()) > 5;" ${m_parameters__database_name}
exit 1

