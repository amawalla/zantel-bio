#!/usr/bin/env bash


file=$1
message='{"operatorName":"Vodacom","operatorId":2,"id":2,"operator":2,"message":"Ndugu Wakala, tafadhali pakua mfumo (v2.0.13) mpya iliyoboreshwa  ya kusajili wateja https://vodalive.registersim.com/appdownload"}';
if [ "x$file" != "x" ];then
    less $file |awk -F',' 'NR>1 {print "iot-2/Device/id/"$2"/evt/notification/fmt/json"}' |while read topic;
    do
        echo $topic;
        `mosquitto_pub -h messaging.registersim.com -t "${topic}" -m "${message}"`
    done
fi