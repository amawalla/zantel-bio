#!/bin/bash
#####
# @Author Oscar Makalr Oscar Makala
# @Date Tue May 816
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/recon.pid"
LOGDIR="${DIR}/bashlogs/recon.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/tigo_recon1.out"
num_files=20
echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")


if test -e "$pidfile"
then
#get the pid value from the file
pid_id=`cat $pidfile`
pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
if [ $pid_count -ge 1 ];then
echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
exit 1
fi
fi
tregCount=$(ps -ef|grep 'rest_api:vodacom_processor_command treg'|wc -l)
if [ $tregCount -gt 1 ];
then
echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
exit 1
fi




id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
php /var/www/html/environments/simreg/test/app/console biometric:registration:verify
rm -f $pidfile
exit 1
