#!/bin/bash
#####
# @Author Oscar Makala
# @Date Tue May 816
# 1. script checks all numbers that are in registration table, but have no status. sends them to treg
# 2. checks all numbers that have been sent to treg succesfully, but have not been sent to freg or have
# been only submitted
#
####
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
echo $DIR
. $DIR"/parse_yaml.sh"

pidfile="${DIR}/plaintext.pid"
LOGDIR="${DIR}/bashlogs/plaintext.log"
CONFIG="${DIR}/../app/config/parameters.yml"
TEMP="${DIR}/vodacom_recon1.out"
num_files=10
today=$(date +"%Y-%m-%d")

echo "`date`    starting script at "; >> $LOGDIR;

eval $(parse_yaml ${CONFIG} "m_")

#Kill zombie processes that took more than 2 minutes
#if [[ "$(uname)" = "Linux" ]];then killall --older-than 2m 'php';fi

if test -e "$pidfile"
then
#get the pid value from the file
pid_id=`cat $pidfile`
pid_count=`ps -ef|grep " $pid_id "|grep -v 'grep' | wc -l`
if [ $pid_count -ge 1 ];then
echo "`date` | another icap resend module is still running ($pid_id)" >> $LOGDIR
exit 1
fi
fi

tregCount=$(ps -ef|grep 'ekyc:registration:process treg'|wc -l)
if [ $tregCount -gt 80 ];
then
echo "`date` | Still running another batch of files. to avoid retry" >> $LOGDIR
exit 1
fi




:> ${TEMP}
mysql_conn="mysql -u${m_parameters__database_user} -h${m_parameters__database_host} -ss -e";

echo "$$" > $pidfile;
#$mysql_conn "  select registrationid From registration where id not in (select registrationId from RegistrationStatus where registrationId is NOT NULL and left(createdDate,10) = current_date) and left(createdDate,10) = current_date  order  by id desc limit 1000" ${m_parameters__database_name} > ${TEMP}
#if [ -s ${TEMP} ];
#then
#total_lines=$(wc -l <${TEMP})
#((lines_per_file = (total_lines + num_files - 1) / num_files))
#split --lines=${lines_per_file} ${TEMP} xyzzy.
#echo "Total lines     = ${total_lines}"
#echo "Lines  per file = ${lines_per_file}"

#for f in xyzzy.*;
#do
#id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
#php /var/www/html/vodacomEKYCLive/app/console rest_api:vodacom_processor_command "treg" "${id}" &
#done
#fi

#:> ${TEMP}

#sleep 5

count=0

$mysql_conn "select registrationId from RegistrationStatus where temporaryRegStatus =0 and  verifyState =1 and createdDate >= subdate(current_date, 3) ORDER BY id DESC LIMIT 100; "  ${m_parameters__database_name} > ${TEMP}

if [ -s ${TEMP} ];
then
total_lines=$(wc -l <${TEMP})
#((lines_per_file = (total_lines + num_files - 1) / num_files))
lines_per_file=1
split --lines=${lines_per_file} ${TEMP} xyzzy.
echo "Total lines     = ${total_lines}"
echo "Lines  per file = ${lines_per_file}"

for f in xyzzy.*;
do
  id=$(less ${f} | awk -vORS=, '{ print $1 }' | sed 's/,$/\n/')
  ${mysql_conn} "UPDATE RegistrationStatus SET temporaryRegStatus = 1, tregDate = NOW() WHERE RegistrationStatus.registrationId IN (${id}) " ${m_parameters__database_name}
  php /var/www/html/environments/simreg/test/app/console --env=prod ekyc:registration:process "treg" "${id}" & 
  rm $f
  #sleep 3
 # exit;
done

fi

#sleep 5
#empty the file
:> ${TEMP}


rm -f $pidfile
exit 1
