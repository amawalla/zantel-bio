<?php

use PHPUnit\Framework\TestCase;

class FooTest  extends TestCase
{
    public function testValueFirst()
    {
        $this->assertTrue(true);
        return 'first';
    }

    public function testValueSecond()
    {
        $this->assertTrue(true);
        return 'second';
    }

    /**
     * @depends testProducerFirst
     * @depends testProducerSecond
     */
    public function testValues()
    {
        $this->assertEquals(
            ['first', 'second'],
            func_get_args()
        );
    }
}