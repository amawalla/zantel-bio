/**
 * Created by oscar on 11/10/15.
 */
var saveSystemConfigurations = function (event) {
    event.preventDefault();
    showBusyDialog();
    var fields = $("#EndpointConfigurationsForm").serializeArray();
    var json = {};
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });
    var request = $.ajax({
        url: window.URL_SAVE_ENDPOINT_CONFIG,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(json),
        timeout: 10000
    });

    request.done(function (data) {
        hideBusyDialog();
        if (data != undefined) {
            alert(data.message);
            if (data.status) {
                location.reload();
            }
        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        alert("SYSTEM ERROR");
    });
};

var loadSavedEnpointConfigurations = function () {

    $.getJSON(URL_GET_CONFIGS, function (data) {
        var inputs = document.getElementById("EndpointConfigurationsForm").elements;
        for (var i = 0; i < inputs.length; i++) {
            $.each(data.item, function (index, item) {
                var json = JSON.parse(item.config);
                for (key in json) {
                    if (inputs[i].name == key) {
                        $("#" + inputs[i].id).val(json[key]);
                    }
                }
            });
        }
    });

};

var loadTopicSelector = function (operator) {
    if (keyValuePair == undefined || keyValuePair.length == 0) {
        $.getJSON(URL_GET_TOPICS, function (data) {
            keyValuePair = new Array();
            availableTags = new Array();
            $.each(data.item, function (index, item) { // Iterates through a collection
                keyValuePair[item.msisdn] = item.deviceId;
                availableTags.push(item.msisdn);
            });
            keyValuePair[operator] = operator;
            availableTags.push(operator);
            $("#form-msisdn").autocomplete({
                source: availableTags
            });
        });
    }

    $('#publishDyanmicFormModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
};

var publishMQTT = function (event) {
    event.preventDefault();

    var msisdn = $("#form-msisdn").val();
    if (msisdn != undefined) {
        //get the device id
        var deviceId = keyValuePair[msisdn];
        //get the text area
        var fields = $("#EndpointConfigurationsForm").serializeArray();
        var json = {};
        jQuery.each(fields, function (i, field) {
            json[field.name] = field.value;
        });

        json["deviceId"] = deviceId;
        json["type"] = "endpointupdate";
        var topic = $("#publishDyanmicFormModal #form-topic").val();

        var request = $.ajax({
            url: URL_PUBLISH_SYSCONFIG,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(json),
            timeout: 10000
        });

        request.done(function (response) {
            hideBusyDialog();
            if (response != undefined) {
                alert(response.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });
    }

};
