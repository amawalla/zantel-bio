/**
 * Created by oscar on 12/4/15.
 */

var URL_SAVE_FORM = "/admin/forms/save_form";
var GET_STEPS_URL = "/admin/forms/lists";
var CREATE_STEPS_URL = "/admin/forms/createStep";
var UPDATE_STEPS_URL = "/admin/forms/updateStep";
var GET_FIELDS_URL = "/admin/forms/listStepField";
var CREATE_FIELDS_URL = "/admin/forms/createStepField";
var UPDATE_FIELDS_URL = "/admin/forms/updateStepField";
var URL_GET_USER_FIELD_KEYS = "/admin/forms/listFieldKeys";
var URL_GET_USER_FIELD_TYPE = "/admin/forms/listFieldTypes";
var URL_GET_TOPICS = "/admin/app_config/get_topics";
var URL_PUBLISH_FORM = "/admin/forms/publish";

var keyValuePair = new Array();
var availableTags = new Array();

//caches
var cachedKeys = null;
var cachedUserFieldType = null;

var loadCreatedDynamicForms = function () {
    //Prepare jTable
    $('#DynamicFormTableContainer').jtable({
        title: 'DynamicTable',
        actions: {
            listAction: GET_STEPS_URL,
            createAction: CREATE_STEPS_URL,
            updateAction: UPDATE_STEPS_URL,
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            Fields: {
                title: '',
                width: '2%',
                sorting: false,
                edit: false,
                create: false,
                display: function (stepData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="/assets/img/list_metro.png" title="Edit Devices" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#DynamicFormTableContainer').jtable('openChildTable',
                            $img.closest('tr'),
                            {
                                title: 'FIELDS',
                                actions: {
                                    listAction: GET_FIELDS_URL + '?stepId=' + stepData.record.id,
                                    createAction: CREATE_FIELDS_URL,
                                    updateAction: UPDATE_FIELDS_URL,
                                },
                                fields: {
                                    id: {
                                        type: 'hidden',
                                        defaultValue: stepData.record.id
                                    },
                                    fieldId: {
                                        key: true,
                                        create: false,
                                        edit: false,
                                        list: false
                                    },
                                    skey: {
                                        title: 'KEY',
                                        width: '10%',
                                        options: function () {
                                            if (cachedKeys) {
                                                return cachedKeys;
                                            }
                                            var options = [];
                                            $.ajax({
                                                url: URL_GET_USER_FIELD_KEYS,
                                                type: 'GET',
                                                async: false,
                                                dataType: 'json',
                                                success: function (result) {
                                                    if (result.Result != 'OK') {
                                                        alert(result.Message);
                                                        return;
                                                    }
                                                    options = result.Options;
                                                }
                                            });
                                            return cachedKeys = options;
                                        }
                                    },
                                    hint: {
                                        title: 'HINT',
                                        width: '10%',
                                    },
                                    hintLangAlt: {
                                        title: 'ALT HINT',
                                        width: '10%',
                                    },
                                    type: {
                                        title: 'TYPE',
                                        width: '2%',
                                        options: function () {
                                            if (cachedUserFieldType) {
                                                return cachedUserFieldType;
                                            }
                                            var options = [];
                                            $.ajax({
                                                url: URL_GET_USER_FIELD_TYPE,
                                                type: 'GET',
                                                async: false,
                                                dataType: 'json',
                                                success: function (result) {
                                                    console.log(result);
                                                    if (result.Result != 'OK') {
                                                        alert(result.Message);
                                                        return;
                                                    }
                                                    options = result.Options;
                                                }
                                            });
                                            return cachedUserFieldType = options;
                                        }
                                    },
                                    required: {
                                        title: 'REQUIRED',
                                        width: '2%',
                                        options: {1: 'TRUE', 0: 'FALSE'}
                                    },
                                    regex: {
                                        title: 'REGEX',
                                        width: '10%'
                                    },
                                    regexError: {
                                        title: 'R-Error',
                                        width: '10%'
                                    },
                                    regexErrorAlt: {
                                        title: 'R-Error Alt',
                                        width: '10%'
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });

                    //Return image to show on the person row
                    return $img;
                }
            },
            step: {
                title: 'Current',
                width: '20%',
                edit: false,
                create: false
            },
            next: {
                title: 'Next',
                width: '20%'
            },
            title: {
                title: 'Title',
                width: '20%'
            },
            titleAlt: {
                title: 'Alt Title',
                width: '20%'
            }
        }
    });

    //Load person list from server
    $('#DynamicFormTableContainer').jtable('load');


}

var saveDynamicForm = function (event) {
    event.preventDefault();
    showBusyDialog();

    try {
        var request = $.ajax({
            url: URL_SAVE_FORM,
            type: 'get',
            timeout: 10000
        });

        request.done(function (response) {
            console.log(response);
            hideBusyDialog();
            if (response != undefined) {
                var json = JSON.parse(response);
                alert(json.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });
    } catch (err) {
        alert("CORRECT JSON VALUE");
    }
};

var loadTopicSelector = function () {
    $.getJSON(URL_GET_TOPICS, function (data) {
        $.each(data.item, function (index, item) { // Iterates through a collection
            keyValuePair[item.msisdn] = item.deviceId;
            availableTags.push(item.msisdn);
        });
        availableTags.push("MOAT");
        keyValuePair.push("moat", "moat");


        $("#form-msisdn").autocomplete({
            source: availableTags
        });
    });

    $('#publishDyanmicFormModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
};

var publishDynamicForm = function () {
    showBusyDialog();
    var msisdn = $("#form-msisdn").val();
    if (msisdn != undefined) {
        //get the device id
        var deviceId = keyValuePair[msisdn];

        var request = $.ajax({
            url: URL_PUBLISH_FORM,
            type: 'POST',
            data: {
                deviceId: deviceId,
                type: "formConfig",
            },
            timeout: 10000
        });

        request.done(function (response) {
            hideBusyDialog();
            if (response != undefined) {
                var json = JSON.parse(response);
                alert(json.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });
    }
}
