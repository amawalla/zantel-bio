var URL_CHANGE_PASSWORD = "passwordChange";
var URL_GET_REGIONS = "/admin/region/list_regions";
var URL_GET_USERTYPES = "/admin/users/list_userTypes";
var URL_CREATE_AGENT = "/admin/agents/createUser";
var URL_CREATE_MANAGERS = "/admin/users/createManagers";
var URL_LIST_MULTIPLE_REGISTRATIONS = "/admin/list_verify_registrations/multiple/all";
var URL_UPDATE_AGENTS = "/admin/agents/updateUser";
var URL_GET_TERRITORY = "/admin/region/list_territories";
var URL_GET_DEVICES = "/admin/device/list_devices";
var URL_CREATE_DEVICES = "/admin/device/create";
var URL_UPDATE_DEVICES = "/admin/device/updateAction";
var URL_GENERATE_ACCESS_TOKEN = "/admin/device/accessToken";
var URL_CHECKSESSION = "/admin/session_page";
var keyValuePair = new Array();
var availableTags = new Array();
var URL_NEWVERIFYACTION_REGISTRATIONS = "/verify/verify_registration_post/" + new Date().getTime();
//var URL_VERIFYACTION_REGISTRATIONS = "/admin/verifyaction_registrations/" + new Date().getTime();
var URL_VERIFYACTION_REGISTRATIONS2 = "/api/registration/api_callback/";
var URL_VERIFYACTION_REGISTRATIONS = "/verify/newverifyaction_registrations/" + new Date().getTime();
var URL_LIST_AUDIT_REGISTRATIONS = "/admin/list_audit_registrations";
var URL_AUDIT_REGISTRATIONS = "/admin/audit_registrations";
var URL_LIST_VERIFY_REGISTRATIONS = "/admin/list_verify_registrations/search/";
var URL_LIST_VERIFY_REGISTRATIONSVIEW = "/admin/list_verify_registrations/list/" + 1;
var image_count = 0;
var req_image_count = 0;
var record_row = 0;
var record_ccount = 0;

var URL_LISTVERIFYDETAIL_REGISTRATIONS = "/admin/list_verify_daily_report_detail/";


/*
 {
 "Result":"OK",
 "Records":[
 {"PersonId":1,"Name":"Benjamin Button","Age":17,"RecordDate":"\/Date(1320259705710)\/"},
 {"PersonId":2,"Name":"Douglas Adams","Age":42,"RecordDate":"\/Date(1320259705710)\/"},
 {"PersonId":3,"Name":"Isaac Asimov","Age":26,"RecordDate":"\/Date(1320259705710)\/"},
 {"PersonId":4,"Name":"Thomas More","Age":65,"RecordDate":"\/Date(1320259705710)\/"}
 ]
 }
 
 
 ms","region":"LK","territory":"89"},
 
 */

var queryString = window.location.href.slice(window.location.href.indexOf('?'));
queryString = queryString && queryString.length > 1 ? queryString : '';
var URL_LIST_REGISTRATIONS = "/admin/list_registrations" + queryString;
// var URL_LIST_REGISTRATIONS = "/admin/registration/list" + queryString;


var loadRegistrationsTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_REGISTRATIONS
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            created_on: {
                title: 'Date'
            },
            msisdn: {
                title: 'MSISDN'
            },
            customerName: {
                title: 'Name'
            },
            identificationType: {
                title: 'ID Type'
            },
            username: {
                title: 'Agent Number'
            },
            type: {
                title: 'Module'
            },
            agentName: {
                title: 'AgentName'
            },
            region: {
                title: 'Region'
            },
            territory: {
                title: 'Territory'
            },
            aimStatus: {
                title: 'EReg Status'
            },
            verifyState: {
                title: 'VerifyState'
            },
            icapStatus: {
                title: 'Operator'
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    if (data.record) {
                        return "<a href='../registration_info/" + data.record.id + "'>View</a>";
                    }
                }
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        }
    });

    $('#RegistrationsTableContainer').jtable('load', {
        path: getParameterByName('path'),
        date: getParameterByName('date')
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').on('click', function (e) {
        e.preventDefault();

        checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            sim_serial: $('#sim_serial').val(),
            id_number: $('#id_number').val(),
                    from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val(),
            ereg_status: $('#ereg_status').val()
        });
    });

    $('#LoadRecordsButtonBBreset').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#icap_status').val("");
        $('#ereg_status').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

}


var verifyRegistrationsTables = function (recordsLimit) {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: false,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_VERIFY_REGISTRATIONS + recordsLimit
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'FingerPrint',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('XXverify');
            //verifyActionListen();
            $('.jtable').ReStable({
                rowHeaders: false
            });
            actionListen();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            territory: $('#territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

    $('#LoadRecordsButtonBBresetVeriy').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });

}


var loadReportDetTables = function (dday, sstate) {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LISTVERIFYDETAIL_REGISTRATIONS + dday + "/" + sstate + "/" + record_ccount + "/" + new Date().getTime()
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            created_on: {
                title: 'Date'
            },
            msisdn: {
                title: 'MSISDN'
            },
            customerName: {
                title: 'Name'
            },
            identificationType: {
                title: 'ID Type'
            },
            username: {
                title: 'Agent Number'
            },
            agentName: {
                title: 'AgentName'
            },
            region: {
                title: 'Region'
            },
            territory: {
                title: 'Territory'
            },
            aimStatus: {
                title: 'EReg Status'
            },
            verifyState: {
                title: 'VerifyState'
            },
            icapStatus: {
                title: 'Operator'
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    if (data.record) {
                        return "<a href='../registration_info/" + data.record.id + "'>View</a>";
                    }
                }
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
            //record_ccount = 
        }
    });

    $('#RegistrationsTableContainer').jtable('load', {
        path: getParameterByName('path'),
        date: getParameterByName('date')
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').on('click', function (e) {
        e.preventDefault();

        checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            sim_serial: $('#sim_serial').val(),
            id_number: $('#id_number').val(),
                    from_date: $('#from_date').val(),
            to_date: $('#to_date').val(),
            icap_status: $('#icap_status').val(),
            ereg_status: $('#ereg_status').val()
        });
    });

    $('#LoadRecordsButtonBBreset').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#from_date').val("");
        $('#to_date').val("");
        $('#icap_status').val("");
        $('#ereg_status').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

}


var loadRegistrationsMultipleTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_MULTIPLE_REGISTRATIONS
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '15%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('multiple');
            $('.jtable').ReStable({rowHeaders: false});
            auditListen();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            territory: $('#territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();


}


var ViewVerifyRegistrationsTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_VERIFY_REGISTRATIONSVIEW
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'FingerPrint',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '1%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('verify_list');
            //verifyActionListen();
            $('.jtable').ReStable({
                rowHeaders: false
            });
            actionListen();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            verifyfrom_date: $('#verifyfrom_date').val(),
            verifyto_date: $('#verifyto_date').val(),
            verifyDescr: $('#verifyDescr').val(),
            verifyBy: $('#verifyBy').val(),
            verifyState: $('#verifyState').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();

    $('#LoadRecordsButtonBBresetVeriy').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#customer_msisdn').val("");
        $('#first_name').val("");
        $('#last_name').val("");
        $('#id_type_name').val("");
        $('#id_number').val("");
        $('#agent_firstname').val("");
        $('#agent_lastname').val("");
        $('#agent_phone').val("");
        $('#search_region').val("");
        $('#search_territory').val("");
        $('#verifyfrom_date').val("");
        $('#verifyto_date').val("");
        $('#verifyState').val("");
        $('#verifyBy').val("");
        $('#verifyDescr').val("");
        $('#RegistrationsTableContainer').jtable('load');
    });

}


var loadRegistrationsAuditTables = function () {

    $('#RegistrationsTableContainer').jtable({
        title: 'Registrations Table',
        paging: true,
        create: false,
        sorting: false,
        actions: {
            listAction: URL_LIST_AUDIT_REGISTRATIONS
        },
        fields: {
            id: {
                key: true,
                list: false,
                create: false
            },
            jsonddata: {
                width: '15%',
                title: 'Data'
            },
            CustomAction4: {
                title: 'Portrait',
                width: '12%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction1: {
                title: 'IDFront',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction2: {
                title: 'IDBack',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction3: {
                title: 'Signature',
                width: '10%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                    "sdf"
                }
            },
            CustomAction: {
                title: '',
                width: '15%',
                sorting: false,
                create: false,
                edit: false,
                list: true,
                display: function (data) {
                }
            }
        },
        recordsLoaded: function (event, data) {
            styleandfetch('audit');
            $('.jtable').ReStable({
                rowHeaders: false
            });
            auditListen();
        }
    });

    $('#RegistrationsTableContainer').jtable('load');

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#RegistrationsTableContainer').jtable('load', {
            created_on: $('#customer_msisdn').val(),
            msisdn: $('#customer_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            identificationType: $('#id_type_name').val(),
            id_number: $('#id_number').val(),
            agent_firstname: $('#agent_firstname').val(),
            agent_lastname: $('#agent_lastname').val(),
            agent_phone: $('#agent_phone').val(),
            territory: $('#territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();


}

function verifyActionListen() {

    $('.verify_btn').click(function () {
        //SEND TO FULL

        var recid = $(this).attr('fooID');

        $('.lloading.' + recid).show();

        var ccolor = '#23C6C8';
        if ($(this).attr('aaction') == 'decline') {
            ccolor = '#ED5565';
        }
        $('.lloading.' + recid).css('color', ccolor);

        jQuery.ajax({
            url: URL_VERIFYACTION_REGISTRATIONS + '/' + recid + '/' + $(this).attr('aaction'),
            type: "GET",
            dataType: "json",
            success: function (data) {

                callAnto(data.recid);
                $('.lloading.' + recid).hide();
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>" + data.message + "</p>");
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', ccolor);
            },
            error: function () {
                $('.lloading.' + recid).hide();
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>Error. Refresh Page</p>");
                $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', ccolor);
            }
        });


    });
}

function callAnto(id) {

    jQuery.ajax({
        url: URL_VERIFYACTION_REGISTRATIONS2 + '/' + id,
        type: "GET",
        dataType: "json",
        success: function (data) {
            //$('.lloading.' + recid).hide();
            $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>Successfully sent to Tigo</p>");
            //$('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', ccolor);
        },
        error: function () {
            //$('.lloading.' + recid).hide();
            $('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv').html("<p class='msgg'>Error22. Refresh Page</p>");
            //$('.jtable tbody tr[data-record-key="' + data.recid + '"] td .msggdiv .msgg').css('background', ccolor);
        }
    });

}

function styleandfetch(verify) {
    $(".jtable tbody tr td:first-child").each(function () {


        var text = $(this).html();
        $(this).html("");
        var object = JSON.parse(text);

        var recordId = "";
        var myHtml = "";
        myHtml = "<ul class='list-unstyled' style='margin: 10px;line-height: 17px; font-size:12px;'>";
        $.each(object, function (key, val) {
            if (key === "id") {
                recordId = val;
            }
            myHtml += "<li class='" + key + "'><strong>" + key + "</strong>: <span>" + val + "</span></li>";
        });
        myHtml += "</ul>";

        $(this).html(myHtml);


        var portrait = "<a href='javascript:;' class='potrait reg_picc white-modal-80'><img class=' " + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "potrait'></a>";
        var frontId = "<a href='javascript:;' class='front-pic reg_picc white-modal-80'><img  class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "front-pic'></a>"
        var backID = "<a href='javascript:;' class='rear-pic reg_picc white-modal-80'><img class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "rear-pic'></a>";
        var signature = "<a href='javascript:;' class='signature reg_picc white-modal-80'><img class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "signature'></a>";
        var fingerprint = "<a href='javascript:;' class='signature reg_picc white-modal-80'><img class='" + $(this).parent().data('record-key') + " regimagee' name='" + $(this).parent().data('record-key') + "signature'></a>";

        $($(this).parent().find('td:nth-child(2)')).html(portrait);
        $($(this).parent().find('td:nth-child(3)')).html(frontId);
        $($(this).parent().find('td:nth-child(4)')).html(backID);
        $($(this).parent().find('td:nth-child(5)')).html(signature);
        $($(this).parent().find('td:nth-child(5)')).html(fingerprint);


        injectAuditRating($(this).parent().data('record-key'), verify, recordId);

        console.log("----1injectAuditRating(" + $(this).parent().data('record-key') + ", " + verify + ", " + recordId + ")");
        console.log("injectAuditRating(" + $(this).parent().data('record-key') + ", " + verify + ", " + recordId + ")");
        console.log("----2injectAuditRating(" + $(this).parent().data('record-key') + ", " + verify + ", " + recordId + ")");

        record_row = $(this).parent().data('record-key');

        getImages("/admin/registration_images/" + $(this).parent().data('record-key'), $(this).parent().data('record-key'));

        $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"]').attr('data-recID', recordId);
        $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] td:nth-child(1) ul').before('<div class="msggdiv"></div>');

        if (verify === 'verify') {

        } else if (verify === 'verify_list') {
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_date').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Date').html());
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_descr').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Description').html());
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verification_by').html($('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verifyByName').html());
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .lloadReg').attr('href', 'registration_info_edit/' + recordId + '/' + new Date().getTime());

            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Date').remove();
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .Description').remove();
            $('.jtable tbody tr[data-record-key="' + $(this).parent().data('record-key') + '"] .verifyByName').remove();
        }
    });


    $(".reg_picc").colorbox({
        iframe: true,
        width: "60%",
        height: "80%"
    });


    $(document).ready(function () {

        var rattingArray = ["", "Bad", "Fair", "Good"];

        var akid = "";

        $('.starrr.lead').on('starrr:change', function (e, value) {
            akid = $(this).parent().parent().data('record-key');
            console.log('¬¬¬¬¬¬¬ ' + akid + ' ¬¬¬¬¬¬¬¬');

            $('.jtable tbody tr[data-record-key="' + akid + '"] td .cchecks input[type=checkbox]').each(function () {
                $(this).prop("checked", false);
            });

            $('.jtable tbody tr[data-record-key="' + akid + '"] td .vcount').html("0");
            $('.jtable tbody tr[data-record-key="' + akid + '"] td .vcount').html(rattingArray[value]);
            $('.jtable tbody tr[data-record-key="' + akid + '"] td .rate_val').val(value);
            if (value != 3) {
                //$('.jtable tbody tr td#' + akid + ' .rate_descr').removeAttr('disabled');
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .rall').removeAttr('disabled');
            }
            if (value == 3) {
                //$('.jtable tbody tr td#' + akid + ' .rate_descr').val("");
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .rall').prop('disabled', true);
            }
            if (value == 1) {
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .fair').addClass("hidden");
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .bad').removeClass("hidden");
            }
            if (value == 2) {
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .bad').addClass("hidden");
                $('.jtable tbody tr[data-record-key="' + akid + '"] td .fair').removeClass("hidden");
            }
        });


    });
}


function getImages(URL, id) {

    $.getJSON(URL, function (data) {
        if (data && data.item != null) {
            $.each(data.item, function (index, item) { // Iterates through a collection

                if (item.imageType != null) {
                    var image = document.getElementsByName(id + item.imageType)[0];
                    var downloadingImage = new Image();
                    downloadingImage.onload = function () {
                        image_count++;
                        $('.' + id + ' [name="' + item.imageType + '"]').attr("src", item.imageType);
                        console.log('ANTOOO:::::: item.imageType: ' + item.imageType);

                        if (item.imageType === "potrait") {
                            req_image_count++;
                        }
                        if (item.imageType === "front-pic") {
                            req_image_count++;
                        }
                        if (item.imageType === "signature") {
                            req_image_count++;
                        }


                        image.src = this.src;
                        $('.jtable tbody tr[data-record-key="' + item.registration + '"] td a.' + item.imageType).attr("href", item.webPath)
                    }
                    downloadingImage.src = item.webPath;
                }

            });
        } else {
            $('.jtable tbody tr[data-record-key="' + id + '"] td a img').remove();
            $('.jtable tbody tr[data-record-key="' + id + '"] td a').append('No Images Yet');

        }
    });
}

function injectAuditRating(id, verify, record_id) {
    console.log('id:' + id + '  verify:' + verify + '  record_id: ' + record_id);

    if (verify !== "XXverify") {
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6)').append($('.audit_rate').html());
    }
    if (verify === "verify") {
        injectVerifyId(id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleConfirm').addClass(id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleConfirm').attr('data-foo', record_id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleConfirm').attr('data-idd', id);

        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleDecline').addClass(id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleDecline').attr('data-foo', record_id);
        $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .simpleDecline').attr('data-idd', id);
    } else if (verify === "verify_list") {

    } else if (verify === "audit") {
        $(function () {
            return $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .starrr').starrr();
        });
    } else {

    }
}

function injectVerifyId(id) {
    //$('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(1) ul').before('<div class="msggdiv"></div>');
    $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .verify_btn').attr('fooID', id);
    $('.jtable tbody tr[data-record-key="' + id + '"] td:nth-child(6) .lloading').addClass(id);
}

function reInitiateImageLoading(record_row) {
    getImages("/admin/registration_images/" + record_row, $(this).parent().data('record-key'));
}

function checkSession() {
    jQuery.ajax({
        url: URL_CHECKSESSION,
        type: "GET",
        dataType: "json",
        success: function (data) {
            console.log("••••••••  checkSession  ••••••••");
            console.log(data);
            console.log("••••••••  checkSession  ••••••••");
        },
        error: function () {
            //Do alert is error
            window.location.href = '/admin/login';
        }
    });
}


function actionListen() {

    var declineTText = $('#declineTText').html();
    var allowTText = $('#allowTText').html();
    $(".simpleConfirm").confirm({
        title: "Approve Registration confirmation",
        text: allowTText, //"Confim do you want to approve the registration?",
        confirm: function (button) {
            button.fadeOut(2000).fadeIn(2000);

            var recid = button.data('foo');
            var row_recId = button.data('idd');

            var allowDescr = "";

            var numallowDescr = 0;
            $('.cchecks .allowDescr:checked').each(function () {
                allowDescr += ',' + $(this).val();
                numallowDescr = numallowDescr + 1;
            });

            var validationCheck = true;

            console.log('rate_descr3::' + allowDescr);

            if (numallowDescr !== 4) {
                alert("All checks to confirm registration quality to be approved must be set");
                validationCheck = false;
            }
            if (validationCheck) {

                $('.lloading.' + row_recId).show();
                $('.lloading.' + row_recId).css('color', '#23C6C8');
                jQuery.ajax({
                    url: URL_VERIFYACTION_REGISTRATIONS + '/' + recid + '/approved/approvedRegistration',
                    type: "GET",
                    dataType: "json",
                    success: function (data) {

                        console.log('confirm resp•••••••••••••••••••••••••••••••••••••');
                        console.log(data);
                        console.log('confirm resp•••••••••••••••••••••••••••••••••••••');
                        $('.lloading.' + row_recId).hide();
                        $('.jtable tbody tr[data-record-key="' + row_recId + '"] td .msggdiv').html("<p class='msgg'>" + data.message + "</p>");
                        $('tr#' + row_recId + ' td:nth-child(2) .msggdiv .msgg').css('background', '#23C6C8');

                        $('.simpleConfirm.' + row_recId).addClass('inactivated');
                        $('.simpleDecline.' + row_recId).addClass('inactivated');
                        $('.simpleConfirm.' + row_recId).removeClass('simpleConfirm');

                        getTallyCount();
                    },
                    error: function () {
                        $('.lloading.' + recid).hide();
                        $('tr#' + recid + ' td:nth-child(2) .msggdiv').html("<p class='msgg'>Error. Refresh Page</p>");
                        $('tr#' + row_recId + ' td:nth-child(2) .msggdiv .msgg').css('background', '#23C6C8');
                    }
                });
            }


        },
        cancel: function (button) {
            button.fadeOut(2000).fadeIn(2000);
            //alert("You aborted the operation.");
        },
        confirmButton: "Approve Record",
        cancelButton: "Cancel"
    });


    $(".simpleDecline").confirm({
        title: "Approve Registration confirmation",
        text: declineTText,
        confirm: function (button) {

            var recid = button.data('foo');
            var row_recId = button.data('idd');

            var validationCheck = true;
            var id = button.data('foo');
            var rate_descr3 = "";

            $('.cchecks .rate_descr2:checked').each(function () {
                rate_descr3 += ',' + $(this).val();
            });

            console.log('rate_descr3::' + rate_descr3);

            if (rate_descr3 == "") {
                alert("Please select one or more reasons for declining registration ");
                validationCheck = false;
            }

            if (validationCheck) {

                button.fadeOut(2000).fadeIn(2000);

                $('.lloading.' + row_recId).show();
                $('.lloading.' + row_recId).css('color', '#ED5565');

                jQuery.ajax({
                    url: URL_VERIFYACTION_REGISTRATIONS + '/' + recid + '/declined/' + rate_descr3,
                    type: "GET",
                    dataType: "json",
                    success: function (data) {
                        console.log('decline resp•••••••••••••••••••••••••••••••••••••');
                        console.log(data);
                        console.log('decline resp•••••••••••••••••••••••••••••••••••••');

                        $('.lloading.' + row_recId).hide();
                        $('.jtable tbody tr[data-record-key="' + row_recId + '"] td .msggdiv').html("<p class='msgg'>" + data.message + "</p>");
                        $('tbody tr[data-record-key="' + row_recId + '"] td .msggdiv .msgg').css('background', '#ED5565');

                        $('.simpleConfirm.' + row_recId).addClass('inactivated');
                        $('.simpleDecline.' + row_recId).addClass('inactivated');
                        $('.simpleConfirm.' + row_recId).removeClass('simpleConfirm');

                        getTallyCount();
                    },
                    error: function () {
                        $('.lloading.' + recid).hide();
                        $('tr#' + recid + ' td .msggdiv').html("<p class='msgg'>Error. Refresh Page</p>");
                        $('tr#' + recid + ' td .msggdiv .msgg').css('background', '#ED5565');
                    }
                });
            } else {
                //alert('validation fail');   
            }
        },
        cancel: function (button) {
            button.fadeOut(2000).fadeIn(2000);
            //alert("You aborted the operation.");
        },
        confirmButton: "Decline Record",
        cancelButton: "Cancel"
    });

}


function auditListen() {


    /** ******  /star rating  *********************** **/

    $(".rate_submit").click(function () {
        var validationCheck = true;
        var btnId = $(this).parent().parent().data('record-key');

        var recordID = $('tbody tr[data-record-key="' + btnId + '"]  td:nth-child(1) ul li.id span').html();

        var rate_val = $('tbody tr[data-record-key="' + btnId + '"]  td .rate_val').val();
        var rate_descr = $('tbody tr[data-record-key="' + btnId + '"]  td .rate_descr').val();
        var rate_descr2 = "___";

        $('tbody tr[data-record-key="' + btnId + '"]  td .cchecks .rate_descr2:checked').each(function () {
            rate_descr2 += ',' + $(this).val();
        });

        if (rate_descr == "" && rate_val != 3 && rate_val != "") {
            alert("SELECT description for record " + btnId);
            validationCheck = false;
        }

        if (rate_val == "") {
            alert("PLEASE rate the record first! ");
            validationCheck = false;
        }

        if (rate_descr2 == "___" && rate_val != 3) {
            alert("PLEASE check what the issue with reg is! ");
            validationCheck = false;
        }

        if (rate_val === 3) {
            rate_descr = "good_registration";
        }

        if (validationCheck) {
            $('tbody tr[data-record-key="' + btnId + '"]  td .rate_progress.lloading').show();
            //TOP DASH SUMMARY
            jQuery.ajax({
                url: URL_AUDIT_REGISTRATIONS + "/" + recordID + "/" + rate_val + "/" + rate_descr2 + "/" + new Date().getTime(), //This URL is for Json file
                type: "GET",
                dataType: "json",
                success: function (data) {
                    //To-do
                    console.log(data);

                    $('tbody tr[data-record-key="' + btnId + '"]  td .rate_progress.lloading').hide();
                    //$('tbody tr[data-record-key="' + btnId + '"]  td .rate_progress').append("<span style='color: #43B153;font-weight: 600;text-rendering: inherit;'>" + data.response_msg + "</span>");
                    $('.jtable tbody tr[data-record-key="' + btnId + '"] td .msggdiv').html("<p class='msgg'>" + data.response_msg + "</p>");
                },
                error: function () {
                    //Do alert is error
                }
            });
        }
    });
}

function getParameterByName(name, url) {
    if (!url)
        url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    if (!results)
        return null;
    if (!results[2])
        return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}




function overrideActionListen(url) { 

    console.log('overrideActionListen called');
    
    $(".overrideLink").confirm({
        title: "Approve Registration confirmation",
        text: "Are you sure you want to send this registration for activation?", //"Confim do you want to approve the registration?",
        confirm: function (button) {
            button.fadeOut(2000).fadeIn(2000);

            jQuery.ajax({
                url: url,
                type: "GET",
                dataType: "json",
                success: function (data) {
                    $('#myModal').modal('hide');
                    alert('Registration Approved Successfully.');
                    ViewVerifyRegistrationsTables();
                    //location.reload();
                },
                error: function () {
                    //Do alert is error
                }
            });

        },
        cancel: function (button) {
            button.fadeOut(2000).fadeIn(2000);
            //alert("You aborted the operation.");
        },
        confirmButton: "Register Number",
        cancelButton: "Cancel"
    });

}


