var idSelected = "";
var loadTopicSelector = function (operator, selected) {
    idSelected = selected;
    if (keyValuePair == undefined) {
        $.getJSON(URL_GET_TOPICS, function (data) {
            keyValuePair = [];
            availableTags = [];
            $.each(data.item, function (index, item) { // Iterates through a collection
                keyValuePair[item.msisdn] = item.deviceId;
                availableTags.push(item.msisdn);
            });
            keyValuePair[operator] = operator;
            availableTags.push(operator);
            $("#form-msisdn").autocomplete({
                source: availableTags
            });
        });
    } else {
        availableTags.push(operator);
        $("#form-msisdn").autocomplete({
            source: availableTags
        });
    }

    $('#publishModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
};

var publishMessage = function (msgType, selected, event) {
    event.preventDefault();
    showBusyDialog();
    var msisdn = $("#form-msisdn").val();

    if (msisdn != undefined) {
        //get the device id
        var deviceId = keyValuePair[msisdn];

        console.log(msisdn);
        console.log(deviceId);

        var request = $.ajax({
            url: URL_PUBLISH_APP_UPDATE,
            type: 'POST',
            data: {
                updateId: idSelected,
                topic: deviceId,
                type: msgType

            },
            timeout: 10000
        });

        request.done(function (response) {
            hideBusyDialog();
            if (response != undefined) {
                console.log(response);
                var json = JSON.parse(response);
                alert(json.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });

    }

}