var keyValuePair = new Array();
var availableTags = new Array()


var loadTopicSelector = function () {

    $.getJSON(window.URL_GET_TOPICS, function (data) {
        $.each(data.item, function (index, item) { // Iterates through a collection
            keyValuePair[item.msisdn] = item.deviceId;
            availableTags.push(item.msisdn);
        });
        availableTags.push("MOAT");
        keyValuePair.push("moat", "moat");


        $("#form-msisdn").autocomplete({
            source: availableTags
        });
    });


    $('#publishDyanmicFormModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });
};




var publishImageConfigurations = function (event) {
    event.preventDefault();
    showBusyDialog();
    var msisdn = $("#form-msisdn").val();
    if (msisdn != undefined) {
        //get the device id
        var deviceId = keyValuePair[msisdn];

        var fields = $("#ImageConfigurationsForm").serializeArray();
        var json = {};
        jQuery.each(fields, function (i, field) {
            json[field.name] = field.value;
        });

        json["deviceId"] = deviceId;
        json["type"] = "image_matrix_update";

        console.log(json);

        var request = $.ajax({
            url: URL_PUBLISH_SYSCONFIG,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(json),
            timeout: 10000
        });

        request.done(function (response) {
            hideBusyDialog();
            if (response != undefined) {
                alert(response.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });

    }
}
/**
 *
 * LOGIN UTILITIES
 * */

var publishLoginConfigs = function (event) {
    showBusyDialog();
    var msisdn = $("#form-msisdn").val();
    if (msisdn != undefined) {
        //get the device id
        var deviceId = keyValuePair[msisdn];
        //get the text area
        var fields = $("#loginConfigurationForm").serializeArray();
        var json = {};
        jQuery.each(fields, function (i, field) {
            json[field.name] = field.value;
        });
        json["deviceId"] = deviceId;
        json["type"] = "login";

        var request = $.ajax({
            url: URL_PUBLISH_SYSCONFIG,
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify(json),
            timeout: 10000
        });

        request.done(function (response) {
            hideBusyDialog();
            if (response != undefined) {
                alert(response.message);
            }
        });
        request.fail(function (error) {
            hideBusyDialog();
            alert("SYSTEM ERROR");
        });
    }

};

var loadLoginConfigurations = function () {
    $.getJSON(URL_GET_LOGIN_CONFIGS, function (data) {
        var inputs = document.getElementById("loginConfigurationForm").elements;
        for (var i = 0; i < inputs.length; i++) {
            $.each(data.item, function (index, item) {
                var json = JSON.parse(item.config);
                for (key in json) {
                    if (inputs[i].name == key) {
                        $("#" + inputs[i].id).val(json[key]);
                    }
                }
            });
        }
    });
};


var saveLoginConfiguration = function (event) {

    showBusyDialog();
    var fields = $("#loginConfigurationForm").serializeArray();
    var json = {};
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });
    var request = $.ajax({
        url: URL_SAVE_LOGIN_CONFIGS,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(json),
        timeout: 10000
    });

    request.done(function (response) {
        hideBusyDialog();
        if (response != undefined) {
            if (response.status) {
                location.reload();
            }
            alert(response.message);
        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        alert("SYSTEM ERROR");
    });

}


/**
 *
 * END LOGIN UTILITIES
 * */
var loadImageConfigurations = function () {
    $.getJSON(URL_GET_IMAGE_CONFIGS, function (data) {
        var inputs = document.getElementById("ImageConfigurationsForm").elements;
        for (var i = 0; i < inputs.length; i++) {
            $.each(data.item, function (index, item) {
                var json = JSON.parse(item.config);
                for (key in json) {
                    if (inputs[i].name == key) {
                        $("#" + inputs[i].id).val(json[key]);
                    }
                }
            });
        }
    });

}


var saveImageConfiguration = function (event) {
    event.preventDefault();
    var fields = $("#ImageConfigurationsForm").serializeArray();
    var json = {};
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });

    console.log(json);

    var request = $.ajax({
        url: URL_SAVE_IMAGE_CONFIG,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(json),
        timeout: 10000
    });

    request.done(function (response) {
        hideBusyDialog();
        if (response != undefined) {
            if (response.status) {
                location.reload();
            }
            alert(response.message);
        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        alert("SYSTEM ERROR");
    });
}

