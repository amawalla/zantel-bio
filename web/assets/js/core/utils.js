/**
 * Created by oscar on 11/10/15.
 */

var _validateEmail = function(email){
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    if(!re.test(email)){
        alert("Please provide a valid Email");
    }
}

var _validateMsisdn = function (msisdn) {

}


var _validateDate = function (dob) {
    if(dob == undefined){
        alert("Fill in the Date");
        return;
    }
    var re=/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/
    if(!re.test(dob)){
        alert("Please provide a valid Date");
    }

}
