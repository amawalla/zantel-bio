var URL_GET_IDTYPE = "/admin/list_id_types";
var URL_CHANGE_PASSWORD = "passwordChange";
var URL_GET_REGIONS = "/admin/region/list_regions";
var URL_GET_USERTYPES = "/admin/users/list_userTypes";
var URL_CREATE_AGENT = "/admin/agents/createUser";
var URL_CREATE_MANAGERS = "/admin/users/createManagers";
var URL_LIST_AGENTS = "/admin/agents/list_agents";
var URL_UPDATE_AGENTS = "/admin/agents/updateUser";
var URL_GET_TERRITORY = "/admin/region/list_territories";
var URL_GET_DEVICES = "/admin/device/list_devices";
var URL_CREATE_DEVICES = "/admin/device/create";
var URL_UPDATE_DEVICES = "/admin/device/updateAction";
var URL_GENERATE_ACCESS_TOKEN = "/admin/device/accessToken";
var URL_CREATE_IDVERIFICATION = "/admin/idverificationConfig";////
var URL_CREATE_VERIFYERLIST = "/admin/verifyerviewlist";////
var URL_GET_SUPERAGENTLIST = "/admin/users/list_superAgents/ROLE_SUPERAGENT"; //super agent, vodas";
var URL_VODASHOP_AGENTS = "/admin/users";////
var URL_CHECKSESSION = "/admin/session_page";
var keyValuePair = [];
var availableTags = [];
var cachedTerritory = null;
var cachedRegions = null;
var cachedSuperAgents = null;
var undecidedPPLE = false;

var loadAgentsTables = function () {

    $('#AgentTableContainer').jtable({
        title: 'Agents',
        paging: true,
        sorting: false,
        actions: {
            listAction: URL_LIST_AGENTS,
            updateAction: URL_UPDATE_AGENTS
        }, toolbar: {
            items: [{
                Tooltip: 'Click here to export this table to excel',
                //icon: '/images/paginate.gif',
                text: 'Export to Excel',
                click: function () {
                    $('#AgentTableContainer').jtable('load', {
                        msisdn: $('#agent_msisdn').val(),
                        first_name: $('#first_name').val(),
                        last_name: $('#last_name').val(),
                        region: $('#search_region').val(),
                        territory: $('#search_territory').val(),
                        from_date: $('#from_date').val(),
                        to_date: $('#to_date').val()
                    });
                    window.location = 'getResult.php?formName=afscpEmp&action=export-excel';
                    e.preventDefault();
                }
            }]
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            createdAt: {
                title: 'createdAt',
                width: '10%',
                edit: false
            },
            Devices: {
                title: '',
                width: '2%',
                sorting: false,
                edit: false,
                create: false,
                display: function (agentData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="/assets/img/phone_metro.png" title="Edit Devices" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#AgentTableContainer').jtable('openChildTable',
                            $img.closest('tr'),
                            {
                                title: agentData.record.firstName + ' - Devices',
                                messages: {
                                    addNewRecord: 'Add new Device'
                                },
                                actions: {
                                    listAction: URL_GET_DEVICES + '?userId=' + agentData.record.id,
                                    createAction: URL_CREATE_DEVICES,
                                    updateAction: URL_UPDATE_DEVICES
                                },
                                fields: {
                                    id: {
                                        type: 'hidden',
                                        defaultValue: agentData.record.id
                                    },
                                    deviceId: {
                                        key: true,
                                        create: false,
                                        edit: false,
                                        list: false
                                    },
                                    msisdn: {
                                        title: 'MSISDN',
                                        width: '5%',
                                        edit: false,
                                        create: undecidedPPLE,
                                    },
                                    accessToken: {
                                        title: 'Access Token',
                                        width: '5%',
                                        edit: false,
                                        create: false,
                                    },
                                    imei: {
                                        title: 'IMEI',
                                        width: '10%',
                                        edit: false,
                                        create: undecidedPPLE,
                                    },
                                    appVersion: {
                                        title: 'AppVersion',
                                        width: '10%',
                                        edit: false,
                                        create: undecidedPPLE,
                                    },
                                    isActive: {
                                        title: 'Is Activated',
                                        width: '5%',
                                        create: false,
                                        edit: showeditField,
                                        options: {'1': 'Yes', '0': 'No'}
                                    },
                                    status: {
                                        title: 'Status',
                                        width: '5%',
                                        create: false,
                                        edit: true,
                                        options: {'1': 'Enable', '0': 'Disable'}
                                    },
                                    isTiedToDevice: {
                                        title: 'Device options',
                                        width: '10%',
                                        //create: true,
                                        create: undecidedPPLE,
                                        edit: true,
                                        options: {'1': 'Use registered device only', '0': 'Use any device'}
                                    },
                                    createdOn: {
                                        title: 'Created On',
                                        width: '10%',
                                        type: 'date',
                                        displayFormat: 'yy-mm-dd',
                                        create: false,
                                        edit: false
                                    },
                                    myButton: {
                                        title: '',
                                        width: '10%',
                                        edit: false,
                                        create: undecidedPPLE,
                                        display: function (data) {
                                            return '<button type="button" onclick="generateAccesToken(' + data.record.deviceId + ');">Generate Access Token</button> ';
                                        }
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });

                    //Return image to show on the person row
                    return $img;
                }
            },
            agentName: {
                title: 'Agent Name',
                width: '10%',
                edit: false
            },
            firstName: {
                title: 'First Name',
                width: '10%',
                inputClass: 'required',
                edit: true,
                list: false
            },
            lastName: {
                title: 'Last Name',
                width: '10%',
                inputClass: 'required',
                edit: true,
                list: false
            },
            mobileNumber: {
                title: 'Phone',
                width: '5%',
                inputClass: 'required',
                edit: false,
                create: false
            },
            email: {
                title: 'Email (Optional)',
                width: '15%',
                edit: false,
            },
            idNumber: {
                title: 'ID Number',
                width: '20%',
                inputClass: 'required',
                list: false,
                edit: false,
            },
            userType: {
                title: 'User Type',
                width: '10%',
                inputClass: 'required',
                edit: true,
                list: false,
                options: URL_GET_USERTYPES
            },
            region: {
                title: 'Region',
                width: '10%',
                list: true,
                edit: true,
                options: function () {
                    if (cachedRegions) {
                        return cachedRegions;
                    }
                    var options = [];
                    $.ajax({
                        url: URL_GET_REGIONS,
                        type: 'GET',
                        async: false,
                        dataType: 'json',
                        success: function (data) {
                            if (data.Result != 'OK') {
                                alert(data.Message);
                                return;
                            }
                            options = data.Options;
                        }
                    });
                    return cachedRegions = options;
                }
            },
            territory: {
                title: 'Territory',
                width: '10%',
                list: true,
                edit: true,
                dependsOn: 'region',
                options: function (data) {

                    if (cachedTerritory) {
                        return cachedTerritory;
                    }
                    var options = [];
                    var url;
                    if (data.source == 'list') {
                        url = URL_GET_TERRITORY + '?regionId=0';
                    } else {
                        url = URL_GET_TERRITORY + '?regionId=' + data.dependedValues.region;
                    }
                    $.ajax({
                        url: url,
                        type: 'GET',
                        async: false,
                        dataType: 'json',
                        success: function (result) {
                            // console.log(result);
                            if (result.Result != 'OK') {
                                alert(result.Message);
                                return;
                            }
                            options = result.Options;
                        }
                    });
                    return cachedTerritory = options;
                }
            },
            /*superagent_id: {
             title: 'Super Agent',
             width: '20%',
             edit: true,
             options: URL_GET_SUPERAGENTLIST
             },*/
            imei: {
                title: 'Agent Device IMEI',
                width: '20%',
                list: false,
                edit: false
            },
            status: {
                title: 'status',
                width: '5%',
                create: false,
                options: {'1': 'Enabled', '0': 'Disabled'}
            },
            isEnrolledToTest: {
                title: 'Can Test',
                width: '5%',
                create: false,
                edit: true,
                list : false,
                options: {'1': 'Yes', '0': 'No'}
            },
            number_devices: {
                title: 'No. of Devices',
                width: '20%',
                list: false,
                create: false,
            },
            username: {
                title: 'username',
                width: '20%',
                list: false,
                create: false,
                edit: false
            },
            exclusive: {
                title: "Make agent eclusive. Exclusive agent can not add other MNOs",
                list: false,
                edit: false,
                type: 'radiobutton',
                options: {'1': 'Exclusive', '0': 'Not Exclusive'}
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        }
    });

    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButtonBB').click(function (e) {
        e.preventDefault();

        checkSession();
        $('#AgentTableContainer').jtable('load', {
            msisdn: $('#agent_msisdn').val(),
            first_name: $('#first_name').val(),
            last_name: $('#last_name').val(),
            region: $('#search_region').val(),
            territory: $('#search_territory').val(),
            from_date: $('#from_date').val(),
            to_date: $('#to_date').val()
        });
    });
    //Load all records when page is first shown
    //$('#LoadRecordsButtonBB').click();


    $('#LoadRecordsButtonBB').click();


}


var generateAccesToken = function (deviceId) {
    showBusyDialog();
    var request = $.ajax({
        url: URL_GENERATE_ACCESS_TOKEN,
        type: 'post',
        data: {
            deviceId: deviceId
        },
        timeout: 10000
    });

    request.done(function (data) {
        hideBusyDialog();

        if (data === undefined) {
        } else {
            var json = JSON.parse(data);
            alert(json.Message);
            if (json.Result === 'OK') {
                //reload the table
                $('#AgentTableContainer').jtable('load');
            }
        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        alert("SYSTEM ERROR");
    });
};

var loadPublishMessageDialog = function (operator) {

    if (keyValuePair == undefined || availableTags.length == 0) {
        $.getJSON(URL_GET_TOPICS, function (data) {
            keyValuePair = new Array();
            availableTags = new Array();
            $.each(data.item, function (index, item) { // Iterates through a collection
                keyValuePair[item.msisdn] = item.deviceId;
                availableTags.push(item.msisdn);
            });
            keyValuePair[operator] = operator;
            availableTags.push(operator);
            $("#form-msisdn").autocomplete({
                source: availableTags
            });

        });
    }
    $('#publishMessageFormModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });

}
var editAgent = function (event) {
    event.preventDefault();
    console.log(agent);

    var request = $.ajax({
        url: URL_CREATE_AGENT,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(json),
        timeout: 10000
    });

}

var addAgent = function (event) {
    showBusyDialog();
    var fields = $("#addAgentModalForm").serializeArray();
    var json = {};
    json["agent_exclusivity"] = 0;
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });

    var request = $.ajax({
        url: URL_CREATE_AGENT,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(json),
        timeout: 10000
    });

    request.done(function (msg) {
        hideBusyDialog();
        if (msg === undefined) {

        } else {
            if (msg.status) {
                /** successfull alert success**/
                $('#addAgentModal').modal('hide');
                $("#addAgentModalForm")[0].reset();
                //Load all records when page is first shown
                $('#LoadRecordsButton').click();
            }
            alert(msg.message);

        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        console.log("error:" + error);
    });
}


var changePassword = function (event) {
    showBusyDialog();
    var fields = $("#changePasswordForm").serializeArray();
    var json = {};
    json["agent_exclusivity"] = 0;
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });

    if ($('#confirmPassword').val() === $('#password').val()) {
        var request = $.ajax({
            url: URL_CHANGE_PASSWORD,
            type: 'post',
            dataType: 'json',
            data: JSON.stringify(json),
            timeout: 10000
        });

        request.done(function (msg) {

            hideBusyDialog();
            console.log(msg);

            if (msg === undefined) {

            } else {
                if (msg.status) {
                    $('#changePassword').modal('hide');
                    $("#changePasswordForm")[0].reset();
                }
                alert(msg.message);

            }
        });

        request.fail(function (error) {
            hideBusyDialog();
            console.log("error:" + error);
        });
    } else {
        alert('Passwords Do not Match');
    }

}


var addUser = function (event) {
    //event.preventDefault();
    var fields = $("#addUserModalForm").serializeArray();
    var json = {};
    json["agent_exclusivity"] = 0;
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });
    $.ajax(
        {
            url: URL_CREATE_MANAGERS, //URL_CREATE_AGENT
            type: 'post',
            dataType: 'json',
            data: JSON.stringify(json),
            success: function (response) {
                console.log(response.message);
                if (response != undefined) {
                    alert(response.message);
                }
            },
            error: function (error) {
                console.log(error);
                if (error != undefined) {
                    alert(error.message);
                }

            },
            timeout: 10000
        }
    );
}


var loadNewAgentForm = function () {
    getRegions();
    getUserTypes();
    $('#addAgentModal').modal({
        backdrop: 'static',
        keyboard: false,
        show: true
    });


};


var getTerritories2 = function () {
    var regionId = $("#search_region").val();
    if (regionId === undefined || regionId < 1) {
        $("#search_territory option").remove();
        return;
    }
    $.getJSON(URL_GET_TERRITORY + '?regionId=' + regionId, function (data) {
        $("#search_territory option").remove(); // Remove all <option> child tags.

        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#search_territory").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.Value)
            );
        });
    });
};

var getRegions2 = function () {
    $.getJSON(URL_GET_REGIONS, function (data) {
        $("#search_region option").remove(); // Remove all <option> child tags.
        $("#search_region").append($("<option></option>").text("select").val(""));
        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#search_region").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.Value)
            );
        });
    });
};


var getRegionsCode = function () {
    $.getJSON(URL_GET_REGIONS, function (data) {
        $("#search_region option").remove(); // Remove all <option> child tags.
        $("#search_region").append($("<option value=''></option>").text("Select Region").val(""));
        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#search_region").append("<option value='" + item.CodeName + "' data-foo='" + item.Value + "'>" + item.DisplayText + "</option>");
        });
    });
};


var getTerritoriesCode = function () {
    var selected = $("#search_region").find('option:selected');
    var regionId = selected.data('foo');
    if (regionId === undefined || regionId < 1) {
        $("#search_territory option").remove();
        return;
    }
    $.getJSON(URL_GET_TERRITORY + '?regionId=' + regionId, function (data) {
        $("#search_territory option").remove(); // Remove all <option> child tags.

        $("#search_territory").append("<option value='0'>Select Territory</option>");

        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#search_territory").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.Value)
            );
        });
    });
};

var getTerritories = function () {
    var regionId = $("#region").val();
    if (regionId === undefined || regionId < 1) {
        $("#addAgentModal #territory option").remove();
        return;
    }
    $.getJSON(URL_GET_TERRITORY + '?regionId=' + regionId, function (data) {
        $("#addAgentModal #territory option").remove(); // Remove all <option> child tags.

        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#addAgentModal #territory").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.Value)
            );
        });
    });
};

var getRegions = function () {
    $.getJSON(URL_GET_REGIONS, function (data) {
        $("#addAgentModal #region option").remove(); // Remove all <option> child tags.
        $("#addAgentModal #region").append($("<option></option>").text("select").val(""));
        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#addAgentModal #region").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.Value)
            );
        });
    });
};
var getUserTypes = function () {
    $.getJSON(URL_GET_USERTYPES, function (data) {
        $("#addAgentModal #userType option").remove(); // Remove all <option> child tags.
        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#addAgentModal #userType").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.Value)
            );
        });
    });
};
var publishMessage = function (msgType, event) {
    event.preventDefault();

    var msisdn = $("#form-msisdn").val();
    var message = $("#message").val();
    if (message) {
        showBusyDialog();
        //get the device id
        var deviceId = keyValuePair[msisdn];
        // Create a new FormData object.
        var formData = new FormData();
        var fields = $("#publishMessageFormModalForm").serializeArray();
        console.log(fields);
        var json = {};
        jQuery.each(fields, function (i, field) {
            // json[field.name] = field.value;
            formData.append(field.name, field.value);
        });

        // json["deviceId"] = deviceId;
        // json["type"] = msgType;


        var file = document.getElementById('myfile').files[0];
        if (file) {
            // Add the file to the request.
            formData.append('fileLocation', file, file.name);
        }
        formData.append("deviceId", deviceId);
        formData.append("type", msgType);


        //set up the request
        var xhr = new XMLHttpRequest();
        xhr.open("POST", URL_PUBLISH_SYSCONFIG_FILE, true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            hideBusyDialog();
            if (xhr.status === 200) {
                // File(s) uploaded.
                alert('Upload successfull');
            } else {
                alert('An error occurred!');
            }
        };

        $("#publishMessageFormModal").modal("hide")

        xhr.send(formData);

    } else {
        alert("Please make sure you enter mobile number and the message");
    }

}
var uploadAgents = function () {

    var file = document.getElementById('bulkuploadfile').files[0];
    if (file) {
        showBusyDialog();
        var formData = new FormData();
        formData.append('fileLocation', file, file.name);
        //set up the request
        var xhr = new XMLHttpRequest();
        xhr.open("POST", URL_BULK_UPLOAD_AGENT, true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            hideBusyDialog();
            if (xhr.status === 200) {
                // File(s) uploaded.
                alert('Upload successfull');
            } else {
                alert('An error occurred!');
            }
        };

        xhr.send(formData);
    } else {
        alert('No file Selected');
    }

}




var uploadAdminUsers = function () {

    var file = document.getElementById('bulkuploadfile').files[0];
    if (file) {
        showBusyDialog();
        var formData = new FormData();
        formData.append('fileLocation', file, file.name);
        //set up the request
        var xhr = new XMLHttpRequest();
        xhr.open("POST", "/admin/admin_users_bulk_upload", true);
        // Set up a handler for when the request finishes.
        xhr.onload = function () {
            hideBusyDialog();
            console.log('xhr');
            console.log(xhr);
            if (xhr.status === 200) {
                // File(s) uploaded.
                var resp = jQuery.parseJSON(xhr.response);
                $('.respmsg').append('<div class="bg-primary p-xs b-r-sm">'+resp.message+'</div>');
            } else {
                alert('An error occurred!'); 
            }
        };

        xhr.send(formData);
    } else {
        alert('No file Selected');
    }

}



var loadManagersTables = function () {
    $('#ManagerTableContainer').jtable({
        title: 'Agents',
        paging: true,
        sorting: false,
        selecting: true, //Enable selecting
        multiselect: true, //Allow multiple selecting
        selectingCheckboxes: true, //Show checkboxes on first column
        actions: {
            listAction: URL_LIST_AGENTS,
            updateAction: URL_UPDATE_AGENTS
        },
        fields: {
            id: {
                key: true,
                create: false,
                edit: false,
                list: false
            },
            Devices: {
                title: '',
                width: '5%',
                sorting: false,
                edit: false,
                create: false,
                display: function (agentData) {
                    //Create an image that will be used to open child table
                    var $img = $('<img src="/assets/img/phone_metro.png" title="Edit Devices" />');
                    //Open child table when user clicks the image
                    $img.click(function () {
                        $('#ManagerTableContainer').jtable('openChildTable',
                            $img.closest('tr'),
                            {
                                title: agentData.record.firstName + ' - Devices',
                                actions: {
                                    listAction: URL_GET_DEVICES + '?userId=' + agentData.record.id,
                                    createAction: URL_CREATE_DEVICES,
                                    updateAction: URL_UPDATE_DEVICES
                                },
                                fields: {
                                    id: {
                                        type: 'hidden',
                                        defaultValue: agentData.record.id
                                    },
                                    deviceId: {
                                        key: true,
                                        create: false,
                                        edit: false,
                                        list: false
                                    },
                                    msisdn: {
                                        title: 'MSISDN',
                                        width: '10%',
                                        edit: false,
                                    },
                                    accessToken: {
                                        title: 'Access Token',
                                        width: '10%',
                                        edit: false,
                                        create: false,
                                    },
                                    imei: {
                                        title: 'IMEI',
                                        width: '10%',
                                        edit: false,
                                    },
                                    isActive: {
                                        title: 'Is Activated',
                                        width: '10%',
                                        create: false,
                                        edit: false,
                                        options: {'1': 'Active', '0': 'InActive'}
                                    },
                                    status: {
                                        title: 'Status',
                                        width: '10%',
                                        create: false,
                                        edit: false,
                                        options: {'1': 'Enable', '0': 'Disable'}
                                    },
                                    createdOn: {
                                        title: 'Created On',
                                        width: '10%',
                                        type: 'date',
                                        displayFormat: 'yy-mm-dd',
                                        create: false,
                                        edit: false
                                    },
                                    myButton: {
                                        title: '',
                                        width: '20%',
                                        display: function (data) {
                                            return '<button type="button" onclick="generateAccesToken(' + data.record.deviceId + ');">Generate Access Token</button> ';
                                        }
                                    }
                                }
                            }, function (data) { //opened handler
                                data.childTable.jtable('load');
                            });
                    });

                    //Return image to show on the person row
                    return $img;
                }
            },
            firstName: {
                title: 'First Name',
                width: '10%'
            },
            lastName: {
                title: 'Last Name',
                width: '10%'
            },
            gkyc: {
                title: 'GKYC Username',
                width: '10%',
                edit: false,
                create: false,
            },
            mobileNumber: {
                title: 'Phone',
                width: '5%',
                edit: false
            },
            email: {
                title: 'Email (Optional)',
                width: '15%',
                edit: false,
                list: false,
            },
            idNumber: {
                title: 'ID Number',
                width: '20%',
                list: false,
                edit: false,
            },
            userType: {
                title: 'User Type',
                width: '5%',
                list: false,
                edit: false,
                options: URL_GET_USERTYPES
            },
            region: {
                title: 'Region',
                width: '10%',
                list: true,
                edit: false,
                options: URL_GET_REGIONS
            },
            territory: {
                title: 'Territory',
                width: '10%',
                list: true,
                edit: false,
                dependsOn: 'region',
                options: function (data) {
                    if (data.source == 'list') {
                        return URL_GET_TERRITORY + '?regionId=0';
                    }
                    return URL_GET_TERRITORY + '?regionId=' + data.dependedValues.region;
                }
            },
            imei: {
                title: 'Agent Device IMEI',
                width: '20%',
                list: false,
                edit: false
            },
            status: {
                title: 'status',
                width: '5%',
                create: false,
                options: {'1': 'Active', '0': 'InActive'}
            },
            agentCode: {
                title: 'Agent Code (Optional)',
                width: '20%',
                list: false
            },
            username: {
                title: 'username',
                width: '20%',
                list: false,
                create: false,
                edit: false
            },
            exclusive: {
                title: "Make agent eclusive. Exclusive agent can not add other MNOs",
                list: false,
                type: 'radiobutton',
                options: {'1': 'Exclusive', '0': 'Not Exclusive'}
            }
        },
        recordsLoaded: function (event, data) {
            $('.jtable').ReStable({rowHeaders: false});
        },
        //Register to selectionChanged event to hanlde events
        selectionChanged: function () {
            //Get all selected rows
            var $selectedRows = $('#ManagerTableContainer').jtable('selectedRows');
            if ($selectedRows.length > 0) {
                //Show selected rows
                $selectedRows.each(function () {
                    var record = $(this).data('record');
                    console.log(record.mobileNumber);

                });
            }
        },
    });


    //Re-load records when user click 'load records' button.
    $('#LoadRecordsButton').click(function (e) {
        e.preventDefault();
        $('#ManagerTableContainer').jtable('load', {
            msisdn: $('#agent-msisdn').val()
        });
    });
    //Load all records when page is first shown
    $('#LoadRecordsButton').click();

    $('#btn-topic-select').button().click(function () {
        var $selectedRows = $('#ManagerTableContainer').jtable('selectedRows');

    });

}

var multiple_devices_check = function () {
    if (document.getElementById('multiple_devices').checked) {
        document.getElementById('number_devices').style.display = 'block';
    } else {
        document.getElementById('number_devices').style.display = 'none';
    }

}


var getIdType = function () {

    $.getJSON(URL_GET_IDTYPE, function (data) {

        console.log(data.Options);

        $.each(data.Options, function (index, item) { // Iterates through a collection
            $("#idtype").append(// Append an object to the inside of the select box
                $("<option></option>") // Yes you can do this.
                    .text(item.DisplayText)
                    .val(item.DisplayText)
            );

            console.log(item.used);

            if (item.used == 1) {
                $('#idtype option[value="' + item.DisplayText + '"]').prop("selected", "selected");
            }
        });


        var config = {
            '.chosen-select': {},
            '.chosen-select-deselect': {allow_single_deselect: true},
            '.chosen-select-no-single': {disable_search_threshold: 10},
            '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
            '.chosen-select-width': {width: "95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }
    });
};


var addIdConfig = function (event) {
    showBusyDialog();
    var fields = $("#idVerificationform").serializeObject();
    var json = {};
    json["agent_exclusivity"] = 0;
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });

    var request = $.ajax({
        url: URL_CREATE_IDVERIFICATION,
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(fields),
        timeout: 10000
    });

    request.done(function (msg) {
        console.log("msg");
        console.log(msg);
        console.log("msg");
        hideBusyDialog();
        if (msg === undefined) {

        } else {
            if (msg.status) {
                /** successfull alert success**/
                alert("saved successfully");
            }

        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        console.log("error:" + error);
    });
}


jQuery.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    jQuery.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


var vodashopAgentsConfig = function (rec_id, event) {

    showBusyDialog();
    var fields = $("#vodashopform").serializeObject();
    var json = {};
    json["record_id"] = rec_id;
    jQuery.each(fields, function (i, field) {
        json[field.name] = field.value;
    });

    fields['rec_id'] = rec_id;
    var request = $.ajax({
        url: URL_VODASHOP_AGENTS + '/addVodashopAgent',
        type: 'post',
        dataType: 'json',
        data: JSON.stringify(fields),
        timeout: 10000
    });

    request.done(function (msg) {
        console.log(msg);
        hideBusyDialog();
        $.each(msg, function (index, val) {
            console.log("val--d----");
            console.log(val);
            console.log("val--d----.");
            if (val.firstName != undefined) {
                append_data = "<tr style='background: #fff;border: 1px solid #eee;padding: 5px; height:1px;'><td style='padding:2px;'></td><td class='ant_td'>" + val.username + "</td><td class='ant_td'>" + val.firstName + "</td><td class='ant_td'>" + val.lastName + "</td><td class='ant_td'>" + val.firstName + "</td><td class='ant_td'><a class='addAgenttoSA' onclick='addAgentToSuperAgent(" + val.id + ")' data-foo='" + val.id + "'>Add to Shop</a></td></tr>";
                $(append_data).appendTo($("#agent_count_table tbody"));
            } else {
                alert("Enter Agent Number or Agent Name in search box");
            }
        });

        if (msg === undefined) {

        } else {
            if (msg.status) {
                /** successfull alert success**/
                alert("saved successfully");
            }

        }
    });

    request.fail(function (error) {
        hideBusyDialog();
        console.log("error:" + error);
    });
}

function checkSession() {
    jQuery.ajax({
        url: URL_CHECKSESSION,
        type: "GET",
        dataType: "json",
        success: function (data) {
            console.log("••••••••  checkSession  ••••••••");
            console.log(data);
            console.log("••••••••  checkSession  ••••••••");
        },
        error: function () {
            //Do alert is error
            window.location.href = '/admin/login';
        }
    });
}


var addVerifierFieldsConfig = function (event) {

    //var URL_CREATE_VERIFYERLIST = "/admin/verifyerviewlist";////

    var allowDescr = "";

    $('.verifierFields:checked').each(function () {
        allowDescr += ':::' + $(this).val();
    });

    console.log('rate_descr3::' + allowDescr);

    jQuery.ajax({
        url: "/admin/verifyerviewlist/" + $('#numRow').val() + ':::' + $('#secsCount').val() + "/" + allowDescr + "/" + new Date().getTime(), //This URL is for Json file
        type: "GET",
        dataType: "json",
        success: function (data) {
            if (data.success === 1) {
                $('.message').html("<pre>" + data.message + "</pre>");
            } else {
                $('.message').html("<pre>Error Saving List kindly check </pre>");
            }
        },
        error: function () {
            $('.message').html("<pre>Critical Error Saving List kindly check </pre>");
        }
    });


}

var uploadDeleteAgents = function () {
    var file = document.getElementById('bulkudeletefile').files[0];
    var r = confirm("Are you sure you want the Agents in this file to be deleted?");
    if (r == true) {
        if (file) {
            showBusyDialog();
            var formData = new FormData();
            formData.append('fileLocation', file, file.name);
            //set up the request
            var xhr = new XMLHttpRequest();
            xhr.open("POST", URL_BULK_DELETE_AGENT, true);
            // Set up a handler for when the request finishes.
            xhr.onload = function () {
                hideBusyDialog();
                if (xhr.status === 200) {
                    // File(s) uploaded.
                    alert('Upload successfull');
                } else {
                    alert('An error occurred!');
                }
            };
            xhr.send(formData);
        } else {
            alert('No file Selected');
        }
    }

};
